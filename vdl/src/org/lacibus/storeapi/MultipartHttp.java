package org.lacibus.storeapi;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.json.simple.JSONObject;
import org.lacibus.storeaccess.InputException;
import org.lacibus.triplestore.Datum;

/**
 * An instance of this class performs processing for HTTP requests
 * and responses with multipart form data content.
 */

public class MultipartHttp implements Serializable {

	private static final long serialVersionUID = -7468208596590409214L;
	
	private static final long BIG = 1024;
		
	/**
	 * Make a request with multipart form encoded content conveying
	 * a set of values, optionally some metadata, optionally a 
	 * reference URL, and optionally a one-time key. One of the parts
	 * is named 'json' and is a utf8 string representation of a JSON 
	 * object. Each binary value is represented in the request by a 
	 * string of the form b<i>n</i> where <i>n</i> is an integer. 
	 * There is then a part named b<i>n</i> with application/octet-stream 
	 * encoding that contains the binary value. Each non-binary value 
	 * is represented by a JSON object with two attributes: <i>type</i> 
	 * and <i>value</i>. If metadata is supplied, then there is a part
	 * named 'meta' that contains it. If a reference URL is supplied, 
	 * then there is a part named 'refurl' that contains it. If a 
	 * one-time key is supplied, then there is a part named 'one-time-key'
	 * that contains it.
	 * 
	 * @param requestURL the URL to which the request is to be made
	 * 
	 * @param values the values to be conveyed.
	 * 
	 * @param meta the metadata to be conveyed, in the form
	 * of a set of name-value pairs.
	 * 
	 * @param refUrl a reference URL from which data can be obtained,
	 * or null. (This is often the requesting system's API URL.)
	 * 
	 * @param key a one-time key, or null
	 * 
	 * @return the response returned by the request, including the
	 * status code and text.
	 * 
	 * @throws Exception
	 */
	
	@SuppressWarnings("unchecked")
	public Response makeJsonWithBinaryRequest(
			String requestURL, Map<String, Datum> values, 
			Map<String, String> meta, String refurl, 
			String oneTimeKey) throws Exception {

		Map<String, Datum> partDatums = new HashMap<String, Datum>();
		JSONObject jo = new JSONObject();
		for (Entry<String, Datum> e : values.entrySet()) {
			jo.put(e.getKey(), StoreApi.getJsonFromDatum(
					e.getValue(), partDatums));
		}
		partDatums.put("json", Datum.createText(jo.toJSONString()));
		if (meta != null) {
			JSONObject metaJo = new JSONObject();
			for (Entry<String, String> e: meta.entrySet())
				metaJo.put(e.getKey(), e.getValue());
			partDatums.put(
				"meta", Datum.createText(metaJo.toJSONString()));
		}
		if (refurl != null) partDatums.put(
				"refurl", Datum.createText(refurl));
		if (oneTimeKey != null) partDatums.put(
				"one-time-key", Datum.createText(oneTimeKey));
		return makeMultipartRequest(requestURL, partDatums);
	}
	
	/**
	 * Make an HTTP POST request with multipart form encoded content
	 * 
	 * @param requestURL the URL to which the request is to be made
	 * 
	 * @param parts the content of the request. Each name-datum pair
	 * of the map is to be a part of the multipart request.
	 * 
	 * @return the response returned by the request, including the
	 * status code and text.
	 * 
	 * @throws Exception
	 */
	
	public Response makeMultipartRequest(
			String requestURL, Map<String, Datum> partDatums)
			throws Exception {
		
		// Get a suitable multipart boundary
		Multipart multipart = new Multipart();
		String boundary = multipart.getBoundary(partDatums);
		
		// Establish an HTTP connection
        URL url = new URL(requestURL);
        HttpURLConnection httpConn = null;
		try {
			// Configure the connection
	        httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setUseCaches(false);
	        httpConn.setDoOutput(true);    // indicates POST method
	        httpConn.setDoInput(true);
	        httpConn.setRequestProperty("Content-Type",
	                "multipart/form-data; boundary=" + boundary);
	        
	        // Output the content
	        OutputStream outputStream = httpConn.getOutputStream();
	        multipart.outputMultipartContent(partDatums, boundary, outputStream);
	        
		    // Return the response
		    return Response.getResponse(httpConn);
		} finally {
            if (httpConn != null) httpConn.disconnect();
        }
	}
	
	/**
	 * Add multipart content to an HTTP response.
	 * 
	 * @param result the data to include in the response, supplied as
	 * a String-Datum map. For each entry, a part will be generated with
	 * the same name as the entry, and with content whose type is
	 * application/octet-stream if the datum is a Binary datum, and
	 * text/plain with utf8 encoding otherwise.
	 * 
	 * @param response the response to which the content is to be added.
	 * 
	 * @throws Exception
	 */
	
	public void makeMultipartResponse(
			Map<String, Datum> result, HttpServletResponse response) 
					throws Exception {
		
		Multipart multipart = new Multipart();
		String boundary = multipart.getBoundary(result);
		response.setContentType("multipart/form-data; boundary=" + boundary);
		
		OutputStream outputStream = response.getOutputStream();
		multipart.outputMultipartContent(result, boundary, outputStream);
		outputStream.close();
	}
	
	/**
	 * Get unique datums from the parts of an HTTP request with
	 * multipart form data content. If there is more than one part
	 * with a particular name, the datums cannot be unique, and an
	 * exception is thrown.
	 * 
	 * @param request an HTTP request with multipart form data content.
	 * 
	 * @param tempFiles a set of temporary files to contain the content of
	 * large parts of the input content.
	 * 
	 * @return a map containing an entry for each part name. The key of 
	 * the entry is the part name. The value of the entry is a datum 
	 * containing the content of the unique part with that name.
	 * 
	 * @throws Exception
	 */
	
	public static Map<String, Datum> getMultipartUniqueDatums(
			HttpServletRequest request, Set<File> tempFiles) 
					throws Exception {
		
		Map<String, Datum> multipartUniqueDatums =
				new HashMap<String, Datum>();
		Map<String, List<Datum>> multipartDatums = 
				getMultipartDatums(request, tempFiles);
		for (Entry<String, List<Datum>> partDatums : multipartDatums.entrySet()) {
			String name = partDatums.getKey();
			List<Datum> values = partDatums.getValue();
			if (values.size() != 1) throw (new InputException(
					"No or multiple inputs for: " + name));
			multipartUniqueDatums.put(name, values.get(0));
		}
		return multipartUniqueDatums;
	}

	/**
	 * Get datums from the parts of an HTTP request with
	 * multipart form data content.
	 *
	 * @param request an HTTP request with multipart form data content.
	 * 
	 * @param tempFiles a set of temporary files to contain the content of
	 * large parts of the input content.
	 * 
	 * @return a map containing an entry for each part name. The key of 
	 * the entry is the part name. The value of the entry is a list of datums, 
	 * each containing the content of a part with that name.
	 * 
	 * @throws Exception
	 */
	
	public static Map<String, List<Datum>> getMultipartDatums(
			HttpServletRequest request, Set<File> tempFiles) 
					throws Exception {
		
		Map<String, List<Datum>> partDatums = new HashMap<String, List<Datum>>();
		Collection<Part> parts = request.getParts();		
		for (Part part : parts) {
			String name = part.getName();
			List<Datum> datums = partDatums.get(name);
			if (datums == null) {
				datums = new ArrayList<Datum>(1);
				partDatums.put(name, datums);
			}
			String contentType = part.getContentType();
			if (isText(contentType)) {
				// A text part
				long n = part.getSize();
				if (n <= 0);
				else if (n <= BIG)
					datums.add(Datum.createText(getStringPart(part)));
				else {
					File file = File.createTempFile("postin", null);
					getTextPart(part, file);
					datums.add(Datum.createText(file));
					tempFiles.add(file);
				}
			}
			else {
				// A binary part
				long n = part.getSize();
				if (n <= 0);
				else if (n <= BIG) {
					datums.add(Datum.createBinary(getBytesPart(part)));
				}
				else {
					File file = File.createTempFile("postin", null);
					getBinaryPart(part, file);
					datums.add(Datum.createBinary(file));
					tempFiles.add(file);
				}
			}
		}
		return partDatums;
	}
	
	/**
	 * Determine whether a content type is a text content type.
	 * A content type is assumed to be a text type if it starts with
	 * "text/" or if it is "application/javascript" or "application/json".
	 * Otherwise it is assumed not to be a text content type.
	 * 
	 * @param contentType a content type.
	 * 
	 * @return whether the content type is a text content type.
	 */
	
	public static boolean isText(String contentType) {
				
		if ((contentType == null) || contentType.startsWith("text/")) return true;
		if (contentType.startsWith("application/")) {
			String appContentType = contentType.substring("application/".length());
			if (appContentType.contains("javascript")) return true;
			if (appContentType.contains("json")) return true;
		}
		return false;
	}

	/**
	 * Get the content of a part as an array of bytes
	 * 
	 * @param part a part of multipart content
	 * 
	 * @return a byte array containing the content of the part.
	 * 
	 * @throws IOException
	 */
	
	public static byte[] getBytesPart(Part part) throws IOException {

		InputStream is = part.getInputStream();
		if (is == null) return null;
		long lsize = part.getSize();
		if ((lsize < 0) || (lsize > Integer.MAX_VALUE)) return null;
		int size = (int)lsize;
		byte[] bytes = new byte[size];
		if (size == 0) return bytes; 
		try {
			if (size != is.read(bytes)) throw(new IOException(
					"Invalid binary input length"));
			return bytes;
		} finally {
			is.close();
		}
	}

	/**
	 * Get the content of a part as a string.
	 * 
	 * @param part a part of multipart content
	 * 
	 * @return a string containing the content of the part.
	 * 
	 * @throws IOException
	 */
	
	public static String getStringPart(Part part) throws IOException {
		
		InputStream is = part.getInputStream();
		if (is == null) return null;
		InputStreamReader reader = new InputStreamReader(is);
		try {
			StringWriter sw = new StringWriter();
			int c;
			while ((c = reader.read()) >= 0) sw.write(c);
			sw.close();
			return sw.toString();
		} finally {
			reader.close();
		}
	}
	
	/**
	 * Get the content of a large binary part as a datum.
	 * 
	 * @param part a binary part of multipart content
	 * 
	 * @param file a file to contain the content of the part
	 * 
	 * @return a binary file datum containing the content of the part.
	 * 
	 * @throws Exception
	 */
	
	public static Datum getBinaryPart(Part part, File file)
			throws Exception {
		
		InputStream is = part.getInputStream();
		if (is == null) return null;
		BufferedInputStream bis = 
				new BufferedInputStream(is);
		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(new FileOutputStream(file));
			int len = 0;
			int c;
			while ((c = bis.read()) >= 0) {
				bos.write(c);
				len++;
			}
			bis.close();
			if (len == 0) return null;
			return Datum.createBinary(file);
		} finally {
			bos.close();
		}
	}
	
	/**
	 * Get the content of a large text part as a datum.
	 * 
	 * @param part a text part of multipart content
	 * 
	 * @param file a file to contain the content of the part
	 * 
	 * @return a text file datum containing the content of the part.
	 * 
	 * @throws Exception
	 */
		
	public static Datum getTextPart(Part part, File file)
			throws Exception {
		
		InputStream is = part.getInputStream();
		if (is == null) return null;
		BufferedReader reader;
		BufferedWriter writer;
		reader = new BufferedReader(
				new InputStreamReader(is));
		try {
			writer = new BufferedWriter(
					new FileWriter(file));
			try {
				int len = 0;
				int c;
				while ((c = reader.read()) >= 0) {
					writer.write(c);
					len++;
				}
				if (len == 0) return null;
			} finally {
				writer.close();
			}
		} finally {
			reader.close();
		}
		return Datum.createText(file);
	}
}
