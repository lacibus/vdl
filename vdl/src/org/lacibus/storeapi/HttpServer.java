package org.lacibus.storeapi;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.lacibus.triplestore.Logger;
import org.lacibus.triplestore.SessionIssuer;
import org.lacibus.triplestore.StoreSession;
import org.lacibus.triplestore.TripleStore;

public class HttpServer extends HttpServlet implements Serializable {

	private static final long serialVersionUID = 2216772208933056472L;
	
	private static final String PATHINFO = "/api";

	/**
	 * The servlet parameter giving the path to the local directory
	 * used to hold information pertaining to the system source
	 */
	
	static final String sysSourceDirParameterName = "sysSource";
		
	/**
	 * The servlet parameter giving the path to the logfile
	 */
	
	static final String logParameterName = "log";
	
	StoreApi storeApi = null;
		
	StoreSession defaultSession = null;
	
	protected String logPath = null;
	
	private Initialiser initialiser;
	
	private boolean initializing;

	public String terminationCause = null;
			
    public void init(ServletConfig config) 
    		throws ServletException {
    	
    	initializing = true;
    	super.init(config);
    	initialiser = new Initialiser(config);
    	initialiser.start();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(
			HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException {
		
		handleRequest(request, response, false);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
			HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException {
		
		handleRequest(request, response, true);
	}
	
	private void handleRequest(
			HttpServletRequest request, HttpServletResponse response, boolean post) 
					throws IOException, ServletException {
		
		if (initializing) {
			response.getWriter().println("Initialising . . please wait.");
		}
		else if (terminationCause == null) {
			if (PATHINFO.equals(request.getPathInfo())) {
				try {
					storeApi.handleHttpRequest(null, request, response, defaultSession);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		else {
			PrintWriter w = response.getWriter();
			w.println("Initialisation failed.");
			w.println(terminationCause);
		}
	}
	
	private class Initialiser extends Thread implements Serializable {

		private static final long serialVersionUID = 4609785621850516552L;

		/**
		 * The environment variable giving the path to the file
		 * containing the home source configuration specification.
		 * This need not be set.
		 */
		
		static final String logFileEnvVar = "LOGFILE";
		
		ServletConfig config;
		
		public Initialiser(ServletConfig config) {
			
			this.config = config;
		}
		
		public void run() {
			
			Logger logger = null;
	    	try {
	    		// Get the log path from the environment variables (this can be null)
	    		logPath = System.getenv(logFileEnvVar);	    		
	    		logger = new Logger(logPath);
	    		logger.initialize();
	    		logger.log("Start at " + new Date(System.currentTimeMillis()));
	    		logger.log("Log path " + logPath);
	    		
	    		// Get the system source directory from the servlet parameters
	    		String systemSourceDirPath = config.getInitParameter(sysSourceDirParameterName);
	    		if (systemSourceDirPath == null) throw (new ApiUserException(
	    				sysSourceDirParameterName + " servlet parameter not found"));
	    		
	    		// Create the triple store
	            TripleStore store = new TripleStore(5000, 5000);
	    		logger.log("Triple store created");
	    		
	        	// Set up the triple store
	            SessionIssuer sessionIssuer = store.initialize(); 
	    		store.setLogger(logger);
	    		logger.log("Triple store initialized");
	    		
	    		storeApi = new StoreApi(sessionIssuer); 
	    		defaultSession = sessionIssuer.getStoreSession(
	    				sessionIssuer.getStore().lowestAccessLevel());
	    		logger.log("Initialization complete");
			} catch (Throwable t) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				t.printStackTrace(pw);
				terminationCause = sw.toString();
				if (logger != null) logger.log(terminationCause);
			} finally {
				initializing = false;
			}
		}		
	}
}
