package org.lacibus.storeapi;

import java.io.Serializable;

public class ApiUserException extends Exception implements Serializable {

	private static final long serialVersionUID = 6751903800656544851L;

	public ApiUserException(String message) {
		
		super(message);
	}
}
