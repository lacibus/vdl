package org.lacibus.storeapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;

/**
 * The status and text of an HTTP response
 */

public class Response implements Serializable {

	private static final long serialVersionUID = -4502055120973126393L;

	/**
	 * The response status
	 */
	
	public int status;
	
	/**
	 * The response text
	 */
	
	public String text;
	
	/**
	 * Construct a response.
	 */
	
	private Response() {}
	
	/**
	 * Get the response from an HTTP connection
	 * 
	 * @param httpConn an HTTP connection
	 * 
	 * @return the response status and text
	 * 
	 * @throws IOException
	 */
	
	public static Response getResponse(HttpURLConnection httpConn) throws IOException {
		
		Response response = new Response();
		// Get the status
		response.status = httpConn.getResponseCode();

	    // Read the response
		BufferedReader reader;
		if (response.status == HttpURLConnection.HTTP_OK) 
			reader = new BufferedReader(new InputStreamReader(
					httpConn.getInputStream()));
		else reader = new BufferedReader(new InputStreamReader(
				httpConn.getErrorStream()));
		StringBuilder builder = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	        builder.append(line);
	        builder.append('\n');
	    }
	    reader.close();
	    response.text = builder.toString();
	    
	    // Return the result
	    return response;
	}
}
