/**
 * <p>This package provides a Web API to virtual data lake triple stores.
 * The API enables HTTP clients to query and update the stored data.
 * </p>
 * <h2>Queries</h2>
 * <p>A query request returns a list of solutions that satisfy a set
 * of constraints.
 * </p>
 * <p>A constraint has a subject, verb and object, each of which can be
 * given or unknown. A solution assigns a typed value to each unknown
 * in the set of constraints.
 * </p>
 * <p>For example, a content management system might organize pages 
 * in workspaces. To list the pages in the 'Site Main Pages' workspace
 * it could make a query with constraints:
 * </p>
 * <ul>
 * <li><i>Workspace</i> <b>has name</b> 'Site Main Pages'</li>
 * <li><i>Workspace</i> <b>contains page</b> <i>Page</i></li>
 * <li><i>Page</i> <b>has name</b> <i>Page Name</i></li>
 * </ul>
 * <p>This would return a list of solutions, each of which would assign a
 * typed value to <i>Workspace</i>, <i>Page</i>, and <i>Page Name</i>.
 * (The workspace would be the same in all the solutions; the pages and 
 * page names would be different.) The workspaces and the pages would be 
 * of type ITEM, and the page names ('Home', 'About', 'Get Involved',  etc.)
 * would be of type TEXT.
 * </p>
 * <h2>Updates</h2>
 * <p>An update request performs a set of changes and returns the new items
 * created by them.
 * </p>
 * <p>The changes that can be made are:
 * </p>
 * <ul>
 * <li><b>Create an item:</b> an item is created in a source, with read and
 * write access levels, and with a handle that can be used to refer to it
 * in other changes.</li>
 * <li><b>Delete an item:</b> a specified item is deleted.</li>
 * <li><b>Put a triple:</b> A triple with a specified subject, verb and object
 * is created if the store does not already contain one.</li>
 * <li><b>Remove triples:</b> All triples with a specified subject, verb and
 * object are deleted or, if no object is specified, all triples with a 
 * specified subject and verb are deleted.</li>
 * <li><b>Set a unique object:</b> if there is no triple with a specified
 * subject, verb and object, then one is created; if there is more than
 * one triple with the specified subject and verb, all but one are deleted; 
 * so that there is exactly one triple with the specified subject and verb,
 * and it has the specified object.</li>
 * </ul>
 * <p>When a request returns, all or none of its changes will have been made.
 * If an error occurs in making one of them, any previously-made changes are
 * reversed.
 * </p>
 * <p>The new items are returned with the handles given in the changes to
 * create them, so that they can be distinguished from each other. The handles
 * are not stored and cannot be used in subsequent API requests.
 * </p>
 * <h2>Access Control</h2>
 * <p>Each request is processed at an <i>access level</i> that determines what
 * data it can read and write. The access level can be set by a credential
 * in the request. If no credential is supplied then the request is processed
 * at the lowest (public) level. This allows reading of some data but does not
 * allow writing of any data.
 * </p>
 * <p>A supplied credential must be accompanied by its correct key. Credentials
 * are not kept secret, but their keys should be kept secret by clients.
 * They are not stored in the triple store; hash digests of them are stored
 * and used to validate them.
 * </p>
 * <p>Credentials and their keys are obtained by out-of-band means, not through
 * the API.
 * </p>
 * <h2>Data Representations</h2>
 * <p>The API uses JSON as its data representation format, but
 * this is complicated by the need to pass binary values, which cannot 
 * natively be included in JSON.
 * </p>
 * <p>The requests and responses have multipart form data content type. 
 * One of the parts is named 'json' and is a utf8 string representation 
 * of a JSON object or of a JSON array of JSON objects. Each binary value 
 * is represented in the JSON object or array by a string of the form 
 * b<i>n</i> where <i>n</i> is an integer. There is then a part named 
 * b<i>n</i> with application/octet-stream encoding that contains the
 * binary value.
 * </p>
 * <p>Another part, which need not be supplied, is named 'auth'. If
 * supplied, it contains a JSON object represented as utf8-encoded 
 * plain text. This has two string attributes: 'credential' and 'key'. 
 * The credential is the item identifier of a credential, and 
 * the key is the credential's key.
 * </p>
 * <h3>Requests and Responses</h3>
 * <p>For a query, the input JSON is a JSON array, each of 
 * whose elements is a JSON array representing a constraint.
 * The output JSON is a JSON array, each of whose elements is
 * a JSON object representing a solution.
 * </p>
 * <p>For an update, the input JSON is a JSON array, each of
 * whose elements is a JSON object representing a change. The
 * output JSON is a JSON object representing the new items
 * with their handles.
 * </p>
 * <h3>Constraints</h3>
 * <p>A JSON array that represents a constraint has three elements,
 * representing the constraint's subject verb and object. Each
 * of these is represented by a JSON object and is either a given,
 * which is a typed value, or an unknown.
 * </p>
 * <h3>Solutions</h3>
 * <p>A JSON object that represents a solution has an attribute
 * for each unknown. Its name is the name of the unknown, and its
 * value represents the typed value assigned to the unknown in
 * the solution.
 * </p>
 * <h3>Changes</h3>
 * <p>A JSON object that represents a change has a 'change'
 * attribute that specifies the type of change, and other 
 * attributes depending on the type. The type can be: 'create item',
 * 'delete item', 'put triple', 'remove triples', or 'set unique
 * object'.
 * </p>
 * <p>For a change to create an item, the other attributes are 'handle',
 * 'source', 'read', and 'write'. The handle is a string that can be used
 * to refer to the item in other changes. The source is an integer that is
 * the numeric identifier of the source that is to contain the item. The
 * 'read' and 'write' attributes are strings that specify the read
 * and write access levels that the new item is to have. They need not
 * be present; they default to the access level of the authorized store
 * session.
 * </p>
 * <p>For a change to delete an item, there is one other attribute:
 * 'item', giving the item identifier of the item to be deleted.
 * </p>
 * <p>For a change to put a triple, the other attributes are 'subject',
 * 'verb', and 'object', representing the subject, verb and object of
 * the triple to be put. Each of these may be given or unknown. If it
 * is unknown, it is the handle of a new item to be created.
 * </p>
 * <p>For a change to remove triples, the other attributes are 'subject',
 * 'verb', and 'object', representing the subject, verb and object of
 * the triples to remove. The subject and verb attributes are strings
 * that specify the subject and verb. The object attribute need not be
 * present. If it is present, it is a typed value.
 * </p>
 * <p>For a change to set a unique object, the other attributes are
 * 'subject', 'verb', and 'object'. The subject and verb attributes  
 * are strings that specify the subject and verb that are to have a 
 * unique object. The object may be given or unknown. An unknown is 
 * the handle of a new item to be created.
 * </p>
 * <h3>New Items</h3>
 * <p>The JSON object representing the new items created by an update
 * has an attribute for each of these items. Its name is the item's
 * handle, and its value is the item's identifier.
 * </p>
 * <h3>Unknowns</h3>
 * <p>A JSON object that represents an unknown has a single 
 * attribute, 'unknown', whose value is the name of the unknown. 
 * </p>
 * <h3>Typed Values</h3>
 * <p>A JSON object that represents a typed value has two attributes:
 * 'type' and 'value'. 
 * </p>
 * <p>The type can be ITEM, BOOLEAN, INTEGRAL, REAL, TEXT, or BINARY.
 * </p>
 * <p>The value is a textual representation of a quantity of the type. 
 * For an ITEM, it is either an item identifier or the qualified 
 * name of a named item. For a BOOLEAN, it is 'true' or 'false', 
 * ignoring case. For an INTEGRAL or REAL, it is the string
 * representation of a decimal integer or real number. For a TEXT,
 * it is the textual value. For a BINARY, it is the name of a multipart
 * form part whose content is the binary value.
 * </p>
 * <h3>Items and Access Levels</h3
 * <p>A string that specifies an item is an item identifier or the
 * qualified name of a named item.
 * </p>
 * <p>A string that specifies an access level is the string that
 * specifies its item.
 * </p>
 */

package org.lacibus.storeapi;