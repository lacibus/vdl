package org.lacibus.storeapi;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.Map;
import java.util.Map.Entry;

import org.lacibus.storeaccess.InputException;
import org.lacibus.triplestore.Datum;

/**
 * An instance of this class can define a multipart boundary
 * and create multipart form encoded content.
 */

public class Multipart {
	
	/**
	 * Binary representation of carriage-return-line-feed
	 */
	
	static final String CRLF = "\r\n";
	
	/**
	 * Maximum length of the encapsulating boundary
	 */
	
	static final int maxEncapsulatingBoundaryLength = 72;
	
	/**
	 * The encapsulating boundary generated by analysing the parts
	 */

	char[] encapsulatingBoundary = new char[maxEncapsulatingBoundaryLength];
	
	/**
	 * The length of the encapsulating boundary generated by analysing the parts
	 */
	
	int encapsulatingBoundaryLength = 0;
	
	/**
	 * The length of the segment of most recent inputs that matches the 
	 * first part of encapsulating boundary being generated by analysing the parts
	 */
	
	int matchingInputs = 0;

	/**
	 * The character to be added to the boundary if it completely matches
	 * the segment of most recent inputs that
	 */
	
	char boundaryChar = '0';
	
	/**
	 * Construct a Multipart
	 */
	
	public Multipart() {
		
		encapsulatingBoundary[0] = '-';
		encapsulatingBoundary[1] = '-';
		encapsulatingBoundary[2] = '&';
		encapsulatingBoundaryLength = 3;
	}
	/**
	 * Output multipart content
	 * 
	 * @param partDatums the data to include in the response, supplied as
	 * a String-Datum map. For each entry, a part will be generated with
	 * the same name as the entry, and with content whose type is
	 * application/octet-stream if the datum is a Binary datum, and
	 * text/plain with utf8 encoding otherwise.
	 * 
	 * @param boundary the multipart boundary
	 * 
	 * @param outputStream the output stream to which the content is to
	 * be written.
	 * 
	 * @throws Exception
	 */
	
	public void outputMultipartContent(
			Map<String, Datum> partDatums, String boundary, 
			OutputStream outputStream) throws Exception {
		
		PrintWriter writer = new PrintWriter(
				new OutputStreamWriter(outputStream, "utf8"), true);
		for (Entry<String, Datum> part : partDatums.entrySet()) {
			String name = part.getKey();
			Datum datum = part.getValue();
		    writer.append("--" + boundary);
		    writer.append(CRLF);
		    // Include the filename in the header so that it can be processed
		    // in the same way as a form, e.g. by Flask.
		    writer.append("Content-Disposition: form-data; name=\"" + name + "\"; filename=\"" + name + "\"");
	        writer.append(CRLF);
			switch(datum.getType()) {
			case ITEM:
		        writer.append("Content-Type: text/plain; charset=utf8");
		        writer.append(CRLF);
		        writer.append(CRLF);
		        writer.append(datum.getItemValue().getId().toString());
			    writer.flush();
				break;
			case BOOLEAN:
		        writer.append("Content-Type: text/plain; charset=utf8");
		        writer.append(CRLF);
		        writer.append(CRLF);
		        writer.append(Boolean.toString(datum.getBooleanValue()));
			    writer.flush();
				break;
			case INTEGRAL:
		        writer.append("Content-Type: text/plain; charset=utf8");
		        writer.append(CRLF);
		        writer.append(CRLF);
		        writer.append(Long.toString(datum.getIntegralValue()));
			    writer.flush();
				break;
			case REAL:
		        writer.append("Content-Type: text/plain; charset=utf8");
		        writer.append(CRLF);
		        writer.append(CRLF);
		        writer.append(Double.toString(datum.getRealValue()));
			    writer.flush();
				break;
			case TEXT:
		        writer.append("Content-Type: text/plain; charset=utf8");
		        writer.append(CRLF);
		        writer.append(CRLF);
		        BufferedReader reader = datum.getTextValueReader();
		        int c;
		        while ((c = reader.read()) >= 0) writer.append((char) c);
			    writer.flush();
				break;
			case BINARY:
		        writer.append("Content-Type: application/octet-stream");
		        writer.append(CRLF);
		        writer.append(CRLF);
			    writer.flush();
		        BufferedInputStream stream = datum.getBinaryValueStream();
		        int b;
		        while ((b = stream.read()) >= 0) outputStream.write(b);
		        outputStream.flush();
				break;
			default: break;
			}
		    writer.append(CRLF);
		    writer.flush();
		}

	    writer.append("--" + boundary + "--");
	    writer.append(CRLF);
	    writer.close();
	}
			
	/**
	 * Get a multipart boundary by analysing the parts. The boundary is
	 * guaranteed not to occur in any of the parts.
	 * 
	 * @param parts the parts to be analysed
	 * 
	 * @return a multipart boundary
	 * 
	 * @throws Exception
	 */
	
	public String getBoundary(Map<String, Datum> parts) throws Exception {
			
		for (Datum datum : parts.values()) {
			switch(datum.getType()) {
			case TEXT:
				checkText(datum.getTextValueReader());
				break;
			case BINARY:
				checkBinary(datum.getBinaryValueStream());
				break;
			default: break;
			}
		}
		return new String(getBoundaryChars());
	}
	
	/**
	 * @return the characters of the defined boundary, 
	 * not including the two initial hyphen
	 * characters of the encapsulating boundary.
	 */
	
	private char[] getBoundaryChars() {
		
		int boundaryLength = encapsulatingBoundaryLength - 2;
		char[] boundary = new char[boundaryLength];
		System.arraycopy(encapsulatingBoundary, 2, boundary, 0, boundaryLength);
		return boundary;
	}
	
	/**
	 * Analyse a text part and update the boundary to ensure
	 * that it does not occur in the part
	 * 
	 * @param reader a reader to read the text part
	 * 
	 * @throws IOException
	 * @throws InputException
	 */
	
	private void checkText(Reader reader) 
			throws IOException, InputException {
		
		int inputChar;
		while ((inputChar = reader.read()) >= 0) checkInput(inputChar);
		checkEndMatch();
	}
	
	/**
	 * Analyse a binary part and update the boundary to ensure
	 * that it does not occur in the part
	 * 
	 * @param stream an input stream to input the text part
	 * 
	 * @throws IOException
	 * @throws InputException
	 */
	
	private void checkBinary(InputStream stream) 
			throws IOException, InputException {
		
		int inputByte;
		while ((inputByte = stream.read()) >= 0)  checkInput(inputByte);
		checkEndMatch();
	}
	
	/**
	 * Check an input byte and extend the boundary if a segment
	 * of the most recent input bytes, with this added, completely
	 * matches the current encapsulating boundary
	 * 
	 * @param input an input byte
	 * 
	 * @throws InputException
	 */
	
	private void checkInput(int input) throws InputException {
		
		if (matchingInputs < encapsulatingBoundaryLength) {
			if (input == encapsulatingBoundary[matchingInputs]) matchingInputs++;
			else matchingInputs = getMatchingInputs();
		}
		else {
			if (encapsulatingBoundaryLength == maxEncapsulatingBoundaryLength)
				throw (new InputException(
					"Cannot define boundary"));
			if (input == boundaryChar) 
				encapsulatingBoundary[encapsulatingBoundaryLength++] = '&';
			else encapsulatingBoundary[encapsulatingBoundaryLength++] = boundaryChar;
			if (boundaryChar == '9') boundaryChar = 0;
			else boundaryChar++;
			matchingInputs = getMatchingInputs();
		}
	}
	
	/**
	 * Extend the boundary if a segment of the most recent input bytes,
	 * matches the current encapsulating boundary.
	 * 
	 * @throws InputException
	 */
	
	private void checkEndMatch() throws InputException {
		
		if (matchingInputs == encapsulatingBoundaryLength) {
			if (encapsulatingBoundaryLength == maxEncapsulatingBoundaryLength) 
				throw (new InputException(
					"Cannot define boundary"));
			else encapsulatingBoundary[encapsulatingBoundaryLength++] = '&';
		}
	}
		
	/**
	 * @return the length of the longest initial segment of the current
	 * encapsulating boundary that is matched by the most recent input.
	 */
	
	private int getMatchingInputs() {
		
		for (int segmentLength = matchingInputs - 1; 
				segmentLength > 0; segmentLength--) {
			if (endSegmentMatchesStart(segmentLength)) return segmentLength;
		}
		return 0;
	}
	
	/**
	 * Determine whether a segment at the end of the current encapsulating
	 * boundary matches the start  of the current encapsulating boundary.
	 * 
	 * @param segmentLength the length of the segment
	 * 
	 * @return whether the segment with the given length at the end of the 
	 * current encapsulating boundary matches the segment with the same length
	 * at the start  of the current encapsulating boundary.
	 */
	
	private boolean endSegmentMatchesStart(int segmentLength) {
		
		int startOfSegment = matchingInputs - segmentLength;
		for (int j = 0; j < segmentLength; j++) {
			if (encapsulatingBoundary[j] != encapsulatingBoundary[startOfSegment + j])
				return false;
		}
		return true;
	}
}
