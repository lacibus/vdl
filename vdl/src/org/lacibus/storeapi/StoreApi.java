package org.lacibus.storeapi;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.lacibus.backingstore.FileSourceConnector;
import org.lacibus.storeaccess.ChangeBundle;
import org.lacibus.storeaccess.ConstrainedTriples;
import org.lacibus.storeaccess.Given;
import org.lacibus.storeaccess.InputException;
import org.lacibus.storeaccess.Join;
import org.lacibus.storeaccess.JoinConstraint;
import org.lacibus.storeaccess.Quantum;
import org.lacibus.storeaccess.Unknown;
import org.lacibus.triplesource.SourceConnector;
import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplestore.AccessLevel;
import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Id;
import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.NotInOperationException;
import org.lacibus.triplestore.SessionIssuer;
import org.lacibus.triplestore.StoreSession;
import org.lacibus.triplestore.TripleStore;
import org.lacibus.triplestore.TripleStoreFatalException;
import org.lacibus.triplestore.TripleStoreNonFatalException;

/**
 * A Web API to a triple store.
 */

public class StoreApi implements Serializable {

	private static final long serialVersionUID = 6444294632982450253L;
	
	final static long homeSourceNr = 1L;

	TripleStore store;
	
	SessionIssuer sessionIssuer;
	
	/**
	 * Construct an interface to the triple store that can be invoked
	 * using HTTP.
	 * 
	 * @param sessionIssuer the session issuer of a triple store. 
	 * It includes a reference to the store.
	 */
	
	public StoreApi(SessionIssuer sessionIssuer) {
		
		this.store = sessionIssuer.getStore();
		this.sessionIssuer = sessionIssuer;
	}
	
	/**
	 * <p>Handle an HTTP request. The request must be a POST request.
	 * </p>
	 * <p>This method gets the inputs, establishes the authorized 
	 * store session to process the request, invokes one
	 * of the specific request handling methods, and places the 
	 * result in the response.
	 * </p>
	 * 
	 * @param command the command to be processed. If it is null, 
	 * the command is obtained from the HTTP request. If it is not 
	 * null, it supersedes the command in the HTTP request.
	 * 
	 * @param request an HTTP POST request conveying a command and
	 * its inputs. The command is the value of the request "command"
	 * parameter. It can be 'query" or 'update'. The inputs are 
	 * supplied in the multipart form data content.
	 * 
	 * @param response the response to the request. 
     *
	 * @param storeSession the requesting store session, or null. 
	 * If the request includes authorization input then its credential
	 * is used to generate the authorized session for processing the 
	 * command. If the request does not include authorization then
	 * the command is processed with an authorized session whose access 
	 * level is the lowest (public) level.
	 * @throws IOException 
	 * 
	 * @throws Exception
	 */
	
	public void handleHttpRequest(
			String command, HttpServletRequest request, 
			HttpServletResponse response, StoreSession storeSession) 
					throws IOException {
		
		if (command == null) command = request.getParameter("command");
		if (command == null) {
			returnError(response, HttpServletResponse.SC_BAD_REQUEST, 
					"No command supplied");
			return;
		}
		
		// Get the inputs
		Set<File> tempFiles = new HashSet<File>();
		Map<String, Datum> inputs = new HashMap<String, Datum>();
		try {
			inputs = MultipartHttp.getMultipartUniqueDatums(request, tempFiles);
		} catch (Exception e) {
			returnError(response, HttpServletResponse.SC_BAD_REQUEST, 
					"Invalid inputs: " + e.getMessage());
			return;
		} finally {
			// Delete any temporary files that were created
			for (File file : tempFiles) file.delete();
		}
		
		// Get the authorized store session to process the command
		StoreSession authorizedSession = null;
		try {
			authorizedSession = 
					getAuthorizedStoreSession(
							inputs.get("auth"), storeSession);
		} catch (Exception e) {
			returnError(response, HttpServletResponse.SC_BAD_REQUEST, 
					"Invalid authorization: " + e.getMessage());
			return;
		}
			
		// Process the command and make the response
		try {
			Map<String, Datum> result = processCommand(
					command, inputs, authorizedSession);
			if (result == null) {
				returnError(response, HttpServletResponse.SC_BAD_REQUEST, 
						"Invalid command: " + command);
				return;				
			}
			new MultipartHttp().makeMultipartResponse(result, response);
		} catch (Exception e) {
			returnError(response, HttpServletResponse.SC_CONFLICT, 
					e.getMessage());
			return;
		}
	}
	
	/**
	 * Process a command, obtaining authorization from inputs,
	 * and place the results in an HTTP response
	 * 
	 * @param command the command to be processed
	 * 
	 * @param inputs authorization and command parameters
	 * 
	 * @param response the HTTP response in which the results
	 * are to be placed
	 * 
	 * @param storeSession the requesting store session
	 * 
	 * @throws Exception
	 */
	
	public void authorizeAndProcessCommand(
				String command, Map<String, Datum> inputs,
				HttpServletResponse response, 
				StoreSession storeSession) 
						throws Exception {
		
		// Get the authorized store session to process the command
		StoreSession authorizedSession = null;
		try {
			authorizedSession = 
					getAuthorizedStoreSession(
							inputs.get("auth"), storeSession);
		} catch (Exception e) {
			returnError(response, HttpServletResponse.SC_BAD_REQUEST, 
					"Invalid authorization: " + e.getMessage());
			return;
		}
			
		// Process the command and make the response
		try {
			Map<String, Datum> result = processCommand(
					command, inputs, authorizedSession);
			if (result == null) {
					returnError(response, HttpServletResponse.SC_BAD_REQUEST, 
							"Invalid command: " + command);
					return;				
				}
			new MultipartHttp().makeMultipartResponse(result, response);
		} catch (Exception e) {
			returnError(response, HttpServletResponse.SC_CONFLICT, 
					e.getMessage());
			return;
		}
	}

	/**
	 * Process a command using an authorized session
	 * 
	 * @param command the command to be processed
	 * 
	 * @param inputs the command parameters
	 * 
	 * @param authorizedSession the store session to process the command
	 * 
	 * @return the values resulting from processing the command
	 * 
	 * @throws Exception
	 */
	
	public Map<String, Datum> processCommand(
				String command, Map<String, Datum> inputs, 
				StoreSession authorizedSession) throws Exception {
		
		Map<String, Datum> result = null;				
		// Process the command
		if ("create_local_source".equals(command)) {
			long sourceNr = inputs.get("number").getIntegralValue();
			if (SourceConnector.isInstallationSourceNr(sourceNr))
				throw (new ApiUserException("Cannot create installation source."));
			String localDirPath = inputs.get("dir").getTextValueAsString();
			SourceConnector connector = new FileSourceConnector(
					store, sourceNr, new File (localDirPath), null, store.getLogger());
			store.create(connector, authorizedSession);
			makeHomeSuperior(sourceNr);
		}
		if ("connect_local_source".equals(command)) {
			long sourceNr = inputs.get("number").getIntegralValue();
			if (SourceConnector.isInstallationSourceNr(sourceNr))
				throw (new ApiUserException("Cannot connect installation source."));
			String localDirPath = inputs.get("dir").getTextValueAsString();
			SourceConnector connector = new FileSourceConnector(
					store, sourceNr, new File (localDirPath), null, store.getLogger());
			store.connect(connector, authorizedSession);
		}
		else if ("disconnect_source".equals(command)) {
			long sourceNr = inputs.get("").getIntegralValue();
			if (SourceConnector.isInstallationSourceNr(sourceNr))
				throw (new ApiUserException("Cannot disconnect installation source."));
			store.disconnect(sourceNr);
		}
		else if ("get_or_create_named_item".equals(command)) {
			long sourceNr = inputs.get("").getIntegralValue();
			String name = inputs.get("").getTextValueAsString();
			Item item = store.getOrCreateNamedItem(
					sourceNr, name, authorizedSession);
			result = new HashMap<String, Datum>();
			result.put("item", Datum.create(item));
		}
		else if ("query".equals(command)) {
			// Get the constraints
			JoinConstraint[] constraints = 
					getConstraintsFromInputs(inputs, authorizedSession);
			
			// Find the solutions, adding binary values to an output
			// binaries map
			Map<String, Datum> outputBinaries = new HashMap<String, Datum>();
			JSONArray solutions = query(
					constraints, outputBinaries, authorizedSession);
			
			// Create the result map with the solutions as its "json" entry
			// and the binaries as its other entries.
			result = outputBinaries;
			result.put("json", Datum.createText(solutions.toJSONString()));
		}
		else if ("update".equals(command)) {
			// Get the changes
			JSONArray changes = getChanges(inputs, authorizedSession);
			
			// Make the changes
			Map<String, Item> newItemsMap = update(
					changes, inputs, authorizedSession);
		
			// Create  the result map with the new items as its "json" entry.
			result = new HashMap<String, Datum>();
			result.put("json", Datum.createText(
					newItemsJo(newItemsMap, authorizedSession).toJSONString()));
		}
		return result;
	}

	/**
	 * Get the authorized store session from an input datum containing
	 * authorization in the form of a credential with its correct key.
	 * The authorized store session has the access level authorized by
	 * the credential. If the input datum is null then the authorized
	 * store session is a session that has the public access level.
	 * 
	 * @param authDatum an input datum containing authorization
	 * information.
	 * 
	 * @param requestingSession the requesting store session.
	 * 
	 * @return the authorized store session, or null if invalid
	 * authorization is supplied.
	 * 
	 * @throws Exception
	 */
	
	public StoreSession getAuthorizedStoreSession(
			Datum authDatum, StoreSession requestingSession)  {
				
		// Get the authorization and return a session with the lowest 
		// access level if no authorization is supplied
		if (authDatum == null) return requestingSession;
		
		// Get the credential and its secret from the authorization	
		try {
			JSONObject authJo = (JSONObject)new JSONParser().parse(
					authDatum.getTextValueAsString());
			Item credential = store.getItem(
					Id.parseId((String)authJo.get("credential")), requestingSession);
			byte[] secret = Base64.getDecoder().decode((String)authJo.get("key"));
			
			// Return the access level for the credential, or null if the
			// secret is not correct
			return store.createStoreSession(
					credential, secret, requestingSession);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Make the home source admin access level superior to the admin access 
	 * level of a new source. It is assumed that a source admin access level
	 * is its own read-or-write access level.
	 * 
	 * @param sourceNr the identification number of a new source.
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 * @throws TripleStoreFatalException
	 */
	
	void makeHomeSuperior(long sourceNr) throws
				TripleStoreNonFatalException, 
				SourceConnectorException, 
				NotInOperationException, 
				TripleStoreFatalException {

		AccessLevel homeSourceLevel = store.getAdminAccessLevel(homeSourceNr);
		Item homeSourceLevelItemItem = homeSourceLevel.getItem();
		StoreSession homeSourceSession = 
				sessionIssuer.getStoreSession(homeSourceLevel);
		AccessLevel newSourceLevel = store.getAdminAccessLevel(sourceNr);
		Item newSourceLevelItem = newSourceLevel.getItem();
		StoreSession newSourceSession = 
				sessionIssuer.getStoreSession(newSourceLevel);
		if (homeSourceLevelItemItem.lock(homeSourceSession)) {
			try {
				if (!store.setSuperiorPermission(
						homeSourceLevel, newSourceLevel, homeSourceSession))
					throw (new TripleStoreNonFatalException(
							"Cannot set superior permission"));
				if (newSourceLevelItem.lock(newSourceSession)) {
					try {
						if (!store.setInferiorPermission(
								newSourceLevel, homeSourceLevel, newSourceSession))
							throw (new TripleStoreNonFatalException(
									"Cannot set inferior permission"));
					} finally {
						newSourceLevelItem.unlock(newSourceSession);
					}
				}
				else throw (new TripleStoreNonFatalException(
						"Cannot lock new source level item"));
			} finally {
				homeSourceLevelItemItem.unlock(homeSourceSession);
			}
		}
		else throw (new TripleStoreNonFatalException(
				"Cannot lock home source level item"));
	}
	
	/**
	 * Get the constraints of a query from the inputs.
	 * 
	 * @param inputs a map whose "json" entry is a string representation of a
	 * JSON array whose members are JSON objects specifying the constraints, and
	 * whose other entries include the binary values referenced by these JSON 
	 * objects. 
	 * 
	 * @param authorizedSession the authorized store session
	 * 
	 * @return an array of join constraints.
	 * 
	 * @throws InputException
	 */
	
	JoinConstraint[] getConstraintsFromInputs(
			Map<String, Datum> inputs, StoreSession authorizedSession) 
					throws InputException {
		
		JSONArray constraintsJa;
		try {
			constraintsJa = (JSONArray)new JSONParser().parse(
					inputs.get("constraints").getTextValueAsString());
		} catch (Exception e) {
			throw (new InputException("Invalid constraints: " + e.getMessage()));
		}
		
		JoinConstraint[] constraints = null;
		try {		
			constraints = new JoinConstraint[constraintsJa.size()];
			int i = 0;
			for (Object o : constraintsJa) {
				JSONArray constraintJa = (JSONArray)o; 
				constraints[i++] = new JoinConstraint(
						getQuantumFromJson(
								(JSONObject)(constraintJa.get(0)),
								inputs, 
								authorizedSession),
						getQuantumFromJson(
								(JSONObject)(constraintJa.get(1)), 
								inputs, 
								authorizedSession),
						getQuantumFromJson(
								(JSONObject)(constraintJa.get(2)), 
								inputs, 
								authorizedSession));
			}
		} catch (Exception e) {
			InputException jx = new InputException(
					"Invalid constraints: " + e.getMessage());
			jx.initCause(e);
			throw(jx);
		}
		return constraints;
	}

	/**
	 * Handle a query.
	 * 
	 * @param constraints the request constraints. 
	 * 
	 * @param outputBinaries a map to which an entry is added for each
	 * binary value in the solutions
	 *
	 * @param authorizedSession the authorized store session
	 * 
	 * @return the data retrieved. 
	 * 
	 * @throws Exception
	 */
	
	@SuppressWarnings("unchecked")
	public JSONArray query(
			JoinConstraint[] constraints, Map<String, Datum> outputBinaries,
			StoreSession authorizedSession) 
					throws Exception {
		
		JSONArray solutions = new JSONArray();
		
		// Make a join from the constraints and create a solution
		// for each constrained triples it produces		
		for (ConstrainedTriples cts : new Join(constraints).make(
				store, authorizedSession)) {
			JSONObject solution = new JSONObject();
			
			// For each unknown assignment of the constrained triples,
			// add to the solution object an attribute whose value is
			// the typed value of the assignment. If it is a binary
			// value, add it to the output binaries map.
			for (Entry<String, Datum> e : cts.getAssignments().entrySet())
				solution.put(e.getKey(), getJsonFromDatum(e.getValue(), outputBinaries));
			
			solutions.add(solution);
		}
		
		return solutions;
	}
	
	/**
	 * Get the changes of an update
	 * 
	 * @param inputs a map whose "json" entry is a string representation of a
	 * JSON array whose members are JSON objects specifying the changes, and
	 * whose other entries include the binary values referenced by 
	 * these JSON objects. 
	 * 
	 * @param authorizedSession the authorized store session
	 * 
	 * @return a JSONArray specifying the changes.
	 * 
	 * @throws Exception
	 */
	
	public JSONArray getChanges(
			Map<String, Datum> inputs, StoreSession authorizedSession) 
					throws Exception {
		
		JSONArray changes;
		try {
			changes = (JSONArray)new JSONParser().parse(
					inputs.get("changes").getTextValueAsString());
		} catch (Exception e) {
			throw (new InputException("Invalid changes: " + e.getMessage()));
		}
		return changes;
	}
	
	/**
	 * Handle an update
	 * 
	 * @param changes a JSONArray specifying the changes.
	 * 
	 * @param inputs the input parameters of the request. It includes
	 * entries giving the binary values of the changes.
	 *
	 * @param authorizedSession the authorized store session
	 * 
	 * @return a map of the new items created by the changes, 
 	 * keyed by their handles.
	 * 
	 * @throws Exception
	 */
		
	public Map<String, Item> update(
			JSONArray changes, Map<String, Datum> inputs,
			StoreSession authorizedSession) 
					throws Exception {
		
		//Define the changes
		ChangeBundle bundle = new ChangeBundle(store);
		Set<String> handles = new HashSet<String>();
		for (Object o : changes) {
			JSONObject change = (JSONObject)o;
			String changeType = (String)change.get("change");
			if (changeType == null) throw (new InputException(
					"Missing change type"));
			switch(changeType) {
			
			case "create item":
				String handle = (String)change.get("handle");
				bundle.setItemToAdd(
						handle, 
						(long)change.get("source"),
						getLevelFromString(
								(String)(change.get("read")), 
								authorizedSession),
						getLevelFromString(
								(String)(change.get("write")),
								authorizedSession));
				handles.add(handle);
				break;
				
			case "update item":
				bundle.setItemToUpdate(
						getUnNamedItemFromString(
								(String)(change.get("item")),
								authorizedSession),
						getLevelFromString(
								(String)(change.get("read")), 
								authorizedSession),
						getLevelFromString(
								(String)(change.get("write")),
								authorizedSession));
				break;
				
			case "delete item":
				bundle.setItemToDelete(
						getUnNamedItemFromString(
								(String)(change.get("item")), 
								authorizedSession));
				break;
				
			case "put triple":
				bundle.setTripleToPut(
						getQuantumFromJson(
								(JSONObject)(change.get("subject")), 
								inputs, 
								authorizedSession), 
						getQuantumFromJson(
								(JSONObject)(change.get("verb")), 
								inputs, 
								authorizedSession), 
						getQuantumFromJson(
								(JSONObject)(change.get("object")), 
								inputs, 
								authorizedSession), 
						authorizedSession);
				break;
				
			case "set unique object":
				bundle.setUniqueTripleObject(
						getItemOrNamedItemFromString(
								(String)(change.get("subject")), 
								authorizedSession),
						getItemOrNamedItemFromString(
								(String)(change.get("verb")), 
								authorizedSession),
						getQuantumFromJson(
								(JSONObject)(change.get("object")), 
								inputs, 
								authorizedSession), 
						authorizedSession);
				break;
				
			case "remove triples":
				bundle.setTriplesToDelete(
						getItemOrNamedItemFromString(
								(String)(change.get("subject")), 
								authorizedSession),
						getItemOrNamedItemFromString(
								(String)(change.get("verb")), 
								authorizedSession),
						getDatumOrNullFromJson(
								(JSONObject)(change.get("object")), 
								inputs, 
								authorizedSession), 
						authorizedSession);
				break;
				
			default:
				throw (new InputException("Unrecognized change: " + change));
			}
		}
		
		// Make the changes
		try {
			bundle.makeChanges(authorizedSession);
		} catch (Exception e) {
			throw (new InputException(
					"Cannot make changes: " + e.getMessage()));
		}
		
		// Return the new items
		return bundle.getNewItems();
	}
		
	/**
	 * Get a JSON object representing the new items.
	 * 
	 * @param newItemsMap a map with an entry for each new item,
	 * whose key is the item's handle, and whose value is the item's
	 * identifier.
	 * 
	 * @param authorizedSession the authorized store session
	 * 
	 * @return a JSON object representing the new items.
	 * 
	 * @throws Exception
	 */
	
	@SuppressWarnings("unchecked")
	public JSONObject newItemsJo(Map<String, Item> newItemsMap,
			StoreSession authorizedSession) 
					throws Exception {
		// Return the new items that have been created
		JSONObject newItemsJo = new JSONObject();
		for (Entry<String, Item> e : newItemsMap.entrySet())
			newItemsJo.put(
					e.getKey(), 
					e.getValue().getId().toString());
		return newItemsJo;
	}
	
	/**
	 * Get the access level represented by a JSON object or, 
	 * if the JSON object is null, return the access level of the
	 * authorized store session.
	 *  
	 * @param givenJo a JSON object representing an ITEM given 
	 * whose item is an access level item, or null.
	 * 
	 * @param authorizedSession the authorized store session.
	 * 
	 * @return the access level represented by the JSON object,
	 * or the access level of the authorized store session.
	 * 
	 * @throws Exception
	 */
	
	private AccessLevel getLevelFromString(
			String levelSpec, StoreSession authorizedSession) 
			throws Exception {
		
		if (levelSpec == null) return authorizedSession.getAccessLevel();
		try {
			Item levelItem = getItemOrNamedItemFromString(
					levelSpec, authorizedSession);
			return store.getAccessLevel(levelItem, authorizedSession);
		} catch (Exception e) {
			InputException x = new InputException(
					"Invalid access level specification: " + levelSpec);
			x.initCause(e);
			throw(x);
		}
	}
	
	/**
	 * Get an item or named item from a string that specifies it.
	 * 
	 * @param spec a string that is either an item identifier or
	 * the qualified name of a named item.
	 * 
	 * @param authorizedSession the authorized store session.
	 * 
	 * @return the specified item
	 * 
	 * @throws InputException
	 */
	
	private Item getItemOrNamedItemFromString(
			String spec, StoreSession authorizedSession) 
					throws InputException {
		
		if (spec == null) return null;
		int colonix = spec.indexOf(':');
		if (colonix > 0) return getNamedItemFromString(
				spec, authorizedSession);
		else return getUnNamedItemFromString(
				spec, authorizedSession);
	}
	
	/**
	 * Get an item from a string that specifies it.
	 * 
	 * @param spec a string that is an item identifier.
	 * 
	 * @param authorizedSession the authorized store session.
	 * 
	 * @return the specified item
	 * 
	 * @throws InputException
	 */
	
	private Item getUnNamedItemFromString(
			String spec, StoreSession authorizedSession) 
					throws InputException {
		
		if (spec == null) return null;
		Item item = null;
		try {
			Id id = Id.parseId(spec);
			item = store.getItem(id, authorizedSession);
		} catch (Exception e) {
			InputException x = new InputException(
					"Invalid item specification: " + spec);
			x.initCause(e);
			throw(x);
		}
		if (item == null) throw (new InputException(
				"Item not found: " + spec));
		else return item;
	}
	
	/**
	 * Get a named item from a string that specifies it.
	 * 
	 * @param spec a string that is  the qualified name of a named item.
	 * 
	 * @param authorizedSession the authorized store session.
	 * 
	 * @return the specified item
	 * 
	 * @throws InputException
	 */
	
	private Item getNamedItemFromString(
			String spec, StoreSession authorizedSession) 
					throws InputException {
		
		if (spec == null) return null;
		int colonix = spec.indexOf(':');
		Item namedItem = null;
		try {
			long source = Long.parseLong(spec.substring(0, colonix));
			String name = spec.substring(colonix + 1);
			namedItem = store.getNamedItem(source, name, authorizedSession);
		} catch (Exception e) {
			InputException x = new InputException(
					"Invalid named item specification: " + spec);
			x.initCause(e);
			throw(x);
		}
		if (namedItem == null) throw (new InputException(
				"Named item not found: " + spec));
		else return namedItem;
	}
	
	/**
	 * Get the Quantum represented by a JSON object, which must not be null.
	 * 
	 * @param jo a JSON object that represents a given or an unknown.
	 * 
	 * @param binaries a map of names to Binary Datums.
	 * 
	 * @param authorizedSession the authorized store session.
	 * 
	 * @return the Quantum represented by the given or unknown.
	 * 
	 * @throws Exception
	 */
	
	private  Quantum getQuantumFromJson(
			JSONObject jo, Map<String, Datum> binaries, 
				StoreSession authorizedSession) 
					throws Exception {

		Object o = jo.get("unknown");
		if (o == null) return new Given(
				getDatumFromJson(jo, binaries, authorizedSession));
		else return new Unknown((String)o);
	}
	
	/**
	 * Get the Datum represented by a JSON object, or null
	 * 
	 * @param givenJo a JSON object that represents a given, or null.
	 * 
	 * @param binaries a map of names to Binary Datums.
	 * 
	 * @param authorizedSession the authorized store session.
	 * 
	 * @return the Datum represented by the JSON object, or null.
	 * 
	 * @throws Exception
	 */
	
	private  Datum getDatumOrNullFromJson(
			JSONObject jo, Map<String, Datum> binaries, 
			StoreSession authorizedSession) 
					throws Exception {
		
		if (jo == null) return null;
		else return getDatumFromJson(jo, binaries, authorizedSession);
	}
	
	/**
	 * Get the Datum represented by a JSON object, which must
	 * not be null.
	 * 
	 * @param givenJo a JSON object that represents a given.
	 * 
	 * @param binaries a map of names to Binary Datums.
	 * 
	 * @param authorizedSession the authorized store session.
	 * 
	 * @return the Datum represented by the JSON object.
	 * 
	 * @throws Exception
	 */
	
	private  Datum getDatumFromJson(
			JSONObject givenJo, Map<String, Datum> binaries, 
			StoreSession authorizedSession) 
					throws Exception {
		
		String type = (String)givenJo.get("type");
		String value = (String)givenJo.get("value");
		switch(type) {
		case "ITEM":
			return Datum.create(getItemOrNamedItemFromString(
					value, authorizedSession));
		case "BOOLEAN":
			return Datum.create(Boolean.parseBoolean(value));
		case "INTEGRAL":
			return Datum.create(Long.parseLong(value));
		case "REAL":
			return Datum.create(Double.parseDouble(value));
		case "TEXT":
			return Datum.createText(value);
		case "BINARY":
			return binaries.get(value);
		default:
			throw (new InputException("Invalid type: " + type));	
		}
	}
	
	/**
	 * Get a JSON object representing a Datum
	 * 
	 * @param datum a datum.
	 * 
	 * @param binaries a String-Datum map to which an
	 * entry will be added if the datum is a Binary datum.
	 * 
	 * @return a JSON object representing the datum.
	 * 
	 * @throws Exception
	 */
	
	@SuppressWarnings("unchecked")
	public static JSONObject getJsonFromDatum(
			Datum datum, Map<String, Datum> binaries) 
					throws Exception {
		
		JSONObject jo = new JSONObject();
		Datum.Type type = datum.getType();
		jo.put("type", type.toString());
		switch(type) {
		case ITEM:
			jo.put("value", datum.getItemValue().getId().toString());
			break;
		case BOOLEAN:
			jo.put("value", Boolean.toString(datum.getBooleanValue()));
			break;
		case INTEGRAL:
			jo.put("value", Long.toString(datum.getIntegralValue()));
			break;
		case REAL:
			jo.put("value", Double.toString(datum.getRealValue()));
			break;
		case TEXT:
			jo.put("value", datum.getTextValueAsString());
			break;
		case BINARY:
			String filename = "b" + binaries.size();
			binaries.put(filename, datum);
			jo.put("value", filename);
			break;
		}
		return jo;
	}

	/**
	 * Build an error response.
	 * 
	 * @param response an HTTP request response
	 * 
	 * @param code the HTTP response code to be returned.
	 * 
	 * @param message a message describing the error that was detected.
	 * 
	 * @throws IOException
	 */
	
	@SuppressWarnings("unchecked")
	static void returnError(
			HttpServletResponse response, int code, String message) 
					throws IOException {

		response.setStatus(code);
		JSONObject jo = new JSONObject();
		jo.put("error", message);
		PrintWriter writer = response.getWriter();
		writer.print(jo.toJSONString());
		writer.close();
	}
}
