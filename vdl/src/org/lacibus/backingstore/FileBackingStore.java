/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.backingstore;

/*
 ***********************************************************************
 *
 *  HOW THIS FILE IS ORGANIZED
 *  
 ***********************************************************************
 * 
 * This file contains:
 *    o Imports and the class definition
 *    o Definitions of the main constants and variables
 *    o Store creation, initialization, and closedown
 *    o Transactions
 *    o Initial return of data
 *    o Association of records with transactions
 *    o Items
 *    o Triples
 *    o Read segments from or write segments to the main channel
 *    o Read or write objects from or to files in the big objects directory
 *    o Maintenance
 *    o Segments
 *    o Diagnostics
 * 
 */


/*
 ***********************************************************************
 *
 *  IMPORTS AND CLASS DEFINITION
 *  
 ***********************************************************************
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Datum.Type;
import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.Logger;
import org.lacibus.triplestore.Transaction;
import org.lacibus.triplestore.TripleStoreNonFatalException;
import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplesource.TransactionsStatus;

/**
 * <p>A backing store implemented by files on disc that maintains 
 * transactional record integrity on system re-start.
 * </p>
 * <p>When a record is stored on disc, a reference is returned 
 * that enables the consuming application to retrieve the record. 
 * </p>
 * <p>Records on disc are not over-written. To change a record, 
 * the old copy is marked as deleted, a new copy is written, and 
 * a new reference is returned. 
 * </p>
 * <p>A transaction identity is stored with each record copy, and 
 * details are kept of which transactions have been completed.
 * </p>
 * <p>When the system is re-started, the records and their references 
 * are read by the consuming application, to enable subsequent
 * record retrieval. Only records written by transactions that have 
 * completed are supplied.
 * </p>
 * <p>Note that if two threads use the same transaction then it may not
 * be possible for the consuming application to determine whether a 
 * write made by one thread occurred before or after the other thread 
 * closed the transaction. 
 * </p>
 * <p>The store performs regular maintenance tasks to remove notes of 
 * old transactions and re-allocate disc areas whose records were
 * deleted long enough ago for re-use to be safe. This is done 
 * concurrently with store read and write operations by a separate 
 * instance of the BackingStoreMaintainer class.
 * </p>
 */

public class FileBackingStore implements BackingStore, Serializable {

	private static final long serialVersionUID = -1723821288847226813L;

/*
 ***********************************************************************
 *
 *  DEFINITIONS OF MAIN CONSTANTS AND VARIABLES
 *  
 ***********************************************************************
 */
	/**
	 * The source connector for which this is the backing store
	 */
	
	private BackingStoreSourceConnector connector;

	/**
	 * The directory that contains the files that comprise
	 * the store and hold other information.
	 */
	
	private File directory;
	
	/**
	 * <p>All small records, and summaries of large records,
	 * are stored in a single file - the main file, which
	 * is accessed via the main channel.
	 * </p>
	 * <p>The main channel contains a number of segments,
	 * each of which contains a record or the summary of
	 * a record. Each segment is identified by its position
	 * in the main file.
	 * </p>
	 * <p>Each record holds an item or a triple. An item record
	 * is always held in a segment. If the object of a triple
	 * is small then the triple is stored in a segment. 
	 * Otherwise, a summary of the triple is stored in a segment
	 * and the triple's object is held in a separate file.
	 * </p>
	 * <p>The position in the main file of the segment containing
	 * the record or its segment is used as a reference to
	 * the record in store.
	 * </p>
	 * <p>A segment of the main file contains a complete
	 * small record or a summary of a large record.
	 * It consists of a header followed by data.
	 * </p>
	 * <p>The header consists of:</p>
	 * <ul>
	 * <li>The segment length - a 4-byte integer
	 *    The next segment of the file starts at the start
	 *    of this segment plus this segment length</li>
	 * <li>The record type - a 2-byte short integer
	 *    This gives the type of small record stored in the
	 *    segment, or the type of large record of which a 
	 *    summary is stored. Possible values are defined as
	 *    RECORD_TYPE_* constants</li>
	 * <li>The 'record valid' indicator - a 1-byte integer</li>
	 * <li>The 'transaction present' indicator - a 1-byte 
	 *    boolean quantity. If it is non-zero then there is 
	 *    a transaction sequence number stored in the
	 *    transaction sequence number field</li>
	 * <li>The transaction sequence number - an 8-byte long
	 *    integer</li>
	 * <p>An item record is stored in a segment. 
	 * The data part of such a segment consists of:</p>
	 * <ul>
	 * <li>The item number - an 8-byte long integer</li>
	 * <li>The only-read access level source number -
	 *    an 8-byte long integer</li>
	 * <li>The only-read access level item number -
	 *    an 8-byte long integer</li>
	 * <li>The read-or-write access level source number -
	 *    an 8-byte long integer</li>
	 * <li>The read-or-write access level item number -
	 *    an 8-byte long integer</li>
	 * </ul>
	 * <p>A simple triple record, item triple record, small
	 * text triple record, small binary triple record, source
	 * text triple record, or source binary triple record
     * is stored in a segment. For a big text triple record
     * or big binary triple record, the summary is stored 
     * in a segment and the data is stored in a separate 
     * file.
	 * </p>
	 * <p>For all such segments, the data part starts with
	 * the initial data part:</p>
	 * <ul>
	 * <li>The triple number - an 8-byte long integer</li>
	 * <li>The triple's subject item number - an 8-byte
	 *    long integer. Note that the subject's source
	 *    is always the source of the connector for which 
	 *    this is the backing store.</li>
	 * <li>The triple's verb source number - an 8-byte
	 *    long integer</li>
	 * <li>The triple's verb item number - an 8-byte
	 *    long integer</li>
	 * <li>The triple's object type - a 4-byte integer</li>
	 * </ul>
	 * <p>For an item triple record, the segment consists of the
	 * initial data part followed by:</p>
	 * <ul>
	 * <li>The triple's object source number - an 8-byte
	 *     long integer</li>
	 * <li>The triple's object item number - an 8-byte
	 *     long integer</li>
	 *     </ul>
	 * <p>For other kinds of triple record, the initial data 
	 * part is followed by:</p>
	 * <ul>
	 * <li>The triple's object's summary value - an 8-byte
	 *    long integer. For a text or binary object, the
	 *    object's length - a 4-byte integer giving
	 *    the number of characters in a text object or the
	 *    number of bytes in a binary object - is contained
	 *    in the top 32 bits of the summary value.<li>
	 * </ul>
	 * <p>For a small text triple record, or small binary triple
	 * record, the object summary is followed by the triple's object.
	 * </p>
	 */
		
	private FileChannel mainChannel = null;
	
	/**
	 * The accessor that synchronizes write access to the
	 * main channel
	 */
	
	private ChannelAccessor mainChannelAccessor = null;
	
	/**
	 * The directory that contains the files that hold the
	 * triple objects that are too large to fit in segments.
	 * The record references, expressed as text strings, form
	 * the names of the files.
	 */
	
	private File bigFileDirectory = null;
	
	/**
	 * @return the directory that contains the files that hold the
	 * triple objects that are too large to fit in segments.
	 */
	
	public File getBigFileDirectory() {
		
		return bigFileDirectory;
	}
	
	/*
	 * Segment header and data offsets
	 */
	
	/**
	 * The segment length offset in a segment
	 */
	
	final int SEGMENT_LENGTH_OFFSET 		=  0;
	
	/**
	 * The record type offset in a segment
	 */
	
	final int SEGMENT_RECORD_TYPE_OFFSET 	=  4;
	
	/**
	 * The record validity indicator offset in a segment
	 */
	
	final int SEGMENT_RECORD_VALID_OFFSET 	=  6;
	
	/**
	 * The transaction present indicator offset in a segment
	 */
	
	final int SEGMENT_TRANS_PRESENT_OFFSET 	=  7;
	
	/**
	 * The transaction sequence number offset in a segment
	 */
	
	final int SEGMENT_TRANS_SEQ_OFFSET 		=  8;
	
	/**
	 * The segment data offset in a segment
	 */
	
	final int SEGMENT_DATA_OFFSET 			= 16;
	
	/*
	 * Record validity indicators.
	 */
	
	/**
	 * The record is not valid
	 */
	
	public final static int VALIDITY_ERROR					= 0;
	
	/**
	 * The record is valid but there is no information
	 * on whether it should be re-instated
	 */
	
	public final static int VALIDITY_NO_INFO				= 1;
	
	/**
	 * The record is valid but should not be reinstated if
	 * the last transaction to touch it does not complete 
	 * (used when creating a record)
	 */
			
	public final static int VALIDITY_VALID_NOREINSTATE		= 2;
	
	/**
	 * The record is valid and should be reinstated if
	 * the last transaction to touch it does not complete 
	 * (used when reinstating a record)
	 */
			
	public final static int VALIDITY_VALID_REINSTATE		= 3;
	
	/**
	 * The record is invalid but should be reinstated if
	 * the last transaction to touch it does not complete 
	 * (used when marking as invalid a record created by
	 * a different transaction)
	 */
			
	public final static int VALIDITY_INVALID_REINSTATE		= 4;
	
	/**
	 * The record is invalid and should not be reinstated if
	 * the last transaction to touch it does not complete 
	 * (used when marking as invalid a record created by
	 * the same transaction and when loading a deleted record)
	 */
			
	public final static int VALIDITY_INVALID_NOREINSTATE	= 5;
	
	/**
	 * Print the record validity indicator as a descriptive string
	 * 
	 * @param validity a validity indicator
	 * 
	 * @return a string describing the indicated validity
	 */
	
	public String printRecordValidity(int validity) {
		
		switch(validity) {
		case VALIDITY_ERROR: return "erroneous";
		case VALIDITY_NO_INFO: return "created";
		case VALIDITY_VALID_NOREINSTATE: return "created but not to be reinstated";
		case VALIDITY_VALID_REINSTATE: return "created and to be reinstated";
		case VALIDITY_INVALID_REINSTATE: return "deleted but to be reinstated";
		case VALIDITY_INVALID_NOREINSTATE: return "deleted and not to be reinstated";
		}
		return "Incorrect validity indicator";
	}
	
	/*
	 * Record types
	 */
	
	/**
	 * Item record type
	 */
	
	final static short RECORD_TYPE_ITEM 				= 1;
	
	/**
	 * Simple triple record type (a triple whose object
	 * is a boolean, an integer, or a real number)
	 */
	
	final static short RECORD_TYPE_SIMPLE_TRIPLE 		= 2;
	
	/**
	 * Item triple record type
	 */
	
	final static short RECORD_TYPE_ITEM_TRIPLE 			= 3;
	
	/**
	 * Small text triple record type
	 */
	
	final static short RECORD_TYPE_SMALL_TEXT_TRIPLE	= 4;
	
	/**
	 * Large text triple record type
	 */
	
	final static short RECORD_TYPE_BIG_TEXT_TRIPLE 		= 5;
	
	/**
	 * Small binary triple record type
	 */
	
	final static short RECORD_TYPE_SMALL_BINARY_TRIPLE 	= 6;
	
	/**
	 * Large binary triple record type
	 */
	
	final static short RECORD_TYPE_BIG_BINARY_TRIPLE 	= 7;
	
	/**
	 * Source text triple record type (a text triple whose
	 * object has not been retrieved from its source)
	 */
	
	final static short RECORD_TYPE_SOURCE_TEXT_TRIPLE 	= 8;
	
	/**
	 * Source binary triple record type (a binary triple whose
	 * object has not been retrieved from its source)
	 */
	
	final static short RECORD_TYPE_SOURCE_BINARY_TRIPLE = 9;
	
	/**
	 * Print the record type as a descriptive string
	 * 
	 * @param recordType a record type
	 * 
	 * @return a description of the type
	 */
	
	String printRecordType(int recordType) {
		
		switch (recordType) {
		case RECORD_TYPE_ITEM: return "item";
		case RECORD_TYPE_SIMPLE_TRIPLE: return "simple triple";
		case RECORD_TYPE_ITEM_TRIPLE: return "item triple";
		case RECORD_TYPE_SMALL_TEXT_TRIPLE: return "small text triple";
		case RECORD_TYPE_BIG_TEXT_TRIPLE: return "big text triple";
		case RECORD_TYPE_SMALL_BINARY_TRIPLE: return "small binary triple";
		case RECORD_TYPE_BIG_BINARY_TRIPLE: return "big binary triple";
		case RECORD_TYPE_SOURCE_TEXT_TRIPLE: return "source text triple";
		case RECORD_TYPE_SOURCE_BINARY_TRIPLE: return "source binary triple";
		}
		return "undefined";
	}

	/**
	 * Determine whether a record type is the type of a triple record
	 * 
	 * @param type a record type
	 * 
	 * @return whether the given type is the type of a triple record
	 */
	
	private boolean isTripleRecordType (short type) {
		return (type == RECORD_TYPE_SIMPLE_TRIPLE) ||
			   (type == RECORD_TYPE_ITEM_TRIPLE) ||
			   (type == RECORD_TYPE_BIG_BINARY_TRIPLE) ||
			   (type == RECORD_TYPE_BIG_TEXT_TRIPLE) ||
			   (type == RECORD_TYPE_SMALL_BINARY_TRIPLE) ||
			   (type == RECORD_TYPE_SMALL_TEXT_TRIPLE) ||
			   (type == RECORD_TYPE_SOURCE_BINARY_TRIPLE) ||
			   (type == RECORD_TYPE_SOURCE_TEXT_TRIPLE);		
	}

	/**
	 * Determine whether a record type is the type of a triple record
	 * with a big object. (A big object is stored in a file.)
	 * 
	 * @param type a record type
	 * 
	 * @return whether the given type is the type of a triple record
	 * with a big object
	 */
	
	public static boolean isBigTripleRecordType (short type) {
		
		return	(type == RECORD_TYPE_BIG_BINARY_TRIPLE) ||
				(type == RECORD_TYPE_BIG_TEXT_TRIPLE);

	}
	
	/**
	 * The offset within the data part of a segment containing an
	 * item record of the numeric identifier of the item in its 
	 * source 
	 */
	
	final int ITEM_NR_OFFSET 				=   0;
	
	/**
	 * The offset within the data part of a segment containing an
	 * item record of the numeric identifier of the source of the
	 * only-read access level of the item
	 */
	
	final int ITEM_READ_SOURCE_NR_OFFSET	=   8;
	
	/**
	 * The offset within the data part of a segment containing an
	 * item record of the numeric identifier in its source of the
	 * only-read access level of the item
	 */
	
	final int ITEM_READ_ITEM_NR_OFFSET		=  16;
	
	/**
	 * The offset within the data part of a segment containing an
	 * item record of the numeric identifier of the source of the
	 * read-or-write access level of the item
	 */
	
	final int ITEM_WRITE_SOURCE_NR_OFFSET	=  24;
	
	/**
	 * The offset within the data part of a segment containing an
	 * item record of the numeric identifier in its source of the
	 * read-or-write access level of the item
	 */
	
	final int ITEM_WRITE_ITEM_NR_OFFSET		=  32;
	
	/**
	 * The size of the data part of a segment containing an item 
	 * record
	 */
	
	final int ITEM_DATA_SIZE				=  40;
		
	/**
	 * The offset within the data part of a segment containing a
	 * triple record of the numeric identifier of the triple in its 
	 * source 
	 */
	
	final int TRIPLE_NR_OFFSET 					=   0;
	
	/**
	 * The offset within the data part of a segment containing a
	 * triple record of the numeric identifier of the subject of
	 * the triple in its source 
	 */

	final int TRIPLE_SUBJECT_ITEM_NR_OFFSET 	=   8;
	
	/**
	 * The offset within the data part of a segment containing a
	 * triple record of the numeric identifier of the source of
	 * the verb of the triple 
	 */

	final int TRIPLE_VERB_SOURCE_NR_OFFSET 		=  16;
	
	/**
	 * The offset within the data part of a segment containing a
	 * triple record of the numeric identifier of the verb of
	 * the triple in its source 
	 */

	final int TRIPLE_VERB_ITEM_NR_OFFSET 		=  24;
	
	/**
	 * The offset within the data part of a segment containing a
	 * triple record of the type of the object of the triple 
	 */

	final int TRIPLE_OBJECT_TYPE_OFFSET 		=  32;
		
	/**
	 * The offset within the data part of a segment containing an
	 * item triple record of the numeric identifier of the source
	 * of the object of the triple 
	 */
	
	final int TRIPLE_OBJECT_SOURCE_NR_OFFSET=  36;
	
	/**
	 * The offset within the data part of a segment containing an
	 * item triple record of the numeric identifier of the object
	 * of the triple in its source
	 */

	final int TRIPLE_OBJECT_ITEM_NR_OFFSET 	=  44;
	
	/**
	 * The size of the data part of a segment containing an item 
	 * triple record
	 */
	
	final int ITEM_TRIPLE_DATA_SIZE 		=  52;
	
	/**
	 * The offset within the data part of a segment containing a
	 * non-item triple record of the summary value of the object
	 * of the triple
	 */
	
	final int TRIPLE_OBJECT_SUMMARY_OFFSET	=  36;
	
	/**
	 * The size of the data part of a segment containing a source text 
	 * or binary triple record, a simple triple record, or a large text
	 * or binary triple record
	 */
	
	final int SSB_TRIPLE_DATA_SIZE			=  44; // Source, simple, or big
		
	/**
	 * The offset of the object of the triple within the data part 
	 * of a segment containing a small text triple record or small 
	 * binary triple record 
	 */
		
	final int TRIPLE_OBJECT_DATA_OFFSET 	=  44;
	
	/**
	 * A list of possible segment types.
	 * 
	 * Segments can be of different lengths, but there are
	 * only a finite number of such lengths, each of which
	 * defines a segment type.
	 * 
	 * The segment types are stored in the list
	 * in order of increasing segment length.
	 */

	private List<SegmentType> segmentTypes =
			new ArrayList<SegmentType>();
	
	/**
	 * <p>The position of the first segment in the main file.
	 * </p>
	 * <p>The main file starts with some initial data,
	 * which is followed by the segments.
	 * </p>
	 */
	
	private int firstSegment = 0;
	
	/**
	 * The maximum number of bytes that can be stored as data
	 * in a segment
	 */
	
	private int maxSegmentDataLength = 0;
	
	/**
	 * The maximum number of bytes in a segment
	 */
	
	private int maxSegmentSize = 0;	
	
	/**
	 * Charset used for converting text values to bytes, and 
	 * vice-versa
	 */
	
	final Charset utf8 = Charset.forName("UTF-8");
	
	/**
	 * The maintainer for this store
	 */
	
	private BackingStoreMaintainer maintainer;
	
	/**
	 * An exception received by the maintainer. If this happens
	 * then the maintainer closes the store and the exception
	 * is stored and can subsequently be retrieved via the
	 * getTerminator() method.
	 */
	
	private Exception terminator = null;

	/*
	 *  Datum type indicators
	 */

	/**
	 * Value indicating no datum type
	 */
	
	static final int NONEINDICATOR = 0;

	/**
	 * Value indicating the item datum type
	 */
	
	static final int ITEMINDICATOR = 1;

	/**
	 * Value indicating the boolean datum type
	 */
	
	static final int BOOLEANINDICATOR = 2;

	/**
	 * Value indicating the integral datum type
	 */
	
	static final int INTEGRALINDICATOR = 3;

	/**
	 * Value indicating the real datum type
	 */
	
	static final int REALINDICATOR = 4;

	/**
	 * Value indicating the text datum type
	 */
	
	static final int TEXTINDICATOR = 5;

	/**
	 * Value indicating the binary datum type
	 */
	
	static final int BINARYINDICATOR = 6;
	
	/**
	 * Get the indicator of a datum type
	 * 
	 * @param type a datum type
	 * 
	 * @return the indicator for the given datum type
	 */
	
	static int indicator(Datum.Type type) {
		switch(type) {
		case ITEM : return ITEMINDICATOR;
		case BOOLEAN : return BOOLEANINDICATOR;
		case INTEGRAL : return INTEGRALINDICATOR;
		case REAL : return REALINDICATOR;
		case TEXT : return TEXTINDICATOR;
		case BINARY : return BINARYINDICATOR;		
		}
		return NONEINDICATOR;
	}
	
	/**
	 * get a datum type from its indicator value
	 * 
	 * @param indicator a value indicating a datum type
	 * 
	 * @return the datum type indicated by the given value
	 */
	
	static Datum.Type datumType (int indicator) {
		switch(indicator) {
		case ITEMINDICATOR : return Datum.Type.ITEM;
		case BOOLEANINDICATOR : return Datum.Type.BOOLEAN;
		case INTEGRALINDICATOR : return Datum.Type.INTEGRAL;
		case REALINDICATOR : return Datum.Type.REAL;
		case TEXTINDICATOR : return Datum.Type.TEXT;
		case BINARYINDICATOR : return Datum.Type.BINARY;		
		}
		return null;
	}
	
	/**
	 * The logger to which status and error messages can be logged
	 */
	
	Logger logger;
	
	/*
	 ***********************************************************************
	 *
	 *  CREATION, INITIALIZATION, AND CLOSEDOWN
	 *  
	 ***********************************************************************
	 */
	
	/**
	 * Whether the store is closed
	 */
	
	private boolean closed = true;
	
	/**
	 * Construct a new file backingStore
	 * 
	 * @param directory the directory containing the
	 * files where the data is stored
	 */
	
	public FileBackingStore(
			BackingStoreSourceConnector connector, 
			File directory, 
			Logger logger) {
		
		this.connector = connector;
		this.directory = directory;
		this.logger = logger;
	}
	
	/**
	 * <p>Initialise the store
	 * </p>
	 * <p>This method must be invoked before any other after the
	 * backingStore has been created
	 * </p>
	 * @throws BackingStoreException
	 */
	
	@SuppressWarnings("resource")
	public void initialise(boolean clear) 
			throws BackingStoreException {
		
		// Create the maintainer for the backing store
		maintainer = new BackingStoreMaintainer(this, 2000);

		// Set up the files in which the backing store stores data
		if (!directory.exists()) {
			if (!directory.mkdir())
				throw new FileStoreException(
						"Cannot create file store directory: " + directory.getAbsolutePath());
		}
		File mainFile = new File(directory, "main");
		if (clear) mainFile.delete();
		try {
			mainChannel = 
				(new RandomAccessFile(
						mainFile, "rw")).getChannel();
		} catch (FileNotFoundException e) {
			throw new FileStoreException(
					"Cannot find main channel file");
		}
		try {
			if (mainChannel.size() >= Integer.MAX_VALUE) 
				throw new FileStoreException(
						"Main channel size limit exceeded");
		} catch (IOException e) {
			throw new FileStoreException(
					"Cannot get main channel size");
		}
		mainChannelAccessor = 
				new ChannelAccessor(mainChannel);

		bigFileDirectory =
				new File(directory, "large");
		if (clear) {
			if (bigFileDirectory.exists() &&
				!clear(bigFileDirectory)) 
					throw new FileStoreException(
							"Cannot clear big file directory");
		}
		if (!bigFileDirectory.exists() &&
				!bigFileDirectory.mkdir()) 
				throw new FileStoreException(
						"Cannot create big file directory");
		
		addSegmentType(SEGMENT_DATA_OFFSET + ITEM_DATA_SIZE, 1024);
									// For item records
		
		addSegmentType(SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE, 1024);
									// For source, simple, big text or big binary
									// triple records
		addSegmentType(SEGMENT_DATA_OFFSET + ITEM_TRIPLE_DATA_SIZE, 1024);
									// For item triple records
		addSegmentType(SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE + 32, 256);
									// For small string or binary triple records
									// holding objects up to 32 bytes long
		addSegmentType(SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE + 64, 256);
									// For small text or binary triple records
									// holding objects up to 64 bytes long
		addSegmentType(SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE + 128, 256);
									// For small text or binary triple records
									// holding objects up to 128 bytes long
		addSegmentType(SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE + 256, 256);
									// For small text or binary triple records
									// holding objects up to 256 bytes long
		addSegmentType(SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE + 512, 256);
									// For small text or binary triple records
									// holding objects up to 512 bytes long
		addSegmentType(SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE + 1024, 256);
									// For small text or binary triple records
									// holding objects up to 1024 bytes long
		addSegmentType(SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE + 2048, 256);
									// For small text or binary triple records
									// holding objects up to 2048 bytes long	
		
		// Create the transaction segment associator
		associator = new Associator();
		
		closed = false;

		// Start the maintainer, but do not yet allow maintenance to proceed.
		// Maintenance will proceed once the initial load has taken place.
		maintainer.allowMaintenance(false);
		maintainer.start();
	}
	
	/**
	 * Delete a file, recursively deleting its contents
	 * if it is a directory
	 * 
	 * @param f a file or directory
	 * 
	 * @return false if the file or a file contained in it
	 * if it is a directory was not deleted, true otherwise
	 */
	
	
	private boolean clear(File f) {
		
		if (f == null) return true;
		boolean result = true;
		File[] files = f.listFiles();
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (!clear(files[i])) result = false;
			}
		}
		if (!f.delete()) result = false;
		return result;
	}
	
	public void close() {
		synchronized (this) {
			closed = true;
		}
		if (maintainer != null) maintainer.interrupt();
		terminate();
	}
	
	/**
	 * <p>Abort operation of the backing store.
	 * </p>
	 * <p>If the store is not already closed then its terminator
	 * is set to the exception prompting the abort
	 * </p>
	 * @param e an exception prompting the abort
	 * 
	 * @return a new BackingStoreClosedException if the store is already 
	 * closed, otherwise, return a FileStoreException with the cause set 
	 * to the exception prompting the abort
	 */
	
	private BackingStoreException abortException(Exception e) {
		
		synchronized (this) {
			if (closed) return new BackingStoreClosedException();
			closed = true;
		}
		maintainer.interrupt();
		terminate();	
		terminator = e;
		BackingStoreException ex = 
				new FileStoreException(
						"File backing store operation aborted");
		ex.initCause(e);
		return ex;
	}
	
	/**
	 * <p>Abort operation of the backing store due to an
	 * exception occurring in the maintenance cycle.
	 * </p>
	 * <p>If the store is not already closed then its terminator
	 * is set to the exception prompting the abort
	 * </p>
	 * @param e an exception prompting the abort
	 */
	
	private void maintenanceAbort(Exception e) {
		
		synchronized (this) {
			if (closed) return;
			closed = true;
		}
		terminate();	
		terminator = e;
	}
	
	/**
	 * <p>Terminate operation of the backing store
	 * </p>
	 * <p>Ignore any exceptions and close everything that
	 * can be closed.
	 * </p>
	 */
	
	private void terminate() {
		
		if (mainChannelAccessor != null) {
			try {
				mainChannelAccessor.close();
				mainChannel.close();
			} catch (IOException e) {}
		}
	}
	
	Exception getTerminator() {
		
		return terminator;
	}

	@Override
	public Exception getBackingStoreTerminator() {
		return terminator;
	}

	/*
	 ***********************************************************************
	 *
	 *  TRANSACTIONS
	 *  
	 ***********************************************************************
	 */

	/**
	 * Note that a transaction is started
	 * 
	 * @param t a transaction
	 * @throws BackingStoreException 
	 */
	
	@Override
	public void transactionStarted(Transaction t) {
		
		associator.transactionStarted(t);
	}
	
	/**
	 * <p>End a transaction.
	 * </p>
	 * <p>All data written by the transaction is stored on disc,
	 * and the transaction is noted as completed.
	 * </p>
	 * <p>No check is made that the transaction is not already
	 * complete. The results of invoking this method twice
	 * for the same transaction are indeterminate.
	 * </p>
	 * @throws BackingStoreException 
	 */
	
	@Override
	public void transactionComplete(Transaction t) 
			throws BackingStoreException {
		
		if (closed) throw(new BackingStoreClosedException());
		// Ensure that all data is written to disc
		try {
			mainChannelAccessor.flush();
		} catch (IOException e) {
			throw(abortException(e));
		}
		
		// End the associations between the transaction and the
		// segments that it updated
		associator.transactionComplete(t);
	}

	/*
	 ***********************************************************************
	 *
	 *  INITIAL RETURN OF DATA
	 *  
	 ***********************************************************************
	 *
	 * At initialization time, a connector using the backing store
	 * as a source reads in all its items and triples.
	 * 
	 * Maintenance of the backing store is not performed until this
	 * method has completed.
	 * 
	 */
	
	/**
	 * The largest sequence number of any transaction that was
	 * encountered during the load operation. If any transaction
	 * with a lower sequence number was not completed, its effects
	 * will be reversed during the load. It and all transactions
	 * with lower sequence numbers can therefore be regarded as
	 * completed once the load has finished.
	 */
	
	private long highestLoadedTransactionNr = Long.MIN_VALUE;
	
	/**
	 * The highest segment start encountered during the load operation.
	 */
	private int endSegment = 0;
	
	/**
	 * <p>Load the stored items and triples
	 * </p>
	 * <p>NOTE that the items are loaded first, because they are
	 * referenced by the triples.
	 * </p>
	 * 
	 * @return the highest transaction sequence number encountered
	 * during the load operation
	 * 
	 * @throws BackingStoreException 
	 * @throws BackingStoreClientException 
	 */

	@Override
	public void load(
			BackingStoreClient client,
			TransactionsStatus status) 
					throws 	BackingStoreException {
	
		highestLoadedTransactionNr = Long.MIN_VALUE;
		endSegment = 0;
		try {
			// Load the segments
			loadItemSegments(client, status);
			loadTripleSegments(client, status);
			
			// Set the end of the segments 
			mainChannelAccessor.setEndOfChannelSegments(endSegment);			
		} catch (InterruptedException e) {
			throw(abortException(e));
		} catch (IOException e) {
			throw(abortException(e));
		} catch (FileTransactionManagerException e) {
			throw(abortException(e));
		} catch (FileStoreException e) {
			throw(abortException(e));
		} catch (BackingStoreException e) {
			throw(abortException(e));
		} catch (BackingStoreClientException e) {
			throw(abortException(e));
		}

		// Ensure that all data is written to disc
		try {
			mainChannelAccessor.flush();
		} catch (IOException e) {
			throw(abortException(e));
		}

		// Allow maintenance to proceed.
		maintainer.allowMaintenance(true);
	}
	
	/**
	 * @return the highest transaction sequence number encountered
	 */
	
	public long getHighestLoadedTransactionNr() {
				
		return highestLoadedTransactionNr;
	}

	/**
	 * Load into a client the items in the store, and set
	 * highestItemNr to be the highest item number of any 
	 * item, current or deleted, in a segment. 
	 * 
	 * @param client the client into which the items
	 * are to be loaded
	 * 
	 * @param status a transactions status indicating which
	 * transactions are complete
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws FileTransactionManagerException
	 * @throws FileStoreException
	 * @throws BackingStoreException
	 * @throws BackingStoreClientException 
	 */
	
	private void loadItemSegments(
			BackingStoreClient client,
			TransactionsStatus status) 
					throws 	InterruptedException, 
					IOException, 
					FileTransactionManagerException,
					FileStoreException,
					BackingStoreException,
					BackingStoreClientException {
				
		SegmentEnumeration segmentEnumeration = new SegmentEnumeration();
		SegmentRecord segmentRecord;
		while ((segmentRecord = segmentEnumeration.nextSegment()) != null) {
			if (segmentRecord instanceof  ItemSegmentRecord) {
				loadSegment(client, segmentRecord, status);
				endSegment = segmentEnumeration.nextSegmentStart();			}
		}
	}
	

	/**
	 * Load into a client the triples in the store, and set
	 * highestTripleNr to be the highest triple number of any 
	 * triple, current or deleted, in a segment. 
	 * 
	 * @param client the client into which the items or triples
	 * are to be loaded
	 * 
	 * @param status a transactions status indicating which
	 * transactions are complete
	 * 
	 * @return the next segment after all the segments have
	 * been read
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 * @throws FileTransactionManagerException
	 * @throws FileStoreException
	 * @throws BackingStoreException
	 * @throws BackingStoreClientException 
	 */

	private void loadTripleSegments(
			BackingStoreClient client,
			TransactionsStatus status) 
					throws 	InterruptedException, 
					IOException, 
					FileTransactionManagerException,
					FileStoreException,
					BackingStoreException,
					BackingStoreClientException {
				
		SegmentEnumeration segmentEnumeration = new SegmentEnumeration();
		SegmentRecord segmentRecord;
		while ((segmentRecord = segmentEnumeration.nextSegment()) != null) {
			if (segmentRecord instanceof  TripleSegmentRecord) {
				loadSegment(client, segmentRecord, status);
			}
		}		
		int end = segmentEnumeration.nextSegmentStart();
		if (end > endSegment) endSegment = end;
	}
		
	/**
	 * Load the contents of a segment into a backing store client
	 * 
	 * @param client a backing store client
	 * 
	 * @param segmentRecord a segment record
	 * 
	 * @param status a transactions status indicating which
	 * transactions are complete
	 * 
	 * @param trans a transaction for any updates to be made
	 * to the store
	 * 
	 * @throws BackingStoreException
	 * @throws IOException 
	 * @throws BackingStoreClientException 
	 */
	
	private void loadSegment(
			BackingStoreClient client,
			SegmentRecord segmentRecord,
			TransactionsStatus status) 
					throws 	BackingStoreException, 
							IOException, 
							BackingStoreClientException {

		if (segmentRecord.transPresent) {
			long transSeq = segmentRecord.transSeq;
			if (status.isCompleted(transSeq)) {
				if (segmentRecord.isValid())	{
					// A completed transaction has created a segment
					loadItemOrTriple(client, segmentRecord);
				}
				else {
				// If a completed transaction has deleted a segment
				// then no further action is required
				}
			}
			else {
				// The transaction that last touched this segment was
				// not completed
				switch (segmentRecord.recordValidIndicator) {
				case VALIDITY_INVALID_REINSTATE:
					mainChannelAccessor.setSegmentValidity(
							segmentRecord.segment.start, (byte)VALIDITY_VALID_REINSTATE);
				case VALIDITY_NO_INFO:
				case VALIDITY_VALID_REINSTATE:
					loadItemOrTriple(client, segmentRecord);
					break;
				case VALIDITY_ERROR:
				case VALIDITY_INVALID_NOREINSTATE: break;
				default: 
					mainChannelAccessor.setSegmentValidity(
							segmentRecord.segment.start, (byte)VALIDITY_INVALID_NOREINSTATE);
				}
			}
			if (transSeq > highestLoadedTransactionNr) 
				highestLoadedTransactionNr = transSeq;
		}
		else {
			// This is an erroneous record. Do nothing.
		}
	}
	
	/**
	 * Load an item or triple in a segment into a client.
	 * 
	 * @param client the client to receive the item or triple
	 * 
	 * @param segmentRecord a record of the segment
	 * 
	 * @throws BackingStoreClientException 
	 * @throws SourceConnectorException 
	 */
	
	void loadItemOrTriple(
			BackingStoreClient client,
			SegmentRecord segmentRecord) 
					throws 	BackingStoreException,
							BackingStoreClientException {
		
		if (segmentRecord instanceof ItemSegmentRecord) {
			ItemSegmentRecord itemSegmentRecord = (ItemSegmentRecord) segmentRecord;
			client.loadItem(
					itemSegmentRecord.itemNr,
					itemSegmentRecord.readAccessLevelSourceNr,
					itemSegmentRecord.readAccessLevelItemNr,
					itemSegmentRecord.writeAccessLevelSourceNr,
					itemSegmentRecord.writeAccessLevelItemNr,
					itemSegmentRecord.segment.start);
		}
		else if (segmentRecord instanceof TripleSegmentRecord){
			TripleSegmentRecord tripleSegmentRecord = 
					(TripleSegmentRecord) segmentRecord;
			client.loadTriple(
					tripleSegmentRecord.tripleNr,
					tripleSegmentRecord.subjectItemNr,
					tripleSegmentRecord.verbSourceNr,
					tripleSegmentRecord.verbItemNr,
					tripleSegmentRecord.getObjectDatum(connector),
					tripleSegmentRecord.segment.start);
		}
	}
	
	/*
	 ***********************************************************************
	 *
	 *  ASSOCIATION OF RECORDS WITH TRANSACTIONS
	 *  
	 ***********************************************************************
	 */

	/**
	 * The instance of the Associator class used by the backing store
	 */
	
	private Associator associator;
	
	/**
	 * <p>An instance of this class maintains associations between 
	 * transactions and records to ensure that records are updated 
	 * consistently.
	 * </p>
	 * <p>The 'record valid' indicator field of a segment record indicates
	 * whether it is valid and whether it should be reinstated if the
	 * last transaction to touch it does not complete.
	 * </p>
	 * <p>Following a restart, only records that are valid and should be
	 * reinstated are passed to the source connector. Records that are
	 * valid but should not be reinstated are marked as invalid.
	 * </p>
	 * <p>A segment can only be written or updated by one transaction
	 * at a time. This is checked to ensure the integrity of the
	 * validity mechanism.
	 * </p>
	 * <p>The methods of this class are synchronized to protect:</p>
	 * <ul>
	 * <li>The segment transactions map and</li>
	 * <li>The list of actions held by each transaction</li>
	 * </ul>
	 */
	
	private class Associator implements Serializable {
		
		private static final long serialVersionUID = 6609499625430135356L;

		private Map<Transaction, TransactionRecord> transactionMap =
				new HashMap<Transaction, TransactionRecord>();
		
		/**
		 * <p>Map that records the association of records with
		 * transactions
		 * </p>
		 * <p>Each transaction also has a transaction record that
		 * contains a list of the records that are associated 
		 * with it.
		 * </p>
		 */
		
		private Map<Integer, TransactionRecord> recordTransactionMap =
				new HashMap<Integer, TransactionRecord>();
		
		/**
		 * <p>Note that a transaction is started
		 * </p>
		 * @param trans a transaction
		 */
		
		private synchronized void transactionStarted(
				Transaction trans) {
			
			if (trans == null) return;
			transactionMap.put(trans, new TransactionRecord(trans));
		}
		
		/**
		 * Note that a record is created by a transaction.
		 * 
		 * @param ref a reference that identifies a record
		 * 
		 * @param trans a transaction
		 * 
		 * @throws FileStoreException 
		 */
		
		private synchronized void createRecord(
				int ref, Transaction trans) 
						throws FileStoreException {

			if (trans == null) 	throw (new FileStoreException(
					"Attempt to write record with null transaction"));
			TransactionRecord transRec = transactionMap.get(trans);
			if (transRec == null) throw (new FileStoreException(
					"Attempt to use transaction with null transaction record"));
			if (recordTransactionMap.containsKey(ref))	
				throw (new FileStoreException(
					"Attempt to write record that is already in use " +
					"by another transaction"));

			recordTransactionMap.put(ref, transRec);
			transRec.actions.add(new Action(ref, true));
		}
		
		/**
		 * <p>Note that a record is marked as valid or deleted by a
		 * transaction.
		 * </p>
		 * <p>The returned record validity indicator need
		 * not correspond to the supplied valid and reinstate
		 * parameters. If the transaction created the record then
		 * it should not be reinstated if the transaction does not
		 * complete, regardless of the value of the reinstate
		 * parameter
		 * </p>
		 * @param ref a reference that identifies a record
		 * that is being marked as valid or deleted
		 * 
		 * @param valid true if the record is being marked
		 * as valid, false if it is being marked as deleted
		 * 
		 * @param reinstate true if the segment is to be
		 * reinstated if the current transaction did not create
		 * it and does not complete, false otherwise
		 * 
		 * @param trans a non-null transaction
		 * 
		 * @return the record validity indicator if the record was
		 * associated with the transaction, VALIDITY_ERROR otherwise.
		 * 
		 * @throws FileStoreException 
		 */
		
		private synchronized short recordMarked(
				int ref, 
				boolean valid,
				boolean reinstate,
				Transaction trans) throws FileStoreException {
	
			TransactionRecord tr = transactionMap.get(trans);
			if (tr == null) throw (new FileStoreException(
					"Attempt to use transaction with null transaction record"));
			
			TransactionRecord tr2 = recordTransactionMap.get(ref);
			
			// If the segment is not already associated with a transaction,
			// associate it with this one and note whether it should be
			// reinstated if the transaction does not complete. (It cannot
			// have been created by this transaction or there would be
			// an association, and the transaction that created it must
			// have completed since only one transaction at a time can
			// create or update a record.)
			if (tr2 == null) {
				recordTransactionMap.put(ref, tr);
				tr.actions.add(new Action(ref, false));
				if (valid) {
					if (reinstate)
						return VALIDITY_VALID_REINSTATE;
					else return VALIDITY_VALID_NOREINSTATE;
				}
				else {
					if (reinstate)
						return VALIDITY_INVALID_REINSTATE;
					else return VALIDITY_INVALID_NOREINSTATE;
				}
			}
			
			// The record is already associated with a transaction.
			// Is it this transaction?
			if (tr2.transaction.equals(tr.transaction)) {
				// It is already associated with this transaction.
				// Did this transaction create it?
				for (Action a : tr.actions) {
					if ((a.reference == ref) && a.create) {
					// It was created by this transaction, and should
					// not be reinstated if the transaction does not
					// complete
						if (valid) return VALIDITY_VALID_NOREINSTATE;
						else return VALIDITY_INVALID_NOREINSTATE;
					}
				}
				
				// It was created by another transaction
				if (valid) {
					if (reinstate)
						return VALIDITY_VALID_REINSTATE;
					else return VALIDITY_VALID_NOREINSTATE;
				}
				else {
					if (reinstate)
						return VALIDITY_INVALID_REINSTATE;
					else return VALIDITY_INVALID_NOREINSTATE;
				}
			}
			else {
				// It is associated with a different transaction
				return VALIDITY_ERROR;
			}
		}
		
		/**
		 * <p>Note that a transaction is complete.
		 * </p>
		 * <p>All associations of segments with the given transaction
		 * are removed.
		 * </p>
		 * <p>This method is invoked when a transaction is complete.
		 * </p>
		 * @param transaction a transaction
		 */
		
		private synchronized void transactionComplete(
				Transaction trans) {
	
			if (trans == null) return;
			TransactionRecord tr = transactionMap.remove(trans);
			if (tr == null) return;
			for (Action a : tr.actions)
				recordTransactionMap.remove(a.reference);
		}
	}
	
	/**
	 * A record of a transaction that has a list of the actions
	 * performed on records in the course of carrying it out 
	 */
	
	class TransactionRecord implements Serializable {
		
		private static final long serialVersionUID = -6247519586500739354L;
		
		Transaction transaction;
		List<Action> actions = new LinkedList<Action>();
		
		TransactionRecord(Transaction trans) {
			
			this.transaction = trans;
		}
		
		public String toString() {
			
			String s = "TRANSREC: " + transaction + " ACTIONS";
			for (Action action : actions) s = s + ' ' + action;
			return s;
		}
	}
	
	/**
	 * An action performed on a record in the course of
	 * a transaction
	 */
	
	class Action implements Serializable {
		
		private static final long serialVersionUID = -8433405476446068862L;

		Action(int ref, boolean create) {
			
			reference = ref;
			this.create = create;
		}
		
		/**
		 * The action is to create the record if this is true,
		 * otherwise it is to mark the record as deleted 
		 */
		
		boolean create;
		
		/**
		 * A reference to the record 
		 */
		
		int reference;
		
		public String toString() {
			
			if (create) return "create " + reference;
			else return "delete " + reference;
		}
	}
	
	/*
	 ***********************************************************************
	 *
	 *  ITEMS
	 *  
	 ***********************************************************************
	 */

	public int addItem(
			long itemNr,
			long onlyReadLevelSourceNr,
			long onlyReadLevelItemNr,
			long readOrWriteLevelSourceNr,
			long readOrWriteLevelItemNr,
			Transaction trans)
						throws 	BackingStoreException {
		
		if (closed) throw(new BackingStoreClosedException());
		ByteBuffer buffer = getItemBuffer(
				itemNr,
				onlyReadLevelSourceNr, onlyReadLevelItemNr,
				readOrWriteLevelSourceNr, readOrWriteLevelItemNr);
		SegmentType segmentType = getBestFitSegmentType(
				SEGMENT_DATA_OFFSET + ITEM_DATA_SIZE);
		int storeRef;
		try {
			storeRef = mainChannelAccessor.writeSegment(
					segmentType, RECORD_TYPE_ITEM,
							buffer, trans);
		} catch (IOException e) {
			throw(abortException(e));
		} catch (FileStoreException e) {
			throw(abortException(e));
		}
		return storeRef;
	}

	public void removeItem(int storeRef, Transaction trans)
			throws 	BackingStoreException, 
					BackingStoreClientException {
	
		if (closed) throw(new BackingStoreClosedException());
		try {
			mainChannelAccessor.markSegment(
					storeRef, 
					false,	// invalid
					true,	// reinstate if this transaction does not
							// complete and did not create the record
					trans);
		} catch (IOException e) {
			throw abortException(e);
		}
	}
	
	/*
	 * 
	 * 
	 * @param id the identifier of the item
	 * 
	 * @param readLevelItemId the id of the item representing
	 * the item's read access level
	 * 
	 * @param witeLevelId the id of the item representing
	 * the item's read access level
	 * 
	 * *@return a Byte Buffer to write the item to a segment
	 * 
	 */
	
	/**
	 * Get a Byte Buffer to write an item record to a segment
	 * 
	 * @param itemNr the numeric identifier of the item that is the
	 * subject of the record
	 * 
	 * @param onlyReadLevelSourceNr the numeric identifier of the source
	 * of the only-read access level of the item
	 * 
	 * @param onlyReadLevelItemNr the numeric identifier in its source
	 * of the only-read access level of the item
	 * 
	 * @param readOrWriteLevelSourceNr the numeric identifier of the source
	 * of the read-or-write access level of the item
	 * 
	 * @param readOrWriteLevelItemNr the numeric identifier in its source
	 * of the read-or-write access level of the item
	 * 
	 * @return a byte buffer containing the data to be written as the item
	 * record
	 */
	
	private ByteBuffer getItemBuffer(
			long itemNr,
			long onlyReadLevelSourceNr,
			long onlyReadLevelItemNr,
			long readOrWriteLevelSourceNr,
			long readOrWriteLevelItemNr) {
		
		ByteBuffer buffer = ByteBuffer.allocate(
				SEGMENT_DATA_OFFSET + ITEM_DATA_SIZE);
		buffer.putLong(
				SEGMENT_DATA_OFFSET + ITEM_NR_OFFSET, itemNr);
		buffer.putLong(
				SEGMENT_DATA_OFFSET + ITEM_READ_SOURCE_NR_OFFSET, 
				onlyReadLevelSourceNr);
		buffer.putLong(
				SEGMENT_DATA_OFFSET + ITEM_READ_ITEM_NR_OFFSET, 
				onlyReadLevelItemNr);
		buffer.putLong(
				SEGMENT_DATA_OFFSET + ITEM_WRITE_SOURCE_NR_OFFSET, 
				readOrWriteLevelSourceNr);
		buffer.putLong(
				SEGMENT_DATA_OFFSET + ITEM_WRITE_ITEM_NR_OFFSET, 
				readOrWriteLevelItemNr);
		return buffer;
	}

	/*
	 ***********************************************************************
	 *
	 *  TRIPLES
	 *  
	 ***********************************************************************
	 */

	@Override
	public RefAndSummary addTriple(
			long tripleNr,
  			long subjectItemNr,
  			long verbSourceNr,
  			long verbItemNr,
  			Datum object,
  			Transaction trans)
		throws 	BackingStoreException, 
				TripleStoreNonFatalException {
		
		RefAndSummary refAndSummary;
		try {
			switch (object.getType()) {
			case ITEM :
				refAndSummary = addItemTriple(
						tripleNr,
						subjectItemNr,
						verbSourceNr, 
						verbItemNr, 
						object,
						trans);
				break;
			case BOOLEAN :
			case INTEGRAL :
			case REAL :
				refAndSummary = addSimpleTriple(
						tripleNr,
			  			subjectItemNr,
			  			verbSourceNr, 
			  			verbItemNr, 
			  			object, 
			  			trans);
				break;
			case TEXT :
				refAndSummary = addTextTriple(
						tripleNr,
			  			subjectItemNr,
			  			verbSourceNr, verbItemNr, 
			  			object, trans);
				break;
			case BINARY : 
				refAndSummary = addBinaryTriple(
						tripleNr,
			  			subjectItemNr,
			  			verbSourceNr, verbItemNr, 
			  			object, trans);
				break;
			default : throw new FileStoreException("Invalid object type");
			}
			return refAndSummary;
		} catch (IOException e) {
			BackingStoreException x = abortException(e);
			throw(x);
		} catch (FileStoreException e) {
			BackingStoreException x = abortException(e);
			throw(x);
		} catch (BackingStoreClientException e) {
			TripleStoreNonFatalException x = 
					new TripleStoreNonFatalException(
							"Cannot add triple to file backing store "
							+ "because of triple store error");
			x.initCause(e);
			throw(x);
		}
	}
	
	@Override
	public void removeTriple(
			int storeRef, Transaction trans)
   					throws 	BackingStoreException,
   							BackingStoreClientException {
		
		if (closed) throw(new BackingStoreClosedException());
		try {
			mainChannelAccessor.markSegment(
					storeRef, 
					false,	// invalid
					true,	// reinstate if this transaction does not
							// complete and did not create the record
					trans);
		} catch (IOException e) {
			throw(abortException(e));
		}
	}
	
	/**
	 * Add a simple triple
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param object the object of the triple
	 * 
	 * @param trans the transaction of which this operation
	 * is part
	 * 
	 * @return the store reference for the record where the
	 * triple was written, with the triple object's summary
	 * 
	 * @throws IOException
	 * @throws FileStoreException
	 */
	
	private RefAndSummary addSimpleTriple(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr,
			long verbItemNr,
			Datum object,
			Transaction trans) 
					throws IOException,
					FileStoreException {
		
		long summary = object.getSummaryValue();
		SegmentType segmentType = getBestFitSegmentType(
				SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE);
		ByteBuffer buffer = getSSBTripleBuffer(
				tripleNr, 
				subjectItemNr, 
				verbSourceNr, verbItemNr, 
				object.getType(),
				summary, 0); 
		return new RefAndSummary(
				mainChannelAccessor.writeSegment(
						segmentType, RECORD_TYPE_SIMPLE_TRIPLE,
						buffer, trans),
				summary);
	}
	
	/**
	 * Add an item triple
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param object the object of the triple
	 * 
	 * @param trans the transaction of which this operation
	 * is part
	 * 
	 * @return the store reference for the record where the
	 * triple was written, with the triple object's summary
	 * 
	 * @throws IOException
	 * @throws FileStoreException
	 * @throws BackingStoreClientException 
	 */
	
	private RefAndSummary addItemTriple(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr,
			long verbItemNr,
			Datum object,
			Transaction trans) 
					throws  IOException,
							FileStoreException,
							BackingStoreClientException {

		SegmentType segmentType = getBestFitSegmentType(
				SEGMENT_DATA_OFFSET + ITEM_TRIPLE_DATA_SIZE);
		Item item;
		try {
			item = object.getItemValue();
		} catch (TripleStoreNonFatalException e) {
			BackingStoreClientException x =
					new BackingStoreClientException(
							"Cannot get object");
			x.initCause(e);
			throw(x);
		}
		long summary = object.getSummaryValue();
		ByteBuffer buffer = getItemTripleBuffer(
				tripleNr, 
				subjectItemNr, 
				verbSourceNr, verbItemNr, 
				item.getSourceNr(),
				item.getItemNr()); 
		return new RefAndSummary(
				mainChannelAccessor.writeSegment(
						segmentType, RECORD_TYPE_ITEM_TRIPLE,
						buffer, trans),
				summary);
	}

	
	/**
	 * Add a text triple
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param object the object of the triple
	 * 
	 * @param trans the transaction of which this operation
	 * is part
	 * 
	 * @return the store reference for the record where the
	 * triple was written, with the triple object's summary
	 * 
	 * @throws IOException
	 * @throws FileStoreException
	 * @throws BackingStoreClientException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private RefAndSummary addTextTriple(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr,
			long verbItemNr,
 			Datum object,
 			Transaction trans) 
 					throws 	IOException,
 							FileStoreException,
 							FileStoreException,
 							BackingStoreClientException,
 							TripleStoreNonFatalException {
		
		// If this is an object held in the source, handle it
		// separately
		if (object.heldInSource()) return addSourceTextTriple(
					tripleNr, 
					subjectItemNr,
					verbSourceNr, 
					verbItemNr,
					object, trans);
		
		// Create a string containing the start of the object
		// and a reader from which the rest can be read such that,
		// if the object is small enough to fit into a main file
		// segment, it is entirely contained in the string.
		String string = "";
		BufferedReader reader;
		if (object.valueShouldBeRead()) {
			
			// The object should be read from a reader.
			// Read the start of it into a character array.
			char[] charbuf = new char[maxSegmentDataLength + 1];
			BufferedReader objectReader;
			try {
				objectReader = object.getTextValueReader();
			} catch (SourceConnectorException e) {
				FileStoreException x = new FileStoreException(
						"Failed to read object in source");
				x.initCause(e);
				throw(x);
			} catch (TripleStoreNonFatalException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot get object");
				x.initCause(e);
				throw(x);
			}
			try {
				int n = objectReader.read(
							charbuf, 0, charbuf.length - 1);
				
				// The start has been read in
				int m = 0;
				if (n >= 0) {
					
					// Some data has been read. Try reading another
					// character to see if that was all the data.
					m = objectReader.read(charbuf, n, 1);
					if (m <= 0) {
						
						// That was the end of the data
						string = new String(charbuf, 0, n);
						reader = null;
					}
					else {
						
						// More input to be read
						string = new String(charbuf, 0, n+1);
						reader = objectReader;				
					}
				}
				else {
					
					// Empty input stream
					reader = null;
				}
				
				// Now create the result by adding the remaining
				// input, if any, to the start string and writing
				// the record
				return writeTextTripleRecord(
						tripleNr, 
						subjectItemNr,
						verbSourceNr,
						verbItemNr,
						string,
						reader, 
						trans);
			}
			finally {
				objectReader.close();
			}
		}
		else {
			// Object value can be accessed as a string
				try {
					string = object.getTextValueAsString();
				} catch (SourceConnectorException e) {
					FileStoreException x = new FileStoreException(
							"Failed to read object in source");
					x.initCause(e);
					throw(x);
				} catch (TripleStoreNonFatalException e) {
					BackingStoreClientException x =
							new BackingStoreClientException(
									"Cannot get object");
					x.initCause(e);
					throw(x);
				}
			reader = null;
			
			// Create the result and write the record
			return writeTextTripleRecord(
					tripleNr, 
					subjectItemNr,
					verbSourceNr, 
					verbItemNr,
					string, 
					reader, 
					trans);
		}
	}
		
	/**
	 * Write a record containing a text triple
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param string a String containing the start of the
	 * text object
	 * 
	 * @param reader a buffered reader from which the
	 * rest of the object can be read
	 * 
	 * @param trans the transaction of which this operation
	 * is a part
	 * 
	 * @return the store reference for the record where the
	 * triple was written, with the triple object's summary
	 * 
	 * @throws IOException 
	 * @throws FileStoreException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private RefAndSummary writeTextTripleRecord(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr,
			long verbItemNr,
			String string, 
			BufferedReader reader,
			Transaction trans)
					throws 	IOException,
							FileStoreException, 
							TripleStoreNonFatalException {
				
		ByteBuffer buffer = null;
		short recordType = -1;
		byte[] objectBytes = string.getBytes(utf8);
		SegmentType segmentType;
		
		// Try and get a segment big enough to hold all the data
		if (reader == null) segmentType = getBestFitSegmentType(
				SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_DATA_OFFSET +
				objectBytes.length);
		
		else segmentType = getBestFitSegmentType(
				// The data definitely will not fit into a segment
				SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_DATA_OFFSET);

		int segmentStart; 	// The start of the segment
		
		long summary;		// The object's summary value
		
		if ((reader == null) && 
			(segmentType.length >= 
				(SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_DATA_OFFSET + 
						objectBytes.length))) {

			//	The object can be stored in the segment
			//	This is a small text triple
			recordType = RECORD_TYPE_SMALL_TEXT_TRIPLE;
			
			// Set up the object summary
			summary = Datum.updateSummary(0L, string);
			
			// Get a buffer big enough to hold the record and
			// triple headers plus the data
			buffer = getSSBTripleBuffer(
					tripleNr, 
					subjectItemNr, 
					verbSourceNr, 
					verbItemNr, 
					Datum.Type.TEXT,
					summary,
					objectBytes.length);
			buffer.position(
					SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_DATA_OFFSET);
			
			// Put the data into the buffer
			buffer.put(objectBytes);
			
			// Now write the segment
			segmentStart = mainChannelAccessor.writeSegment(
					segmentType, recordType,
					buffer, trans);
		}
		else {
			// This is a big text triple
			recordType = RECORD_TYPE_BIG_TEXT_TRIPLE;
			
			// Get a buffer big enough to hold the record and
			// triple headers
			buffer = getSSBTripleBuffer(
					tripleNr, 
					subjectItemNr, 
					verbSourceNr, 
					verbItemNr,
					Datum.Type.TEXT, 
					0,
					0);

			// Write the summary record but without the correct
			// summary value
			segmentStart = mainChannelAccessor.writeSegment(
					segmentType,
					recordType,
					buffer, 
					trans);
			
			// Set up the file to which the object is to be written
			File f = new File(
					bigFileDirectory, 
					Long.toString(segmentStart));
			
			// Write the data to the file in the big file
			// directory
			summary = writeBigTextTripleObject(
							string, reader, f);
			
			// Set the summary value in the summary record
			mainChannelAccessor.updateTripleSummary(
					segmentStart, summary);
		}
		
		// Return the result
		return new RefAndSummary(segmentStart, summary);
	}
	
	/**
	 * Add a source text triple
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param object the object of the triple
	 * 
	 * @param trans the transaction of which this operation
	 * is part
	 * 
	 * @return the store reference for the record where the
	 * triple was written, with the triple object's summary
	 * 
	 * @throws IOException
	 * @throws FileStoreException
	 */
	
	private RefAndSummary addSourceTextTriple(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr,
			long verbItemNr,
			Datum object,
 			Transaction trans) 
 					throws 	IOException, 
 							FileStoreException {
		
		SegmentType segmentType = getBestFitSegmentType(
				SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE);
		ByteBuffer buffer = getSSBTripleBuffer(
				tripleNr, 
				subjectItemNr, 
				verbSourceNr, verbItemNr, 
				object.getType(),
				object.getSummaryValue(), 0);
		return new RefAndSummary(
				mainChannelAccessor.writeSegment(
						segmentType, RECORD_TYPE_SOURCE_TEXT_TRIPLE,
						buffer, trans),
				object.getSummaryValue());
	}
		
	/**
	 * Add a binary triple 
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param object the object of the triple
	 * 
	 * @param trans the transaction of which this operation
	 * is part
	 * 
	 * @return the store reference for the record where the
	 * triple was written, with the triple object's summary
	 * 
	 * @throws IOException
	 * @throws FileStoreException
	 * @throws BackingStoreClientException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private RefAndSummary addBinaryTriple(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr,
			long verbItemNr,
			Datum object,
 			Transaction trans) 
 					throws 	IOException,
 							FileStoreException, 
 							BackingStoreClientException,
 							TripleStoreNonFatalException {

		// If this is a source object, handle it separately
		if (object.heldInSource()) return addSourceBinaryTriple(
					tripleNr, 
					subjectItemNr,
					verbSourceNr, verbItemNr,
					object, trans);

		// Create a byte array containing the start of the object
		// and a stream from which the rest can be read such that,
		// if the object is small enough to fit into a main file
		// segment, it is entirely contained in the byte array.
		byte[] buf;
		int length;
		BufferedInputStream stream;
		if (object.valueShouldBeRead()) {
			
			// The object should be read from an input stream.
			// Read the start of it into a byte array.
			BufferedInputStream objectStream;
			try {
				objectStream = object.getBinaryValueStream();
			} catch (SourceConnectorException e) {
				FileStoreException x = new FileStoreException(
						"Failed to read object in source");
				x.initCause(e);
				throw(x);
			} catch (TripleStoreNonFatalException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot get object");
				x.initCause(e);
				throw(x);
			}
			buf = new byte[maxSegmentDataLength + 1];
			try {
				int n = objectStream.read(buf, 0, buf.length - 1);
				
				// The start has been read in. 
				int m = 0;
				if (n >= 0) {
					
					// Some data has been read. Try reading another
					// byte to see if that was all the data.
					m = objectStream.read(buf, n, 1);
					if (m <= 0) {
						
						// That was the end of the data
						length = n;
						stream = null;
					}
					else {
						
						// More input to be read
						length = n+1;
						stream = objectStream;
					}
				}
				else {
					
					// Empty input stream
					length = 0;
					stream = null;
				}
				
				// Now create the result by adding the remaining
				// input, if any, to the start byte array and writing
				// the record
				return writeBinaryTripleRecord(
						tripleNr, 
						subjectItemNr,
						verbSourceNr, verbItemNr,
						object, buf, length, stream, trans);
			}
			finally {
				objectStream.close();
			}
		}
		else {
			// Object value can be accessed as a byte array
			try {
				buf = object.getBinaryValueAsArray();
			} catch (SourceConnectorException e) {
				FileStoreException x = new FileStoreException(
						"Failed to read object in source");
				x.initCause(e);
				throw(x);
			} catch (TripleStoreNonFatalException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot get object");
				x.initCause(e);
				throw(x);
			}
			length = buf.length;
			stream = null;
			
			// Create the result and write the record
			return writeBinaryTripleRecord(
					tripleNr, 
					subjectItemNr,
					verbSourceNr, verbItemNr,
					object, buf, length, stream, trans);
		}
	}

	/**
	 * Write a record containing a binary triple
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param object the object of the triple
	 * 
	 * @param buf a buffer containing the start of the
	 * binary object
	 * 
	 * @param length the number of bytes in the buffer
	 * 
	 * @param stream a buffered input stream from which the
	 * rest of the object can be read
	 * 
	 * @param trans the transaction of which this operation
	 * is a part
	 * 
	 * @return the store reference for the record where the
	 * triple was written, with the triple object's summary
	 * 
	 * @throws IOException 
	 * @throws FileStoreException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private RefAndSummary writeBinaryTripleRecord(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr,
			long verbItemNr,
			Datum object,
			byte[] buf, 
			int length, 
			BufferedInputStream stream,
			Transaction trans) 
					throws 	IOException,
							FileStoreException,
							TripleStoreNonFatalException {
		
		ByteBuffer buffer = null;
		short recordType = -1;
		SegmentType segmentType;
		if (stream == null)
			
			// Try and get a segment big enough to hold all the data
			segmentType = getBestFitSegmentType(
				SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_DATA_OFFSET +
					length);
		else
			// The data definitely will not fit into a segment
			segmentType = getBestFitSegmentType(
				SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_DATA_OFFSET);
		
		int segmentStart;	// The start of the segment

		long summary;		// The object's summary value
		
		if ((stream == null) && 
			(segmentType.length >= 
				(SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_DATA_OFFSET + 
						length))) {
			
			//	The object can be stored in the segment
			//	This is a small binary triple
			recordType = RECORD_TYPE_SMALL_BINARY_TRIPLE;
			
			// Set up the summary value
			summary = Datum.updateSummary(0L, buf, length);
			
			// Get a buffer big enough to hold the record and
			// triple headers plus the data
			buffer = getSSBTripleBuffer(
					tripleNr, 
					subjectItemNr, 
					verbSourceNr, 
					verbItemNr, 
					Datum.Type.BINARY, 
					summary, 
					length);
			
			// Put the data into the buffer
			buffer.position(
					SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_DATA_OFFSET);
			buffer.put(buf, 0, length);
			
			// Now write the segment
			segmentStart = mainChannelAccessor.writeSegment(
					segmentType, recordType,
					buffer, trans);			
		}
		else {
			
			// This is a big binary triple
			recordType = RECORD_TYPE_BIG_BINARY_TRIPLE;
			
			// Get a buffer big enough to hold the record and
			// triple headers
			buffer = getSSBTripleBuffer(
					tripleNr, 
					subjectItemNr, 
					verbSourceNr,
					verbItemNr, 
					Type.BINARY,
					0,
					0);
			
			// Write the summary record but without the correct
			// summary value
			segmentStart = mainChannelAccessor.writeSegment(
					segmentType, recordType,
					buffer, trans);
			
			// Set up a file in the big file directory
			File f = new File(
					bigFileDirectory, Long.toString(segmentStart));

			// Write the data to the file in the big file
			// directory
			summary = writeBigBinaryTripleObject(
							buf, length, stream, f);
			
			// Set the summary value in the summary record
			mainChannelAccessor.updateTripleSummary(
					segmentStart, summary);
		}
		
		// Return the result
		return new RefAndSummary(segmentStart, summary);
	}
	
	/**
	 * Add a source binary triple
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param object the object of the triple
	 * 
	 * @param trans the transaction of which this operation
	 * is part
	 * 
	 * @return the store reference for the record where the
	 * triple was written, with the triple object's summary
	 * 
	 * @throws IOException
	 * @throws FileStoreException
	 */
	
	private RefAndSummary addSourceBinaryTriple(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr,
			long verbItemNr,
			Datum object,
 			Transaction trans) 
 					throws 	IOException, 
 							FileStoreException {
		
		SegmentType segmentType = getBestFitSegmentType(
				SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE);
		ByteBuffer buffer = getSSBTripleBuffer(
				tripleNr, 
				subjectItemNr, 
				verbSourceNr, verbItemNr, 
				object.getType(),
				object.getSummaryValue(), 0);
		return new RefAndSummary(
				mainChannelAccessor.writeSegment(
						segmentType, RECORD_TYPE_SOURCE_BINARY_TRIPLE,
						buffer, trans),
				object.getSummaryValue());
			}
		
	@Override
	public Datum getTextTripleObject(int storeRef)
			throws BackingStoreException, BackingStoreClosedException, TripleStoreNonFatalException {

		if (closed) throw(new BackingStoreClosedException());
		ByteBuffer buffer;
		try {
			buffer = readSegmentStart(storeRef);
		} catch (IOException e) {
			throw(abortException(e));
		}
		short type = buffer.getShort(SEGMENT_RECORD_TYPE_OFFSET);
		long summary = buffer.getLong(
				SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_SUMMARY_OFFSET);
		Datum datum = null;
		switch (type) {
		case  RECORD_TYPE_SMALL_TEXT_TRIPLE	: {
			try {
				datum = Datum.createText(
							getSegmentText(storeRef, buffer));
			} catch (FileStoreException e) {
				throw(abortException(e));
			} catch (IOException e) {
				throw(abortException(e));
			}
			break;
		}
		case  RECORD_TYPE_BIG_TEXT_TRIPLE :
			File f = new File(bigFileDirectory, 
					Long.toString(storeRef));
			datum = Datum.createText(f);
			break;
		case RECORD_TYPE_SOURCE_TEXT_TRIPLE :
			throw (new ObjectNotInStoreException());
		case RECORD_TYPE_SIMPLE_TRIPLE :
		case RECORD_TYPE_SMALL_BINARY_TRIPLE :
		case RECORD_TYPE_BIG_BINARY_TRIPLE :
		case RECORD_TYPE_SOURCE_BINARY_TRIPLE :
			try {
				throw (new FileStoreException(
						"Incorrect triple type"));
			} catch (FileStoreException e) {
				throw(abortException(e));
			}
		default: 
			try {
				throw (new FileStoreException(
						"Invalid segment type"));
			} catch (FileStoreException e) {
				throw(abortException(e));
			}
		}
		datum.setSummaryValue(summary);
		return datum;
		
	}

	@Override
	public Datum getBinaryTripleObject(int storeRef)
			throws BackingStoreException, TripleStoreNonFatalException {

		if (closed) throw(new BackingStoreClosedException());
		ByteBuffer buffer;
		try {
			buffer = readSegmentStart(storeRef);
		} catch (IOException e) {
			throw(abortException(e));
		}
		short type = buffer.getShort(SEGMENT_RECORD_TYPE_OFFSET);
		long summary = buffer.getLong(
				SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_SUMMARY_OFFSET);
		Datum datum = null;
		switch (type) {
		case RECORD_TYPE_SIMPLE_TRIPLE :
		case RECORD_TYPE_SMALL_TEXT_TRIPLE	: 
		case RECORD_TYPE_BIG_TEXT_TRIPLE :
		case RECORD_TYPE_SOURCE_TEXT_TRIPLE :
			try {
				throw (new FileStoreException(
						"Incorrect triple type"));
			} catch (FileStoreException e) {
				throw(abortException(e));
			}
		case  RECORD_TYPE_SMALL_BINARY_TRIPLE :
			try {
				datum = Datum.createBinary(
						getSegmentBinary(storeRef, buffer));
			} catch (FileStoreException e) {
				throw(abortException(e));
			} catch (IOException e) {
				throw(abortException(e));
			}
			break;
		case  RECORD_TYPE_BIG_BINARY_TRIPLE :
			File g = new File(bigFileDirectory, 
					Long.toString(storeRef));
			datum = Datum.createBinary(g);
			break;
		case  RECORD_TYPE_SOURCE_BINARY_TRIPLE :
			throw (new ObjectNotInStoreException());
		default : 
			try {
				throw (new FileStoreException(
						"Invalid segment type"));
			} catch (FileStoreException e) {
				throw(abortException(e));
			}
		}
		datum.setSummaryValue(summary);
		return datum;
	}

	/**
	 * Get the file in the big file directory for the
	 * object of the triple with a given store reference.
	 * 
	 * @param storeRef the reference to a triple in store
	 * 
	 * @return the file in the big file directory for the
	 * object of the triple with the given store reference
	 * if the store reference identifies a triple whose
	 * object is stored in the big file directory, null
	 * otherwise.
	 */
	
	File bigFile(int storeRef) {
		
		File f = new File(
				bigFileDirectory, Long.toString(storeRef));
		if (f.exists()) return f;
		return null;
	}
	
	/**
	 * Get a byte buffer to write a source, simple, big text or big 
	 * binary triple record, or a summary of it, to the main channel.
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param objectType the object type of the triple 
	 * to be written.
	 * 
	 * @param summaryValue the summary value of the triple 
	 * to be written. 
	 * 
	 * @param segmentObjectDataSize the amount of object data 
	 * to be stored in the segment, in bytes
	 * 
	 * @return a byte buffer to write the specified triple to the
	 * main channel
	 */
	
	private ByteBuffer getSSBTripleBuffer(
			long tripleNr,
			long subjectItemNr, 
			long verbSourceNr, 
			long verbItemNr, 
			Datum.Type objectType,
			long objectSummaryValue, 
			int segmentObjectDataSize) {
		
		ByteBuffer buffer = ByteBuffer.allocate(
				SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE +
				segmentObjectDataSize);
		setCommonBufferFields(
				buffer, tripleNr,
				subjectItemNr, 
				verbSourceNr, verbItemNr,
				objectType);
		buffer.putLong(SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_SUMMARY_OFFSET,
				objectSummaryValue);
		return buffer;
	}
	
	/**
	 * Get a byte buffer to write an item triple record to the main 
	 * channel.
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param itemObjectSourceNr the numeric identifier of the
	 * source of the item that is the object of the triple
	 * 
	 * @param itemObjectItemNr the numeric identifier in its
	 * source of the item that is the object of the triple
	 * 
	 * @return a byte buffer to write the specified item triple record
	 * to the main channel
	 */
	
	private ByteBuffer getItemTripleBuffer(
			long tripleNr,
			long subjectItemNr, 
			long verbSourceNr, 
			long verbItemNr, 
			long itemObjectSourceNr,
			long itemObjectItemNr) {
		
		ByteBuffer buffer = ByteBuffer.allocate(
				SEGMENT_DATA_OFFSET + ITEM_TRIPLE_DATA_SIZE);
		setCommonBufferFields(
				buffer, tripleNr,
				subjectItemNr, 
				verbSourceNr, verbItemNr,
				Type.ITEM);
		buffer.putLong(SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_SOURCE_NR_OFFSET,
				itemObjectSourceNr);
		buffer.putLong(SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_ITEM_NR_OFFSET,
				itemObjectItemNr);
		return buffer;
	}
		
	/**
	 * Set the common fields of a buffer
	 * 
	 * @param buffer the buffer whose fields are to be set
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject of the triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb of the triple in its source
	 * 
	 * @param objectType the type of the object of the triple
	 */
	
	private void setCommonBufferFields(
			ByteBuffer buffer,
			long tripleNr,
			long subjectItemNr, 
			long verbSourceNr, 
			long verbItemNr,
			Datum.Type objectType) {
		
		buffer.putLong(SEGMENT_DATA_OFFSET + TRIPLE_NR_OFFSET, 
				tripleNr);
		buffer.putLong(SEGMENT_DATA_OFFSET + TRIPLE_SUBJECT_ITEM_NR_OFFSET, 
				subjectItemNr);
		buffer.putLong(SEGMENT_DATA_OFFSET + TRIPLE_VERB_SOURCE_NR_OFFSET,
				verbSourceNr);
		buffer.putLong(SEGMENT_DATA_OFFSET + TRIPLE_VERB_ITEM_NR_OFFSET,
				verbItemNr);
		buffer.putInt(SEGMENT_DATA_OFFSET + TRIPLE_OBJECT_TYPE_OFFSET,
				indicator(objectType));		
	}

	/*
	 ***********************************************************************
	 *
	 *  READ SEGMENTS FROM OR WRITE SEGMENTS TO THE MAIN CHANNEL
	 *  
	 ***********************************************************************
	 */
	
	/**
	 * Read the start of a segment, including the segment header
	 * and the item or triple header.
	 * 
	 * @param ref the position of the segment in the main file.
	 * 
	 * @throws IOException 
	 */
	
	private ByteBuffer readSegmentStart(long ref) 
			throws IOException {
		
		ByteBuffer buffer = ByteBuffer.allocate(
				SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE);
		mainChannel.read(buffer, ref);
		return buffer;
	}
	
	/**
	 * Get the value of a text triple's object that is stored in
	 * a segment
	 * 
	 * @param storeRef the backingStore reference of the triple. This is the
	 * position of the segment in the main file.
	 * 
	 * @param startBuf a ByteBuffer holding the start of the segment.
	 * It contains the segment header and the triple header
	 * 
	 * @return a string containing the value of the triple's
	 * object.
	 * 
	 * @throws FileStoreException 
	 * @throws IOException 
	 * 
	 */
	
	private String getSegmentText(
			long storeRef, 
			ByteBuffer startBuf) 
					throws 	FileStoreException,
							IOException {
		
		long textStart = storeRef + SEGMENT_DATA_OFFSET +
								TRIPLE_OBJECT_DATA_OFFSET;
		int maxBytes = startBuf.getInt(SEGMENT_LENGTH_OFFSET) -
							(SEGMENT_DATA_OFFSET +
									TRIPLE_OBJECT_DATA_OFFSET);
		long summary = startBuf.getLong(SEGMENT_DATA_OFFSET +
									TRIPLE_OBJECT_SUMMARY_OFFSET);
		int length = Datum.lengthComponent(summary);
		ByteBuffer textBuffer = ByteBuffer.allocate(maxBytes);

		// Read in the data. Note that it may not fill the segment
		// and, if it is at the end of the file, a full segment
		// may not be read
		byte[] bytes;
		bytes = new byte[mainChannel.read(
							textBuffer, textStart)];
		textBuffer.position(0);
		textBuffer.get(bytes);
		
		String s = new String(bytes, utf8);

		// Check the length of the string. Note that it can have
		// too many characters, if the segment was not full, but
		// not too few characters
		if (s.length() < length)
			throw (new FileStoreException(
						"Invalid text length"));
		
		// Return the result
		return s.substring(0, length);
	}
	
	/**
	 * Get the value of a binary triple's object that is stored in
	 * a segment
	 * 
	 * @param storeRef the backingStore reference of the triple. This is the
	 * position of the segment in the main file.
	 * 
	 * @param startBuf a ByteBuffer holding the start of the segment.
	 * It contains the segment header and the triple header
	 * 
	 * @return a byte array containing the value of the triple's
	 * object.
	 * 
	 * @throws FileStoreException 
	 * @throws IOException 
	 * 
	 */
	
	private byte[] getSegmentBinary(
			long storeRef,
			ByteBuffer startBuf)
					throws 	FileStoreException, 
							IOException {
		
		long binStart = storeRef + SEGMENT_DATA_OFFSET +
								TRIPLE_OBJECT_DATA_OFFSET;
		int maxBytes = startBuf.getInt(SEGMENT_LENGTH_OFFSET) -
							(SEGMENT_DATA_OFFSET +
									TRIPLE_OBJECT_DATA_OFFSET);
		long summary = startBuf.getLong(SEGMENT_DATA_OFFSET + 
									TRIPLE_OBJECT_SUMMARY_OFFSET);
		int length = Datum.lengthComponent(summary);
		
		// Check that the length of the data is not more than
		// the amount available in the segment
		if (maxBytes < length) {
			throw(new FileStoreException(
					"Invalid binary length at " + storeRef));
		}
					
		// Read in the data. 
		ByteBuffer binBuffer = ByteBuffer.allocate(length);
		if (mainChannel.read(binBuffer, binStart) != length)
			throw (new FileStoreException(
					"Invalid binary length at " + storeRef));
		byte[] bytes = new byte[length];
		binBuffer.position(0);
		binBuffer.get(bytes);
		
		// Return the result
		return bytes;
	}
	
	/**
	 * An instance of this class provides synchronized write
	 * access to a channel. Only one instance should be used
	 * for a given channel. A single instance is used in a
	 * file backing store to synchronize write access to the
	 * main channel. This is necessary to avoid errors. It 
	 * does not appear to be necessary to synchronize read 
	 * access.
	 */
	
	private class ChannelAccessor implements Serializable {
	
		private static final long serialVersionUID = 7182116588357321609L;

		/**
		 * The channel to which write access is synchronized
		 */
		
		private FileChannel channel;
		
		/**
		 * The position of the channel that follows the
		 * last segment. Initially zero.
		 */
		
		private Integer endOfChannelSegments = 0;
		
		/**
		 * Create a new Channel Accessor
		 * 
		 * @param channel the channel to which write access
		 * is to be synchronized
		 */
		
		private ChannelAccessor(FileChannel channel) {
			this.channel = channel;
		}
		
		/**
		 * Set the position of the channel that follows the
		 * last segment.
		 * 
		 * @param end the position to be set
		 */
		
		private synchronized void setEndOfChannelSegments(int end) {
			endOfChannelSegments = end;
		}
		
		/**
		 * Get the position of the channel that follows the
		 * last segment.
		 * 
		 * @return the position of the channel that follows the
		 * last segment.
		 */
		
		private synchronized int getEndOfChannelSegments() {
			return endOfChannelSegments;
		}
		
		/**
		 * Write a segment
		 * 
		 * @param type the type of the segment
		 * 
		 * @param recordType the type of record to be stored or summarized 
		 * in the segment
		 * 
		 * @param buffer the bytes to be written to the segment.
		 * The item or triple data should already be in this buffer.
		 * 
		 * @param trans the transaction of which this operation
		 * is a part, or null if is is part of a completed transaction
		 * 
		 * @return the start of the segment that was written
		 * 
		 * @throws IOException 
		 * @throws FileStoreException 
		 */
		
		private synchronized int writeSegment(
				SegmentType type,
				short recordType,
				ByteBuffer buffer, 
				Transaction trans) 
						throws IOException,
						FileStoreException {

			if (trans == null) throw (new FileStoreException(
					"Attempt to write record with null transaction"));
			buffer.putShort(SEGMENT_RECORD_TYPE_OFFSET, 
					recordType);
			buffer.put(SEGMENT_TRANS_PRESENT_OFFSET, (byte)1);
			buffer.putLong(SEGMENT_TRANS_SEQ_OFFSET,
				trans.seq());
			Segment segment = getFreeSegment(type);
			int writePos;
			if (segment == null) {
				segment = getNewSegment(type);
				buffer.putInt(SEGMENT_LENGTH_OFFSET, 
						type.length);
				buffer.position(0);
				writePos = segment.start;
			}
			else {
				// Don't re-write the segment length so that the
				// integrity of the file is less likely to be
				// compromised by an IO error
				buffer.position(SEGMENT_RECORD_TYPE_OFFSET);
				writePos = segment.start + SEGMENT_RECORD_TYPE_OFFSET;
			}
			associator.createRecord(segment.start, trans);
			byte validIndicator = VALIDITY_VALID_NOREINSTATE;
			buffer.put(SEGMENT_RECORD_VALID_OFFSET, validIndicator);
			byte transPresent = 0;
			if (trans != null) transPresent = 1;
			buffer.put(SEGMENT_TRANS_PRESENT_OFFSET, transPresent);
			channel.write(buffer, writePos);
			return segment.start;
		}
		
		/**
		 * Mark a segment 
		 * 
		 * @param ref the position in the main file of the
		 * segment to be marked
		 * 
		 * @param valid true if the segment is to be marked
		 * as valid, false if it is to be marked as deleted
		 * 
		 * @param reinstate true if the segment is to be
		 * reinstated if the current transaction did not create
		 * it and does not complete, false otherwise
		 * 
		 * @param trans the transaction of which this operation
		 * is a part, or null if is is part of a completed transaction
		 * 
		 * @throws IOException 
		 * @throws FileStoreException 
		 * @throws BackingStoreClientException 
		 */
		
		 private synchronized void markSegment(
				 int ref, 
				 boolean valid,
				 boolean reinstate,
				 Transaction trans) 
						throws 	IOException,
								FileStoreException, 
								BackingStoreClientException {
			
			if (trans == null) throw (new FileStoreException(
					"Attempt to mark record with null transaction"));
			ByteBuffer buffer = ByteBuffer.allocate(
					SEGMENT_DATA_OFFSET);
			byte validIndicator = 
					(byte) associator.recordMarked(ref, valid, reinstate, trans);
			if (validIndicator == VALIDITY_ERROR) 
				throw (new BackingStoreClientException(
						"Record touched by two transactions at once"));
			buffer.put(SEGMENT_RECORD_VALID_OFFSET,
					validIndicator);
			buffer.put(SEGMENT_TRANS_PRESENT_OFFSET, (byte)1);
			buffer.putLong(SEGMENT_TRANS_SEQ_OFFSET,
				trans.seq());
			buffer.position(SEGMENT_RECORD_VALID_OFFSET);
			channel.write(buffer,
					ref + SEGMENT_RECORD_VALID_OFFSET);
		}
			
		/**
		 * Set the validity indicator of a segment
		 * 
		 * @param ref the position in the main file of the
		 * segment to be reinstated
		 * 
		 * @param validity the value to be set as the validity indicator
		 * 
		 * @throws IOException
		 */
		 
		private synchronized void setSegmentValidity(int ref, byte validity) 
				throws IOException  {
			
			ByteBuffer buffer = ByteBuffer.allocate(1);
			buffer.put(validity);
			buffer.position(0);
			channel.write(buffer,
					ref + SEGMENT_RECORD_VALID_OFFSET);
		}
		
		/**
		 * Update the summary stored in a summary triple record
		 * in the main file.
		 * 
		 * @param segmentStart The start of the segment containing
		 * the summary record
		 * 
		 * @param summary the triple object's summary value
		 * 
		 * @throws IOException 
		 */
		
		 private synchronized void updateTripleSummary(
				int segmentStart, long summary) 
						throws IOException {
			
			ByteBuffer buffer = ByteBuffer.allocate(8);
			buffer.putLong(summary);
			buffer.position(0);
			channel.write(buffer,
					segmentStart + SEGMENT_DATA_OFFSET +
					TRIPLE_OBJECT_SUMMARY_OFFSET);
		}
		
		/**
		 * Ensure that all data is written to the channel's storage medium
		 * 
		 * @throws IOException 
		 */
		
		 private synchronized void flush() 
				 throws IOException {
			
			// Need to ensure that file metadata is written or
			// the segments at the end of the file may be lost
			// when an error occurs			
			channel.force(true);
		 }
		
		/**
		 * Close the channel
		 * 
		 * @throws IOException 
		 */
		
		 private synchronized void close() throws IOException {
			 
			channel.close();
		 }
		
		/**
		 * <p>Start a re-use cycle.
		 * </p>
		 * <p>Ensure that all recently-used segments are written to
		 * disc, and stop considering them as recently used so
		 * that they can be made available for-reuse once they
		 * become free again.
		 * </p>
		 * @return a set containing the segment type lengths for
		 * which the size of the pool is below the nominal size
		 * 
		 * @throws IOException 
		 */
		
		private synchronized Set<Integer> startReuseCycle()
				throws IOException {
			
			// Ensure that all data is written to disc. No need to
			// ensure metadata is written, though.
			channel.force(false);

			// The segments can now be checked for availability
			// for reuse. Indicate this and check which segment
			// types need more segments in their pools.
			Set<Integer> result = new HashSet<Integer>();
			for (SegmentType st : segmentTypes) {
				st.locked.clear();
				if (st.pool.size() < st.poolSize)
					result.add(st.length);
			}
			return result;
		}
		
		/**
		 * Allocate a new segment at the end of the file
		 * 
		 * @param st the type of segment to allocate
		 * 
		 * @return a segment of the requested type
		 */
		
		private synchronized Segment getNewSegment(SegmentType st) {
			
			int start = endOfChannelSegments;
			endOfChannelSegments = endOfChannelSegments + st.length;
			return new Segment(start, st.length);
		}
		
		/**
		 * Return an unused segment from the pool if there is one
		 * 
		 * @param st the type of segment to allocate
		 * 
		 * @return a segment of the requested type if there is one,
		 * null otherwise
		 */
		
		private synchronized Segment getFreeSegment(SegmentType st) {
			
			if (!st.pool.isEmpty()) {
				for (Integer start : st.pool) {
					st.pool.remove(start);
					st.locked.add(start);
					return new Segment(start, st.length);
							// Just returns the first one found
				}
			}
			return null;
		}
		
		/**
		 * Check whether a segment can be made available by adding
		 * it to the pool of its segment type. If so, note that it
		 * is in process of being made available.
		 * 
		 * @param st the type of the segment
		 * 
		 * @param start the start byte of the segment
		 * in the main file
		 * 
		 * @return true if the segment can be added to the pool of its
		 * segment type, false if there was no type for that segment
		 * or if the pool of the type was full or if the segment is
		 * already in the pool or is in use.
		 */
		
		private synchronized boolean canMakeSegmentAvailable(
									SegmentType st, int start) {
			
			// Check that the segment is not already in the pool
			// of available segments, has not recently been re-used,
			// and is not in process of being made available
			if (st.pool.contains(start) ||
				st.locked.contains(start)) return false;
			
			// Note that the segment is in process of being made
			// available
			st.locked.add(start);
			
			// The segment can now be made available
			return true;
			
		}
		
		/**
		 * Make a segment available by adding it to the pool of
		 * its segment type
		 * 
		 * @param st the type of the segment
		 * 
		 * @param start the start byte of the segment
		 * in the main file
		 */
		
		private synchronized void makeSegmentAvailable(
								SegmentType st, int start) {

			// Add the segment to the pool
			st.addSegment(start);
		}

	}
	
	/*
	 ***********************************************************************
	 *
	 *  READ OR WRITE BIG OBJECTS FROM OR TO FILES IN THE BIG OBJECTS
	 *  DIRECTORY
	 *  
	 ***********************************************************************
	 */
	
	/**
	 * Write a big text triple object to a file in the big object
	 * directory.
	 * 
	 * @param string a string containing the first part of the
	 * text object.
	 * 
	 * @param reader a BufferedReader from which the remainder of
	 * the object can be read.
	 * 
	 * @param f the file to which the object is to be written.
	 * 
	 * @return the object's summary value
	 * 
	 * @throws IOException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private long writeBigTextTripleObject(
			String string, BufferedReader reader, File f) 
					throws IOException, TripleStoreNonFatalException {
		
		OutputStreamWriter w;
		w = new OutputStreamWriter(
				new BufferedOutputStream(
						new FileOutputStream(f)),
						utf8);
		long summary = 0;
		try {
			if (string != null) {
				w.write(string);
				summary = Datum.updateSummary(summary, string);
			}
			if (reader != null) {
				int c;
				while ((c = reader.read()) >= 0) {
					w.write(c);
					summary = Datum.updateSummary(summary, (char) c);
				}
			}
		} finally {
			w.close();
		}
		return summary;
	}

	/**
	 * Write a big binary triple object to a file in the big object
	 * directory.
	 * 
	 * @param bytes a byte array containing the first part of the
	 * binary object.
	 * 
	 * @param len the number of bytes in the array that make up the
	 * first part
	 * 
	 * @param stream a BufferedInputStream from which the remainder of
	 * the object can be read.
	 * 
	 * @param f the file to which the object is to be written.
	 * 
	 * @return the object's summary value
	 * 
	 * @throws IOException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private long writeBigBinaryTripleObject(
			byte[] bytes, int len, BufferedInputStream stream, File f)
					throws IOException, TripleStoreNonFatalException {
		BufferedOutputStream s;
		s = new BufferedOutputStream(new FileOutputStream(f));
		long summary = 0;
		try {
			if (bytes != null) {
				s.write(bytes, 0, len);
				summary = Datum.updateSummary(summary, bytes, len);
			}
			if (stream != null) {
				byte[] buf = new byte[8192];
				int n = 0;
				while ((n = stream.read(buf, 0, buf.length)) > 0) {
					s.write(buf, 0, n);
					summary = Datum.updateSummary(summary, buf, n);
				}
			}
		} finally {
			s.close();
		}
		return summary;
	}

	/*
	 ***********************************************************************
	 *
	 *  MAINTENANCE
	 *  
	 ***********************************************************************
	 */	

	/**
	 * <p>Perform a cycle of maintenance operations.
	 * </p>
	 * <p>These operations are performed concurrently with
	 * normal backingStore access. They include re-allocating
	 * disc space used for records deleted long-enough ago
	 * for this to be safe, and maintaining the notes of
	 * which transactions have completed.
	 * </p>
	 * @return true if the cycle of maintenance operations
	 * completed successfully, false otherwise.
	 * 
	 * @throws FileStoreException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws BackingStoreClosedException
	 * @throws FileTransactionManagerClosedException
	 * @throws FileTransactionManagerException
	 * @throws SourceConnectorException 
	 */
	
	private void performMaintenance() 
			throws 	FileStoreException, 
					IOException, 
					InterruptedException, 
					BackingStoreClosedException,
					FileTransactionManagerClosedException, 
					FileTransactionManagerException,
					SourceConnectorException {
		
		if (closed) throw(new BackingStoreClosedException());
		if (maintainer.maintenanceNotInProgress()) return;
		reUseOldRecords();
		maintainer.allowMaintenance(true);
	}
	
	/**
	 * Re-allocate disc space used for records deleted
	 * long-enough ago for this to be safe
	 * 
	 * @return true if the operation completed
	 * successfully, false otherwise.
	 * 
	 * @throws FileStoreException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws FileTransactionManagerClosedException
	 * @throws FileTransactionManagerException
	 * @throws SourceConnectorException 
	 */
	
	private void reUseOldRecords()
			throws 	FileStoreException, 
					IOException, 
					InterruptedException,
					FileTransactionManagerClosedException, 
					FileTransactionManagerException, 
					SourceConnectorException {
		
		int segmentCount = 0;
		int freeSegments = 0;
		SegmentEnumeration segmentEnumeration =
				new SegmentEnumeration();
		SegmentRecord segmentRecord;
		Set<Integer> lengthsToConsider =
				mainChannelAccessor.startReuseCycle();
		while ((segmentRecord = segmentEnumeration.nextSegment())
				!= null) {
			segmentCount++;
			if (	(	!segmentRecord.transPresent ||
						(	!segmentRecord.isValid() &&				
							connector.isCompleted(segmentRecord.transSeq)
						)
					) &&
					lengthsToConsider.contains(segmentRecord.segment.length)
				) {
				// The segment is potentially available for re-use,
				// being not associated with a transaction, or
				// having been marked as invalid by a completed
				// transaction, and the level of the pool for its
				// type is low.
					
				// Find the segment type for this length
				SegmentType st = getSegmentType(
						segmentRecord.segment.length);

				// If the segment cannot be made available then move on
				// to the next potential segment
					
				if (mainChannelAccessor.canMakeSegmentAvailable(
							st, segmentRecord.segment.start)) {
					
					// If there is an associated big file, try to delete it.
					// There may be a datum containing a reader for the
					// file, in which case it cannot be deleted. This is not
					// a problem, provided that the segment is not re-used.
					if ((segmentRecord.recordType ==
							RECORD_TYPE_BIG_BINARY_TRIPLE) ||
						(segmentRecord.recordType == 
							RECORD_TYPE_BIG_TEXT_TRIPLE)) {
						File f = new File(
								bigFileDirectory,
								Long.toString(segmentRecord.segment.start));
					if (f.exists()) {
						if (f.delete()) {
							mainChannelAccessor.makeSegmentAvailable(
										st, segmentRecord.segment.start);
							freeSegments++;
						}
					}
					else {
						// No associated big file - just make the segment
						// available
						mainChannelAccessor.makeSegmentAvailable(
									st, segmentRecord.segment.start);
						freeSegments++;					
					}
					}
					else {
						// No associated big file - just make the segment
						// available
						mainChannelAccessor.makeSegmentAvailable(
									st, segmentRecord.segment.start);
						freeSegments++;					
					}
				}
			}
			if (Thread.interrupted()) {
				throw (new FileStoreException(
							"Segment maintenance interrupted"));
			}
		}
		// Create report for logging - not currently used
		String s =  "Cycle of " + segmentCount + " segments completed. " +
					freeSegments + " were free. ";
		for (Integer length : lengthsToConsider) {
			SegmentType st = getSegmentType(length);
			s =  s +"Segments of length " + st.length +
					": pool has " + st.pool.size() + " of " + st.poolSize + ". ";
		}
	}
	
	/**
	 * The backing store creates a single instance of the BackingStoreMaintainer
	 * class. This runs in a separate thread and performs regular maintenance
	 * operations.
	 *
	 */
	
	private class BackingStoreMaintainer extends Thread implements Serializable {
		
		private static final long serialVersionUID = -7920597547248521156L;

		/**
		 * Whether maintenance can currently be carried out
		 */
		
		private Boolean maintenanceAllowed = false;

		/**
		 * The store to be maintained
		 */
		
		FileBackingStore store;
		
		/**
		 * The number of milliseconds between maintenance cycles
		 */
		
		int maintenanceInterval;

		/**
		 * Create a backing store maintainer
		 * 
		 * @param store the store to be maintained
		 * 
		 * @param maintenanceInterval the number of milliseconds 
		 * between maintenance cycles
		 */
		
		private BackingStoreMaintainer(
				FileBackingStore store,
				int maintenanceInterval) {
			
			this.store = store;
			this.maintenanceInterval = maintenanceInterval;
		}
		
		/**
		 * Set whether maintenance operations can be performed
		 * 
		 * @param whether true if maintenance operations are to
		 * be performed, false otherwise
		 */
		
		private synchronized void allowMaintenance(boolean whether) {
			maintenanceAllowed = whether;
		}
		
		/**
		 * <p>If maintenance is allowed, note that it is not
		 * allowed.
		 * </p>
		 * <p>Whether it was allowed when the method was invoked
		 * or not, maintenance will not be allowed when the
		 * method returns.
		 * </p>
		 * @return true if maintenance was not allowed when the
		 * method was invoked, false otherwise.
		 */
		
		private synchronized boolean maintenanceNotInProgress() {
			if (maintenanceAllowed) {
				maintenanceAllowed = false;
				return false;
			}
			else return true;
		}
		
		public void run() {
			try {
				while (true) {
					store.performMaintenance();
					sleep(maintenanceInterval);
					if(isInterrupted()) break;
				}
			} catch (BackingStoreClosedException e) {
				// Store is closed
			} catch (InterruptedException e) {
				maintenanceAbort(e);
			} catch (FileStoreException e) {
				maintenanceAbort(e);
			} catch (IOException e) {
				maintenanceAbort(e);
			} catch (FileTransactionManagerException e) {
				maintenanceAbort(e);
			} catch (FileSourceConnectorException e) {
				maintenanceAbort(e);
			} catch (SourceConnectorException e) {
				maintenanceAbort(e);
			}
		}		
	}

	/*
	 ***********************************************************************
	 *
	 *  SEGMENTS
	 *  
	 ***********************************************************************
	 */
	
	/**
	 * Add a segment type
	 * 
	 * @param length the length that characterizes the type of segment
	 * 
	 * @param poolSize the size of the pool of unused segments of this type
	 */
	
	private void addSegmentType(int length, int poolSize) {
		
		if (length > maxSegmentSize) maxSegmentSize = length;
		int payLoad = length - (SEGMENT_DATA_OFFSET + 
								TRIPLE_OBJECT_DATA_OFFSET);
		if (payLoad > maxSegmentDataLength) maxSegmentDataLength = payLoad;
		for (int i = 0; i < segmentTypes.size(); i++) {
			int l = segmentTypes.get(i).length;
			if (l == length) return;	// This type already there
			if (l > length) {
				segmentTypes.add(i, new SegmentType(length, poolSize));
										// Insert new type in the list
				return;
			}
		}
		segmentTypes.add(new SegmentType(length, poolSize));		
										// Add new type to the end of
										// the list.
	}
	
	/**
	 * Get the type of a segment that can be used to store a record
	 * or summary of a record
	 * 
	 * @param length the length of segment required
	 *
	 * @return a free segment that is either at least as long as
	 * the given length or long enough to store a record summary.
	 */
	
	private SegmentType getBestFitSegmentType(int length) {
		
		if (length > segmentTypes.get(segmentTypes.size()-1).length) {
			// There is no segment that is  at least as long as
			// the given length. Get a segment that is long enough
			// to backingStore a record summary.
			length =  SEGMENT_DATA_OFFSET + SSB_TRIPLE_DATA_SIZE;
		}
		
		// Find the segment type for this length
		SegmentType st = null;
		for (int i = 0; i < segmentTypes.size(); i++) {
			st = segmentTypes.get(i);
			if (st.length >= length) break;
		}
		
		return st;
	}
		
	/**
	 * Get the segment type for a given segment length.
	 * 
	 * @param length the segment length
	 * 
	 * @return the segment type associated with that length,
	 * or null if there is no such type.
	 */
	
	private SegmentType getSegmentType(int length) {
		for (int i = 0; i < segmentTypes.size(); i++) {
			SegmentType st = segmentTypes.get(i);
			if (st.length == length) return st;
		}
		return null;
	}
	
	/**
	 * An instance of this class represents a segment used to
	 * store a record or a record summary
	 */
	
	public class Segment implements Comparable<Segment>, Serializable {
		
		private static final long serialVersionUID = -2561029498291122642L;

		/**
		 * The length of the segment, including header and data
		 */
		
		public int length;
		
		/**
		 * The segment's position - the start byte of the segment
		 * in the main file
		 */
		
		public int start;
		
		/**
		 * Create a segment
		 * 
		 * @param start the start byte of the segment
		 * in the main file
		 * 
		 * @param length the length of the segment
		 */
		
		private Segment (int start, int length) {
			this.length = length;
			this.start = start;
		}

		@Override
		public int compareTo(Segment seg) {
			if (length < seg.length) return -1;
			if (length > seg.length) return 1;
			if (start < seg.start) return -1;
			if (start > seg.start) return 1;
			return 0;
		}
		
		public boolean equals (Object o) {
			try {
				return (compareTo((Segment) o) == 0);
			}
			catch (Exception e) {
				return false;
			}
		}
	}
	
	/**
	 * An instance of this class represents a type of segment,
	 * characterized by the segment length
	 */
	
	private class SegmentType implements Serializable {
		
		private static final long serialVersionUID = -406225340309020816L;

		/**
		 * The segment length.
		 */
		private int length;
		
		/**
		 * The start bytes in the main file of segments of this type
		 * that are available for re-use. This is the pool of
		 * available segments of the type.
		 */
		
		private Set<Integer> pool = new HashSet<Integer>();
		
		/**
		 * The nominal number of available segments in the pool. If the
		 * size of the pool is below this figure then more segments are
		 * added if possible during the maintenance cycle.
		 */	
		
		private int poolSize = 0;
		
		/**
		 * The start bytes in the main file of segments of this type
		 * that have been used recently or are in process of being made
		 * available and are therefore not available for re-use.
		 * These segments may not yet have been written to disc
		 * so the fact that they are not available can not be
		 * ascertained by reading them.
		 */
		
		private Set<Integer> locked = new HashSet<Integer>();
		
		/**
		 * Create a segment type
		 * 
		 * @param length the length that characterizes segments of
		 * this type
		 * 
		 * @param poolSize the maximum size of the pool of available
		 * segments
		 */
		
		private SegmentType(int length, int poolSize) {
			this.length = length;
			this.poolSize = poolSize;
		}
		
		/**
		 *  Add a segment to the pool
		 *  
		 *  @param start the start position of the segment
		 *  
		 *  @return true if the segment was added to the pool,
		 *  false otherwise
		 */
		
		private boolean addSegment(int start) {
			return pool.add(start);
		}
	}
	
	/**
	 * An instance of this class describes a segment
	 */
	
	public class SegmentRecord implements Serializable {
		
		private static final long serialVersionUID = -2563942552555802972L;

		/**
		 * The segment that is described by the segment record
		 */
		
		public Segment segment;
		
		/**
		 * The type of the segment
		 */
		
		public short recordType;
		
		/**
		 * Whether the record is valid
		 */
		
		public short recordValidIndicator;
		
		/**
		 * Whether the record describes a segment written as part of a 
		 * transaction
		 */
		
		public boolean transPresent;
		
		/**
		 * If the record describes a segment written as part of a 
		 * transaction, the sequence number of the transaction
		 */	
		
		public long transSeq;	
		
		/**
		 * @return whether the record is valid
		 */
		
		public boolean isValid() {
			
			return (	(recordValidIndicator == 
							VALIDITY_NO_INFO) ||
						(recordValidIndicator == 
							VALIDITY_VALID_REINSTATE) ||
						(recordValidIndicator == 
							VALIDITY_VALID_NOREINSTATE));
		}
		
		public String toString() {
			
			return "SegmentRecord at " + segment.start + ' ' +
					printRecordType(recordType) + ' ' +
					printRecordValidity(recordValidIndicator) +
					" trans: " + transPresent + ", " + transSeq;
		}
	}
	
	/**
	 * An instance of this class describes a segment containing
	 * an item
	 */
	
	public class ItemSegmentRecord extends SegmentRecord implements Serializable {
		
		private static final long serialVersionUID = 6573944704809500386L;

		/**
		 * The numeric identifier of the item in its source
		 */
		
		public long itemNr;
		
		/**
		 * The numeric identifier of the source of the only-read
		 * access level of the item
		 */
		
		public long readAccessLevelSourceNr;
		
		/**
		 * The numeric identifier in its source of the only-read
		 * access level of the item
		 */
		
		public long readAccessLevelItemNr;
		
		/**
		 * The numeric identifier of the source of the read-or-write
		 * access level of the item
		 */
		
		public long writeAccessLevelSourceNr;
		
		/**
		 * The numeric identifier in its source of the read-or-write
		 * access level of the item
		 */
		
		public long writeAccessLevelItemNr;
		
		public String toString() {
			
			return super.toString() + " for item " + itemNr;
		}
	}
	
	/**
	 * An instance of this class describes a segment containing
	 * a triple
	 */
	
	public abstract class TripleSegmentRecord extends SegmentRecord implements Serializable {
				
		private static final long serialVersionUID = 7921318174459594490L;

		/**
		 * The numeric identifier of the triple in its source
		 */
		
		public long tripleNr;
		
		/**
		 * The numeric identifier of the subject of the triple in its source
		 */
		
		public long subjectItemNr;
		
		/**
		 * The numeric identifier of the source of the verb of the triple
		 */
		
		public long verbSourceNr;
		
		/**
		 * The numeric identifier of the verb of the triple in its source
		 */
		
		public long verbItemNr;
		
		/**
		 * The type of the object of the triple
		 */
		
		public int objectType;
		
		/**
		 * Get the object of the triple
		 * 
		 * @param connector the source connector of the source of the triple
		 * 
		 * @return the object of the triple
		 */
		
		public abstract Datum getObjectDatum(BackingStoreSourceConnector connector);
		
		public String toString() {
			
			return super.toString() + " for triple " + tripleNr + " <" + 
			connector.getSourceNr() + ':' + subjectItemNr + ',' + 
			verbSourceNr + ':' + verbItemNr + ','  + 
			datumType(objectType) + '>';
		}
	}
		
	/**
	 * An instance of this class describes a segment containing
	 * a boolean, integral, real, text or binary triple. 
	 */
	
	private class NonItemTripleSegmentRecord extends TripleSegmentRecord implements Serializable {
		
		private static final long serialVersionUID = -3661304050199700572L;
		/**
		 * The summary of the value of the object of the triple
		 */
		
		long objectSummary;
		
		public Datum getObjectDatum(BackingStoreSourceConnector connector) {
			
			switch(objectType) {
			case BOOLEANINDICATOR : return Datum.create(
					Datum.getBooleanValueFromSummary(objectSummary));
			case INTEGRALINDICATOR :  return Datum.create(
					Datum.getIntegralValueFromSummary(objectSummary));
			case REALINDICATOR :  return Datum.create(
					Datum.getRealValueFromSummary(objectSummary));
			case TEXTINDICATOR : return Datum.createText(
					connector.getSourceNr(), tripleNr, objectSummary);
			case BINARYINDICATOR : return Datum.createBinary(
					connector.getSourceNr(), tripleNr, objectSummary);
			default : return null;
			}
		}
		
		public String toString() {
			
			return super.toString() + "Object " + getObjectString();
		}
		
		private String getObjectString() {
			
			switch(objectType) {
			case BOOLEANINDICATOR : 
				return Boolean.toString(Datum.getBooleanValueFromSummary(objectSummary));
			case INTEGRALINDICATOR :  
				return Long.toString(Datum.getIntegralValueFromSummary(objectSummary));
			case REALINDICATOR :  
				return Double.toString(Datum.getRealValueFromSummary(objectSummary));
			case TEXTINDICATOR : 
				String text; 
				try {
					Datum dt = getTextTripleObject(segment.start);
					BufferedReader r = dt.getTextValueReader();
					char[] buf = new char[32];
					try {
						int l = r.read(buf);
						if (l >= 0) text = new String(buf, 0, l);
						else text = "";
						if (l == buf.length) text = text + " . . .";
					} finally {
						r.close();
					}
				} catch (Exception e) {
					text = "Cannot read object: " + e.getMessage();
				}
				return text;
			case BINARYINDICATOR : 
				String btext;
				try {
					Datum db = getBinaryTripleObject(segment.start);
					BufferedInputStream s = db.getBinaryValueStream();
					try {
						byte[] bin = new byte[32];
						int lb = s.read(bin);
						if (lb >= 0) btext = bin.toString();
						else btext = "";
						if (lb == bin.length) btext = btext + " . . .";
					} finally {
						s.close();
					}
				} catch (Exception e) {
					btext = "Cannot read object: " + e.getMessage();
				}
				return btext;
			default : return null;
			}
		}
	}
			
	
	/**
	 * An instance of this class describes a segment containing
	 * an item triple
	 */

	private class ItemTripleSegmentRecord extends TripleSegmentRecord implements Serializable {
		
		private static final long serialVersionUID = -411847906528968047L;
		
		long objectSourceNr;
		long objectItemNr;
		
		public Datum getObjectDatum(BackingStoreSourceConnector connector) {
			
			return Datum.create(objectSourceNr, objectItemNr);
		}
		
		public String toString() {
			
			return super.toString() + "Object " + objectSourceNr + ':' + objectItemNr;
		}
	}
		
	
	/**
	 * An instance of the SegmentEnumeration class generates
	 * a series of segments, one at a time, in ascending order of
	 * start field. Successive calls to the nextSegment method
	 * return successive segments. 
	 */
	
	private class SegmentEnumeration implements Serializable {
		
		private static final long serialVersionUID = -1696402540523013406L;

		/**
		 * The position where the next segment 
		 * starts. If the end of the file has been
		 * reached, this will be the next segment to
		 * be written to extend the file.
		 */
		
		int nextSegmentStart = firstSegment;
		
		/**
		 * The position in the main channel up to which segments
		 * are to be enumerated.
		 */
		
		int segmentLimit = 0;
		
		/**
		 * A buffer used to hold segments
		 */
		
		ByteBuffer buffer = ByteBuffer.allocate(8192);
		
		/**
		 * Whether the last segment has been reached
		 */
		
		boolean lastSegmentReached = false;
		
		/**
		 * Create a segment enumeration
		 */
		
		private SegmentEnumeration() {
			
			// Set the limit 
			// If the end of the segments has been set,
			// make it the limit beyond which no more
			// segments will be retrieved
			segmentLimit = mainChannelAccessor.getEndOfChannelSegments();
						
			// Start the buffer with no data remaining
			buffer.position(buffer.limit());
		}
		
		/**
		 * @return the position where the next segment 
		 * starts. If the end of the file has been
		 * reached, this will be the next segment to
		 * be written to extend the file.
		 */
		
		private int nextSegmentStart() {
			return nextSegmentStart;
		}
		
		/**
		 * Get the next segment record
		 * 
		 * @return the next segment record, or null if there
		 * are none left
		 * 
		 * @throws FileStoreException 
		 * @throws IOException
		 * @throws InterruptedException 
		 */

		private SegmentRecord nextSegment() 
				throws 	FileStoreException, 
						IOException, 
						InterruptedException {

			// If the last segment has been reached, return null
			if (lastSegmentReached) return null;
			
			// If the segment limit has been set,
			// and we have reached it, return null
			if (	(segmentLimit > 0) &&
					(nextSegmentStart >= segmentLimit))
				return null;
			
			int segmentLength = 0;
			SegmentRecord segmentRecord;
			int positionInBuffer = buffer.position();

			// Does the buffer hold a segment?
			boolean holdsSegment;
			if (buffer.remaining() < 4) {
				holdsSegment = false;
			}
			else {
				segmentLength = buffer.getInt();
				holdsSegment = 
						(segmentLength < (buffer.remaining()+4));
			}
			if (!holdsSegment)	{
				// Note that reading the segment length 
				// decreases buffer.remaining() by 4
				// The buffer does not hold another full segment
				// Read more segments into the buffer
				buffer.position(0);
				buffer.limit(buffer.capacity());
				mainChannel.read(buffer, nextSegmentStart);
				buffer.limit(buffer.position());
				buffer.position(0);
				positionInBuffer = 0;
				if (buffer.remaining() < SEGMENT_DATA_OFFSET) {
					// End of segments reached
					lastSegmentReached = true;
					if (buffer.remaining() != 0) 	
						// There is spurious data at the end of the file
						; 
					return null;
				}
				
				segmentLength = buffer.getInt();

				// Have we reached the last segment?
				if (segmentLength >= buffer.remaining()+4) 
					lastSegmentReached = true;
			}
			if (segmentLength < SEGMENT_DATA_OFFSET) {
				if ((nextSegmentStart + mainChannel.size()) >=
						mainChannel.size()) {
					// Assume we have reached the end of the file
					// and there is spurious data
					lastSegmentReached = true;
					return null;
				}
				throw (new FileStoreException(
						"Invalid segment length " + segmentLength  +
						" at " + nextSegmentStart));
			}
			// The buffer holds at least the next segment header
			// and is positioned after the segment length field.
			// The segment length is stored in segmentLength.
			
			// Read the segment header
			short recordType = buffer.getShort();
			if (recordType == RECORD_TYPE_ITEM)
				segmentRecord = new ItemSegmentRecord();
			else if (recordType == RECORD_TYPE_ITEM_TRIPLE)
				segmentRecord = new ItemTripleSegmentRecord();
			else if (isTripleRecordType (recordType))
				segmentRecord = new NonItemTripleSegmentRecord();
			else segmentRecord = new SegmentRecord();
			segmentRecord.recordType = recordType;
			short valid = (short) buffer.get();
			switch (valid) {
			case VALIDITY_NO_INFO :
			case VALIDITY_VALID_NOREINSTATE :
			case VALIDITY_VALID_REINSTATE :
			case VALIDITY_INVALID_REINSTATE :
			case VALIDITY_INVALID_NOREINSTATE :
				segmentRecord.recordValidIndicator = valid;
				break;
			default :
				if ((nextSegmentStart + maxSegmentSize) >=
						mainChannel.size()) {
					// Assume we have reached the end of the file
					// and there is spurious data
					lastSegmentReached = true;
					return null;
				}
				throw (new FileStoreException(
						"Invalid segment valid indicator at " +
						nextSegmentStart));
			}
			segmentRecord.transPresent = (buffer.get() != 0);
			segmentRecord.transSeq = buffer.getLong();

			// Set up the segment information
			segmentRecord.segment = new Segment(
					nextSegmentStart, segmentLength);
			
			// Read the item or triple information
			if (segmentRecord instanceof ItemSegmentRecord) {
				ItemSegmentRecord itemSegmentRecord =
						(ItemSegmentRecord)segmentRecord;
				
				// Check that enough data has been read
				if (buffer.remaining() < ITEM_DATA_SIZE) {
					if (lastSegmentReached) return null;
					else {
						if ((nextSegmentStart + maxSegmentSize) >=
								mainChannel.size()) {
							// Assume we have reached the end of the file
							// and there is spurious data
							lastSegmentReached = true;
							return null;
						}
						throw (new FileStoreException(
								"Invalid item segment at " + nextSegmentStart));
					}
				}
				
				// Read the item information
				itemSegmentRecord.itemNr = buffer.getLong();
				itemSegmentRecord.readAccessLevelSourceNr = buffer.getLong();
				itemSegmentRecord.readAccessLevelItemNr = buffer.getLong();
				itemSegmentRecord.writeAccessLevelSourceNr = buffer.getLong();
				itemSegmentRecord.writeAccessLevelItemNr = buffer.getLong();
			}
			else if (segmentRecord instanceof ItemTripleSegmentRecord) {
				ItemTripleSegmentRecord itemTripleSegmentRecord =
						(ItemTripleSegmentRecord)segmentRecord;
				
				// Check that enough data has been read
				if (buffer.remaining() < ITEM_TRIPLE_DATA_SIZE) {
					if (lastSegmentReached) return null;
					else {
						if ((nextSegmentStart + maxSegmentSize) >=
								mainChannel.size()) {
							// Assume we have reached the end of the file
							// and there is spurious data
							lastSegmentReached = true;
							return null;
						}
						throw (new FileStoreException(
								"Invalid triple segment at " + nextSegmentStart));
					}
				}

				// Read the triple information
				itemTripleSegmentRecord.tripleNr = buffer.getLong();
				itemTripleSegmentRecord.subjectItemNr = buffer.getLong();
				itemTripleSegmentRecord.verbSourceNr = buffer.getLong();
				itemTripleSegmentRecord.verbItemNr = buffer.getLong();
				itemTripleSegmentRecord.objectType = buffer.getInt();
				itemTripleSegmentRecord.objectSourceNr = buffer.getLong();
				itemTripleSegmentRecord.objectItemNr = buffer.getLong();
			}
			else if (segmentRecord instanceof NonItemTripleSegmentRecord) {
				NonItemTripleSegmentRecord nonItemTripleSegmentRecord =
						(NonItemTripleSegmentRecord)segmentRecord;
				
				// Check that enough data has been read
				if (buffer.remaining() < SSB_TRIPLE_DATA_SIZE) {
					if (lastSegmentReached) return null;
					else {
						if ((nextSegmentStart + maxSegmentSize) >=
								mainChannel.size()) {
							// Assume we have reached the end of the file
							// and there is spurious data
							lastSegmentReached = true;
							return null;
						}
						throw (new FileStoreException(
								"Invalid triple segment at " + nextSegmentStart));
					}
				}

				// Read the triple information
				nonItemTripleSegmentRecord.tripleNr = buffer.getLong();
				nonItemTripleSegmentRecord.subjectItemNr = buffer.getLong();
				nonItemTripleSegmentRecord.verbSourceNr = buffer.getLong();
				nonItemTripleSegmentRecord.verbItemNr = buffer.getLong();
				nonItemTripleSegmentRecord.objectType = buffer.getInt();
				nonItemTripleSegmentRecord.objectSummary = buffer.getLong();
			}
			
			// Prepare to read the next segment
			if ((positionInBuffer + segmentLength) < buffer.limit())
				buffer.position(positionInBuffer + segmentLength);
			else buffer.position(buffer.limit());
			nextSegmentStart = nextSegmentStart + segmentLength;

			return segmentRecord;
		}		
	}	
	
	/*
	 ***********************************************************************
	 *
	 *  DIAGNOSICS
	 *  
	 ***********************************************************************
	 */
	
	@Override
	public void simulateIoError() {
		try {
			mainChannel.close();
		}
		catch(Exception e) {}
	}
	
	/**
	 * Get the item segment records, grouped by item
	 *  
	 * @return A map from item numbers to lists of segment records for
	 * the numbered items. Used for maintenance and diagnostic purposes.
	 * 
	 * @throws Exception
	 */
	
	public Map<Long, List<ItemSegmentRecord>> getItemRecords() throws Exception {
		
		Map<Long, List<ItemSegmentRecord>> itemSegmentRecords = 
				new HashMap<Long, List<ItemSegmentRecord>>();
		SegmentEnumeration segmentEnumeration = new SegmentEnumeration();
		SegmentRecord segmentRecord;
		while ((segmentRecord = segmentEnumeration.nextSegment()) != null) {
			if (segmentRecord instanceof  ItemSegmentRecord) {
				ItemSegmentRecord isr = (ItemSegmentRecord)segmentRecord;
				List<ItemSegmentRecord> list = itemSegmentRecords.get(isr.itemNr);
				if (list == null) list = new LinkedList<ItemSegmentRecord>();
				list.add(isr);
				itemSegmentRecords.put(isr.itemNr, list);
			}
		}
		return itemSegmentRecords;
	}
	
	/**
	 * Get the triple segment records, grouped by triple
	 *  
	 * @return A map from triple numbers to lists of segment records for
	 * the numbered triples. Used for maintenance and diagnostic purposes.
	 * 
	 * @throws Exception
	 */
	
	public Map<Long, List<TripleSegmentRecord>> getTripleRecords() throws Exception {
		
		Map<Long, List<TripleSegmentRecord>> tripleSegmentRecords = 
				new HashMap<Long, List<TripleSegmentRecord>>();
		SegmentEnumeration segmentEnumeration = new SegmentEnumeration();
		SegmentRecord segmentRecord;
		while ((segmentRecord = segmentEnumeration.nextSegment()) != null) {
			if (segmentRecord instanceof  TripleSegmentRecord) {
				TripleSegmentRecord tsr = (TripleSegmentRecord)segmentRecord;
				List<TripleSegmentRecord> list = tripleSegmentRecords.get(tsr.tripleNr);
				if (list == null) list = new LinkedList<TripleSegmentRecord>();
				list.add(tsr);
				tripleSegmentRecords.put(tsr.tripleNr, list);
			}
		}
		return tripleSegmentRecords;
	}
	
	/**
	 * Find the records of transactions with sequence numbers in a given range.
	 * Used for management and diagnostic purposes.
	 * 
	 * @param firstSeq the first sequence number in the range
	 * 
	 * @param lastSeq the last sequence number in the range
	 * 
	 * @return a printout of the records of transactions in the range 
	 * that were found. Note that a transaction may appear in more than one
	 * record.
	 */
	
	public String findTransactionRecords(long firstSeq, long lastSeq) {
	
		StringBuilder builder = new StringBuilder();
		try {
			initialise(false);
			SegmentEnumeration segmentEnumeration = new SegmentEnumeration();
			SegmentRecord segmentRecord;
			long time = System.currentTimeMillis();
			int nrOfRecords = 0;
			int nrOfMatchingRecords = 0;
			while ((segmentRecord = segmentEnumeration.nextSegment()) != null) {
				if (	(segmentRecord.transSeq >= firstSeq) && 
						(segmentRecord.transSeq <= lastSeq)) {
					builder.append(segmentRecord.toString());
					builder.append('\n');
					nrOfMatchingRecords++;
				}
				nrOfRecords++;
				if (System.currentTimeMillis() > time + 60000) {
					System.out.println("Processed " + nrOfRecords + " records");
					time = time + 60000;
				}
			}
			builder.append("Found " + nrOfMatchingRecords + " in " + nrOfRecords + " records");
			return builder.toString();
		} catch (Exception e) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(baos));
			return baos.toString();
		} 
	}
	
	/**
	 * Find the records of actions on a particular triple.
	 * Used for management and diagnostic purposes.
	 * 
	 * @param tripleNr the identifying number of the triple
	 * 
	 * @return a printout of the records of actions on the triple
	 * that were found.
	 */
	
	public String findTripleRecords(long tripleIdNr) {
	
		StringBuilder builder = new StringBuilder();
		try {
			initialise(false);
			SegmentEnumeration segmentEnumeration = new SegmentEnumeration();
			SegmentRecord segmentRecord;
			long time = System.currentTimeMillis();
			int nrOfRecords = 0;
			int nrOfMatchingRecords = 0;
			while ((segmentRecord = segmentEnumeration.nextSegment()) != null) {
				if (segmentRecord instanceof  TripleSegmentRecord) {
					TripleSegmentRecord tsr = (TripleSegmentRecord) segmentRecord;
					if (tsr.tripleNr == tripleIdNr) {
						builder.append(segmentRecord.toString());
						builder.append('\n');
						nrOfMatchingRecords++;
					}
				}
				nrOfRecords++;
				if (System.currentTimeMillis() > time + 60000) {
					System.out.println("Processed " + nrOfRecords + " records");
					time = time + 60000;
				}
			}
			builder.append("Found " + nrOfMatchingRecords + " in " + nrOfRecords + " records");
			return builder.toString();
		} catch (Exception e) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(baos));
			return baos.toString();
		} 
	}
}
