package org.lacibus.backingstore;

import java.io.File;
import java.io.IOException;

import org.lacibus.triplesource.HighestIdentifier;
import org.lacibus.triplesource.SourceConnectorIOException;
import org.lacibus.triplesource.SourceConnectorStateException;

/**
 * A highest identifier that can be safely restored after
 * a restart because a safe value of it is stored in a
 * filesystem
 */

public class FiledHighestIdentifier extends HighestIdentifier {

	private static final long serialVersionUID = -6082216561553806024L;
	
	private final long safeMargin = 1000;
	
	private File directory;
	
	private long source;
	
	private long storedNextSafeValue; 

	/**
	 * Create a Filed Highest Identifier. The value is set
	 * to the stored safe value. 
	 * 
	 * @param directory the filesystem directory in which safe
	 * values are stored.
	 * 
	 * @throws SourceConnectorIOException 
	 * @throws SourceConnectorStateException 
	 */
	
	public FiledHighestIdentifier(File directory, long source) 
			throws SourceConnectorStateException, SourceConnectorIOException {
		
		this.directory = directory;
		this.source = source;
		storedNextSafeValue = getStoredSafeValue();
		setIfHigher(storedNextSafeValue);
	}
	
	public synchronized void setIfHigher(long newValue) 
			throws SourceConnectorStateException, SourceConnectorIOException {
		
		super.setIfHigher(newValue);
		if (newValue  >= storedNextSafeValue) updateStoredSafeValue(newValue + safeMargin);
	}
	
	public synchronized long next() 
			throws SourceConnectorStateException, SourceConnectorIOException {
		
		long next = super.next();
		if (next >= storedNextSafeValue) updateStoredSafeValue(next + safeMargin);
		return next;
	}
	
	private long getStoredSafeValue() 
			throws SourceConnectorStateException, SourceConnectorIOException {
		
		String[] files = directory.list();
		if (files.length == 0) {
			// No value is stored. Store and return the minimum value.
			File f = new File(directory, Long.toString(Long.MIN_VALUE));
			boolean created = false;
			try {
				created = f.createNewFile();
			} catch (IOException x) {}
			if (!created) throw (new SourceConnectorIOException(
					source, "Cannot create safe highest identifier file: " + f.getAbsolutePath()));
			return Long.MIN_VALUE;
		}
		else if (files.length == 1) {
			long value;
			try {
				value = Long.parseLong(files[0]);
			} catch (Exception e) {
				throw (new SourceConnectorStateException(
						source, "Invalid safe highest identifier file: " + files[0] +
						" in directory: " + directory.getAbsolutePath()));
			}
			return value;
		}
		else throw (new SourceConnectorStateException(
				source, "Invalid safe highest identifier directory: " + directory.getAbsolutePath()));
	}
	
	private void updateStoredSafeValue(long newValue) 
			throws SourceConnectorStateException, SourceConnectorIOException {
		
		String[] files = directory.list();
		if (files.length != 1) throw (new SourceConnectorStateException(
				source, "Invalid safe highest identifier directory: " + directory.getAbsolutePath()));
		String fileName = Long.toString(newValue);
		if (fileName.equals(files[0])) 
			// The safe value has not changed
			return;
		
		// Store the new safe value
		File f = new File(directory, fileName);
		boolean created = false;
		try {
			created = f.createNewFile();
		} catch (IOException x) {}
		if (!created) throw (new SourceConnectorIOException(
				source, "Cannot create safe highest identifier file: " + f.getAbsolutePath()));
		
		// Delete the previous stored value
		File oldFile = new File (directory, files[0]);
		if (!oldFile.delete())  throw (new SourceConnectorIOException(
				source, "Cannot delete old safe highest identifier file: " + oldFile.getAbsolutePath()));
		
		storedNextSafeValue = newValue;
	}
}
