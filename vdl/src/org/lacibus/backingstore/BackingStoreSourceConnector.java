/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.backingstore;

import java.io.File;

import org.lacibus.triplesource.SourceConnector;
import org.lacibus.triplesource.SourceConnectorClient;
import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplestore.Logger;

/**
 * A source connector that connects to a backing store as a source
 * of items and triples. 
 */

abstract class BackingStoreSourceConnector extends SourceConnector {

	private static final long serialVersionUID = 4035780630696849709L;

	/**
	 * Create a backing store source connector
	 * 
	 * @param client the client of the source connector
	 * 
	 * @param sourceNr the numeric identifier of the source that the
	 * connector is connected to
	 * 
	 * @throws SourceConnectorException 
	 */
	
	BackingStoreSourceConnector(
			SourceConnectorClient client, long sourceNr, boolean owner,
			File localDir, File specFile, Logger logger)
					throws SourceConnectorException {
		
		super(client, sourceNr, owner, localDir, specFile, logger);
	}
	
	/**
	 * Determine whether a transaction has completed
	 * 
	 * @param transSeq a transaction sequence number
	 * 
	 * @return true if the transaction with the given 
	 * sequence number has completed, false otherwise
	 * @throws FileSourceConnectorException 
	 */
	
	abstract boolean isCompleted(long transSeq) 
			throws SourceConnectorException;	
}
