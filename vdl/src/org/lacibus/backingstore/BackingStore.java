/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.backingstore;

import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Transaction;
import org.lacibus.triplestore.TripleStoreNonFatalException;
import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplesource.TransactionsStatus;

/**
 * Interface to a backing store used by a connector to a source 
 * of items and triples.
 */

interface BackingStore {

	/**
	 * Initialise the backing store
	 * 
	 * @param clear whether the store is to be cleared of all
	 * content
	 * 
	 * @throws BackingStoreException 
	 */
	
	void initialise(boolean clear) 
			throws BackingStoreException;
	
	/**
	 * Load all the items and triples in the backing store
	 * into a backing store client
	 * 
	 * @param client the client into which the items and
	 * triples are to be loaded
	 * 
	 * @param status the transactions that have completed
	 * prior to the load operation
	 * 
	 * @throws BackingStoreException
	 */
	
	void load(BackingStoreClient client, TransactionsStatus status) 
					throws BackingStoreException;
	
	/**
	 * Add an item
	 * 
	 * @param itemNr the numeric identifier of the item in its source
	 * 
	 * @param onlyReadLevelSourceNr the numeric identifier of the source of 
	 * the item representing the only-read level
	 * 
	 * @param onlyReadLevelItemNr the numeric identifier in its source
	 * of the item representing the only-read level
	 * 
	 * @param readOrWriteLevelSourceNr the numeric identifier of the source of 
	 * the item representing the read-or-write level
	 * 
	 * @param readOrWriteLevelItemNr the numeric identifier in its source
	 * of the item representing the read-or-write level
	 * 
	 * @param trans the transaction of which this operation is part
	 * 
	 * @return a reference to the item in store
	 * 
	 * @throws BackingStoreException
	 */
	
	int addItem(
			long itemNr,
			long onlyReadLevelSourceNr,
			long onlyReadLevelItemNr,
			long readOrWriteLevelSourceNr,
			long readOrWriteLevelItemNr,
			Transaction trans)
					throws 	BackingStoreException;	

	/**
	 * Remove an item from the backingStore
	 * 
	 * @param storeRef the reference to the item in store.
	 * 
	 * @param trans the transaction of which this operation is part
	 * 
	 * @return a delta that describes the removal of the item, or null
	 * if the item is not in store
	 * 
	 * @throws BackingStoreException
	 * @throws BackingStoreClientException 
	 */
	
	void removeItem(
			int storeRef, 
			Transaction trans)
			   		throws 	BackingStoreException,
			   				BackingStoreClientException;

	/**
	 * Add a triple
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the
	 * subject in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the
	 * source of the verb
	 * 
	 * @param verbItemNr the numeric identifier of the
	 * verb in its source
	 * 
	 * @param object the triple's object
	 * 
	 * @param trans the transaction of which this operation is part
	 * 
	 * @return a reference to the triple in store with the
	 * object's summary value
	 * 
	 * @throws BackingStoreException
	 * @throws SourceConnectorException
	 * @throws BackingStoreClientException 
	 * @throws TripleStoreNonFatalException 
	 * 
	 */
	
	RefAndSummary addTriple(
			long tripleNr,
  			long subjectItemNr,
  			long verbSourceNr,
  			long verbItemNr,
  			Datum object,
  			Transaction trans)
					throws 	BackingStoreException, 
							SourceConnectorException,
							BackingStoreClientException,
							TripleStoreNonFatalException;
	
	/**
	 * Get a stored text triple's object
	 * 
	 * @param storeRef the reference to the triple in store
	 * 
	 * @return the triple's object as a StringDatum or a TextFileDatum. 
	 * This method never returns a TextSourceConnectorDatum
	 * 
	 * @throws ObjectNotInStoreException if the object has not been
	 * copied into the backing store from its source
	 * 
	 * @throws BackingStoreException
	 * @throws TripleStoreNonFatalException 
	 * @throws BackingStoreClosedException 
	 */
	
	Datum getTextTripleObject(int storeRef)
					throws 	BackingStoreException, TripleStoreNonFatalException;
	
	/**
	 * Get a stored binary triple's object
	 * 
	 * @param storeRef the reference to the triple in store
	 * 
	 * @return the triple's object
	 * 
	 * @throws BackingStoreException
	 * @throws TripleStoreNonFatalException 
	 */
	
	Datum getBinaryTripleObject(int storeRef)
					throws 	BackingStoreException, 
							TripleStoreNonFatalException;
	
	/**
	 * Remove a triple from the backingStore.
	 * 
	 * @param storeRef the reference to the triple in store
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @throws BackingStoreException 
	 * @throws BackingStoreClientException 
	 */
	
	void removeTriple(
			int storeRef, Transaction trans) 
					throws 	BackingStoreException, 
							BackingStoreClientException;
	
	/**
	 * Note that a transaction is started
	 * 
	 * @param t a transaction
	 * 
	 * @throws BackingStoreException 
	 */
	
	void transactionStarted(Transaction t)
			throws BackingStoreException;

	/**
	 * Note that a transaction is complete
	 * 
	 * @param t a completed transaction
	 * 
	 * @throws BackingStoreException 
	 */
	
	void transactionComplete(Transaction t)
			throws BackingStoreException;
	
	/**
	 * Close the backing backingStore
	 */
	
	void close();
	
	/**
	 * <p>Return an exception that caused backing store operation
	 * to abort.
	 * </p>
	 * <p>If an exception is thrown in the course of background
	 * processing by the backing store then operation of the
	 * backing store is aborted and the exception is stored.
	 * </p>
	 * @return the Exception that aborted operation
	 * 
	 */
	
	Exception getBackingStoreTerminator();
	
	/**
	 * Simulate an I/O error for testing purposes
	 */
	
	void simulateIoError();
	
}
