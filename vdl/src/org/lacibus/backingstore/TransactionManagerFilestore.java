/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.backingstore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.lacibus.triplestore.Logger;

/**
 * An instance of this class is a store of records of completed
 * transactions. The class includes maintenance methods that
 * remove old records so that the store does not become too 
 * large.
 */

class TransactionManagerFilestore {
	
	/**
	 * The largest size that a file of completed transaction sequence 
	 * numbers should have when it is checked in the maintenance cycle. 
	 * This is not an absolute limit, but if a file gets too big and 
	 * the source is re-loaded then the reload will be slow.
	 */
	
	static final long completedTransactionsFileLength = 10240;

	/**
	 * Directory containing the files in which completed
	 * transactions are recorded.
	 */
	
	private File directory = null;
	
	/**
	 * File in which records of completed transactions are stored.
	 * Multiple generations of this file are kept.
	 */
	
	private File completedTransactionsFile = null;
	
	/**
	 * File writer used to append records of completed transactions
	 * to the completed transactions file as completions are noted.
	 */
	
	private FileWriter completedTransactionsWriter = null;
	
	/**
	 * The line separator string.
	 */
	
	final String lineSeparator = 
			System.getProperty("line.separator");

	/**
	 * Period for which old files are kept in
	 * milliseconds
	 */
	
	final long oldFileDeletionPeriod = 7*24*3600*1000;	// 1 week

	/**
	 * Lock used to control access to the file store and
	 * ensure that it is only accessed by one thread at a time.
	 */
	
	private ReentrantLock lock = new ReentrantLock();
	
	@SuppressWarnings("unused")
	private Logger logger;
	
	/**
	 * Create a new file store
	 * 
	 * @param directory the directory in which the file store's
	 * files are kept
	 */
	
	public TransactionManagerFilestore(File directory, Logger logger) {
		this.directory = directory;
		this.logger = logger;
	}
	
	/**
	 * Prepare the file store for operation. 
	 * This involves establishing a new current file, 
	 * and cycling the backup and old files.
	 * 
	 * @return a list of sequence numbers. The list is null
	 * if the file store does not contain a valid set of
	 * completed transaction records. If the list is not null
	 * then it is not empty and its first member is the
	 * sequence number of a transaction such that all previous
	 * transactions have completed.
	 * 
	 * @throws InterruptedException 
	 * @throws IOException 
	 * 
	 */
	
	public List<Long> prepare()
			throws 	InterruptedException,
					IOException {
	
		lock.lockInterruptibly();
		List<Long> completedTransactionSequenceNumbers = null;
		try {
			List<Integer> transFilesList = 
					getTransactionFilenameInts();
			for (int i = transFilesList.size()-1; i >= 0; i--) {
				completedTransactionSequenceNumbers =
					readCompletedTransactions(
						transFilesList.get(i));
				if (completedTransactionSequenceNumbers != null)
						break;
				(new File(directory,
						name(transFilesList.get(i)))).delete();
				transFilesList.remove(i);
			}
			return completedTransactionSequenceNumbers;
		}
		finally {
			lock.unlock();
		}
	}
	
	/**
	 * Read the completed transactions from a file.
	 * 
	 * @param fileName the name of the file relative to
	 * the transactions file directory
	 * 
	 * @return a list of sequence numbers of completed
	 * transactions. The list is null if the file is empty
	 * or otherwise invalid. If the list is not null then
	 * it is not empty and its first member is the sequence
	 * number of a transaction such that all previous
	 * transactions have completed.
	 * 
	 * @throws IOException 
	 * 
	 */

	private List<Long> readCompletedTransactions(int n) 
			throws IOException {
	
		File inputFile = new File(directory, name(n));
		BufferedReader reader;
		reader = new BufferedReader(
				new FileReader(inputFile));
		try {
			return read(reader);
		}
		finally {
			reader.close();
		}
	}

	/**
	 * Read a list of completed transactions.
	 * A list in the form output by write() is read
	 * and noted.
	 * 
	 * @param r a BufferedReader via which the list is to
	 * be read.
	 * 
	 * @return a list of sequence numbers of completed
	 * transactions. The list is null if the file is empty
	 * or otherwise invalid. If the list is not null then
	 * it is not empty and its first member is the sequence
	 * number of a transaction such that all previous
	 * transactions have completed.
	 * 
	 * @throws IOException 
	 * 
	 */

	private List<Long> read(BufferedReader r) 
			throws IOException  {
		
		String line;
		int nrOfLines = 0;
		List<Long> sequenceNumbers = new ArrayList<Long>();
		while ((line = r.readLine()) != null) {
			nrOfLines++;
			try {
				sequenceNumbers.add(
						Long.parseLong(line.substring(
								0, line.indexOf(';'))));
			}
			catch (Exception e) {
				if (nrOfLines == 1)
					// The file is invalid. It must start
					// with a line consisting of a sequence
					// number terminated by a semicolon
					return null;
					
				// Errors on other lines are ignored because
				// the last line may contain an error if
				// operation terminated by exception last
				// time the transaction manager ran.
			}
		}
		if (nrOfLines == 0) 
			// The file is empty and therefore invalid
			return null;
		return sequenceNumbers;
	}
	
	/**
	 * Write a record indicating that a transaction
	 * is complete.
	 * 
	 * @param seq the transaction sequence number
	 * 
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws TransactionManagerFilestoreClosedException 
	 * 
	 */
	
	public void writeCompletedTransaction(long seq)
				throws 	InterruptedException, 
						IOException, 
						TransactionManagerFilestoreClosedException {
		
		lock.lockInterruptibly();
		try {
			writeSeq(seq);
		}
		finally {
			lock.unlock();
		}
	}
	
	/**
	 * Write a transaction sequence number.
	 * 
	 * @param seq the sequence number to be written
	 * 
	 * @throws IOException 
	 * @throws TransactionManagerFilestoreClosedException 
	 * 
	 */
	
	private void writeSeq(long seq) 
			throws 	IOException, 
					TransactionManagerFilestoreClosedException {
		
		if (completedTransactionsWriter == null) 
			throw(new TransactionManagerFilestoreClosedException());
		completedTransactionsWriter.write(
				Long.toString(seq) + ";" + lineSeparator);
		completedTransactionsWriter.flush();
	}

	/**
	 * Cycle the files used to store records of completed
	 * transactions. This means starting a new current
	 * file and deleting files that are older than a certain
	 * time period.
	 * 
	 * @throws InterruptedException 
	 * @throws IOException 
	 * @throws TransactionManagerFilestoreClosedException 
	 * 
	 */
	
	public void cycle(
			List<Long> completedTransactionsSequenceNumbers) 
					throws 	InterruptedException, 
							IOException,
							TransactionManagerFilestoreClosedException {

		lock.lockInterruptibly();
		try {
			// Return if the completed transactions file exists and is
			// not too long.
			if (	(completedTransactionsFile != null) && 
					(completedTransactionsFile.length() <
							completedTransactionsFileLength)) return;
			
			// The new current file will have a name that is
			// the next after that of the latest transactions
			// file if there is one
			String currentName;
			List<Integer> transFileInts = getTransactionFilenameInts();
			if (transFileInts.isEmpty()) currentName = "trans0";
			else {
				int n = transFileInts.get(transFileInts.size()-1);
				currentName = name(n+1);
			}
		
			// Set up the file writer used to append records of
			// completed transactions to the current file
			if (completedTransactionsWriter != null)
				completedTransactionsWriter.close();
			completedTransactionsFile =
						new File(directory, currentName);
			completedTransactionsWriter =
						new FileWriter(completedTransactionsFile);

			// Write the completed transactions to the new current file
			writeCompletedTransactions(
					completedTransactionsSequenceNumbers);			
		}
		finally {
			lock.unlock();
		}			
	}
	
	/**
	 * <p>Print out the sequence number before which all transactions
	 * have completed and the sequence numbers of recent completed
	 * transactions to the current file. 
	 * </p>
	 * <p>All transactions before the first sequence number printed
	 * will have completed. Uncompleted transactions can therefore
	 * be identified by having sequence numbers equal to or after
	 * the first one and not appearing after the first in the list.
	 * </p>
	 * <p>Invoked when the transaction manager is started, and then
	 * regularly as part of the cyclic file reconfiguration process. 
	 * </p>
	 * @throws IOException 
	 * @throws TransactionManagerFilestoreClosedException 
	 * 
	 */
	
	private void writeCompletedTransactions(
			List<Long> sequenceNumbers) 
					throws 	IOException,
							TransactionManagerFilestoreClosedException {
		
		for (Long seq : sequenceNumbers) writeSeq(seq);	
	}
	
	/**
	 * Delete old transaction files - but leave the last
	 * file undeleted even if it is old
	 * 
	 * @throws InterruptedException 
	 * 
	 */

	public void deleteOldFiles() throws InterruptedException {

		List<Integer> transFileInts = 
				getTransactionFilenameInts();
		if (transFileInts.isEmpty()) return;
		long currentTime = System.currentTimeMillis();
		int last = transFileInts.get(transFileInts.size() - 1);
		for (Integer i : transFileInts) {
			if (i.intValue() != last) {
				File f = transFile(i);
				if ((currentTime - f.lastModified()) > 
					oldFileDeletionPeriod)
					f.delete();
				if (Thread.interrupted()) 
					throw (new InterruptedException());
			}
		}		
	}
	
	/**
	 * Get a sorted list of the names of the transaction
	 * files in the transactions directory
	 * 
	 * @return a sorted list of the names of the transaction
	 * files in the transactions directory.
	 * 
	 * @throws InterruptedException 
	 */
	
	private List<Integer> getTransactionFilenameInts() 
			throws InterruptedException {
	
		lock.lockInterruptibly();
		try {
			String[] transFilesArray = directory.list();
			ArrayList<Integer> transFilesList =
					new ArrayList<Integer>();
			for (int i = 0; i < transFilesArray.length; i++){
				if (transFilesArray[i].startsWith("trans")) {
					transFilesList.add(
						Integer.parseInt(
							transFilesArray[i].substring(5)));
				}
			}
			Collections.sort(transFilesList);
			return transFilesList;
		}
		finally {
			lock.unlock();
		}
	}
	
	/**
	 * Returns the name of the nth transactions file
	 */

	private String name(int i) {
		return "trans" + i;
	}
	
	/**
	 * Returns the nth transactions file
	 */

	private File transFile(int i) {
		return new File(directory, name(i));
	}
	
	/**
	 * Shut down the file  store
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 * 
	 */
	
	public void shutdown() 
			throws 	IOException,
					InterruptedException {
		
		lock.lockInterruptibly();
		try {
			if (completedTransactionsWriter != null) {
				completedTransactionsWriter.close();
				completedTransactionsWriter = null;
			}
		}
		finally {
			lock.unlock();
		}
	}
	
	public class TransactionManagerFilestoreClosedException
			extends FileTransactionManagerClosedException {

		private static final long serialVersionUID = 1L;
	}
}
