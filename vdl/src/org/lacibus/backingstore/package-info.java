/**
 * <p>This package contains an implementation of a source of items and triples that uses
 * local files, and a source connector that can be used by a triple store to connect to
 * that source.
 * </p>
 * <p>The implementation stores items and small triples in a single main file. Summaries
 * of triples with large objects are stored in the main file, and each large object is stored
 * in a separate file. The implementation includes a transaction manager for transactions
 * that modify the store in order to maintain transactional record integrity on system 
 * re-start.
 * </p>
 * <p>The source and source connector support java backing store and backing store
 * client interfaces that might also be used by other local source implementations.
 * </p>
 **/

package org.lacibus.backingstore;
