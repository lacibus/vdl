/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.backingstore;

import org.lacibus.triplesource.SourceConnectorException;


/**
 * An exception that indicates an error in the operation
 * of a file source connector.
 */

public class FileSourceConnectorException
				extends SourceConnectorException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Create a file source connector exception
	 * 
	 * @param sourceNr the numeric identifier of the source that the
	 * connector connects to
	 * 
	 * @param s a description of the reason for throwing the exception
	 */
	
	public FileSourceConnectorException(Long sourceNr, String s) {
		super(0, s);
		if (sourceNr != null) this.sourceNr = sourceNr;
	}

}
