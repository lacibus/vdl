/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.backingstore;

/*
 ***********************************************************************
 *
 *  HOW THIS FILE IS ORGANIZED
 *  
 ***********************************************************************
 * 
 * This file contains:
 *    o Imports and the class definition
 *    o Definitions of the main constants and variables
 *    o Creation, initialization, and closedown
 *    o Transactions
 *    o High item and triple identifiers
 *    o Initial return of data
 *    o Items
 *    o Triples
 */


/*
 ***********************************************************************
 *
 *  IMPORTS AND CLASS DEFINITION
 *  
 ***********************************************************************
 */

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Datum.Type;
import org.lacibus.triplestore.Id;
import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.Logger;
import org.lacibus.triplestore.NotInOperationException;
import org.lacibus.triplestore.Transaction;
import org.lacibus.triplestore.Triple;
import org.lacibus.triplestore.TripleStoreFatalException;
import org.lacibus.triplestore.TripleStoreNonFatalException;
import org.lacibus.triplesource.AddItemDelta;
import org.lacibus.triplesource.AddItemTripleDelta;
import org.lacibus.triplesource.AddNonItemTripleDelta;
import org.lacibus.triplesource.SourceConnectorClient;
import org.lacibus.triplesource.SourceConnectorContentException;
import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplesource.SourceConnectorStateException;
import org.lacibus.triplesource.StoredItemTriple;
import org.lacibus.triplesource.StoredNonItemTriple;
import org.lacibus.triplesource.StoredTriple;
import org.lacibus.triplesource.TransactionSequenceException;
import org.lacibus.triplesource.TransactionsStatus;

/**
 * An instance of this class is a connector to a file backing store
 * that acts as a source of items and triples. The client of the
 * connector must be the owner of the source.
 */

public class FileSourceConnector 
		extends BackingStoreSourceConnector implements BackingStoreClient {

	private static final long serialVersionUID = 1267305678179326906L;

/*
 ***********************************************************************
 *
 *  DEFINITIONS OF MAIN CONSTANTS AND VARIABLES
 *  
 ***********************************************************************
 */
	
	/**
	 * The file backing store that is the source of items and
	 * triples that this source connector connects to
	 */
	
	FileBackingStore fileBackingStore;
	
	/**
	 * The directory containing the file backing store that is
	 * the source of items and triples that this source connector
	 * connects to
	 */
	
	File directory;
	
	/**
	 * The next transaction sequence number
	 */
	
	private TransactionSequenceNumber transactionSequenceNumber =
			new TransactionSequenceNumber();
	
	/**
	 * The transaction manager that maintains a list of completed 
	 * transactions.
	 */

	private FileTransactionManager transactionManager = null;
	
	/**
	 * The index by item identifiers of reference numbers for
	 * items in the backing store. It enables the store reference
	 * to be found from a numeric item identifier. 
	 */
	
	private ConcurrentHashMap<Long, Integer> itemsIndex =
			new ConcurrentHashMap<Long, Integer>();
	
	/**
	 * The index by triple identifiers of reference numbers for
	 * triples in the backing store. It enables the store reference 
	 * to be found from a numeric triple identifier. 
	 */
	
	private ConcurrentHashMap<Long, Integer> triplesIndex =
			new ConcurrentHashMap<Long, Integer>();
	
	/*
	 ***********************************************************************
	 *
	 *  CREATION, INITIALIZATION, AND CLOSEDOWN
	 *  
	 ***********************************************************************
	 */
	
	/**
	 * The states that the source connector can be in
	 */
	
	private enum State {INITIAL, OPERATIONAL, CLOSED}
	
	/**
	 * The current state of the source connector
	 */
	
	private State state = State.INITIAL;
	
	/**
	 * <p>Construct a new file source connector.
	 * </p>
	 * <p>A file source connector can only connect to a private source
	 * </p>
	 * @param directory the directory containing the the file backing store
	 * that is the source of items and triples
	 * 
	 * @param sourceNr the numeric identifier of the source
	 * 
	 * @throws SourceConnectorStateException 
	 */
	
	public FileSourceConnector(
			SourceConnectorClient client, long sourceNr,
			File localDir, File specFile, Logger logger) 
					throws SourceConnectorException {
		
		super(client, sourceNr, true, localDir, specFile, logger);
		directory = new File(localDir, "source");
	}
	
	@Override
	public long getSourceNr() {
		
		return sourceNr;
	}
	
	public boolean sourceExists() throws SourceConnectorException {
				
		try {
			return directory.exists();
		} catch (SecurityException e) {
			SourceConnectorStateException x =
					new SourceConnectorStateException(sourceNr,
							"Cannot use directory at " + 
							directory.getAbsolutePath());
			x.initCause(e);
			throw(x);
		}
	}

	@Override
	public void create() 
			throws 	SourceConnectorException,
					TripleStoreNonFatalException {
		
		try {
			if (directory.exists())
				throw (new SourceConnectorStateException(sourceNr,
						"A resource already exists at " + 
						directory.getAbsolutePath()));
			if (!directory.mkdir())
				throw (new SourceConnectorStateException(sourceNr,
						"Cannot create directory at " + 
						directory.getAbsolutePath()));
		} catch (SecurityException e) {
			SourceConnectorStateException x =
					new SourceConnectorStateException(sourceNr,
							"Cannot use directory at " + 
							directory.getAbsolutePath());
			x.initCause(e);
			throw(x);
		}
		
		setup();
		
		// Now add the source item and the item representing
		// the administration access level
		
		// Ensure their numeric identifiers cannot be used
		// for other items and triples. (Note that the source
		// item number is the lowest long integer, and the
		// number of the item representing the administration
		// access level is the next lowest, and the triple
		// number of the the administration access level
		// triple is the lowest long integer.)
		highestItemNumber.setIfHigher(adminLevelItemNr);
		highestTripleNumber.setIfHigher(adminLevelTripleNr);
		
		// Start a transaction to store these items and
		// the access level triple for the administration
		// access level
		FileTransaction trans = new FileTransaction(
				transactionSequenceNumber.getAndUpdate());
		try {
			fileBackingStore.transactionStarted(trans);
			
			// Add the items to the backing store and
			// index them
			int storeRef = fileBackingStore.addItem(
					sourceItemNr, 
					client.getSpecialItemSourceNr(),
					client.getLowestAccessLevelNr(), 
					client.getSpecialItemSourceNr(), 
					client.getHighestAccessLevelNr(), 
					trans);
			itemsIndex.put(sourceItemNr, storeRef);
			storeRef = fileBackingStore.addItem(
					adminLevelItemNr, 
					client.getSpecialItemSourceNr(),
					client.getLowestAccessLevelNr(), 
					sourceNr, 
					adminLevelItemNr, 
					trans);
			itemsIndex.put(adminLevelItemNr, storeRef);
			
			// Add the access level triple for the administration
			// access level
			
			RefAndSummary refAndSummary = fileBackingStore.addTriple(
					adminLevelTripleNr, 
					adminLevelItemNr, 
					client.getSpecialItemSourceNr(), 
					client.getIsAccessLevelRelationNr(), 
					Datum.create(
							sourceNr, 
							adminLevelItemNr), 
					trans);
			triplesIndex.put(adminLevelTripleNr, refAndSummary.ref);
			
			// End the transaction
			fileBackingStore.transactionComplete(trans);
			try {
				transactionManager.noteCompleted(trans);
			} catch (InterruptedException e) {
				throw(abortSourceException(e));
			} catch (FileTransactionManagerException e) {
				throw(abortSourceException(e));
			} catch (IOException e) {
				throw(abortSourceException(e));
			}			
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		}		
		
		// Install the items and triple in the client
		try {
			client.applyDelta(
					sourceNr,
					new AddItemDelta(
							sourceItemNr,
							client.getSpecialItemSourceNr(),
							client.getLowestAccessLevelNr(),
							client.getSpecialItemSourceNr(),
							client.getHighestAccessLevelNr()),
					true);
			client.applyDelta(
					sourceNr,
					new AddItemDelta(
							adminLevelItemNr,
							client.getSpecialItemSourceNr(),
							client.getLowestAccessLevelNr(),
							sourceNr, 
							adminLevelItemNr),
					true);
			client.applyDelta(
					sourceNr,
					new AddItemTripleDelta(
							adminLevelTripleNr,
							adminLevelItemNr,
							client.getSpecialItemSourceNr(), 
							client.getIsAccessLevelRelationNr(), 
							new Id(sourceNr, adminLevelItemNr)),
					true);
		} catch (TripleStoreNonFatalException e) {
			SourceConnectorStateException x =
					new SourceConnectorStateException(sourceNr,
							"Cannot install source item");
			x.initCause(e);
			throw(x);
		} catch (TripleStoreFatalException e) {
			SourceConnectorStateException x =
					new SourceConnectorStateException(sourceNr,
							"Cannot install source item");
			x.initCause(e);
			throw(x);
		} catch (NotInOperationException e) {
			SourceConnectorStateException x =
					new SourceConnectorStateException(sourceNr,
							"Cannot install source item");
			x.initCause(e);
			throw(x);
		}
		

		// The connector can now be used
		state = State.OPERATIONAL;
	}
	
	public void load() 
			throws SourceConnectorException {
		
		if (!directory.exists())
			throw (new SourceConnectorStateException(sourceNr,
					"No resource exists at " + 
					directory.getAbsolutePath()));
		setup();
		
		// The connector can now be used
		state = State.OPERATIONAL;
	}

	/**
	 * <p>Set up the source connector.
	 * </p>
	 * <p>Establish the backing store and transaction manager, 
	 * and load the client from the backing store.
	 * </p>
	 * @throws SourceConnectorException
	 */
	
	private void setup()
			throws SourceConnectorException {
		
		fileBackingStore = new FileBackingStore(this, directory, logger);
		
		try {
			fileBackingStore.initialise(false);
		} catch (BackingStoreException e) {
			SourceConnectorStateException x =
					new SourceConnectorStateException(sourceNr,
							"Cannot initialize source");
			x.initCause(e);
			throw(x);
		}
		
		File transactionsDirectory = 
				setUpDirectory(directory, "trans", "transactions");
		transactionManager =
				new FileTransactionManager(
						new TransactionManagerFilestore(
								transactionsDirectory, logger), logger);		
		try {
			transactionManager.initialize();
		} catch (Exception e) {
			SourceConnectorStateException x = 
					new SourceConnectorStateException(
							sourceNr,
							"Cannot initialize transaction manager");
			x.initCause(e);
			throw(x);
		}	
		
		File safeHighestIdentifiersDirectory = 
				setUpDirectory(directory, "highest", "safe highest identifiers");
		File safeHighestItemIdentifierDirectory = 
				setUpDirectory(safeHighestIdentifiersDirectory, "item", "safe highest item identifier");
		highestItemNumber = new FiledHighestIdentifier(
				safeHighestItemIdentifierDirectory, sourceNr);
		File safeHighestTripleIdentifierDirectory = 
				setUpDirectory(safeHighestIdentifiersDirectory, "triple", "safe highest triple identifier");
		highestTripleNumber = new FiledHighestIdentifier(
				safeHighestTripleIdentifierDirectory, sourceNr);
		
		loadFromBackingStore();
	}
	
	private File setUpDirectory(File parent, String name, String description) 
			throws SourceConnectorStateException {
		
		File dir = new File(parent, name);
		if (dir.exists()) {
			if (!dir.isDirectory())
					throw new SourceConnectorStateException(
							sourceNr, 
							"Invalid " + description + " directory");
		}
		else if (!dir.mkdir())
				throw new SourceConnectorStateException(
						sourceNr, 
						"Cannot create " + description + " directory");
		return dir;
	}

	@Override
	public boolean isOwner() {

		return (true);
	}


	@Override
	public void close() {

		terminate();
		if (fileBackingStore != null) fileBackingStore.close();
	}

	@Override
	public void simulateIoError() {
		
		fileBackingStore.simulateIoError();
	}

	/**
	 * The exception that caused closure of the store
	 */
	
	private Exception closureCause = null;
	
	/**
	 * Abort operation of the connector.
	 * 
	 * @param e an exception prompting the abort.
	 * 
	 * @return a new FileBackingStoreSourceConnectorException saying so,
	 * f the store is already closed, otherwise a 
	 * FileBackingStoreSourceConnectorException with the cause set to 
	 * the exception prompting the abort.
	 */
	
	private SourceConnectorException abortSourceException(
			Exception e) {

		boolean alreadyClosed = false;
		synchronized (this) {
			if (state == State.CLOSED) alreadyClosed = true;
			else {
				state = State.CLOSED;
				closureCause = e;
			}
		}
		SourceConnectorException ex; 
		if (alreadyClosed) {
			ex = new FileSourceConnectorException(sourceNr, 
					"The source connector is already closed.");
			ex.initCause(closureCause);
		}
		else {
			terminate();
			fileBackingStore.close();
			ex = new FileSourceConnectorException(sourceNr,
					"File backing store operation aborted");
			ex.initCause(e);
		}
		return ex;
	}
	
	/**
	 * Implements the getTerminator() method of SourceConnector
	 */
	
	public Exception getTerminator() {
		
		return fileBackingStore.getTerminator();
	}

	/**
	 * <p>Terminate operation of the connector
	 * </p>
	 * <p>Ignore any exceptions and close everything that
	 * can be closed.
	 * </p>
	 */
	
	private void terminate() {
		
		if (transactionManager != null)
			try {
				transactionManager.close();
			} catch (IOException e) {				
			} catch (InterruptedException e) {
			}
	}
	
	/*
	 ***********************************************************************
	 *
	 *  TRANSACTIONS
	 *  
	 ***********************************************************************
	 */

	/**
	 * An instance of this class holds the next transaction sequence 
	 * number and ensures that access is thread-safe.
	 */
	
	private class TransactionSequenceNumber {
		
		/**
		 * <p>The id of the next transaction to be created.
		 * </p>
		 * <p>The lowest possible id is reserved for a special
		 * transaction used at start-up time.
		 * </p>
		 */
	
		private long nextSeq = Long.MIN_VALUE + 1;
		
		/**
		 * Get the next transaction sequence number and increment
		 * the value that will be returned subsequently.
		 * 
		 * @return the next transaction sequence number.
		 * 
		 * @throws SourceConnectorException 
		 */
		
		private synchronized long getAndUpdate()
				throws SourceConnectorException {
			
			if (nextSeq == Long.MAX_VALUE)
				throw (abortSourceException(
						new FileSourceConnectorException(
								sourceNr,
								"Transaction sequence number" +
								" is max value")));
			long seq = nextSeq++;
			return seq;
		}
		
		/**
		 * Update the next transaction sequence number to be greater
		 * than a given existing value.
		 * 
		 * @param existingValue the current value that the next
		 * transaction sequence number must be greater than
		 * 
		 * @throws SourceConnectorException 
		 */
		
		private synchronized void update(long existingValue) 
				throws SourceConnectorException {	
			
			if (existingValue == Long.MAX_VALUE)
				throw (abortSourceException(
						new FileSourceConnectorException(
								sourceNr,
								"Transaction sequence number " +
								"is max value")));
			if (nextSeq <= existingValue) nextSeq = existingValue+1;
		}		
	}
	
	/**
	 * Start a transaction.
	 * 
	 * @return a new, unique, transaction
	 * 
	 * @throws FileSourceConnectorException 
	 */
	
	@Override
	public Transaction startTransaction() 
					throws 	SourceConnectorException {
	
		if (state != State.OPERATIONAL) {
			FileSourceConnectorException x =
					new FileSourceConnectorException(
							sourceNr,
							"Connector not operational");	
			Exception e = getTerminator();
			if (e != null) x.initCause(e);
			throw(x);
		}
		if (isOwner()) {
			FileTransaction trans = new FileTransaction (
					transactionSequenceNumber.getAndUpdate());
			fileBackingStore.transactionStarted(trans);
			return trans;
		}
		else throw (new SourceConnectorStateException(
				sourceNr,"Cannot start non-owner transaction"));
	}

	/**
	 * <p>End a transaction.
	 * </p>
	 * <p>All data written by the transaction is stored on disc,
	 * and the transaction is noted as completed.
	 * </p>
	 * <p>No check is made that the transaction is not already
	 * complete. The results of invoking this method twice
	 * for the same transaction are indeterminate.
	 * </p>
	 * @throws SourceConnectorException 
	 */
	
	@Override
	public void endTransaction(Transaction trans)
			throws 	SourceConnectorException {
		
		if (state != State.OPERATIONAL) {
			FileSourceConnectorException x =
						new FileSourceConnectorException(
								sourceNr,
								"Connector not operational");	
			Exception e = getTerminator();
			if (e != null) x.initCause(e);
			throw(x);
		}
		
		if (!isOwner())
			throw (new SourceConnectorStateException(
					sourceNr,"Cannot end non-owner transaction"));
		
		if (!(trans instanceof FileTransaction))
			throw (new FileSourceConnectorException(
					sourceNr,
					"Invalid transaction type"));
		
		FileTransaction ft = (FileTransaction) trans;
		try {
			// Note that the transaction is completed
			fileBackingStore.transactionComplete(ft);
			try {
				transactionManager.noteCompleted(ft);
			} catch (InterruptedException e) {
				throw(abortSourceException(e));
			} catch (FileTransactionManagerException e) {
				throw(abortSourceException(e));
			} catch (IOException e) {
				throw(abortSourceException(e));
			}			
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		}		
	}

	@Override
	public boolean isCompleted(long transSeq) 
			throws SourceConnectorException {

		try {
			return transactionManager.isCompleted(transSeq);
		} catch (FileTransactionManagerClosedException e) {
			throw (abortSourceException(e));
		} catch (FileTransactionManagerException e) {
			throw (abortSourceException(e));
		}
	}

	/*
	 ***********************************************************************
	 *
	 *  HIGH ITEM AND TRIPLE IDENTIFIERS
	 *  
	 ***********************************************************************
	 *
	 * Note is kept of the highest identifier of any item that is stored
	 * and the highest identifier of any triple that is stored. This
	 * enables source connector to ensure that identifiers are never 
	 * re-allocated.
	 */
	
	/**
	 * The highest identifier of any item that has been stored
	 */
	
	private FiledHighestIdentifier highestItemNumber;;

	/**
	 * The highest identifier of any triple that has been stored
	 */
	
	private FiledHighestIdentifier highestTripleNumber;			
	
	/**
	 * @return the highest identifier of any item that has been stored
	 */
	
	public long getHighestItemNr() {

		return highestItemNumber.get();
	}

	/**
	 * @return the highest identifier of any triple that has been stored
	 */
	
	public long getHighestTripleNr() {

		return highestTripleNumber.get();
	}

	/*
	 ***********************************************************************
	 *
	 *  INITIAL RETURN OF DATA
	 *  
	 ***********************************************************************
	 *
	 * At initialization time, the source connector asks the
	 * backing store to load items and triples into it, and
	 * in turn loads them into the triple store.
	 * 
	 */
	
	/**
	 * Load the stored segments
	 * 
	 * @throws SourceConnectorException 
	 */

	private void loadFromBackingStore() 
					throws SourceConnectorException { 
		
		// Get the transactions status from the transactions manager
		TransactionsStatus status;
		try {
			status = transactionManager.getStatus();
		} catch (TransactionSequenceException e) {
			throw (abortSourceException(e));
		}
	
		// Load the items and triples from the backing store
		try {
			fileBackingStore.load(this, status);
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		}
		
		// Note that all transactions up to and including the
		// latest one are now completed.
		long highestTransSeq = fileBackingStore.getHighestLoadedTransactionNr();
		transactionSequenceNumber.update(highestTransSeq);
		
		// Start the transactions manager
		try {
			transactionManager.start(highestTransSeq);
		} catch (FileTransactionManagerException e) {
			throw(abortSourceException(e));
		} catch (IOException e) {
			throw(abortSourceException(e));
		} catch (InterruptedException e) {
			throw(abortSourceException(e));
		} catch (FileTransactionManagerClosedException e) {
			throw(abortSourceException(e));
		}
	}
	
	@Override
	public void loadItem(
			long itemNr, 
			long readLevelSourceNr,
			long readLevelItemNr, 
			long writeLevelSourceNr,
			long writeLevelItemNr, 
			int storeRef) 
					throws BackingStoreClientException {

		try {
			highestItemNumber.setIfHigher(itemNr);
		} catch (SourceConnectorException e) {
			BackingStoreClientException x = new BackingStoreClientException(
					"Cannot set highest item number");
			x.initCause(e);
			throw (x);
		}
		itemsIndex.put(itemNr, storeRef);
		try {
			client.applyDelta(
					sourceNr, 
					new AddItemDelta(
							itemNr,
							readLevelSourceNr,
							readLevelItemNr,
							writeLevelSourceNr,
							writeLevelItemNr),
					true);
		} catch (TripleStoreNonFatalException e) {
			BackingStoreClientException x =
					new BackingStoreClientException(
							"Cannot apply add item delta");
			x.initCause(e);
			throw(x);
		} catch (SourceConnectorException e) {
			BackingStoreClientException x =
					new BackingStoreClientException(
							"Cannot apply add item delta");
			x.initCause(e);
			throw(x);
		} catch (TripleStoreFatalException e) {
			BackingStoreClientException x =
					new BackingStoreClientException(
							"Cannot apply add item delta");
			x.initCause(e);
			throw(x);
		} catch (NotInOperationException e) {
			BackingStoreClientException x =
					new BackingStoreClientException(
							"Cannot apply add item delta");
			x.initCause(e);
			throw(x);
		}
	}

	@Override
	public void loadTriple(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr, 
			long verbItemNr, 
			Datum object, 
			int storeRef) 
					throws 	BackingStoreClientException {

		try {
			highestTripleNumber.setIfHigher(tripleNr);
		} catch (SourceConnectorException e) {
			BackingStoreClientException x = new BackingStoreClientException(
					"Cannot set highest triple number");
			x.initCause(e);
			throw (x);
		}
		triplesIndex.put(tripleNr, storeRef); 
		if (object.getType() == Datum.Type.ITEM)
			try {
				client.applyDelta(
						sourceNr, 
						new AddItemTripleDelta(
								tripleNr,
								subjectItemNr, 
								verbSourceNr, 
								verbItemNr, 
								object.getItemValue().getId()),
						true);
			} catch (TripleStoreNonFatalException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot apply add item triple delta");
				x.initCause(e);
				throw(x);
			} catch (SourceConnectorException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot apply add item triple delta");
				x.initCause(e);
				throw(x);
			} catch (TripleStoreFatalException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot apply add item triple delta");
				x.initCause(e);
				throw(x);
			} catch (NotInOperationException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot apply add item triple delta");
				x.initCause(e);
				throw(x);
			}
		else
			try {
				client.applyDelta(
						sourceNr, 
						new AddNonItemTripleDelta(
								tripleNr,
								subjectItemNr, 
								verbSourceNr, 
								verbItemNr, 
								object),
						true);
			} catch (TripleStoreNonFatalException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot apply add non-item triple delta");
				x.initCause(e);
				throw(x);
			} catch (SourceConnectorException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot apply add non-item triple delta");
				x.initCause(e);
				throw(x);
			} catch (TripleStoreFatalException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot apply add non-item triple delta");
				x.initCause(e);
				throw(x);
			} catch (NotInOperationException e) {
				BackingStoreClientException x =
						new BackingStoreClientException(
								"Cannot apply add non-item triple delta");
				x.initCause(e);
				throw(x);
			}
	}

	/*
	 ***********************************************************************
	 *
	 *  ITEMS
	 *  
	 ***********************************************************************
	 */

	@Override
	public IdAndTrans addItem(
			Id onlyReadLevelId,
			Id readOrWriteLevelId)
					throws 	SourceConnectorException {
		
		long itemNr = highestItemNumber.next(); 
		Transaction trans = new FileTransaction(
				transactionSequenceNumber.getAndUpdate());
		try {
			fileBackingStore.transactionStarted(trans);
			int storeRef = fileBackingStore.addItem(
					itemNr, 
					onlyReadLevelId.getSourceNr(),
					onlyReadLevelId.getIdNr(), 
					readOrWriteLevelId.getSourceNr(), 
					readOrWriteLevelId.getIdNr(), 
					trans);
			itemsIndex.put(itemNr, storeRef);
			return new IdAndTrans(itemNr, trans);
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		}
	}

	public void updateItem(
			long itemNr,
			Id onlyReadLevelId,
   			Id readOrWriteLevelId,
   			Transaction trans)
   					throws 	SourceConnectorException {
		
		Integer storeRef = itemsIndex.get(itemNr);
		if (storeRef == null)
			// The item is not in store
			throw (new SourceConnectorContentException(
					sourceNr, "Item " + itemNr + " not found."));
		try {
			fileBackingStore.removeItem(storeRef, trans);
			int newRef = fileBackingStore.addItem(
				itemNr, 
				onlyReadLevelId.getSourceNr(), 
				onlyReadLevelId.getIdNr(), 
				readOrWriteLevelId.getSourceNr(), 
				readOrWriteLevelId.getIdNr(), 
				trans);
			itemsIndex.put(itemNr, newRef);
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		} catch (BackingStoreClientException e) {
			throw (abortSourceException(e));
		}
	}

	public void removeItem(long itemNr, Transaction trans)
   					throws 	SourceConnectorException {
		
		Integer storeRef = itemsIndex.get(itemNr);
		if (storeRef == null)
			// The item is not in store
			throw (new SourceConnectorContentException(
					sourceNr, "Item " + itemNr + " not found."));
		try {
			fileBackingStore.removeItem(storeRef, trans);
			itemsIndex.remove(itemNr);
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		} catch (BackingStoreClientException e) {
			throw (abortSourceException(e));
		}
	}

	/*
	 ***********************************************************************
	 *
	 *  TRIPLES
	 *  
	 ***********************************************************************
	 */

	public StoredTriple createTriple(
  			Item subject,
  			Item verb,
  			Datum object,
  			Transaction trans)
		throws 	SourceConnectorException,
				TripleStoreNonFatalException {

		long tripleNr = highestTripleNumber.next();
		if (subject.getSourceNr() != sourceNr)
				throw (new FileSourceConnectorException(
						sourceNr, 
						"Subject item " + subject + 
						" is not from this source."));
		
		RefAndSummary refAndSummary;
		try {
			refAndSummary = fileBackingStore.addTriple(
					tripleNr, 
					subject.getItemNr(), 
					verb.getSourceNr(),
					verb.getItemNr(), 
					object, 
					trans);
			triplesIndex.put(tripleNr, refAndSummary.ref);
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		}
		if (object.getType() == Type.ITEM)
			try {
				return new StoredItemTriple(
						tripleNr, subject, verb, 
						object.getItemValue());
			} catch (TripleStoreNonFatalException e) {
				throw (abortSourceException(e));
			}
		else return new StoredNonItemTriple(
						tripleNr, subject, verb,
						object.getType(), refAndSummary.summary);
	}

	@Override
	public StoredTriple replaceTriple(
			Triple triple,
			Datum newObject,
			Transaction trans) 
					throws 	SourceConnectorException, 
							TripleStoreNonFatalException {
		
		// remove the triple
		Integer storeRef = triplesIndex.remove(triple.getTripleNr());
		if (storeRef == null)
			// The triple is not in store
			return null;
		try {
			fileBackingStore.removeTriple(
					storeRef, trans);
			// add a new one
			return createTriple(
					triple.getSubject(), 
					triple.getVerb(), 
					newObject, 
					trans);
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		} catch (BackingStoreClientException e) {
			throw (abortSourceException(e));
		}
	}

	public void removeTriple(long tripleNr,
   						     Transaction trans)
   					throws 	SourceConnectorException {
		
		Integer storeRef = triplesIndex.remove(tripleNr);
		if (storeRef == null)
			// The triple is not in store
			return;
		try {
			fileBackingStore.removeTriple(storeRef, trans);
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		} catch (BackingStoreClientException e) {
			throw (abortSourceException(e));
		}
	}

	public Datum getTextTripleObject(long tripleNr) 
			throws 	SourceConnectorException, TripleStoreNonFatalException {
		
		Integer storeRef = triplesIndex.get(tripleNr);
		if (storeRef == null) return null;
		try {
			return fileBackingStore.getTextTripleObject(
					storeRef);
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		}
	}

	public Datum getBinaryTripleObject(long tripleNr) 
			throws 	SourceConnectorException, TripleStoreNonFatalException {
		
		Integer storeRef = triplesIndex.get(tripleNr);
		if (storeRef == null) return null;
		try {
			return fileBackingStore.getBinaryTripleObject(storeRef);
		} catch (BackingStoreException e) {
			throw (abortSourceException(e));
		}
	}
}
