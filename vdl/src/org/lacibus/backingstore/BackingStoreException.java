/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.backingstore;

/**
 * An exception that is thrown when a fatal error is detected
 * in a backing store 
 */

abstract class BackingStoreException extends Exception {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Create a backing store exception
	 */
	
	BackingStoreException() {
		super();
	}
	
	/**
	 * Create a backing store exception
	 * 
	 * @param s a description of the reason for throwing the
	 * exception
	 */
	
	BackingStoreException(String s) {
		super(s);
	}
}
