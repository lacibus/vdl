/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.backingstore;

import org.lacibus.triplestore.Datum;

/**
 * An interface to a client of a backing store - typically a source connector
 * that uses the backing store as a source of items and triples.
 */

interface BackingStoreClient {

	/**
	 * Load an item from the backing store
	 * 
	 * @param itemNr the numeric identifier of the item in its source
	 * 
	 * @param readLevelSourceNr the numeric identifier of the source of
	 * the only-read access level of the item
	 * 
	 * @param readLevelItemNr the numeric identifier in its  source of
	 * the only-read access level of the item
	 * 
	 * @param writeLevelSourceNr the numeric identifier of the source of
	 * the read-or-write access level of the item
	 * 
	 * @param writeLevelItemNr the numeric identifier in its  source of
	 * the read-or-write access level of the item
	 * 
	 * @param storeRef a reference to the item in the backing store
	 * 
	 * @throws BackingStoreClientException
	 */
	
	void loadItem(
			long itemNr,
			long readLevelSourceNr,
			long readLevelItemNr,
			long writeLevelSourceNr,
			long writeLevelItemNr,
			int storeRef) 
					throws 	BackingStoreClientException;
	
	/**
	 * Load a triple from the backing store
	 * 
	 * @param tripleNr the numeric identifier of the triple in its source
	 * 
	 * @param subjectItemNr the numeric identifier of the subject of the
	 * triple in its source
	 * 
	 * @param verbSourceNr the numeric identifier of the source of the verb
	 * of the triple
	 * 
	 * @param verbItemNr the numeric identifier of the verb of the triple 
	 * in its source
	 * 
	 * @param object the object of the triple
	 * 
	 * @param storeRef a reference to the triple in the backing store
	 * 
	 * @throws BackingStoreClientException
	 */
	
	void loadTriple(
			 long tripleNr,
			 long subjectItemNr, 
			 long verbSourceNr, 
			 long verbItemNr, 
			 Datum object,
			 int storeRef) 
					 throws BackingStoreClientException;	
}
