/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.backingstore;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import org.lacibus.backingstore.TransactionManagerFilestore.TransactionManagerFilestoreClosedException;
import org.lacibus.triplesource.TransactionSequenceException;
import org.lacibus.triplesource.TransactionsStatus;
import org.lacibus.triplestore.Logger;

/**
 * 
 * An instance of this class keeps records of completed
 * file transactions.
 * 
 */

class FileTransactionManager {
	
	/*
	 ******************************************************************
	 *
	 *     FILESTORE
	 *     
	 ******************************************************************
	 */
	
	/**
	 * The transaction file store
	 */
	
	private TransactionManagerFilestore fileStore;
	
	/**
	 * The transaction memory store
	 */
	
	private TransactionsStatus memoryStore;
	
	/**
	 * Lock used to ensure that updates to the file store and
	 * memory store are synchronized.
	 */
	
	private ReentrantLock lock = new ReentrantLock();
	
	/**
	 * A logger to which error and status messages can be written
	 */
	
	@SuppressWarnings("unused")
	private Logger logger = null;
	
	/*
	 ******************************************************************
	 *
	 *     CONSTRUCTOR AND EXTERNALLY-INVOKED METHODS
	 *     
	 ******************************************************************
	 */
	
	/**
	 * Create a file transaction manager
	 * 
	 * @param directory the directory containing the file
	 * in which completed transactions are recorded.
	 * 
	 */
	
	FileTransactionManager(TransactionManagerFilestore filestore, Logger logger) {
		this.logger = logger;
		managerState = new ManagerState();
		this.fileStore = filestore;
		maintainer = new Maintainer();
	}
	
	/**
	 * Initialize the transaction manager. This method must be
	 * invoked before any other.
	 * 
	 * @throws IOException  
	 * @throws InterruptedException 
	 * @throws FileTransactionManagerException 
	 * @throws FileTransactionManagerClosedException 
	 * 
	 */
	
	void initialize() 
			throws 	IOException, 
					InterruptedException,
					FileTransactionManagerException, 
					FileTransactionManagerClosedException {
		
		try {
			managerState.setInitializing();
			lock.lockInterruptibly();
			try {
				memoryStore = new 
						TransactionsStatus(fileStore.prepare());
			} finally {
				lock.unlock();
			}
			managerState.setInitialized();
		} catch (FileTransactionManagerClosedException e) {
			abort(e);
			throw(e);
		} catch (FileTransactionManagerException e) {
			abort(e);
			throw(e);
		} catch (InterruptedException e) {
			abort(e);
			throw(e);
		} catch (IOException e) {
			abort(e);
			throw(e);
		}
	}
	
	/**
	 * Start the transaction manager..
	 * 
	 * @param seq the new value of the sequence number such that
	 * it and all transactions with lower sequence numbers have been
	 * completed.
	 * 
	 * @throws FileTransactionManagerException 
	 * @throws FileTransactionManagerClosedException 
	 * @throws IOException 
	 * @throws InterruptedException 
	 * 
	 */
	
	void start(long highestCompletedSeq)
			throws 	FileTransactionManagerException,
					FileTransactionManagerClosedException, 
					IOException, 
					InterruptedException {
		
		try {
			managerState.setStarting();
			memoryStore.recalcAllCompletedBefore(highestCompletedSeq);
			List<Long> transSeqs = memoryStore.getTransactionSequenceNumbers();
			fileStore.cycle(transSeqs);
			managerState.setOperational();
		} catch (InterruptedException e) {
			abort(e);
			throw(e);
		} catch (IOException e) {
			abort(e);
			throw(e);
		} catch (FileTransactionManagerException e) {
			abort(e);
			throw(e);
		} catch (FileTransactionManagerClosedException e) {
			abort(e);
			throw(e);
		}		
		maintainer.start();
	}
	
	/**
	 * Determine whether a transaction has been completed.
	 * 
	 * @param seq the sequence number of the transaction whose
	 * completion is to be checked
	 * 
	 * @return true if that transaction has been completed, false
	 * otherwise.
	 * 
	 * @throws FileTransactionManagerClosedException 
	 * @throws FileTransactionManagerException 
	 * 
	 */
	
	boolean isCompleted(long seq)
			throws 	FileTransactionManagerClosedException, 
					FileTransactionManagerException {
		
		try {
			managerState.checkReady();
		} catch (FileTransactionManagerClosedException e) {
			abort(e);
			throw(e);
		} catch (FileTransactionManagerException e) {
			abort(e);
			throw(e);
		}
		return memoryStore.isCompleted(seq);
	}
		
	/**
	 * Note that a transaction is completed and record the
	 * completion in the current file in which completed
	 * transactions are recorded.
	 * 
	 * @param t the completed transaction
	 * 
	 * @throws IOException 
	 * @throws InterruptedException 
	 * @throws FileTransactionManagerException 
	 * @throws FileTransactionManagerClosedException 
	 * 
	 */
	
	void noteCompleted(FileTransaction t) 
			throws 	InterruptedException, 
					IOException,
					FileTransactionManagerException,
					FileTransactionManagerClosedException {
		
		try {
			managerState.checkOperational();
		
			// The transaction with lowest possible sequence number
			// is automatically regarded as complete and should
			// not be noted.
			if (t.seq() == Long.MIN_VALUE) return;
			
			lock.lockInterruptibly();
			try {
				// Record the fact that the transaction is completed
				// on disc
				fileStore.writeCompletedTransaction(t.seq());
			
				// Update the list of completed transactions in
				// memory
				memoryStore.noteCompleted(t.seq());
			} finally {
				lock.unlock();
			}
		} catch (InterruptedException e) {
			abort(e);
			throw(e);
		} catch (IOException e) {
			abort(e);
			throw(e);
		} catch (TransactionManagerFilestoreClosedException e) {
			abort(e);
			throw(e);
		}
	}
	
	/**
	 * Get the completed transactions status
	 * 
	 * @return the completed transactions status
	 * 
	 * @throws TransactionSequenceException
	 * 
	 */
	
	TransactionsStatus getStatus() throws TransactionSequenceException {
		
		return memoryStore.getClone();
	}
		
	/**
	 * Close the file transaction manager
	 * 
	 * @throws InterruptedException 
	 * @throws IOException 
	 * 
	 */
	
	void close() 
			throws 	IOException, 
					InterruptedException {
		
		managerState.setClosed();
		maintainer.interrupt();
		try {
			fileStore.shutdown();
		} catch (IOException e) {
			abort(e);
			throw(e);
		} catch (InterruptedException e) {
			abort(e);
			throw(e);
		}
	}
	
	/*
	 ******************************************************************
	 *
	 *     ABORT
	 *     
	 ******************************************************************
	 */
	
	/**
	 * Exception thrown during a maintenance cycle
	 * Stored as the underlying cause of closure of
	 * the transaction manager
	 */
	
	private Exception maintenanceException = null;
	
	/**
	 * Boolean on which access to the maintenance exception is
	 * synchronized
	 */
	
	private Boolean maintenanceExceptionSync = false;
	
	/**
	 * Abort operation of the transaction manager when an exception
	 * has occurred. The state is set to closed, the file store
	 * is shut down, and the original exception is thrown, regardless
	 * of any other exceptions that may have occurred during the
	 * abort process.
	 * 
	 * If the exception is a FileTransactionManagerClosedException
	 * and a mainenanceException has been recorded then the
	 * mainenanceException is set as the underlying cause of the
	 * FileTransactionManagerClosedException
	 * 
	 * @param e an exception that has occurred
	 * 
	 */
	
	private void abort(Exception e) {
		
		managerState.setClosed();
		synchronized (maintenanceExceptionSync) {
			if (	(maintenanceException != null) &&
					e.getClass().equals(
						FileTransactionManagerClosedException.class)) {
				e.initCause(maintenanceException);
			}
		}
		maintainer.interrupt();
		shutdown();
	}
	
	/**
	 * Abort due to error during maintenance operations
	 */
	
	private void maintenanceAbort(Exception e) {
		
		managerState.setClosed();
		synchronized (maintenanceExceptionSync) {
			maintenanceException = e;
		}
		shutdown();
	}
	
	/**
	 * Shut down the transaction manager
	 */
	
	private void shutdown() {
		
		try {
			fileStore.shutdown();
		}
		catch (IOException e) {}
		catch (InterruptedException e) {}
	}
	
	/*
	 ******************************************************************
	 *
	 *      MANAGER STATE
	 *      
	 ******************************************************************
	 */
	
	/**
	 * The states that the transaction manager can be in
	 *
	 */
	
	enum State {INITIAL, 
			    INITIALIZING, 
			    INITIALIZED,
				STARTING, 
				OPERATIONAL, 
				CLOSED}
	
	/**
	 * The current state of the transaction manager
	 * 
	 */
	
	private ManagerState managerState;

	/**
	 * 
	 * An instance of the ManagerState class maintains the current
	 * state of the transaction manager. The class provides
	 * synchronized methods to check and change the state.
	 *
	 */
	
	private class ManagerState {
		
		private State state = State.INITIAL;
		
		/**
		 * Set the state to INITIALIZING
		 * 
		 * @throws FileTransactionManagerClosedException 
		 * @throws FileTransactionManagerException 
		 * 
		 */
		
		private synchronized void setInitializing() 
				throws 	FileTransactionManagerClosedException, 
						FileTransactionManagerException {
			
			if (state != State.INITIAL) {
				if (state == State.CLOSED)
					throw (new FileTransactionManagerClosedException());
				else throw (new FileTransactionManagerException(
						"Invalid transaction manager initialization"));
			}
			state = State.INITIALIZING;
		}

		/**
		 * Set the state to INITIALIZED
		 * 
		 * @throws FileTransactionManagerException 
		 * @throws FileTransactionManagerClosedException 
		 * 
		 */
		
		private synchronized void setInitialized() 
				throws 	FileTransactionManagerException,
						FileTransactionManagerClosedException {
			
			if (state != State.INITIALIZING) {
				if (state == State.CLOSED)
					throw (new FileTransactionManagerClosedException());
				else throw (new FileTransactionManagerException(
						"Invalid initialization"));
			}
			state = State.INITIALIZED;
		}
		
		/**
		 * Set the state to STARTING
		 * 
		 * @throws FileTransactionManagerException 
		 * @throws FileTransactionManagerClosedException 
		 * 
		 */
		
		private synchronized void setStarting() 
				throws 	FileTransactionManagerException, 
						FileTransactionManagerClosedException {
			
			if (state != State.INITIALIZED) {
				if (state == State.CLOSED)
					throw (new FileTransactionManagerClosedException());
				else throw (new FileTransactionManagerException(
						"Invalid start"));
			}
			state = State.STARTING;
		}
		
		/**
		 * Set the state to OPERATIONAL
		 * 
		 * @throws FileTransactionManagerException 
		 * @throws FileTransactionManagerClosedException 
		 * 
		 */
		
		private synchronized void setOperational() 
				throws 	FileTransactionManagerException, 
						FileTransactionManagerClosedException {
			
			if (state != State.STARTING) {
				if (state == State.CLOSED)
					throw (new FileTransactionManagerClosedException());
				else throw (new FileTransactionManagerException(
						"Invalid start"));
			}
			state = State.OPERATIONAL;
		}
		
		/**
		 * Check that the file transaction manager
		 * is ready to operate
		 * 
		 * @throws FileTransactionManagerException 
		 * @throws FileTransactionManagerClosedException 
		 * 
		 */
		
		private synchronized void checkReady() 
				throws 	FileTransactionManagerException, 
						FileTransactionManagerClosedException {
			
			if ((state != State.INITIALIZED) &&
				(state != State.OPERATIONAL)) {
				if (state == State.CLOSED)
					throw (new FileTransactionManagerClosedException());
				else throw (new FileTransactionManagerException(
						"Manager not ready"));
			}
		}
		
		/**
		 * Check that the file transaction manager
		 * is operational
		 * 
		 * @throws FileTransactionManagerException 
		 * @throws FileTransactionManagerClosedException 
		 * 
		 */
		
		private synchronized void checkOperational() 
				throws 	FileTransactionManagerException, 
						FileTransactionManagerClosedException {
			
			if (state != State.OPERATIONAL) {
				if (state == State.CLOSED)
					throw (new FileTransactionManagerClosedException());
				else throw (new FileTransactionManagerException(
						"Manager not in operation"));
			}
		}
		
		/**
		 * Set the state to CLOSED
		 * 
		 */
		
		private synchronized void setClosed() {
			state = State.CLOSED;
		}
	
	}
	
		
	/*
	 ******************************************************************
	 *
	 *      MAINTENANCE
	 *      
	 ******************************************************************
	 */
	
	/**
	 * The maintainer of the transaction manager
	 */
	
	private Maintainer maintainer;
	
	/**
	 * The maintainer class is a thread that runs asynchronously
	 * and performs regular maintenance operations. These are
	 * to cycle the file used to maintain records of
	 * completed transactions and delete old files.
	 *
	 */
	
	private class Maintainer extends Thread {
		
		/**
		 * Transaction file cycle time in milliseconds
		 */
	
		final long cycleTime = 60*1000;	// 1 minute
		
		final int cyclesPerOldFileDeletion = 60*24;	// Delete dailly
	
		public void run() {
			
			int cyclesWithoutDeletion = 0;
			try {
				do {
					sleep(cycleTime);
					if (isInterrupted())
						throw(new InterruptedException());
					lock.lockInterruptibly();
					try {
						List<Long> transSeqs = memoryStore.getTransactionSequenceNumbers();
						fileStore.cycle(transSeqs);
					} finally {
						lock.unlock();
					}
					cyclesWithoutDeletion++;
					if (cyclesWithoutDeletion > cyclesPerOldFileDeletion) {
						fileStore.deleteOldFiles();
						cyclesWithoutDeletion = 0;
					}
				} while (true);
			}
			catch (InterruptedException e) {
				maintenanceAbort(e);
			} catch (IOException e) {
				maintenanceAbort(e);
			} catch (TransactionManagerFilestoreClosedException e) {
				maintenanceAbort(e);
			}
		}
	
	}	
}
