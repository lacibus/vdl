package org.lacibus.storeaccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.NotInOperationException;
import org.lacibus.triplestore.StoreSession;
import org.lacibus.triplestore.Triple;
import org.lacibus.triplestore.TripleStore;
import org.lacibus.triplestore.TripleStoreFatalException;
import org.lacibus.triplestore.TripleStoreNonFatalException;
import org.lacibus.triplesource.SourceConnectorException;

/**
 * An instance of this class applies a set of constraints to the triples 
 * in a triple store to obtain a set of constrained triples.
 */

public class Join {
	
	/**
	 * Constant used to determine the join limit size
	 */
	
	public final static int limitSizeMultiplier = 25;
	
	/**
	 * The constraints that are applied.
	 */
	
	JoinConstraint[] constraints;
	
	/**
	 * <p>The sections of the join currently being processed, defined by 
	 * subsets of the join constraints.
	 * </p>
	 * <p>Initially, there is a section for each constraint, defined by the 
	 * singleton set containing that constraint. As the join proceeds,
	 * sections are joined and their defining sets are merged.
	 * </p>
	 */
	
	Set<Section> sections = new HashSet<Section>();
	
	/**
	 * The limit to the size of the join. It is determined by multiplying the
	 * size indicator of the triple store by the limit size multiplier
	 */
	
	long limit = 0;
	
	
	/**
	 * Create a join
	 * 
	 * @param constraints the constraints that are applied in the join
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public Join(JoinConstraint[] constraints)
					throws TripleStoreNonFatalException {
		
		this.constraints = constraints;
		int ix = 0;
		for (JoinConstraint constraint : constraints) {
			sections.add(new QuerySection(this, constraint, ix++));
		}
	}
	
	/**
	 * Copy a join
	 * 
	 * @param join the join to be copied
	 * 
	 * @return a copy of the join
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public static Join copy(Join join) throws TripleStoreNonFatalException {
		
		Join copyJoin = new Join(join.constraints);
		copyJoin.limit = join.limit;
		copyJoin.sections = new HashSet<Section>(join.sections);
		return copyJoin;
	}
	
	/**
	 * Make the join
	 * 
	 * @param store the triple store to whose triples the constraints
	 * are to be applied
	 * 
	 * @param session the requesting store session.
	 * 
	 * @return a list of the constrained triples resulting from the join 
	 * 
	 * @throws JoinException
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	public List<ConstrainedTriples> make(
			TripleStore store,
			StoreSession session) 
					throws 	JoinException, 
							TripleStoreNonFatalException, 
							TripleStoreFatalException, 
							SourceConnectorException, 
							NotInOperationException {
		
		return make(store, session, true);
	}
	
	public List<ConstrainedTriples> make(

			TripleStore store,
			StoreSession session, boolean test) 
					throws 	JoinException, 
							TripleStoreNonFatalException, 
							TripleStoreFatalException, 
							SourceConnectorException, 
							NotInOperationException {
		
		// Determine the size limit for the join
		limit = (long)store.sizeIndicator()*(long)limitSizeMultiplier;
		
		// Set up each section by doing a query for
		// triples that satisfy its constraint, provided
		// that each previous section has some entries
		boolean nullJoin = false;
		for (Section section : sections) {
			if (!nullJoin) {
				((QuerySection) section).doQuery(store, session);
				// If the constraint is not satisfied by any triples,
				// then the complete set of constraints cannot be
				// satisfied. Proceed with the join in case it is
				// subsequently extended, but do not perform further
				// queries.
				if (section.entries.length == 0) nullJoin = true;
			}			
		}
		
		// Join the queries and return the result
		return joinQueries(store, session);
	}
	
	/**
	 * Extend the join by adding new constraints, and make
	 * the extended join.
	 * 
	 * @param newConstraints new constraints to be added to
	 * the join
	 * 
	 * @param store the triple store in which the join is
	 * being extended
	 * 
	 * @param session the requesting store session
	 * 
	 * @return a list of the constrained triples resulting from the 
	 * extended join. Each of these will be an extension of a
	 * constrained triples from the original join. Some constrained
	 * triples from the original join may not have extensions that
	 * are present in the extended join. 
	 * 
	 * @throws JoinException
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	public List<ConstrainedTriples> extend(
			JoinConstraint[] newConstraints,
			TripleStore store,
			StoreSession session) 
					throws 	JoinException, 
							TripleStoreNonFatalException, 
							TripleStoreFatalException, 
							SourceConnectorException, 
							NotInOperationException {
		
		// Add the new constraints to the constraints
		JoinConstraint[] extendedConstraints = 
				new JoinConstraint[constraints.length + newConstraints.length];
		System.arraycopy(constraints, 0, extendedConstraints, 0, constraints.length);
		System.arraycopy(newConstraints, 0, extendedConstraints, constraints.length,
				newConstraints.length);
		constraints = extendedConstraints;
		
		// Add a section for each new constraint, numbering it
		// to follow the original sections, and setting it up 
		// by doing a query for triples that satisfy the
		// constraint, provided that each previous section
		// has some entries.
		boolean nullJoin = false;
		for (Section section : sections) {
			if ((section.entries == null) || (section.entries.length == 0)) {
				nullJoin = true;
				break;
			}
		}
		int ix = constraints.length - newConstraints.length;
		for (JoinConstraint constraint : newConstraints) {
			QuerySection qs = new QuerySection(this, constraint, ix++);
			sections.add(qs);
			if (!nullJoin) {
				qs.doQuery(store, session);
				// If the constraint is not satisfied by any triples,
				// then the complete set of constraints cannot be
				// satisfied. Proceed with the join in case it is
				// subsequently extended, but do not perform further
				// queries.
				if (qs.entries.length == 0) nullJoin = true;
			}
		}
		
		// Join the queries and return the result
		return joinQueries(store, session);
	}
		
	/**
	 * Join the queries of the join
	 * 
	 * @param store the triple store in which the join is
	 * being made or extended
	 * 
	 * @param session the requesting store session
	 * 
	 * @return a list of the constrained triples resulting from the join 
	 * 
	 * @throws JoinException
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	private List<ConstrainedTriples> joinQueries(
			TripleStore store,
			StoreSession session) 
					throws 	JoinException, 
							TripleStoreNonFatalException, 
							TripleStoreFatalException, 
							SourceConnectorException, 
							NotInOperationException {
		
		// Set up the result
		List<ConstrainedTriples> results = new ArrayList<ConstrainedTriples>();
		
		// Perform the join and get the section that was formed by 
		// merging all the original sections
		Section mergedSection = perform();		
		if (mergedSection == null) return results;
		
		// If there are no results, return
		if ((mergedSection == null) || (mergedSection.entries == null))
			return results;

		// The triples in the entry are ordered by the constraints map, 
		// and do not follow the original order of constraints, so must be
		// re-ordered.
		mergedSection.arrangeEntriesByConstraints();
		
		// For each entry in the merged section, create a
		// result element and add it to the results, unless
		// it is a spurious entry caused by using hashes 
		// rather than actual values in the join
		Section.Entry[] entries = mergedSection.entries;
		for (int i = 0; i < entries.length; i++) {
			ConstrainedTriples result = 
					getConstrainedTriples(entries[i].triples, session);
			if (result != null) results.add(result);
		}
		
		// Return the result
		return results;
	}
	
	/**
	 * Perform the join by picking a section then picking all the other sections
	 * in turn to merge into it.
	 *  
	 * @throws JoinException
	 */
	
	private Section perform() throws JoinException {
		
		Section mergedSection = null;
		Set<String> previouslyConstrainedUnknowns = new HashSet<String>();
		Set<Section> sectionsToBeMerged = new HashSet<Section>(sections);
		while(sectionsToBeMerged.size() > 0) {
			// Find best candidate section to merge
			int bestScore = 0;
			int bestSize = 0;
			Section bestSection = null;
			for (Section section : sectionsToBeMerged) {
				int score = getScore(section, previouslyConstrainedUnknowns);
				int size = section.sizeIndicator();
				if (	(bestSection == null) || 
						(score > bestScore) ||
						(	(score == bestScore) && (size < bestSize))) {
					bestSection = section;
					bestScore = score;
					bestSize = size;
				}
			}
			if (mergedSection == null) {
				mergedSection = bestSection;
				addConstrainedUnknowns(previouslyConstrainedUnknowns, bestSection);
				sectionsToBeMerged.remove(bestSection);
			}
			else {
				// Check that the size is within the limit
				if (mergedSection.sizeIndicator() > limit) {
					throw (new JoinException("Join exceeds limit"));
				}
				if (bestSize > limit) {
					throw (new JoinException("Join section exceeds limit"));
				}

				// Merge the pair
				try {
					SectionPair pair = new SectionPair(mergedSection, bestSection);
					pair.join();
				
					// Update the previously constrained unknowns
					addConstrainedUnknowns(previouslyConstrainedUnknowns, bestSection);
			
					// Replace the merged sections by the resulting new section
					sectionsToBeMerged.remove(bestSection);
					mergedSection = new Section(pair);
				} catch (OutOfMemoryError oom) {
					throw (new JoinException("Join caused out of memory error"));
				}
			}
		}		
		sections = new HashSet<Section>();
		if (mergedSection != null) sections.add(mergedSection);
		return mergedSection;
	}
	
	/**
	 * Get the score of a section determined by how many givens and
	 * previously constrained unknowns its join constraints have. 
	 * The sections with the highest scores will be considered for 
	 * the next merge, depending on their sizes.
	 * 
	 * @param section a section
	 * 
	 * @param previouslyConstrainedUnknowns the set of previously constrained 
	 * unknowns
	 * 
	 * @return the score of the section
	 */
	
	private int getScore(Section section, Set<String> previouslyConstrainedUnknowns) {
		
		int score = 0;
		for (int i = 0; i < section.constraintIndexes.length; i++) {
			JoinConstraint jc = constraints[section.constraintIndexes[i]];
			score = score + getScore(jc, previouslyConstrainedUnknowns);
		}
		return score;
	}
	
	/**
	 * Get the score of a join constraint determined by how many givens and
	 * previously constrained unknowns it has. 
	 * 
	 * @param jc a join constraint
	 * 
	 * @param previouslyConstrainedUnknowns the set of previously constrained 
	 * unknowns
	 * 
	 * @return the score of the join constraint
	 */
	
	private int getScore(JoinConstraint jc, Set<String> previouslyConstrainedUnknowns) {
		
		int score = 0;
		String subj = jc.subject.id;
		if (subj == null) score = score + 3;
		else if (previouslyConstrainedUnknowns.contains(subj)) score = score + 2;
		String verb = jc.verb.id;
		if (verb == null) score = score + 3;
		else if (previouslyConstrainedUnknowns.contains(verb)) score = score + 2;
		String obj = jc.object.id;
		if (obj == null) score = score + 3;
		else if (previouslyConstrainedUnknowns.contains(obj)) score = score + 2;
		return score;
	}
	
	/**
	 * Add the unknowns of a section to the set of previously constrained 
	 * unknowns
	 * 
	 * @param previouslyConstrainedUnknowns the set of previously constrained 
	 * unknowns
	 * 
	 * @param section a section with unknowns to be added
	 */
	
	private void addConstrainedUnknowns(
			Set<String> previouslyConstrainedUnknowns, Section section) {
		
		for (int i = 0; i < section.constraintIndexes.length; i++) {
			JoinConstraint jc = constraints[section.constraintIndexes[i]];
			String subj = jc.subject.id;
			if (subj != null) previouslyConstrainedUnknowns.add(subj);
			String verb = jc.verb.id;
			if (verb != null) previouslyConstrainedUnknowns.add(verb);
			String obj = jc.object.id;
			if (obj != null) previouslyConstrainedUnknowns.add(obj);
		}
	}
	
	/**
	 * Get a constrained triples record from a triples array. This is
	 * done by checking that the triples in the given array satisfy
	 * the constraints and creating an assignments map for them.
	 * 
	 * @param triples an array of triples that correspond to the
	 * constraints of the join.
	 * 
	 * @param session the requesting store session
	 * 
	 * @return an instance of the constrained triples class with the
	 * given array of triples and the assignment of values to unknowns
	 * that it implies, or null if there is no implied assignment of
	 * values to unknowns.
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	public ConstrainedTriples getConstrainedTriples(
			Triple[] triples, StoreSession session) 
					throws 	TripleStoreNonFatalException,
							TripleStoreFatalException, 
							SourceConnectorException, 
							NotInOperationException {

		if (triples.length != constraints.length) return null;
		Map <String, Datum> varsMap = new HashMap <String, Datum>();
		for (int i = 0; i < constraints.length; i++) {
			JoinConstraint constraint = constraints[i];
			Triple triple = triples[i];
			String var = constraint.subject.getId();			
			if (var != null) {
				Datum val = Datum.create(triple.getSubject());
				Datum prevVal = varsMap.get(var);
				if (prevVal == null) varsMap.put(var, val);
				else if (!val.equals(prevVal)) return null;
			}
			var = constraint.verb.getId();
			if (var != null) {
				Datum val = Datum.create(triple.getVerb());
				Datum prevVal = varsMap.get(var);
				if (prevVal == null) varsMap.put(var, val);
				else if (!val.equals(prevVal)) return null;
			}
			var = constraint.object.getId();
			if (var != null) {
				Datum val = triple.getObject(session);
				if (val == null) return null;
				Datum prevVal = varsMap.get(var);
				if (prevVal == null) varsMap.put(var, val);
				else if (!val.equals(prevVal)) return null;
			}
		}
		return new ConstrainedTriples(triples, varsMap);
	}
}
