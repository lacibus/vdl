/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.lacibus.triplesource.SourceConnector;
import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplestore.AccessLevel;
import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Id;
import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.NotInOperationException;
import org.lacibus.triplestore.StoreSession;
import org.lacibus.triplestore.TripleStore;
import org.lacibus.triplestore.TripleStoreFatalException;
import org.lacibus.triplestore.TripleStoreNonFatalException;

/**
 * <p>An instance of this class executes on a triple store a set of commands 
 * contained in a JSON array.
 * </p>
 * <p>The following commands can be supplied for execution. A <i>string</i>,
 * a <i>handle</i>, an <i>item</i>, or an <i>access level</i> is supplied as 
 * a text string in quotes. A <i>non-item</i> can be a boolean (<i>true</i> or 
 * <i>false</i>), an integer, a decimal number, or a text string in quotes. 
 * A <i>b64</i> is a base64-encoded binary value. A <i>source</i> is supplied 
 * as an integer. A <i>json object</i> is supplied as the textual representation
 * of a JSON object ({ . . . }). The text string describing an item or an 
 * access level can be a handle set in a previous command, the textual 
 * representation of an item identifier (<i>sourcenr</i>:<i>itemnr</i>) or 
 * a numeric source identifier and the name of a named item in that source, 
 * separated by a colon (<i>sourcenr</i>:<i>name</i>). It MUST NOT be a
 * textual source name and the name of a named item in that source.
 * </p>
 * <p>The result returned from the execution is a map of the values assigned
 * to handles.
 * </p> 
 * <p><b>Get or create a named item.</b><br><pre>
 *    "command"="named item";
 *    "source"=<i>source</i>;
 *    "name"=<i>string</i>;
 *    "handle"=<i>handle</i>
 * </pre>   
 * </p>
 * <p><b>Get the unique subject with a given verb and item object.</b><br><pre>
 *    "command"="get unique subject with item object";
 *    "verb"=<i>item</i>;
 *    "object"=<i>item</i>;
 *    "handle"=handle (optional)
 * </pre>   
 * </p>
 * <p><b>Get the unique subject with a given verb and item object,
 * creating it in the given source and with the given access levels
 * if it does not exist.</b><br><pre>
 *    "command"="unique subject with item object";
 *    "source"=<i>source</i>;
 *    "read"=<i>access level</i>;
 *    "write"=<i>access level</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>item</i>;
 *    "handle"=handle (optional)
 * </pre>   
 * </p>
 * <p><b>Get the unique subject with a given verb and boolean, integral,
 * real or string object.</b><br><pre>
 *    "command"="get unique subject with non-item object";
 *    "verb"=<i>item</i>;
 *    "object"=<i>non-item</i>;
 *    "handle"=handle (optional)
 * </pre> 
 * </p>  
 * <p><b>Get the unique subject with a given verb and boolean, integral,
 * real or string object, creating it in the given source and with the 
 * given access levels if it does not exist.</b><br><pre>
 *    "command"="unique subject with non-item object";
 *    "source"=<i>source</i>;
 *    "read"=<i>access level</i>;
 *    "write"=<i>access level</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>non-item</i>;
 *    "handle"=handle (optional)
 * </pre>
 * </p>
 * <p><b>Get the unique subject with a given verb and binary object.</b><br><pre>
 *    "command"="get unique subject with binary object";
 *    "verb"=<i>item</i>;
 *    "object"=<i>b64</i>;
 *    "handle"=handle (optional)
 * </pre>
 * </p> 
 * <p><b>Get the unique subject with a given verb and binary object, 
 * creating it in the given source and with the given access levels 
 * if it does not exist.</b><br><pre>
 *    "command"="unique subject with binary object";
 *    "source"=<i>source</i>;
 *    "read"=<i>access level</i>;
 *    "write"=<i>access level</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>b64</i>;
 *    "handle"=handle (optional)
 * </pre>   
 * <p><b>Get the item that is the unique object with a given subject
 * and verb, creating it in the given source and with the given access
 * levels, and also creating the appropriate triple, if it does not 
 * exist.</b><br><pre>
 *    "command"="unique item object";
 *    "source"=<i>source</i>;
 *    "subject"=<i>item</i>;
 *    "verb"=<i>item</i>;
 *    "read"=<i>access level</i>;
 *    "write"=<i>access level</i>;
 *    "handle"=handle (optional)
 * </pre>   
 * </p>
 * </p>
 * <p><b>Create an item.</b><br><pre>
 *    "command"="create item";
 *    "source"=<i>source</i>;
 *    "read"=<i>access level</i>;
 *    "write"=<i>access level</i>;
 *    "handle"=handle
 * </pre>   
 * </p>
 * <p><b>Ensure that there is a unique triple with a given subject, verb 
 * and object, where the object is an item.</b><br><pre>
 *    "command"="put triple with item object";
 *    "subject"=<i>item</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>item</i>
 * </pre>   
 * </p>
 * <p><b>Ensure that there is a unique triple with a given subject, verb 
 * and object, where the object is a boolean, integer, real, or string.
 * </b><br><pre>
 *    "command"="put triple with non-item object";
 *    "subject"=<i>item</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>non-item</i>
 * </pre>   
 * </p>
 * <p><b>Ensure that there is a unique triple with a given subject, verb 
 * and object, where the object is a binary.
 * </b><br><pre>
 *    "command"="put triple with non-item object";
 *    "subject"=<i>item</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>b64</i>
 * </pre>   
 * </p>
 * <p><b>Ensure that there is a unique triple with a given subject and verb,
 * and that it has the given object, which is an item.</b><br><pre>
 *    "command"="set unique item object";
 *    "subject"=<i>item</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>item</i>
 * </pre>   
 * </p>
 * <p><b>Ensure that there is a unique triple with a given subject and verb,
 * and that it has the given object, which is a boolean, integer, real or
 * string.</b><br><pre>
 *    "command"="set unique non-item object";
 *    "subject"=<i>item</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>non-item</i>
 * </pre>   
 * </p>
 * <p><b>Ensure that there is a unique triple with a given subject and verb,
 * and that it has the given object, which is a binary.</b><br><pre>
 *    "command"="set unique non-item object";
 *    "subject"=<i>item</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>b64</i>
 * </pre>   
 * </p>
 * <p><b>Ensure that there is a unique triple with a given subject and verb,
 * and that it has the given object, which is the textual representation of
 * a JSON object.</b><br><pre>
 *    "command"="set unique json object";
 *    "subject"=<i>item</i>;
 *    "verb"=<i>item</i>;
 *    "object"=<i>json object</i>
 * </pre>   
 * </p>
 */

public class Executor implements Serializable {

	private static final long serialVersionUID = -7107435303289229811L;
	
	/**
	 * The triple store on which the commands are executed.
	 */
	
	TripleStore store;
	
	/**
	 * Create an executor
	 * 
	 * @param store the triple store on which the commands are executed
	 */
			
	public Executor (TripleStore store) {
		
		this.store = store;
	}

	/**
	 * Execute the commands in a JSON array
	 * 
	 * @param script a JSON array containing commands to be executed
	 * 
	 * @param session the requesting store session
	 * 
	 * @throws Exception
	 */
	
	public Map<String, Datum> execute(
			JSONArray script, Map<String, Datum> binaries,
			StoreSession session) throws Exception {
		
		Map<String, Datum> handles = new HashMap<String, Datum>();
		
		for (Object obj : script) executeJsonCommand(
				getJsonCommand(obj), handles, binaries, session);
		return handles;
	}
	
	/**
	 * Get a command from an object
	 * 
	 * @param obj an object
	 * 
	 * @return the object cast as a JSON object if it can be so cast
	 * 
	 * @throws InputException
	 */
	
	public JSONObject getJsonCommand(Object obj) throws InputException {
		
		if (obj == null) throw (new InputException("Null script command"));
		try {
			return (JSONObject) obj; 
		} catch (Exception e) {
			InputException x = new InputException("Invalid script command");
			x.initCause(e);
			throw(x);
		}
	}
	
	/**
	 * Execute a command
	 * 
	 * @param jo a JSON object containing the command to be executed
	 * 
	 * @param handles the handles for items created by previous commands,
	 * and to which a new handle can be added if an item is created by
	 * this command.
	 * 
	 * @param session the requesting store session
	 * 
	 * @throws InputException
	 */
	
	public void executeJsonCommand(JSONObject jo, Map<String, Datum> handles, 
			Map<String, Datum> binaries, StoreSession session)
					throws Exception {
		
		String command = (String)jo.get("command");
		
		if ("named item".equals(command)) {
			String name = getStringFromJson("name", jo);
			if (name == null) throw (new InputException("Name missing in command " + jo));
			Long sourceNr = getLongFromJson("source", jo); 
			if (sourceNr == null) throw (new InputException("Source missing in command " + jo));
			Item item = store.getOrCreateNamedItem(sourceNr, name, session);
			if (item == null) throw (new AccessException(
					"Cannot get or create named item for command " + jo));
			String handle = (String)jo.get("handle");
			if (handle != null) handles.put(handle, Datum.create(item));
		}
			
		else if ("get unique subject with item object".equals(command)) {
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Item object = getItemFromJson("object", jo, handles, session);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			Item subject = StoreAccess.getUniqueSubject(verb, Datum.create(object), store, session);
			if (subject == null) throw (new AccessException(
					"Cannot get unique subject for command " + jo));
			String handle = (String)jo.get("handle");
			if (handle != null) handles.put(handle, Datum.create(subject));
		}
			
		else if ("unique subject with item object".equals(command)) {
			Long sourceNr = getLongFromJson("source", jo); 
			if (sourceNr == null) throw (new InputException("Source missing in command " + jo));
			AccessLevel onlyReadLevel = getAccessLevelFromJson("read", jo, handles, session);
			if (onlyReadLevel == null) throw (new InputException(
					"Read level missing in command " + jo));
			AccessLevel readOrWriteLevel = getAccessLevelFromJson("write", jo, handles, session);
			if (readOrWriteLevel == null) throw (new InputException(
					"Write level missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Item object = getItemFromJson("object", jo, handles, session);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			Item subject = StoreAccess.uniqueSubject(
					sourceNr, onlyReadLevel, readOrWriteLevel, verb, 
					Datum.create(object), store, session);
			if (subject == null) throw (new AccessException(
					"Cannot get or create unique subject for command " + jo));
			String handle = (String)jo.get("handle");
			if (handle != null) handles.put(handle, Datum.create(subject));
		}
			
		else if ("get unique subject with non-item object".equals(command)) {
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Datum object = getBoolIntRealOrStringFromJson("object", jo, session);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			Item subject = StoreAccess.getUniqueSubject(verb, object, store, session);
			if (subject == null) throw (new AccessException(
					"Cannot get unique subject for command " + jo));
			String handle = (String)jo.get("handle");
			if (handle != null) handles.put(handle, Datum.create(subject));
		}
			
		else if ("unique subject with non-item object".equals(command)) {
			Long sourceNr = getLongFromJson("source", jo); 
			if (sourceNr == null) throw (new InputException("Source missing in command " + jo));
			AccessLevel onlyReadLevel = getAccessLevelFromJson("read", jo, handles, session);
			if (onlyReadLevel == null) throw (new InputException(
					"Read level missing in command " + jo));
			AccessLevel readOrWriteLevel = getAccessLevelFromJson("write", jo, handles, session);
			if (readOrWriteLevel == null) throw (new InputException(
					"Write level missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Datum object = getBoolIntRealOrStringFromJson("object", jo, session);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			Item subject = StoreAccess.uniqueSubject(
					sourceNr, onlyReadLevel, readOrWriteLevel, verb, object, store, session);
			if (subject == null) throw (new AccessException(
					"Cannot get or create unique subject for command " + jo));
			String handle = (String)jo.get("handle");
			if (handle != null) handles.put(handle, Datum.create(subject));
		}
		
		else if ("get unique subject with binary object".equals(command)) {
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Datum object = getBinaryFromJson("object", binaries, jo);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			Item subject = StoreAccess.getUniqueSubject(verb, object, store, session);
			if (subject == null) throw (new AccessException(
					"Cannot get unique subject for command " + jo));
			String handle = (String)jo.get("handle");
			if (handle != null) handles.put(handle, Datum.create(subject));
		}
			
		else if ("unique subject with binary object".equals(command)) {
			Long sourceNr = getLongFromJson("source", jo); 
			if (sourceNr == null) throw (new InputException("Source missing in command " + jo));
			AccessLevel onlyReadLevel = getAccessLevelFromJson("read", jo, handles, session);
			if (onlyReadLevel == null) throw (new InputException(
					"Read level missing in command " + jo));
			AccessLevel readOrWriteLevel = getAccessLevelFromJson("write", jo, handles, session);
			if (readOrWriteLevel == null) throw (new InputException(
					"Write level missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Datum object = getBinaryFromJson("object", binaries, jo);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			Item subject = StoreAccess.uniqueSubject(
					sourceNr, onlyReadLevel, readOrWriteLevel, verb, object, store, session);
			if (subject == null) throw (new AccessException(
					"Cannot get or create unique subject for command " + jo));
			String handle = (String)jo.get("handle");
			if (handle != null) handles.put(handle, Datum.create(subject));
		}
			
		else if ("unique item object".equals(command)) {
			Long sourceNr = getLongFromJson("source", jo); 
			if (sourceNr == null) throw (new InputException("Source missing in command " + jo));
			Item subject = getItemFromJson("subject", jo, handles, session);
			if (subject == null) throw (new InputException("Subject missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			AccessLevel onlyReadLevel = getAccessLevelFromJson("read", jo, handles, session);
			if (onlyReadLevel == null) throw (new InputException(
					"Read level missing in command " + jo));
			AccessLevel readOrWriteLevel = getAccessLevelFromJson("write", jo, handles, session);
			if (readOrWriteLevel == null) throw (new InputException(
					"Write level missing in command " + jo));
			Item object = StoreAccess.uniqueItemObject(subject, verb, sourceNr, 
					onlyReadLevel, readOrWriteLevel, store, session);
			if (object == null) throw (new AccessException(
					"Cannot get or create unique item object for command " + jo));
			String handle = (String)jo.get("handle");
			if (handle != null) handles.put(handle, Datum.create(object));
		}
			
		else if ("create item".equals(command)) {
			Long sourceNr = getLongFromJson("source", jo); 
			if (sourceNr == null) throw (new InputException("Source missing in command " + jo));
			AccessLevel onlyReadLevel = getAccessLevelFromJson("read", jo, handles, session);
			if (onlyReadLevel == null) throw (new InputException(
					"Read level missing in command " + jo));
			AccessLevel readOrWriteLevel = getAccessLevelFromJson("write", jo, handles, session);
			if (readOrWriteLevel == null) throw (new InputException(
					"Write level missing in command " + jo));
			String handle = (String)jo.get("handle");
			if (handle == null) throw (new InputException("Handle missing in command " + jo));
			SourceConnector connector = store.getSourceConnector(sourceNr);
			if (connector == null) throw (new InputException(
					"Source " + sourceNr + " is not connected for command " + jo));
			Item item = store.createUnlockedItem(connector, onlyReadLevel, readOrWriteLevel, session);
			if (item == null) throw (new AccessException(
					"Cannot create item for command " + jo));
			handles.put(handle, Datum.create(item));
		}
		
		else if ("put triple with item object".equals(command)) {
			Item subject = getItemFromJson("subject", jo, handles, session);
			if (subject == null) throw (new InputException("Subject missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Item object = getItemFromJson("object", jo, handles, session);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			if (StoreAccess.setUniqueTriple(
					subject, verb, Datum.create(object), store, session) == null)
				throw (new AccessException("Cannot put triple for command " + jo));
		}
			
		else if ("put triple with non-item object".equals(command)) {
			Item subject = getItemFromJson("subject", jo, handles, session);
			if (subject == null) throw (new InputException("Subject missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Datum object = getBoolIntRealOrStringFromJson("object", jo, session);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			if (StoreAccess.setUniqueTriple(
					subject, verb, object, store, session) == null)
				throw (new AccessException("Cannot put triple for command " + jo));
		}
		
		else if ("put triple with binary object".equals(command)) {
			Item subject = getItemFromJson("subject", jo, handles, session);
			if (subject == null) throw (new InputException("Subject missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Datum object = getBinaryFromJson("object", binaries, jo);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			if (StoreAccess.setUniqueTriple(
					subject, verb, object, store, session) == null)
				throw (new AccessException("Cannot put triple for command " + jo));
		}
			
		else if ("set unique item object".equals(command)) {
			Item subject = getItemFromJson("subject", jo, handles, session);
			if (subject == null) throw (new InputException("Subject missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Item object = getItemFromJson("object", jo, handles, session);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			if (StoreAccess.setUniqueObject(
					subject, verb, Datum.create(object), store, session) == null)
				throw (new AccessException("Cannot set unique object for command " + jo));
		}
		
		else if ("set unique non-item object".equals(command)) {
			Item subject = getItemFromJson("subject", jo, handles, session);
			if (subject == null) throw (new InputException("Subject missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Datum object = getBoolIntRealOrStringFromJson("object", jo, session);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			if (StoreAccess.setUniqueObject(
					subject, verb, object, store, session) == null)
				throw (new AccessException("Cannot set unique object for command " + jo));
		}
		
		else if ("set unique binary object".equals(command)) {
			Item subject = getItemFromJson("subject", jo, handles, session);
			if (subject == null) throw (new InputException("Subject missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			Datum object = getBinaryFromJson("object", binaries, jo);
			if (object == null) throw (new InputException("Object missing in command " + jo));
			if (StoreAccess.setUniqueObject(
					subject, verb, object, store, session) == null)
				throw (new AccessException("Cannot set unique object for command " + jo));
		}
		
		else if ("set unique json object".equals(command)) {
			Item subject = getItemFromJson("subject", jo, handles, session);
			if (subject == null) throw (new InputException("Subject missing in command " + jo));
			Item verb = getItemFromJson("verb", jo, handles, session);
			if (verb == null) throw (new InputException("Verb missing in command " + jo));
			JSONObject object = (JSONObject)jo.get("object");
			if (object == null) throw (new InputException("Object missing in command " + jo));
			if (StoreAccess.setUniqueObject(
					subject, verb, Datum.createText(object.toString()), store, session) == null)
				throw (new AccessException("Cannot set unique object for command " + jo));
		}
	}

	/**
	 * Get a Long value from a JSON object
	 * 
	 * @param name the name in a name-value pair in the JSON object
	 * 
	 * @param jo a JSON object
	 * 
	 * @return the value of the name-value pair if it can be cast
	 * as Long
	 */
	
	public Long getLongFromJson(String name, JSONObject jo) {
		
		Long l = null;
		try {
			l = (Long)jo.get(name);
		} catch (Exception e) {} 
		return l;
	}

	/**
	 * Get a String value from a JSON object
	 * 
	 * @param name the name in a name-value pair in the JSON object
	 * 
	 * @param jo a JSON object
	 * 
	 * @return the value of the name-value pair if it can be cast
	 * as String
	 */
	
	public String getStringFromJson(String name, JSONObject jo) {
		
		String s = null;
		try {
			s = (String)jo.get(name);
		} catch (Exception e) {} 
		return s;
	}
	
	/**
	 * Get a boolean, integer, real, or string from a JSON object
	 * 
	 * @param name the name in a name-value pair in the JSON object
	 * 
	 * @param jo a JSON object
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the Boolean, Long, Double or String that is the value 
	 * of the name-value pair if this can be so cast.
	 */
	
	public Datum getBoolIntRealOrStringFromJson(
			String name, JSONObject jo, StoreSession session) {
		
		Object o = jo.get(name);
		try {
			return Datum.create((Boolean)o);
		} catch (Exception e) {} 
		try {
			return Datum.create((Long)o);
		} catch (Exception e) {} 
		try {
			return Datum.create((Double)o);
		} catch (Exception e) {} 
		try {
			return Datum.createText((String)o);
		} catch (Exception e) {} 
		return null;
	}
	
	/**
	 * Get a binary value from a JSON object and a map of binary
	 * values
	 * 
	 * @param name the name in a name-value pair in the JSON object
	 * 
	 * @param binaries a map giving the binary values referred to in
	 * the JSON object
	 * 
	 * @param jo a JSON object
	 * 
	 * @return the byte array that is the base64-decoded value 
	 * of the name-value pair if this can be cast to a string.
	 * 
	 * @throws AccessException 
	 */
	
	public Datum getBinaryFromJson(String name, Map<String, Datum> binaries, JSONObject jo)
			throws AccessException {
		
		if (binaries == null) throw (new AccessException("No binaries supplied"));
		try {
			JSONObject binjo = (JSONObject)jo.get(name);
			return binaries.get(binjo.get("bin"));
		} catch (Exception e) { 
			return null;
		}
	}
	
	/**
	 * Get an access level specified in a JSON object
	 * 
	 * @param name the name in a name-value pair in the JSON object
	 * 
	 * @param jo a JSON object
	 * 
	 * @param assignments the assignments created by previous
	 * commands
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the access level specified by the value of the 
	 * name-value pair if this can be cast as String
	 */
	
	public AccessLevel getAccessLevelFromJson(
			String name, JSONObject jo, Map<String, Datum> assignments, StoreSession session) 
					throws Exception {
		
		String s = null;
		try {
			s = (String)jo.get(name);
		} catch (Exception e) {} 
		if (s == null) return null;
		
		// If this is the public level, return it
		if (s.equals("public")) return store.lowestAccessLevel();
		
		// Get the item specified by the value assigned to the name
		Item item = getSpecifiedItem(s, assignments, session);
		if (item == null) return null;
		
		// Return the access level represented by the item
		return store.getAccessLevel(item, session);
	}
	
	/**
	 * Get an item specified in a JSON object
	 * 
	 * @param name the name in a name-value pair in the JSON object
	 * 
	 * @param jo a JSON object
	 * 
	 * @param assignments the assignments created by previous
	 * commands
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the item specified by the value of the name-value 
	 * pair if this can be cast as String
	 */
	
	public Item getItemFromJson(
			String name, JSONObject jo, Map<String, Datum> assignments, StoreSession session) 
					throws Exception {
		
		String s = null;
		try {
			s = (String)jo.get(name);
		} catch (Exception e) {} 
		if (s == null) return null;
		return getSpecifiedItem(s, assignments, session);
	}
	
	/**
	 * Get an item from its specification
	 * 
	 * @param spec a specification of an item
	 * 
	 * @param assignments the assignments created by previous
	 * commands
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the specified item, or null if it cannot be found
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 * @throws TripleStoreFatalException
	 */
	
	private Item getSpecifiedItem(String spec, Map<String, Datum> assignments, StoreSession session)
			throws 	NotInOperationException, 
					TripleStoreNonFatalException, 
					SourceConnectorException, 
					TripleStoreFatalException {
		
		// If the item is specified by its identifier, return it
		Id id = Id.parseId(spec);
		if (id != null) return store.getItem(id, session);
		
		// If the item is specified by its name, return it
		int colonix = spec.indexOf(':');
		if (colonix >= 0) return getNamedItem(spec, session);

		// If the item is specified by a handle, return it
		if (assignments != null) {
			Item item = null;
			try {
				item = assignments.get(spec).getItemValue();
			} catch (Exception e) {}
			if (item != null) return item;
		}
			
		// Return null
		return null;
	}
	
	/**
	 * Get a named item from its specification
	 * 
	 * @param spec a specification of a named item
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the specified named item, or null if it cannot be found
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 * @throws TripleStoreFatalException
	 * @throws NotInOperationException
	 */
	
	public Item getNamedItem(String spec, StoreSession session) 
			throws 	TripleStoreNonFatalException,
					SourceConnectorException, 
					TripleStoreFatalException,
					NotInOperationException {
		
		int colonix = spec.indexOf(':');
		if (colonix < 0) return null;
		String sourceName = spec.substring(0, colonix);
		String itemName = spec.substring(colonix + 1);
		Long sourceNr = null;
		try {
			sourceNr = Long.parseLong(sourceName);
		} catch (Exception e) {}
		if (sourceNr == null) return null;
		return store.getNamedItem(sourceNr, itemName, session);
	}	
}
