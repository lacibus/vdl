/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

/**
 * A quantity that is unknown, identified by a string 
 * (such as &quot;x&quot;).
 */

public class Unknown extends Quantum {

	/**
	 * Create an unknown
	 * 
	 * @param x the identifier of the unknown to be created
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Unknown(String x) throws NullQuantumException {
		
		super(x);
	}
}
