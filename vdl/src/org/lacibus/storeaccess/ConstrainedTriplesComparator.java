/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import java.util.Comparator;

/**
 * A comparator that orders constrained triples by the values of
 * their assignments to unknowns in a given order of importance.
 */

class ConstrainedTriplesComparator implements Comparator<ConstrainedTriples> {

	/**
	 * The unknowns used in the comparison, in order of importance
	 */
	
	String[] keys;
	
	/**
	 * Create a constrained triples comparator
	 * 
	 * @param keys the unknowns to be used in the comparison, in order of 
	 * importance
	 */
	
	ConstrainedTriplesComparator(String[] keys) {
		
		this.keys = keys;
	}
	
	@Override
	public int compare(ConstrainedTriples cts1, ConstrainedTriples cts2) {

		for (String key : keys) {
			int c = cts1.getAssignment(key).compareTo(cts2.getAssignment(key));
			if (c != 0) return c;
		}
		return 0;
	}
}
