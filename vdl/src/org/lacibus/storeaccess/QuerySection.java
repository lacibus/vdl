/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.NotInOperationException;
import org.lacibus.triplestore.StoreSession;
import org.lacibus.triplestore.Triple;
import org.lacibus.triplestore.TripleStore;
import org.lacibus.triplestore.TripleStoreFatalException;
import org.lacibus.triplestore.TripleStoreNonFatalException;
import org.lacibus.triplesource.SourceConnectorException;

/**
 * A section defined by a query of a triple store
 */

class QuerySection extends Section {
	
	/**
	 * <p>Construct a QuerySection
	 * </p>
	 * <p>The section is defined, but its entries are not yet
	 * found.
	 * </p>
	 * @param constraint a constraint that defines the section.
	 * (The constraint is the sole member of the set of 
	 * constraints that defines the section.) The subject,
	 * verb, and object of the query are respectively the 
	 * subject, verb, and object of the constraint where these
	 * are given, and null where they are unknown.
	 * 
	 * @param constraintIndex the index of the constraint in
	 * the constraints array of a join.
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	QuerySection(Join join, JoinConstraint constraint, int constraintIndex)
			throws TripleStoreNonFatalException {

		// Set the join
		this.join = join;
		
		// Set the constraint indexes
		constraintIndexes = new int[1];
		constraintIndexes[0] = constraintIndex;
		
		// Set the variable names
		List<String> vars = new ArrayList<String>();
		String subjvar = constraint.subject.getId();
		if (subjvar != null) vars.add(subjvar);
		String verbvar = constraint.verb.getId();
		if ((verbvar != null) && !verbvar.equals(subjvar)) vars.add(verbvar);
		String objvar = constraint.object.getId(); 
		if ((objvar != null) && !objvar.equals(subjvar) && !objvar.equals(verbvar)) vars.add(objvar);		
		varNames = new String[vars.size()];
		int i = 0;
		for (String v : vars) varNames[i++] = v;
	}
	
	/**
	 * Query a triple store to find the entries of the
	 * section.
	 * 
	 * @param store the triple store to be queried
	 * 
	 * @param session the requesting StoreSession
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 * @throws JoinException 
	 */
	
	void doQuery(TripleStore store, StoreSession session)
			throws 	TripleStoreNonFatalException, 
					TripleStoreFatalException, 
					SourceConnectorException, 
					NotInOperationException, 
					JoinException {
		
		// Find the triples whose givens match those of the query constraint
		JoinConstraint constraint = join.constraints[constraintIndexes[0]];
		Item givenSubject = null;
		Datum d = constraint.subject.getDatum();
		if ((d != null) && d.getType() == Datum.Type.ITEM) givenSubject = d.getItemValue();
		Item givenVerb = null;
		d = constraint.verb.getDatum();
		if ((d != null) && d.getType() == Datum.Type.ITEM) givenVerb = d.getItemValue();
		d = constraint.object.getDatum();
		List<Triple> triples = store.getTriples(givenSubject, givenVerb, d, session);
		if (triples.size() > store.sizeIndicator()*Join.limitSizeMultiplier) {
			throw (new JoinException(
				"Query results size exceeds limit"));
		}
			
		// Find the assignments of triple subjects, verbs, and objects
		// to variables
		Integer subjix = null;
		String subjvar = constraint.subject.getId();
		if (subjvar != null) subjix = 0;
		Integer verbix = null;
		String verbvar = constraint.verb.getId();
		if (verbvar != null) {
			if (subjvar == null) verbix = 0;
			else if (verbvar.equals(subjvar)) verbix = 0;
			else verbix = 1;
		}
		Integer objix = null;
		String objvar = constraint.object.getId(); 
		if (objvar != null) {
			if (subjvar == null) {
				if (verbvar == null) objix = 0;
				else if (objvar.equals(verbvar)) objix = 0;
				else objix = 1;				
			}
			else if (verbvar == null) {
				if (objvar.equals(subjvar)) objix = 0;
				else objix = 1;				
			}
			else {
				if (objvar.equals(subjvar)) objix = 0;
				else if (subjvar.equals(verbvar)) objix = 1;
				else if (objvar.equals(verbvar)) objix = 1;
				else objix = 2;								
			}
		}
		
		// Filter the triples to remove those that have 
		// conflicting assignments to variables
		LinkedList<Entry> filteredEntries = new LinkedList<Entry>();
		for (Triple triple : triples) {
			// Create an entry for the triple and determine whether
			// there is an assignment conflict
			Entry entry = new Entry();
			entry.triples[0] = triple;
			boolean conflict = false;
			if (subjix != null) entry.varValues[subjix] = triple.getSubject().summary();
			if (verbix != null) {
				if (verbix == subjix) {
					if (!triple.getSubject().equals(triple.getVerb())) conflict = true;
					// else (if subject and verb are equal) no need do do anything as
					// variable is already assigned from subject
				}
				else entry.varValues[verbix] = triple.getVerb().summary();
			}
			if ((objix != null) && !conflict) {
				long objvalue = triple.getObjectSummaryValue();
				if (triple.getObjectType() == Datum.Type.ITEM) {
					// The object is an item, so could be the same as
					// subject or verb
					if (objix == subjix) {
						if (triple.getSubject().summary() != objvalue) conflict = true;
						// else (if subject and object are equal) no need do do anything as
						// variable is already assigned from subject
					}
					else if (objix == verbix) {
						if (triple.getVerb().summary() != objvalue) conflict = true;
						// else (if verb and object are equal) no need do do anything as
						// variable is already assigned from verb
					}
					else entry.varValues[objix] = objvalue;
				}
				else {
					// The object is not an item so must be different from
					// subject and verb
					if ((objix == subjix) || (objix == verbix)) conflict = true;
					else entry.varValues[objix] = objvalue;
				}
			}
			
			// If there is no conflict, add the entry to the filtered list
			if (!conflict) filteredEntries.add(entry);
		}
		
		// Set up the entries array from the filtered list
		entries = new Entry[filteredEntries.size()];
		int i = 0;
		for (Entry entry : filteredEntries) entries[i++] = entry;
	}
}
