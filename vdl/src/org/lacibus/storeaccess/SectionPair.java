/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.lacibus.triplestore.Triple;

/**
 * A pair of sections that can be joined
 */

class SectionPair extends EntryContainer {

	/**
	 * The first section of the pair
	 */
	
	Section section1;

	/**
	 * The second section of the pair
	 */
	
	Section section2;

	/**
	 * The constraints map of the first section of the pair
	 */
	
	int[] constraintsMap1;

	/**
	 * The constraints map of the first section of the pair
	 */
	
	int[] constraintsMap2;

	/**
	 * The names of the unknowns that are common to the two sections
	 * of the pair
	 */
	
	String commonNames[];

	/**
	 * The unknowns map of the first section of the pair
	 */
	
	int[] varsMap1;

	/**
	 * The unknowns map of the second section of the pair
	 */
	
	int[] varsMap2;
	
	/**
	 * The entries of the joined pair of constraint
	 */
	
	List<Entry> entries;
	
	/**
	 * Create a section Pair
	 * 
	 * @param section1 the first section of the pair
	 * 
	 * @param section2 the second section of the pair
	 */
	
	SectionPair (Section section1, Section section2) {
		
		join = section1.join;

		this.section1 = section1;
		this.section2 = section2;
		
		constraintsMap1 = new int[section1.constraintIndexes.length];
		constraintsMap2 = new int[section2.constraintIndexes.length];
		int i = 0;
		List<Integer> constraints = new ArrayList<Integer>();
		for (int j = 0; j < section1.constraintIndexes.length; j++) {
			constraints.add(section1.constraintIndexes[j]);
			constraintsMap1[j] = i++;
		}
		for (int j = 0; j < section2.constraintIndexes.length; j++) {
			int k = section2.constraintIndexes[j];
			int ix = indexOf(section1.constraintIndexes, k);
			if (ix >= 0) constraintsMap2[j] = ix;
			else {
				constraints.add(section2.constraintIndexes[j]);
				constraintsMap2[j] = i++;
			}
		}
		constraintIndexes = new int[i];
		i = 0;
		for (Integer jInt : constraints) constraintIndexes[i++] = jInt;
		
		varsMap1 = new int[section1.varNames.length];
		varsMap2 = new int[section2.varNames.length];
		i = 0;
		List<String> vars = new ArrayList<String>();
		List<String> commonVars = new ArrayList<String>();
		for (int j = 0; j < section1.varNames.length; j++) {
			vars.add(section1.varNames[j]);
			varsMap1[j] = i++;
		}
		for (int j = 0; j < section2.varNames.length; j++) {
			String s = section2.varNames[j];
			int ix = indexOf(section1.varNames, s);
			if (ix >= 0) {
				commonVars.add(section2.varNames[j]);
				varsMap2[j] = ix;
			}
			else {
				vars.add(section2.varNames[j]);
				varsMap2[j] = i++;
			}
		}
		varNames = new String[i];
		i = 0;
		for (String s : vars) varNames[i++] = s;
		commonNames = new String[commonVars.size()];
		i = 0;
		for (String s : commonVars) commonNames[i++] = s;
		
		entries = new LinkedList<Entry>();
	}
	
	/**
	 * Join the two constraints of the pair
	 * 
	 * @throws JoinException
	 */
	
	void join() throws JoinException {
		
		// This is a hash join. It uses a hash map of entries whose key
		// is an array of values of the unknowns that are common to the
		// sections being joined. The array is wrapped in an instance of
		// the Values class because java treats arrays as being equal
		// only if they are the same, so that different arrays with the
		// same values are not equal. The Values class has a hashCode()
		// method that uses the values of the array elements, and an 
		// equality method that treats members as being equal if their
		// arrays have the same values.
		
		// If either of the sections to be joined has no entries, return
		// a result with no entries
		if (	(section1.entries == null) || (section1.entries.length == 0) ||
				(section2.entries == null) || (section2.entries.length == 0))
			return;

		// Create the entries map from the first section
		Map<Values, Entries> entriesMap = new HashMap<Values, Entries>();
		for (Entry e : section1.entries) {
			Values commonValues = new Values(e.subValues(commonNames));
			Entries entries = entriesMap.get(commonValues);
			if (entries == null) entriesMap.put(commonValues, new Entries(e));
			else {
				entries.add(e);
				entriesMap.put(commonValues, entries);
			}
		}
		
		// Merge the entries from the second section using the maps
		for (Entry e : section2.entries) {
			Values commonValues = new Values(e.subValues(commonNames));
			Entries entries = entriesMap.get(commonValues);
			while (entries != null) {
				addMergedEntry(entries.entry, e);
				entries = entries.next;
			}
		}
	}	
	
	/**
	 * An instance of this class wraps an array of summary values.
	 * The hashCode() method that uses the values of the array elements, 
	 * and the equality method that treats members as being equal if 
	 * their arrays have the same values.
	 */
	
	class Values {
		
		long[] values;
		
		Values(long[] values) {
			
			this.values = values;
		}
		
		public int hashCode() {
			
			int hc = 0;
			for (long l : values) hc = hc^(int)l;
			return hc;
		}
		
		public boolean equals(Object v) {
			
			return Arrays.equals(values, ((Values)v).values);
		}
	}
	
	/**
	 * Add to the join of the two sections an entry formed by merging
	 * an entry from each section.
	 *  
	 * @param entry1 the entry from the first section to be merged
	 * 
	 * @param entry2 the entry from the second section to be merged
	 */
	
	void addMergedEntry(Section.Entry entry1, Section.Entry entry2) {
	
		Entry entry = new Entry();
		entry.triples = new Triple[constraintIndexes.length];
		for (int i = 0; i < section1.constraintIndexes.length; i++) {
			entry.triples[constraintsMap1[i]] = entry1.triples[i];
		}
		for (int i = 0; i < section2.constraintIndexes.length; i++) {
			entry.triples[constraintsMap2[i]] = entry2.triples[i];
		}
		entry.varValues = new long[varNames.length];
		for (int i = 0; i < section1.varNames.length; i++) {
			entry.varValues[varsMap1[i]] = entry1.varValues[i];
		}
		for (int i = 0; i < section2.varNames.length; i++) {
			entry.varValues[varsMap2[i]] = entry2.varValues[i];
		}
		entries.add(entry);
	}
	
	/**
	 * Find the index of a string in an array of strings
	 * 
	 * @param array an array of strings
	 * 
	 * @param member a string
	 * 
	 * @return the index of the string in the array, or -1 if the
	 * string is not in the array
	 */
	
	private static int indexOf(String[] array, String member) {
		
		for (int i = 0; i < array.length; i++)
			if(array[i].equals(member)) return i;
		return -1;
	}
	
	/**
	 * Find the index of an integer in an array of integers
	 * 
	 * @param array an array of integers
	 * 
	 * @param member an integer
	 * 
	 * @return the index of the integer in the array, or -1 if the
	 * integer is not in the array
	 */
	
	private static int indexOf(int[] array, int member) {
		
		for (int i = 0; i < array.length; i++)
			if(array[i] == member) return i;
		return -1;
	}
	
	public String toString() {
		
		return "SECTION PAIR{\n" + constraintsToString() + section1 + section2 + "}\n";
	}
	
	/**
	 * An instance of this class is a member of a list of entries. It
	 * contains an entry plus a reference to the next member of the list. 
	 */
	
	private class Entries {
		
		/**
		 * An entry in a list of entries
		 */
		
		Entry entry;
		
		/**
		 * The remainder of the list
		 */
		
		Entries next; 
		
		/**
		 * Construct the last member of a list of entries
		 * 
		 * @param entry the last member of a list of entries
		 */
		
		Entries(Entry entry) {
			
			this.entry = entry;
			next = null;
		}
		
		/**
		 * Construct a member of a list of entries
		 * 
		 * @param entry an entry in a list of entries
		 * 
		 * @param next the remainder of the list
		 */
		
		Entries(Entry entry, Entries next) {
			
			this.entry = entry;
			this.next = next;
		}
		
		/**
		 * Extend a list of entries by adding a new member at its start
		 * 
		 * @param entry an entry to be added at the start of the list
		 */
		
		void add(Entry entry) {
			
			next = new Entries(this.entry, next);
			this.entry = entry;
		}
	}
}
