package org.lacibus.storeaccess;

import java.io.Serializable;

public class NullQuantumException extends Exception implements Serializable {

	private static final long serialVersionUID = 4423491626908353181L;
}
