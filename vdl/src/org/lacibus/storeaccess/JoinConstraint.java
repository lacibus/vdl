/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

/**
 * <p>An assertion that quantities are related in a certain way. 
 * </p>
 * <p>A constraint has a subject, a verb, and an object, each of 
 * which can either be unknown or given. The unknown quantities 
 * are termed "unknowns". Each unknown has a name, and can appear 
 * in multiple constraints.
 * </p>
 * <p>A triple satisfies a constraint if it satisfies the following
 * conditions.</p>
 * <ul>
 * <li>If the subject of the constraint is a given
 *    then the subject of the triple is that given</li>
 * <li>If the verb of the constraint is a given
 *    then the verb of the triple is that given</li>
 * <li>If the object of the constraint is a given
 *    then the object of the triple is that given.</li>
 * <li>If the subject and verb of the constraint are
 *    the same unknown, then the subject and verb
 *    of the triple are the same item.</li>
 * </li>If the subject and object of the constraint are
 *    the same unknown, then the object of the triple
 *    is an item datum whose value is the same as the
 *    subject of the triple.</li>
 * <li>If the verb and object of the constraint are
 *    the same unknown, then the object of the triple
 *    is an item datum whose value is the same as the
 *    verb of the triple.</li>
 * </ul>   
 * <p>If a triple satisfies a constraint then it defines
 * an assignment of values to the variables in the
 * constraint.</p>
 * <ul>
 * <li>If the subject of the constraint is an unknown
 *    then the value assigned to it is the subject of
 *    the triple.</li>
 * <li>If the verb of the constraint is an unknown
 *    then the value assigned to it is the verb of
 *    the triple.</li>
 * <li>If the object of the constraint is an unknown
 *    then the value assigned to it is the object of
 *    the triple.</li>
 * </ul>
 */

public class JoinConstraint {
	
	/**
	 * The subject of the constraint
	 */
	
	Quantum subject;
	
	/**
	 * The verb of the constraint
	 */
	
	Quantum verb;
	
	/**
	 * The object of the constraint
	 */
	
	Quantum object;
	
	/**
	 * Create a join constraint
	 * 
	 * @param subject the subject of the constraint
	 * 
	 * @param verb the verb of the constraint
	 * 
	 * @param object the object of the constraint
	 */
	
	public JoinConstraint(
			Quantum subject,
			Quantum verb,
			Quantum object) {
		
		this.subject = subject;
		this.verb = verb;
		this.object = object;
	}
	
	/**
	 * @return the subject of the constraint
	 */
	
	public Quantum getSubject() {
		
		return subject;
	}
	
	/**
	 * @return the verb of the constraint
	 */
	
	public Quantum getVerb() {
		
		return verb;
	}
	
	/**
	 * @return the object of the constraint
	 */
	
	public Quantum getObject() {
		
		return object;
	}
	
	public boolean equals(Object obj) {

		try {
			JoinConstraint c = (JoinConstraint) obj;
			return (subject.equals(c.subject) &&
					verb.equals(c.verb) &&
					object.equals(c.object));
		} catch (Exception e) {
			return false;
		}
	}
	
	public int hashCode() {
		
		return subject.hashCode() ^ verb.hashCode() ^ object.hashCode();
	}
	
	public String toString() {
		
		return "CONSTRAINT{" + subject + "," + verb  + "," + object + "}";
	}
}
