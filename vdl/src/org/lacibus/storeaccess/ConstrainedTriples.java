/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.lacibus.triplesource.SourceConnectorStateException;
import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Triple;
import org.lacibus.triplestore.TripleStoreNonFatalException;

/**
 * An array of triples that satisfy a set of constraints, with
 * the assignments of values to constraint unknowns that they imply.
 */

public class ConstrainedTriples {

	/**
	 * The triples that satisfy the constraints
	 */
	
	Triple[] triples;
	
	/**
	 * The assignments of values to constraint unknowns that the triples
	 * imply
	 */
	
	Map<String, Datum> assignments;
	
	/**
	 * Create an array of constrained triples with assignments to constraint
	 * unknowns.
	 * 
	 * @param triples the triples
	 * @param assignments the assignments to constraint unknowns.
	 */
	
	public ConstrainedTriples(Triple[] triples, Map<String, Datum> assignments) {

		this.triples = triples;
		this.assignments = assignments;
	}
	
	/**
	 * Get the value assigned to an unknown
	 * 
	 * @param var an unknown
	 * 
	 * @return the value assigned to the unknown
	 */
	
	public Datum getAssignment(String var) {
		
		return assignments.get(var);
	}
	
	/** 
	 * @return the assignments of values to unknowns
	 */
	
	public Map<String, Datum> getAssignments() {
		
		return assignments;
	}
	
	/**
	 * @return the triples
	 */
	
	public Triple[] getTriples() {
		
		return triples;
	}
	
	public String toString() {
		
		String s = "CONSTRAINED TRIPLES{\n\tASSIGNMENTS{";
		if (assignments == null) s = s + "NULL";
		else {
			for (Entry<String, Datum> e : assignments.entrySet()) {
				Datum v = e.getValue();
				if (v == null) s = s + "\n\t\t" + e.getKey() + "=NULL";
				else s = s + "\n\t\t" + e.getKey() + '=' + e.getValue().simpleTextRepresentation();
			}
		}
		s = s + "}\n\tTRIPLES{";
		if (triples == null) s = s + "NULL";
		else {
			for (Triple t : triples) {
				if (t == null) s = s + "\n\t\tNULL";
				else s = s + "\n\t\t" + t.getId() + " = " + t.getSubjectId() + ", " + t.getVerbId()  + ", " + 
						t.getObjectType() + ":" + t.getObjectSummaryValue();
			}
		}
		s = s + "}\n}";
		return s;
	}
	
	/**
	 * @return a JSON object representing the constrained triples
	 * 
	 * @throws SourceConnectorStateException
	 * @throws TripleStoreNonFatalException
	 */
	
	@SuppressWarnings("unchecked")
	public JSONObject toJsonObject() 
			throws SourceConnectorStateException, TripleStoreNonFatalException {
		
		JSONObject jo = new JSONObject();
		JSONObject assignmentsJo = new JSONObject();
		for (Entry<String, Datum> e : assignments.entrySet()) 
			assignmentsJo.put(e.getKey(), e.getValue().toJsonObject());
		jo.put("assignments", assignmentsJo);
		JSONArray triplesJa = new JSONArray();
		for (Triple t : triples) {
			if (t == null) triplesJa.add(null);
			else triplesJa.add(t.getId().toString());
		}
		jo.put("triples", triplesJa);
		return jo;
	}
}
