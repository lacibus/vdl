/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.TripleStoreNonFatalException;

/**
 * A quantity that is a known datum.
 */

public class Given extends Quantum {
	
	/**
	 * Construct a Given from a Datum
	 * 
	 * @param datum a Datum that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Given(Datum datum) throws NullQuantumException {
		
		super(datum);
	}
	
	/**
	 * Construct a Given from an Item
	 * 
	 * @param item an Item that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Given(Item item) throws NullQuantumException {
		
		super(Datum.create(item));
	}
	
	/**
	 * Construct a Given from a short integer
	 * 
	 * @param s a short integer that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Given(short s) throws NullQuantumException {
		super(Datum.create(s));
	}
	
	/**
	 * Construct a Given from an integer
	 * 
	 * @param i an integer that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Given(int i) throws NullQuantumException {
		super(Datum.create(i));
	}
	
	/**
	 * Construct a Given from a long integer
	 * 
	 * @param l a long integer that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Given(long l) throws NullQuantumException {
		super(Datum.create(l));
	}
	
	/**
	 * Construct a Given from a float real number
	 * 
	 * @param f a float that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Given(float f) throws NullQuantumException {
		super(Datum.create(f));
	}
	
	/**
	 * Construct a Given from a double real number
	 * 
	 * @param d a double that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Given(double d) throws NullQuantumException {
		super(Datum.create(d));
	}
	
	/**
	 * Construct a Given from a boolean
	 * 
	 * @param b a boolean that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Given(boolean b) throws NullQuantumException {
		super(Datum.create(b));
	}
	
	/**
	 * Construct a Given from a string
	 * 
	 * @param s a string that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public Given(String s) 
			throws NullQuantumException, TripleStoreNonFatalException {
		super(Datum.createText(s));
	}
	
	/**
	 * Construct a Given from a byte array
	 * 
	 * @param b a byte array that is to be the Given value
	 * 
	 * @throws NullQuantumException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public Given(byte[] b) 
			throws NullQuantumException, TripleStoreNonFatalException {
		super(Datum.createBinary(b));
	}
}
