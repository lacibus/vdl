/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import org.lacibus.triplestore.Datum;

/**
 * An instance of this class is a quantity that could be unknown, 
 * identified by a string (such as &quot;x&quot;), or could be a 
 * known datum.
 */

public abstract class Quantum {

	/**
	 * The identifier of the unknown if this is an unknown,
	 * null if this is a known datum
	 */
	
	String id = null;
	
	/**
	 * The datum if this is a known datum, or null if this is
	 * an unknown.
	 */
	
	Datum datum = null;
	
	/**
	 * Create a quantum that is an unknown
	 * 
	 * @param x the identifier of the unknown
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Quantum (String x) throws NullQuantumException {
		
		if (x == null) throw (new NullQuantumException());
		id = x;
	}
	
	/**
	 * Create a quantum that is a known datum
	 * 
	 * @param d the known datum
	 * 
	 * @throws NullQuantumException 
	 */
	
	public Quantum (Datum d) throws NullQuantumException {
		
		if (d == null) throw (new NullQuantumException());
		datum = d;
	}
	
	/**
	 * @return the identifier of the unknown if this is an unknown,
	 * null if this is a known datum
	 */
	
	public String getId() {	
		
		return id;
	}
	
	/**
	 * @return the datum if this is a known datum, or null if this is
	 * an unknown.
	 */
	
	public Datum getDatum() {
		
		return datum;
	}
	
	public boolean equals(Object obj) {
		
		try {
			Quantum q = (Quantum) obj;
			if (id == null) {
				if (datum == null) return ((q.id == null) && (q.datum == null));
				else return ((q.id == null) && datum.equals(q.datum));
			}
			else {
				if (datum == null) return ((q.datum == null) && id.equals(q.id));
				else return (id.equals(q.id) && datum.equals(q.datum));
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public String toString() {
		if (id == null) return "GIVEN: " + datum.simpleTextRepresentation();
		else return "UNKNOWN: " + id;

	}
	
	public int hashCode() {
		
		if (id == null) return datum.hashCode();
		else if (datum == null) return id.hashCode();
		else return id.hashCode() ^ datum.hashCode();
	}
}
