/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

/**
 * An exception that is thrown by a method when there is an error
 * in its input.
 */

public class InputException extends Exception {
	
	private static final long serialVersionUID = 8485266142773713753L;

	/**
	 * Create a user input exception
	 * 
	 * @param message the reason for throwing the exception
	 */
	
	public InputException(String message) {
		
		super(message);
	}
}
