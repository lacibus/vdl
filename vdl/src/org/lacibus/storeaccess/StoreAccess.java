/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.lacibus.triplestore.AccessLevel;
import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.StoreSession;
import org.lacibus.triplestore.Triple;
import org.lacibus.triplestore.TripleStore;

/**
 * The methods of this class implement specific operations using
 * basic generic triple store methods.
 */

public class StoreAccess {

	/**
	 * Get the subjects of a collection of triples
	 * 
	 * @param triples a collection of triples
	 * 
	 * @return an array containing the triples' subjects
	 */
	
	public static Item[] getSubjects(Collection<Triple> triples) {
		
		Set<Item> subjects = new HashSet<Item>();
		for (Triple t : triples) {
			subjects.add(t.getSubject());
		}
		return (Item[])subjects.toArray();
	}
	
	/**
	 * Get the unique subject with a given verb and object.
	 * creating it if it does not exist.
	 * 
	 * @param sourceNr the identification number of the source 
	 * in which the subject is to be created if it does not exist.
	 * 
	 * @param onlyReadLevel the only-read level of the subject if
	 * it is to be created because it does not exist.
	 * 
	 * @param readOrWriteLevel the read-or-write level of the subject
	 * if it is to be created because it does not exist.
	 * 
	 * @param verb a verb
	 * 
	 * @param object an object
	 * 
	 * @param store the triple store containing the subject, verb,
	 * object, and unique triple with that subject, verb, and
	 * object
	 * 
	 * @param session the requesting session
	 * 
	 * @return the subject of the unique triple with the given verb
	 * and object, if it exists or can be created, otherwise null.
	 * If the subject already exists then it need not have the given
	 * source or access levels. If there are multiple triples with 
	 * the given verb and object, then null is returned.
	 * 
	 * @throws Exception
	 */
	
	public static Item uniqueSubject(
			long sourceNr, AccessLevel onlyReadLevel, AccessLevel readOrWriteLevel,
			Item verb, Datum object, TripleStore store, StoreSession session) 
					throws Exception {
	
		Item subject;
		Triple triple = store.uniqueTriple(null, verb, object, session);
		if (triple == null) {
			subject = store.createLockedItem(sourceNr, onlyReadLevel, readOrWriteLevel, session);
			if (subject == null) return null;
			try {
				if (store.createTriple(subject, verb, object, session) == null) {
					store.deleteItem(subject, session);
					return null;
				}
				triple = store.uniqueTriple(null, verb, object, session);
				if ((triple == null) || !triple.getSubject().equals(subject)) {
					store.deleteItem(subject, session);
					return null;
				}
			} finally {
				if (subject.isCurrent()) subject.unlock(session);
			}
		}
		else subject = triple.getSubject();
		return subject;
	}
	/**
	 * Get the unique item in a triple store that is the subject of a triple with a
	 * given verb and object
	 * 
	 * @param verb the given verb
	 * 
	 * @param object the given object
	 * 
	 * @param store the triple store
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the unique item in the triple store that is the subject of a triple 
	 * with the given verb and object
	 * 
	 * @throws Exception
	 */
	
	public static Item getUniqueSubject(
			Item verb, Datum object, TripleStore store, StoreSession session) 
					throws Exception {
		
		Triple triple = store.uniqueTriple(null, verb, object, session);
		if (triple == null) return null;
		else return triple.getSubject();
	}
	
	/**
	 * Get the item that is the unique object with a given subject
	 * and verb, creating it and the appropriate triple if it does 
	 * not exist. 
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
	 * @param sourceNr the identification number of the source 
	 * in which the object is to be created if it does not exist.
	 * 
	 * @param onlyReadLevel the only-read level of the object if
	 * it is to be created because it does not exist.
	 * 
	 * @param readOrWriteLevel the read-or-write level of the object
	 * if it is to be created because it does not exist.
	 * 
	 * @param store the triple store containing the subject, verb,
	 * object, and unique triple with that subject, verb, and
	 * object
	 * 
	 * @param session the requesting session
	 * 
	 * @return the item that is the object of the unique triple with
	 * the given subject and verb, if it exists or can be created, 
	 * otherwise null. If the item already exists then it need not 
	 * have the given source or access levels. If there are multiple
	 * triples with given subject and verb, or a single triple but 
	 * which has an object that is not an item, then null is returned.
	 */
	
	public static Item uniqueItemObject(Item subject, Item verb, long sourceNr, 
			AccessLevel onlyReadLevel, AccessLevel readOrWriteLevel, 
			TripleStore store, StoreSession session) throws Exception {
		
		boolean previouslyLocked = false;
		if (subject.checkLocked(session)) previouslyLocked = true;
		else if (!subject.lock(session)) throw (new ResourceLockedException("Cannot lock item: " + subject));
		try {
			Triple t = store.uniqueTriple(subject, verb, null, session);
			if (t == null) {
				Item object = store.createLockedItem(sourceNr, onlyReadLevel, readOrWriteLevel, session);
				if (object == null) return null;
				try {
					t = store.setUniqueObject(subject, verb, Datum.create(object), session);
					if (t== null) {
						store.deleteItem(object, session);
						return null;
					}
					return object;
				} finally {
					if (object.isCurrent()) object.unlock(session);
				}
			}
			else {
				return t.getObject(session).getItemValue();
			}
		} finally {
			if (!previouslyLocked) subject.unlock(session);
		}		
	}

	/**
	 * Get the string that is the unique object with a given subject
	 * and verb. 
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
	 * @param store the triple store containing the subject and verb,
	 * and unique triple with that subject and verb
	 * 
	 * @param session the requesting session
	 * 
	 * @return the string that is the object of the unique triple with
	 * the given subject and verb if it exists, otherwise null. If there 
	 * are multiple triples with given subject and verb, or a single 
	 * triple but which has an object that is not a text value, then 
	 * null is returned.
	 */
	
	public static String getUniqueTextObject(
			Item subject, Item verb, TripleStore store, StoreSession session)
					throws Exception {
		
		Datum d = getUniqueObject(subject, verb, store, session);
		if (d == null) return null;
		if (d.getType() != Datum.Type.TEXT) throw (new BadDataException(
				"Unique object for subject " + subject.getId() +
				", verb " + verb.getId() + " has type " + d.getType() +
				" not TEXT."));
		return d.getTextValueAsString();
	}

	/**
	 * Get the integer that is the unique object with a given subject
	 * and verb. 
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
	 * @param store the triple store containing the subject and verb,
	 * and unique triple with that subject and verb
	 * 
	 * @param session the requesting session
	 * 
	 * @return the integer that is the object of the unique triple with
	 * the given subject and verb if it exists, otherwise null. If there 
	 * are multiple triples with given subject and verb, or a single 
	 * triple but which has an object that is not an integer, then null 
	 * is returned.
	 */
	
	public static Long getUniqueIntegralObject(
			Item subject, Item verb, TripleStore store, StoreSession session)
					throws Exception {
		
		Datum d = getUniqueObject(subject, verb, store, session);
		if (d == null) return null;
		if (d.getType() != Datum.Type.INTEGRAL) throw (new BadDataException(
				"Unique object for subject " + subject.getId() +
				", verb " + verb.getId() + " has type " + d.getType() +
				" not INTEGRAL."));
		return d.getIntegralValue();
	}

	/**
	 * Get the boolean that is the unique object with a given subject
	 * and verb. 
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
	 * @param store the triple store containing the subject and verb,
	 * and unique triple with that subject and verb
	 * 
	 * @param session the requesting session
	 * 
	 * @return the boolean that is the object of the unique triple with
	 * the given subject and verb if it exists, otherwise null. If there 
	 * are multiple triples with given subject and verb, or a single 
	 * triple but which has an object that is not a boolean, then null 
	 * is returned.
	 */
	
	public static Boolean getUniqueBooleanObject(
			Item subject, Item verb, TripleStore store, StoreSession session)
					throws Exception {
		
		Datum d = getUniqueObject(subject, verb, store, session);
		if (d == null) return null;
		if (d.getType() != Datum.Type.BOOLEAN) throw (new BadDataException(
				"Unique object for subject " + subject.getId() +
				", verb " + verb.getId() + " has type " + d.getType() +
				" not BOOLEAN."));
		return d.getBooleanValue();
	}

	/**
	 * Get the object of the unique triple with a given subject and verb. 
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
	 * @param store the triple store containing the subject and verb,
	 * and unique triple with that subject and verb
	 * 
	 * @param session the requesting session
	 * 
	 * @return the object of the unique triple with the given subject and 
	 * verb if it exists, otherwise null. If there are multiple triples 
	 * with given subject and verb, then null is returned.
	 */
	
	public static Datum getUniqueObject(
			Item subject, Item verb, TripleStore store, StoreSession session) 
					throws Exception {
		
		Triple triple = store.uniqueTriple(subject, verb, null, session);
		if (triple == null) return null;
		return triple.getObject(session);
	}
	
	/**
	 * Set a unique triple with a given subject, verb and object. 
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
	 * @param object the given object
	 * 
	 * @param store the triple store containing the subject verb, and
	 * object, and which contains or will contain the unique triple.
	 * 
	 * @param session the requesting session
	 * 
	 * @return the unique triple with a given subject, verb and object
	 * if it exists or can be created
	 * 
	 * @throws Exception
	 */
	
	public static Triple setUniqueTriple(
			Item subject, Item verb, Datum object, TripleStore store, StoreSession session)
					throws Exception {
		
		boolean previouslyLocked = false;
		if (subject.checkLocked(session)) previouslyLocked = true;
		else if (!subject.lock(session)) throw (new ResourceLockedException("Cannot lock item: " + subject));
		try {
			Triple t = store.setUniqueTriple(subject, verb, object, session);
			if (t == null) throw new BadDataException("Cannot set unique triple");
			return t;
		} finally {
			if (!previouslyLocked) subject.unlock(session);
		}
	}
	
	/**
	 * Ensure that there is a unique triple with a given subject and verb,
	 * and that it has a given object.
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
	 * @param object the given object
	 * 
	 * @param store the triple store containing the subject verb, and
	 * object, and which contains or will contain the unique triple.
	 * 
	 * @param session the requesting session
	 * 
	 * @return the unique triple with a given subject and verb, and that
	 * has the given object, if it exists or can be created
	 * 
	 * @throws Exception
	 */
	
	public static Triple setUniqueObject(
			Item subject, Item verb, Datum object, TripleStore store, StoreSession session)
					throws Exception {
		
		boolean previouslyLocked = false;
		if (subject.checkLocked(session)) previouslyLocked = true;
		else if (!subject.lock(session)) throw (new ResourceLockedException("Cannot lock item: " + subject));
		try {
			Triple t = store.setUniqueObject(subject, verb, object, session);
			if (t == null) throw new BadDataException("Cannot set unique object");
			return t;
		} finally {
			if (!previouslyLocked) subject.unlock(session);
		}
	}
	/**
	 * Ensure that there are no triples with a given subject
	 * and verb
	 * 
	 * @param subject the subject
	 * 
	 * @param verb the verb
	 * 
	 * @param store the triple store containing the subject and verb.
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if there were no matching triples or all
	 * matching triples have been deleted, false if there were
	 * matching triples that could not be deleted. If false is
	 * returned then no triples will have been deleted.
	 * 
	 * @throws Exception
	 */
	
	public static boolean ensureNoMatchingTriples(
			Item subject, Item verb, TripleStore store, StoreSession session)
					throws Exception {
		
		boolean previouslyLocked = false;
		if (subject.checkLocked(session)) previouslyLocked = true;
		else if (!subject.lock(session)) throw (new ResourceLockedException("Cannot lock item: " + subject));
		try {
			return store.ensureNoMatchingTriples(subject, verb, session);
		} finally {
			if (!previouslyLocked) subject.unlock(session);
		}
	}
}
