package org.lacibus.storeaccess;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.StoreSession;
import org.lacibus.triplestore.Triple;
import org.lacibus.triplestore.TripleStore;

/**
 * A specification of dependencies between items. When item I2 is dependent
 * on item I1, and a change is made to I1, then a similar change should be
 * made to I2. For example, if a page is deleted, then all the blocks on the
 * page should be deleted. In the simplest case, the dependencies can be 
 * specified by a single verb, for example "Page Has Block", or set of verbs,
 * for example {"Page Has Block", "Page Has Assignment"}. In more complex 
 * cases, there are items that are dependent on the dependent items, for example
 * drafts of blocks. The specification therefore consists of a map whose keys
 * are verbs and whose values are dependent specifications. In the simplest case,
 * the values are null.
 */

public class DependentsSpec implements Serializable {

	private static final long serialVersionUID = 8897487497071098067L;
	
	Map<Item, DependentsSpec> dependencies = new HashMap<Item, DependentsSpec>();	
	
	/**
	 * Add a simple dependency
	 * 
	 * @param verb a verb whose objects have no items dependent on them.
	 */
	
	public void addDependent(Item verb) {
		
		dependencies.put(verb, null);
	}
	
	/**
	 * Add a complex dependency
	 * 
	 * @param verb a verb whose objects have items dependent on them.
	 * 
	 * @param subDependents a specification of the dependencies on the
	 * objects of the verb
	 */
	
	public void addDependent(Item verb, DependentsSpec subDependents) {
		
		dependencies.put(verb, subDependents);
	}
	
	/**
	 * Get the dependencies on the objects of a verb
	 * 
	 * @param verb a verb
	 * 
	 * @return the dependencies on the objects of the verb
	 */
	
	public DependentsSpec getSubDependents(Item verb) {
		
		return dependencies.get(verb);
	}
	
	/**
	 * Get the verbs defining first-level dependencies
	 * 
	 * @return the verbs defining first-level dependencies
	 */
	
	public Set<Item> getVerbs() {
		
		return dependencies.keySet();
	}
	
	/**
	 * Add dependencies. Existing dependencies are not duplicated.
	 * 
	 * @param spec the dependencies to be added
	 */
	
	public void add(DependentsSpec spec) {
		
		// Capture the initial key set of the mapping
		Set<Item> startItems = new HashSet<Item>(dependencies.keySet());
		for (Item item : startItems) {
			DependentsSpec thisSubSpec = dependencies.get(item);
			DependentsSpec specSubSpec = spec.dependencies.get(item);
			// If an entry in the mapping to be added is not already
			// in the mapping, add it as a new entry
			if (thisSubSpec == null) dependencies.put(item, specSubSpec);
			// But if an entry in the mapping to be added is already
			// in the mapping, merge the two entries
			else if (specSubSpec != null) thisSubSpec.add(specSubSpec);
		}
	}
	
	/**
	 * Get the items that are the objects of triples with a given
	 * subject and verb.
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
	 * @param store the triple store containing the triples
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the items that are objects of triples with the
	 * given subject and verb
	 * 
	 * @throws Exception
	 */
	
	public Set<Item> getImmediatelyDependentItems(
			Item subject, Item verb, TripleStore store, StoreSession session)
					throws Exception {
		
		List<Triple> triples = store.getTriples(subject, verb, null, session);
		Set<Item> items = new HashSet<Item>();
		for (Triple t : triples) {
			try {
				items.add(t.getObject(session).getItemValue());
			} catch (Exception e) {}
		}
		return items;
	}
}
