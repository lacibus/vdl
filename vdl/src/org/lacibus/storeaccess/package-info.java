/**
 * <p>This package contains classes that use the basic access methods of the triple
 * store class to provide access to a triple store in more powerful and sophisticated 
 * ways.
 * </p>
 * <p>These include:
 * </p>
 * <ul>
 * <li>Searching for sets of triples that satisfy combinations of constraints</li>
 * <li>Bundling multiple changes so that they are executed together</li>
 * <li>Executing a list of changes specified in a script</li>
 * <li>Specific operations implemented using basic generic triple store methods.</li>
 * </ul>
 **/

package org.lacibus.storeaccess;
