/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.lacibus.triplestore.AccessLevel;
import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.NotInOperationException;
import org.lacibus.triplestore.StoreSession;
import org.lacibus.triplestore.Triple;
import org.lacibus.triplestore.TripleStore;
import org.lacibus.triplestore.TripleStoreFatalException;
import org.lacibus.triplestore.TripleStoreNonFatalException;
import org.lacibus.triplesource.SourceConnectorException;

/**
 * <p>A bundle of changes to a triple store that are to be
 * performed together.
 * </p>
 * <p>The changes may not be able to be performed in a single
 * transaction, as they may require changes to different items
 * or to triples whose subjects are different items.
 * </p>
 * <p>The class contains methods to add changes to the bundle
 * and to perform the changes.
 * </p>
 */

public class ChangeBundle {
	
	/**
	 * The triple store to which the changes are to be made.
	 */
	
	private TripleStore store;
	
	/**
	 * The specifications of changes to add items to the store
	 */
	
	private Map<String, ItemToAdd> itemsToAdd = new HashMap<String, ItemToAdd>();
	
	/**
	 * The specifications of changes to update items in the store
	 */
	
	private Map<Item, ItemToUpdate> itemsToUpdate = new HashMap<Item, ItemToUpdate>();

	/**
	 * The items to be deleted from the store by the changes
	 */
	
	private Map<Item, DependentsSpec> itemsToDelete = new HashMap<Item, DependentsSpec>();

	/**
	 * The specifications of changes to put triples to the store.
	 * A put operation adds a triple, but only if there is no triple 
	 * already in the store with the given subject, verb, and object.
	 */
	
	private Set<QTripleSpec> triplesToPut = new HashSet<QTripleSpec>();
	
	/**
	 * The triples to be deleted from the store by the changes
	 */
	
	private Set<TripleSpec> triplesToDelete = new HashSet<TripleSpec>();
	
	/**
	 * The specifications of changes to establish sets of triples in the
	 * store. Such a change ensures that there is a set of triples that 
	 * have the same subjects and verbs, which are given as items, and 
	 * objects that may be given  by item handles or datum values.
	 */
	
	private Set<QoTripleSetSpec> tripleSets = new HashSet<QoTripleSetSpec>();
	
	/**
	 * The specifications of changes to rank new items in the store
	 */
	
	private Set<NewItemToRank> newItemsToRank = new HashSet<NewItemToRank>();
	
	/**
	 * The new items added to the store, indexed by their handles
	 */
	
	private Map<String, Item> newItems;
	
	/**
	 * Create a change bundle
	 * 
	 * @param store the triple store to which the changes are to be made
	 */
	
	public ChangeBundle(TripleStore store) {
		
		this.store = store;
	}
	
	/**
	 * Add a change that will add an item to the store
	 * 
	 * @param handle a name used to refer to the item in other changes,
	 * or after the changes have been made.
	 * 
	 * @param source the source in which the item is to be created
	 * 
	 * @param onlyReadLevel the only-read access level that the item is to have
	 * 
	 * @param readOrWriteLevel the read-or-write access level that the item
	 * is to have
	 */
	
	public void setItemToAdd(
			String handle, Long source, 
			AccessLevel onlyReadLevel, AccessLevel readOrWriteLevel) {

		if (itemsToAdd.containsKey(handle)) 
			// Item already set to be added
			return;
		ItemToAdd itemToAdd = new ItemToAdd();
		itemToAdd.source = source;
		itemToAdd.handle = handle;
		itemToAdd.onlyReadLevel = onlyReadLevel;
		itemToAdd.readOrWriteLevel = readOrWriteLevel;
		itemsToAdd.put(handle, itemToAdd);
	}
	
	/**
	 * Get an item that has been added
	 * 
	 * @param handle the handle of the item specified in the change to add it
	 * 
	 * @return the item that was added with the given handle by a change
	 */
	
	public Item getAddedItem(String handle) {
		
		return newItems.get(handle);
	}
	
	/**
	 * Add a change that will update an item in the store
	 * 
	 * @param item the item to be updated
	 * 
	 * @param onlyReadLevel the only-read access level that the item is to have
	 * after the change
	 * 
	 * @param readOrWriteLevel the read-or-write access level that the item is to 
	 * have after the change
	 * @throws InputException 
	 */
	
	public void setItemToUpdate(Item item, AccessLevel onlyReadLevel, AccessLevel readOrWriteLevel) 
			throws InputException {
		
		setItemToUpdate(item, onlyReadLevel, readOrWriteLevel, null);
	}
	
	/**
	 * Add a change that will update an item in the store
	 * 
	 * @param item the item to be updated
	 * 
	 * @param onlyReadLevel the only-read access level that the item is to have
	 * after the change
	 * 
	 * @param readOrWriteLevel the read-or-write access level that the item is to 
	 * have after the change
	 * 
	 * @param dependentsSpec the specification of dependent items, if any, that 
	 * must have the same access levels as the item
	 * @throws InputException 
	 */
	
	public void setItemToUpdate(
			Item item, AccessLevel onlyReadLevel, AccessLevel readOrWriteLevel, DependentsSpec dependentsSpec) 
					throws InputException {
		
		if (itemsToUpdate.containsKey(item))
			throw (new InputException("Item to be updated twice: " + item.getId()));
		ItemToUpdate itemToUpdate = new ItemToUpdate();
		itemToUpdate.item = item;
		itemToUpdate.onlyReadLevel = onlyReadLevel;
		itemToUpdate.readOrWriteLevel = readOrWriteLevel;
		itemToUpdate.dependentsSpec = dependentsSpec;
		itemsToUpdate.put(item, itemToUpdate);
	}
	
	/**
	 * Add a change that will delete an item from the store
	 * 
	 * @param item the item to be deleted
	 * @throws InputException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public void setItemToDelete(Item item) 
			throws InputException, TripleStoreNonFatalException {
		
		checkItemToDelete(item);
		itemsToDelete.put(item, null);
	}
	
	/**
	 * Add a change that will delete an item from the store
	 * 
	 * @param item the item to be deleted
	 * 
	 * @param dependentsSpec the specification of dependent items, if any, that 
	 * must also be deleted
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws InputException 
	 */
	
	public void setItemToDelete(Item item, DependentsSpec dependentsSpec) 
			throws InputException, TripleStoreNonFatalException {
		
		checkItemToDelete(item);
		itemsToDelete.put(item, dependentsSpec);
	}
	
	/**
	 * Check consistency with other changes of a change to
	 * delete an item
	 * 
	 * @param item an item to be deleted
	 * 
	 * @throws InputException
	 * @throws TripleStoreNonFatalException
	 */
	
	private void checkItemToDelete(Item item) 
			throws InputException, TripleStoreNonFatalException {
		
		// Check consistency with triples to put
		for (QTripleSpec qspec : triplesToPut) {
			Datum d = qspec.subject.datum;
			if ((d != null) && item.equals(d.getItemValue()))
				throw (new InputException("Inconsistent changes"));
			d = qspec.verb.datum;
			if ((d != null) && item.equals(d.getItemValue()))
				throw (new InputException("Inconsistent changes"));
		}
			
		// Check consistency with triple sets to establish
		for (QoTripleSetSpec qoSpec : tripleSets) {
			if (!qoSpec.objects.isEmpty()) {
				if (item.equals(qoSpec.subject) || item.equals(qoSpec.verb))
					throw (new InputException("Inconsistent changes"));
			}
		}
	}
		
	/**
	 * Add a change that will put a triple to the store. If a triple
	 * with the given subject, verb and object already exists, do nothing, 
	 * otherwise add a triple with that subject, verb and object.
	 * 
	 * @param subject the subject of the triple. This is either
	 * an item already in the store or the handle of an item to be
	 * added.
	 * 
	 * @param verb the verb of the triple. This is either
	 * an item already in the store or the handle of an item to be
	 * added.
	 * 
	 * @param object the object of the triple. This is either a
	 * datum or the handle of an item to be added. If it is a
	 * datum then it can be an item already in the store.
	 * 
	 * @param session the requesting store session
	 * 
	 * @throws InputException
	 * @throws TripleStoreNonFatalException 
	 */
	
	public void setTripleToPut(Quantum subject, Quantum verb, Quantum object, StoreSession session)
			throws 	InputException, TripleStoreNonFatalException {
		
		// Validate the arguments
		String subjectHandle = subject.getId();
		if ((subjectHandle != null) && !itemsToAdd.containsKey(subjectHandle))
			throw (new InputException("Undeclared item handle: " + subjectHandle));
		String verbHandle = verb.getId();
		if ((verbHandle != null) && !itemsToAdd.containsKey(verbHandle))
			throw (new InputException("Undeclared item handle: " + verbHandle));
		String objectHandle = object.getId();
		if ((objectHandle != null) && !itemsToAdd.containsKey(objectHandle))
			throw (new InputException("Undeclared item handle: " + objectHandle));
		QTripleSpec qspec = new QTripleSpec();
		qspec.subject = subject;
		qspec.verb = verb;
		qspec.object = object;
		
		// Check for consistency with items to delete
		if ((subjectHandle == null) && itemsToDelete.containsKey(subject.datum.getItemValue()))
			throw (new InputException("Inconsistent changes"));
		if ((verbHandle == null) && itemsToDelete.containsKey(verb.datum.getItemValue()))
			throw (new InputException("Inconsistent changes"));
		
		// Check for consistency with triples to delete
		for (TripleSpec tripleSpec : triplesToDelete) {
			if (	((tripleSpec.subject == null) || eqItems(tripleSpec.subject, qspec.subject)) &&
					((tripleSpec.verb == null) || eqItems(tripleSpec.verb, qspec.verb)) &&
					((tripleSpec.object == null) || eqDatums(tripleSpec.object, qspec.object))) {
				throw (new InputException("Inconsistent changes"));
			}
		}
		
		// Check for consistency with triple sets
		for (QoTripleSetSpec uspec : tripleSets) {
			if (	eqItems(uspec.subject, qspec.subject) &&
					eqItems(uspec.verb, qspec.verb) &&
					!uspec.objects.contains(qspec.object))
				throw (new InputException("Inconsistent changes"));
		}
		
		// Mark the triple for addition
		triplesToPut.add(qspec);
	}
	
	/**
	 * Add a change to delete a triple.
	 * 
	 * @param triple the triple to be deleted
	 * 
	 * @param session the requesting store session
	 * 
	 * @throws InputException
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	public void setTripleToDelete(Triple triple, StoreSession session)
			throws 	InputException, 
					TripleStoreNonFatalException, 
					TripleStoreFatalException, 
					SourceConnectorException, 
					NotInOperationException  {
		
		// Create a triple spec for the triple
		TripleSpec tspec = new TripleSpec();
		tspec.subject = triple.getSubject();
		tspec.verb = triple.getVerb();
		tspec.object = triple.getObject(session);
		setTripleSpecToDelete(tspec);
	}
	
	/**
	 * Add a change to delete triples with a given subject, verb,
	 * and object.
	 * 
	 * @param subject the subject of the triple to be deleted.
	 * 
	 * @param verb the verb of the triple to be deleted.
	 * 
	 * @param object the object of the triple to be deleted.
	 * 
	 * @param session  the requesting store session
	 * 
	 * @throws InputException
	 */
	
	public void setTripleToDelete(Item subject, Item verb, Datum object, StoreSession session) 
			throws InputException {
		
		if ((subject == null) || (verb == null) || (object == null))
			throw (new InputException("Underspecified triple to delete"));
		// Create a triple spec for the triple
		TripleSpec tspec = new TripleSpec();
		tspec.subject = subject;
		tspec.verb = verb;
		tspec.object = object;
		setTripleSpecToDelete(tspec);
	}

	/**
	 * Add a change to delete triples with a given subject, verb,
	 * and object.
	 * 
	 * @param subject the subject of the triple to be deleted or null,
	 * in which case all subjects match
	 * 
	 * @param verb the verb of the triple to be deleted or null,
	 * in which case all verbs match
	 * 
	 * @param object the object of the triple to be deleted or null,
	 * in which case all objects match
	 * 
	 * @param session  the requesting store session
	 * 
	 * @throws InputException
	 */
	
	public void setTriplesToDelete(Item subject, Item verb, Datum object, StoreSession session) 
			throws InputException {
		
		// Create a triple spec for the triple
		TripleSpec tspec = new TripleSpec();
		tspec.subject = subject;
		tspec.verb = verb;
		tspec.object = object;
		setTripleSpecToDelete(tspec);
	}
	
	/**
	 * Add the specification of a triple to the specifications of triples
	 * to be deleted
	 * 
	 * @param tspec the specification of a triple
	 * 
	 * @throws InputException
	 */
	
	private void setTripleSpecToDelete(TripleSpec tspec)
			throws InputException {
		
		// Check for consistency with triples to add
		for (QTripleSpec qspec : triplesToPut) {
			if (	((tspec.subject == null) || eqItems(tspec.subject, qspec.subject)) &&
					((tspec.verb == null) || eqItems(tspec.verb, qspec.verb)) &&
					((tspec.object == null) || eqDatums(tspec.object, qspec.object)))
				throw (new InputException("Inconsistent changes"));
		}
		
		// Check for consistency with triple sets
		for (QoTripleSetSpec uspec : tripleSets) {
			try {
				if (	((tspec.subject == null) || uspec.subject.equals(tspec.subject)) &&
						((tspec.verb == null) || uspec.verb.equals(tspec.verb)) &&
						((tspec.object == null) || uspec.objects.contains(new Given(tspec.object))))
					throw (new InputException("Inconsistent changes"));
			} catch (NullQuantumException e) {}	// Cannot occur
		}
		
		// Mark the triple for deletion
		triplesToDelete.add(tspec);
	}
	
	/**
	 * Add a change to ensure that there is exactly one triple 
	 * with a given subject and verb, and that it has a given object,
	 * which is a non-item value or an existing item.
	 * 
	 * @param subject the subject.
	 * 
	 * @param verb the verb.
	 * 
	 * @param object the object.
	 * 
	 * @param session the requesting session.
	 * 
	 * @throws InputException
	 */
	
	public void setUniqueTripleObject(Item subject, Item verb, Datum object, StoreSession session)
			throws 	InputException {
		
		try {
			setUniqueTripleObject(subject, verb, new Given(object), session);
		} catch (NullQuantumException e) {
			throw (new InputException("Invalid unique triple object"));
		}
	}
				
	/**
	 * Add a change to ensure that there is exactly one triple 
	 * with a given subject and verb, and that it has a given object,
	 * which can be a new item to be created.
	 * 
	 * @param subject the subject.
	 * 
	 * @param verb the verb.
	 * 
	 * @param object the object.
	 * 
	 * @param session the requesting session.
	 * 
	 * @throws InputException
	 */
	
	public void setUniqueTripleObject(Item subject, Item verb, Quantum object, StoreSession session)
			throws 	InputException {
		
		Set<Quantum> objects = new HashSet<Quantum>();
		objects.add(object);
		establishTripleSet(subject, verb, objects, session);
	}

	/**
	 * Add a change to ensure that there are no triples with a given 
	 * subject and verb.
	 * 
	 * @param subject the subject.
	 * 
	 * @param verb the verb.
	 * 
	 * @param session the requesting session.
	 * 
	 * @throws InputException
	 */
	
	public void ensureNoMatchingTriples(Item subject, Item verb, StoreSession session)
			throws 	InputException {
		
		Set<Quantum> objects = new HashSet<Quantum>();
		establishTripleSet(subject, verb, objects, session);
	}
		
	/**
	 * Add a change to establish a set of triples that have the same subjects
	 * and verbs, which are given as items, and objects that may be given 
	 * by item handles or datum values.
	 * 
	 * @param subject the subject.
	 * 
	 * @param verb the verb.
	 * 
	 * @param object the objects.
	 * 
	 * @param session the requesting session.
	 * 
	 * @throws InputException
	 */
	
	public void establishTripleSet(
			Item subject, Item verb, Set<Quantum> objects, StoreSession session)
					throws 	InputException {
		
		// Check for consistency with items to delete
		if (itemsToDelete.containsKey(subject) || itemsToDelete.containsKey(verb))
			throw (new InputException("Inconsistent changes"));
				
		// Check for consistency with triples to add
		for (QTripleSpec qspec : triplesToPut) {
			if (	eqItems(subject, qspec.subject) &&
					eqItems(verb, qspec.verb) &&
					!objects.contains(qspec.object))
				throw (new InputException("Inconsistent changes"));
		}
		
		// Check for consistency with triples to delete
		for (TripleSpec tspec : triplesToDelete) {
			try {
				if (	((tspec.subject == null) || tspec.subject.equals(subject)) &&
						((tspec.verb == null) || tspec.verb.equals(verb)) &&
						((tspec.object == null) || objects.contains(new Given(tspec.object))))
					throw (new InputException("Inconsistent changes"));
			} catch (NullQuantumException e) {}	// Cannot occur
		}
		
		// Check for consistency with other triple sets
		for (QoTripleSetSpec spec : tripleSets) {
			if (	spec.subject.equals(subject) &&
					spec.verb.equals(verb) &&
					!spec.objects.equals(objects)) 
				throw (new InputException("Inconsistent changes"));
		}
		
		// Add to triple sets to be established
		QoTripleSetSpec spec = new QoTripleSetSpec();
		spec.subject = subject;
		spec.verb = verb;
		spec.objects = objects;
		tripleSets.add(spec);
	}
	
	/**
	 * <p>Add a change to give a rank to an item specified to be added by
	 * another change.
	 * </p>
	 * <p>A set of items is ranked by making each of them the subject of a
	 * triple whose verb is a member rank relation and whose object is an
	 * integer that specifies its rank. The new item is to be ranked in the
	 * set of items that are the values of a particular unknown - the rank 
	 * unknown - when a set of constraints is applied. It will be given a
	 * rank that is greater than the rank of any item currently in that set.
	 * </p>
	 * @param context the context
	 * 
	 * @param constraints the constraints to be applied
	 * 
	 * @param memberRankRelation the member rank relation
	 * 
	 * @param rankUnknown the rank unknown
	 * 
	 * @param newItemHandle the handle specified in the change to add the 
	 * new item.
	 * 
	 * @throws InputException
	 */
	
	public void rankNewItem (JoinConstraint[] constraints, 
			Item memberRankRelation, String rankUnknown, String newItemHandle) 
					throws InputException {
		
		if (!itemsToAdd.containsKey(newItemHandle))
			throw (new InputException("Undeclared item handle: " + newItemHandle));
		NewItemToRank newItemToRank = new NewItemToRank();
		newItemToRank.constraints = constraints;
		newItemToRank.memberRankRelation = memberRankRelation;
		newItemToRank.rankUnknown = rankUnknown;		
		newItemToRank.newItemHandle = newItemHandle;
		newItemsToRank.add(newItemToRank);
	}
	
	/**
	 * @return the new items added to the store, indexed by their handles
	 */
	
	public Map<String, Item> getNewItems() {
		
		return newItems;
	}
	
	/**
	 * <p>Determine whether an item is the value of a quantum.
	 * </p>
	 * <p>A quantum can be an unknown or a given. If it is a given, then
	 * its value can be an item datum.
	 * </p>
	 * @param i an item
	 * 
	 * @param q a quantum
	 * 
	 * @return true if the quantum is a given whose value is an item that is
	 * equal to the item, false otherwise.
	 */
	
	private boolean eqItems(Item i, Quantum q) {
		
		try {
			return i.equals(q.getDatum().getItemValue());
		} catch (Exception e) {
			return false;
		}
	}
		
	/**
	 * <p>Determine whether a datum is the value of a quantum.
	 * </p>
	 * <p>A quantum can be an unknown or a given. If it is a given, then
	 * its value is a datum.
	 * </p>
	 * @param d a datum
	 * 
	 * @param q a quantum
	 * 
	 * @return  true if the quantum is a given whose value is equal to the 
	 * datum, false otherwise.
	 */
	
	private boolean eqDatums(Datum d, Quantum q) {
		
		try {
			return d.equals(q.getDatum());
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * <p>Make the changes that have been added to the change bundle.
	 * </p>
	 * <p>If one of the changes cannot be made then the changes that
	 * have been made so far are reversed if possible.
	 * </p>
	 * @param session the requesting store session
	 * 
	 * @throws Exception
	 */
	
	public void makeChanges(StoreSession session) 
			throws Exception {
		
		// Lock the items to be deleted or updated, and the subjects of
		// triples to be created or deleted, and add any depoendent
		// changes to the list of changes
		Set<Item> lockedItems = new HashSet<Item>();
		LinkedList<Item> itemsToUnlock = new LinkedList<Item>();
		LinkedList<Change> changesToMake = new LinkedList<Change>();
		LinkedList<Change> madeChanges = new LinkedList<Change>();
		Exception exception = null;
		
		try {
			for (ItemToAdd itemToAdd : itemsToAdd.values()) {
				itemToAdd.lock(lockedItems, itemsToUnlock, session);
				changesToMake.add(itemToAdd);
			}
			
			LinkedList<ItemToUpdate> itemUpdates = new LinkedList<ItemToUpdate>();
			for (ItemToUpdate i : itemsToUpdate.values()) itemUpdates.add(i);
			ItemToUpdate itemToUpdate = itemUpdates.pollFirst();
			while (itemToUpdate != null) {
				itemToUpdate.lock(lockedItems, itemsToUnlock, session);
				changesToMake.add(itemToUpdate);
				itemToUpdate.getDependents(itemUpdates, session);
				itemToUpdate = itemUpdates.pollFirst();
			}
			
			for (TripleSpec tspec : triplesToDelete) {
				
				TriplesToDelete tripleToDelete = new TriplesToDelete(tspec);
				tripleToDelete.lock(lockedItems, itemsToUnlock, session);
				changesToMake.add(tripleToDelete);
			}
			
			for (QTripleSpec qTripleSpec : triplesToPut) {
				TripleToPut tripleToPut = new TripleToPut(qTripleSpec);
				tripleToPut.lock(lockedItems, itemsToUnlock, session);
				changesToMake.add(tripleToPut);
			}
			
			for (QoTripleSetSpec qospec : tripleSets) {
				TripleSet tripleSet = new TripleSet(qospec);
				tripleSet.lock(lockedItems, itemsToUnlock, session);
				changesToMake.add(tripleSet);
			}
			
			// Collect garbage before locking items and making changes
			Runtime.getRuntime().gc();
			
			for (NewItemToRank itemToRank : newItemsToRank) {
				itemToRank.lock(lockedItems, itemsToUnlock, session);
				changesToMake.add(itemToRank);
			}
			
			LinkedList<ItemToDelete> itemDeletes = new LinkedList<ItemToDelete>();
			for (Item item : itemsToDelete.keySet())
				itemDeletes.add(new ItemToDelete(item, itemsToDelete.get(item)));
			ItemToDelete itemToDelete = itemDeletes.pollFirst();
			while (itemToDelete != null) {
				itemToDelete.lock(lockedItems, itemsToUnlock, session);
				changesToMake.add(itemToDelete);
				itemToDelete.getDependents(itemDeletes, session);
				itemToDelete = itemDeletes.pollFirst();
			}

			newItems = new HashMap<String, Item>();
			for (Change change : changesToMake) {
				try {
					madeChanges.addFirst(change);
					change.perform(session);
					Item newItem = change.getNewItem();
					if (newItem != null) {
						itemsToUnlock.addFirst(newItem);
					}
				} catch (Exception e) {
					exception = e;
					break;
				}
			}
			
			if (exception != null) {
				Exception revertException = null;
				for (Change madeChange : madeChanges) {
					try {
						madeChange.revert(session);
					} catch (Exception e) {
						revertException = e;
					}
				}
				if (revertException != null) {
					store.log(revertException);
					Exception originalException = exception;
					exception = new BadDataException(
							"An error occurred when making a bundle of changes. " +
							"Some of the changes could not be reversed: " +
							originalException.getMessage());
					exception.initCause(originalException);
				}
				throw (exception);
			}
		} finally {
			Exception finalException = null;
			for (Item item : itemsToUnlock) {
				try {
					if (item.isCurrent()) {
						item.unlock(session);
					}
				} catch (Exception e) {
					if ((exception == null) && (finalException == null)) 
						finalException = new BadDataException(
							"Some of the items that were locked to make the changes" +
							"could not be unlocked: " + e.getMessage());
					exception.initCause(e);
				}
			}
			if (finalException != null) throw (finalException);
		}
	}
	
	/**
	 * A specification of a triple in which the subject, verb, or object
	 * may be given by an item handle or by a datum value.
	 */
	
	private class QTripleSpec {

		/**
		 * The subject of the triple. If a Given, it is the handle of an
		 * item added by a change. If it is a Datum then its value is an
		 * item in the store.
		 */
		
		Quantum subject = null;

		/**
		 * The verb of the triple. If a Given, it is the handle of an
		 * item added by a change. If it is a Datum then its value is an
		 * item in the store.
		 */
		
		Quantum verb = null;

		/**
		 * The object of the triple. If a Given, it is the handle of an
		 * item added by a change. If it is a Datum then its value is an
		 * item in the store.
		 */
		
		Quantum object = null;
		
		public String toString() {
			
			return "QTripleSpec " + subject + "; " + verb + "; " + object;
		}
	}
	
	/**
	 * A specification of a set of triples that have the same subjects
	 * and verbs, which are given as items, and objects that may be given 
	 * by item handles or datum values.
	 */
	
	private class QoTripleSetSpec {

		/**
		 * The subject of the triples. 
		 */
		
		Item subject = null;
		
		/**
		 * The verb of the triples. 
		 */
		
		Item verb = null;
		
		/**
		 * The objects of the triples. If an object is a Given, it is the 
		 * handle of an item added by a change. If it is a Datum then its 
		 * value is a scalar or an item in the store.
		 */
		
		Set<Quantum> objects = new HashSet<Quantum>();
		
		public String toString() {
			
			StringBuilder builder = new StringBuilder();
			builder.append("QoTripleSetSpec ");
			builder.append(subject);
			builder.append("; ");
			builder.append(verb);
			builder.append("; ");
			boolean first = true;
			for (Quantum object : objects) {
				if (first) first = false;
				else builder.append(", ");
				builder.append(object);
			}
			return builder.toString();
		}
	}
	
	/**
	 * A specification of a triple in which the subject and verb are
	 * given as items and the object is given as a datum.
	 */
	
	private class TripleSpec {


		/**
		 * The subject of the triple. 
		 */
		
		Item subject = null;
		
		/**
		 * The verb of the triple. 
		 */
		
		Item verb = null;
		
		/**
		 * The object of the triple. 
		 */
		
		Datum object = null;
		
		public String toString() {
			
			return "TripleSpec " + subject + "; " + verb + "; " + object;
		}
	}
	
	/**
	 * A change to be made to the items and triples in a triple store.
	 */
	
	private abstract class Change implements Serializable {
		
		private static final long serialVersionUID = -4842502744418192293L;
		
		/**
		 * Lock the item or items that must be locked before the
		 * change can be made.
		 * 
		 * @param lockedItems the set of items that have been locked.
		 * The item or items that must be locked before the
		 * change can be made are added to this set.
		 * 
		 * @param itemsToUnlock the list of items to be unlocked when
		 * all changes are complete. The item or items that must be 
		 * locked before the change can be made are added to the
		 * start of this list.
		 * 
		 * @param session the requesting store session
		 * 
		 * @throws Exception 
		 */
		
		abstract void lock(Set<Item> lockedItems, LinkedList<Item> itemsToUnlock, StoreSession session) throws Exception;

		/**
		 * Make the change
		 * 
		 * @param session the requesting store session
		 * 
		 * @throws Exception
		 */
		
		abstract void perform(StoreSession session) throws Exception;
		
		/**
		 * Reverse the change. This method is invoked if the change,
		 * or a subsequent change, fails.
		 * 
		 * @param session the requesting store session
		 * 
		 * @throws Exception
		 */
		
		abstract void revert(StoreSession session) throws Exception;
		
		/**
		 * The new item that was created by the change, if there is one
		 * 
		 * @return the new item, or null
		 */
		
		Item getNewItem() {
			
			return null;
		}
	}

	/**
	 * A change to add an item.
	 */
	
	private class ItemToAdd extends Change {

		private static final long serialVersionUID = 5866651520840567905L;

		/**
		 * The numeric identifier of the source of the item to be added
		 */
		
		Long source = 0L;
		
		/**
		 * The handle used to refer to the item in other changes or
		 * after the changes have been made
		 */
		
		String handle = null;
		
		/**
		 * The item's only-read level
		 */
		
		AccessLevel onlyReadLevel = null;
		
		/**
		 * The item's read-or-write level
		 */
		
		AccessLevel readOrWriteLevel = null;
		
		/**
		 * The added item
		 */
		
		Item item = null;
		
		/**
		 * Construct a change to add an item
		 * 
		 * @param itemSpec the specification of the item to be added
		 */
		
		@Override
		void lock(Set<Item> lockedItems, LinkedList<Item> itemsToUnlock, StoreSession session) {}

		@Override
		void perform(StoreSession session) throws Exception {
			item = store.createLockedItem(
					source, onlyReadLevel, readOrWriteLevel, session);	
			if (item == null) 
				throw (new AccessException("Cannot create item for " + handle));
			newItems.put(handle, item);
		}
					
		@Override
		void revert(StoreSession session) throws Exception {
			
			// Undo the addition if something went wrong
			if (item != null) {
				store.deleteItem(item, session);
				newItems.remove(handle);
			}
		}
		
		@Override
		Item getNewItem() {
			
			return item;
		}
		
		public String toString() {
			
			return "Change - Add Item " + handle + " in source " + source;
		}
	}
	
	/**
	 * A change to update an item.
	 */
	
	private class ItemToUpdate extends Change {

		private static final long serialVersionUID = -8243888449958206638L;

		/**
		 * The only-read level that the item is to have after the change
		 */
		
		AccessLevel onlyReadLevel = null;
		
		/**
		 * The read-or-write level that the item is to have after the change
		 */
		
		AccessLevel readOrWriteLevel = null;
		
		/**
		 * The specification of any dependent items that are to be updated
		 * so that they have the same access levels as the item
		 */
		
		DependentsSpec dependentsSpec = null;

		/**
		 * The item to be updated
		 */
		
		Item item;
		
		/**
		 * The only-read level that the item had before being updated
		 */
		
		AccessLevel oldOnlyReadLevel = null;
		
		/**
		 * The read-or-write level that the item had before being updated
		 */
		
		AccessLevel oldReadOrWriteLevel = null;
		
		/**
		 * Construct a change to update an item
		 * 
		 * @param item the item to be updated
		 * 
		 * @param spec the specification of the update
		 */
		
		@Override
		void lock(Set<Item> lockedItems, LinkedList<Item> itemsToUnlock, StoreSession session) 
				throws Exception {
			
			if (!lockedItems.contains(item)) {
				if (item.lock(session)) {
					lockedItems.add(item);
					itemsToUnlock.addFirst(item);
				}
				else throw (new AccessException(
						"Cannot lock item: " + item.getId()));
			}
		}
		
		@Override
		void perform(StoreSession session) throws Exception {
			
			// Get the old access levels
			if (onlyReadLevel != null) {
				oldOnlyReadLevel = item.getEffectiveOnlyReadLevel(session);
				if (oldOnlyReadLevel == null) 
					throw (new AccessException("Cannot get read level for " + item));
			}
			if (readOrWriteLevel != null) {
				oldReadOrWriteLevel = item.getEffectiveReadOrWriteLevel(session);
				if (oldReadOrWriteLevel == null) 
					throw (new AccessException("Cannot get write level for " + item));
			}
			
			// Update the item
			if ((onlyReadLevel != null) && !onlyReadLevel.equals(oldOnlyReadLevel)) {
				if (!item.setOnlyReadLevel(onlyReadLevel, session))
					throw (new AccessException("Cannot set read level for " + item));
			}
			if ((readOrWriteLevel != null) && !readOrWriteLevel.equals(oldReadOrWriteLevel)) {
				if (!item.setReadOrWriteLevel(readOrWriteLevel, session)) {
					if ((onlyReadLevel != null) && !onlyReadLevel.equals(oldOnlyReadLevel))
						item.setOnlyReadLevel(oldOnlyReadLevel, session);
					throw (new AccessException("Cannot set write level for " + item));
				}
			}
		}
		
		@Override
		void revert(StoreSession session) throws Exception {

			// Undo the update if something went wrong
			if ((onlyReadLevel != null) && !onlyReadLevel.equals(oldOnlyReadLevel))
				item.setOnlyReadLevel(oldOnlyReadLevel, session);
			if ((readOrWriteLevel != null) && !readOrWriteLevel.equals(oldReadOrWriteLevel))
				item.setReadOrWriteLevel(oldReadOrWriteLevel, session);
		}
		
		private void getDependents(LinkedList<ItemToUpdate> itemUpdates, StoreSession session) 
				throws Exception {
			
			if (dependentsSpec != null) {
				for (Item verb : dependentsSpec.getVerbs()) {
					Set<Item> dependents = dependentsSpec.getImmediatelyDependentItems(
							item, verb, store, session);
					for (Item dependent : dependents) {
						ItemToUpdate dependentItemToUpdate = new ItemToUpdate();
						dependentItemToUpdate.item = dependent;
						dependentItemToUpdate.onlyReadLevel = onlyReadLevel;
						dependentItemToUpdate.readOrWriteLevel = readOrWriteLevel;
						dependentItemToUpdate.dependentsSpec = dependentsSpec.getSubDependents(verb);
						itemUpdates.add(dependentItemToUpdate);
					}
				}
			}
		}
		
		public String toString() {
			
			return "Change - Update Item " + item;
		}
	}

	/**
	 * A change to delete triples.
	 */
	
	private class TriplesToDelete extends Change {

		private static final long serialVersionUID = 1L;

		/**
		 * The specification of the triples to be deleted
		 */
		
		TripleSpec tripleSpec;
		
		/**
		 * The triples, if any, satisfying the specification, with
		 * their objects
		 */
		
		Map<Triple, Datum> triplesMap = null;
		
		/**
		 * Construct a change to delete a triple
		 * 
		 * @param tripleSpec the specification of the triple to be deleted.
		 * All matching triples are deleted.
		 */
		
		TriplesToDelete(TripleSpec tripleSpec) {
			
			this.tripleSpec = tripleSpec;
		}

		@Override
		void lock(Set<Item> lockedItems, LinkedList<Item> itemsToUnlock, StoreSession session) throws Exception {
			
			Item item = tripleSpec.subject;
			if (!lockedItems.contains(item)) {
				if (item.lock(session)) {
					lockedItems.add(item);
					itemsToUnlock.addFirst(item);
				}
				else throw (new AccessException(
						"Cannot lock item: " + item.getId()));
			}
		}

		@Override
		void perform(StoreSession session) throws Exception {
			
			// Delete the triples matching the spec
			List<Triple> triples = store.getTriples(
					tripleSpec.subject, tripleSpec.verb, tripleSpec.object, session);
			if (!triples.isEmpty()) {
				triplesMap = new HashMap<Triple, Datum>();
				for (Triple t : triples) {
					Datum object = t.getObject(session);
					if (store.deleteTriple(t, session)) triplesMap.put(t, object);
					else throw (new AccessException("Cannot delete triple " + t.getId()));					
				}
			}
		}
		
		@Override
		void revert(StoreSession session) throws Exception {

			// Undo the deletion if something went wrong
			if ((triplesMap != null) && !triplesMap.isEmpty()) {
				for (Entry<Triple, Datum> e : triplesMap.entrySet()) {
					Triple t = e.getKey();
					store.createTriple(
							t.getSubject(), 
							t.getVerb(), 
							e.getValue(), 
							session);
				}
			}
		}
		
		public String toString() {
			
			return "Change - Delete Triple " + tripleSpec;
		}
	}
	
	/**
	 * A change to put a triple.
	 */
	
	private class TripleToPut extends Change {

		private static final long serialVersionUID = -8604755791175961090L;

		/**
		 * The specification of the triple to be put
		 */
		
		QTripleSpec qTripleSpec;
		
		/**
		 * The new triple satisfying the specification that was
		 * created if one did not exist already
		 */
		
		Triple triple = null;
		
		/**
		 * Construct a change to put a triple
		 * 
		 * @param qTripleSpec the specification of the triple to be put
		 */
		
		TripleToPut(QTripleSpec qTripleSpec) {
			
			this.qTripleSpec = qTripleSpec;
		}

		@Override
		void lock(Set<Item> lockedItems, LinkedList<Item> itemsToUnlock, StoreSession session) throws Exception {
			
			Datum d = qTripleSpec.subject.getDatum();
			if ((d == null) || (d.getType() != Datum.Type.ITEM)) return;
			Item item = d.getItemValue();
			if (!lockedItems.contains(item)) {
				if (item.lock(session)) {
					lockedItems.add(item);
					itemsToUnlock.addFirst(item);
				}
				else throw (new AccessException(
						"Cannot lock item: " + item.getId()));
			}
		}

		@Override
		void perform(StoreSession session) throws Exception {
			
			// Set the triple's subject, verb, and object
			Item subject = null;
			String var = qTripleSpec.subject.getId();
			if (var == null)
				subject = qTripleSpec.subject.getDatum().getItemValue();
			else subject = newItems.get(var);
			Item verb = null;
			var = qTripleSpec.verb.getId();
			if (var == null)
				verb = qTripleSpec.verb.getDatum().getItemValue();
			else verb = newItems.get(var);
			Datum object = null;
			var = qTripleSpec.object.getId();
			if (var == null)
				object = qTripleSpec.object.getDatum();
			else object = Datum.create(newItems.get(var));
			
			// Does an equivalent triple already exist?
			List<Triple> triples = store.getTriples(subject, verb, object, session);
			if (triples.isEmpty()) {
				// No - add the triple
				triple = store.createTriple(subject, verb, object, session);
				if (triple == null)
					throw (new AccessException("Cannot create triple  with subject " + subject.getId()));				
			}
		}
		
		@Override
		void revert(StoreSession session) throws Exception {
			
			// Undo the creation if something went wrong
			if (triple != null) store.deleteTriple(triple, session);
		}
		
		public String toString() {
			
			return "Change - Put Triple " + qTripleSpec;
		}
	}
	
	/**
	 * A change to establish a set of triples with a given subject, verb, and
	 * set of objects. For each object in the set, it ensures that there is
	 * a triple with the given subject and verb and that object, and that 
	 * these are the only triples with that subject and verb.
	 */
	
	private class TripleSet extends Change {

		private static final long serialVersionUID = -7046142137193333251L;
		
		/**
		 * The specification of the triple, giving the subject, verb,
		 * and unique object.
		 */
		
		QoTripleSetSpec qoSpec;
		
		/**
		 * The new triples created
		 */
		
		Set<Triple> newTriples = new HashSet<Triple>();
		
		/**
		 * Redundant triples with the given subject and verb that
		 * were removed, if any
		 */
		
		Set<RemovedTriple> removedTriples = new HashSet<RemovedTriple>();

		/**
		 * Construct a change to establish a set of triples
		 * 
		 * @param qoSpec the specification of the triples, giving 
		 * the subject, verb, and set of objects.
		 */
		
		TripleSet(QoTripleSetSpec qoSpec) {
			
			this.qoSpec = qoSpec;
		}
		
		@Override
		void lock(Set<Item> lockedItems, LinkedList<Item> itemsToUnlock, StoreSession session) throws Exception {
			
			Item item = qoSpec.subject;
			if (!lockedItems.contains(item)) {
				if (item.lock(session)) {
					lockedItems.add(item);
					itemsToUnlock.addFirst(item);
				}
				else throw (new AccessException(
						"Cannot lock item: " + item.getId()));
			}
		}

		@Override
		void perform(StoreSession session) throws Exception {
			
			// Get all the triples with the same subject and verb
			List<Triple> triples = store.getAllTriples(
					qoSpec.subject, qoSpec.verb, session);
			if (triples == null)  throw (new AccessException(
					"Cannot get all triples to establish triple set"));
			
			// Get the objects that are not new added items
			Set<Datum> objectDatums = new HashSet<Datum>();
			for (Quantum q : qoSpec.objects) {
				Datum d = q.datum;
				if (d != null) objectDatums.add(d);
			}
			
			// Review the matching triples
			Set<Datum> foundObjects = new HashSet<Datum>();
			for (Triple triple : triples) {

				// If the triple object is in the set of objects that
				// are not new added items, check whether another 
				// matching triple with the same object has been found already
				Datum d = triple.getObject(session);
				if (objectDatums.contains(d)) {
					// If another matching triple with the same object has been 
					// found already, this is a duplicate. Remove it
					if (foundObjects.contains(d)) { 
						removedTriples.add(new RemovedTriple(triple, session));
						if (!store.deleteTriple(triple, session)) throw (new AccessException(
								"Cannot delete triple " + triple.getId()));
					}
					// If another matching triple with the same object has not
					// been found already, this could be the triple with the
					// unique object
					else foundObjects.add(d);
				}
				
				// If the triple object is not in the set of objects that
				// are not new added items, remove the triple
				else {
					removedTriples.add(new RemovedTriple(triple, session));
					if (!store.deleteTriple(triple, session)) throw (new AccessException(
							"Cannot delete triple " + triple.getId()));
				}				
			}
			
			// If there are triples to be established for which an equivalent triple 
			// does not already exist, add triples with the given subject, verb, and objects
			for (Quantum q : qoSpec.objects) {
				Datum d = q.datum;
				if ((d == null) || !foundObjects.contains(d)) {					
					Triple newTriple;					
					// If the object is a new added item, get it.
					if (d == null) {
						Item objectItem = newItems.get(q.id);
						if (objectItem == null)
							throw (new InputException(
									"Triple set object has not been created: " + q.id));				
						newTriple = store.createTriple(
								qoSpec.subject, qoSpec.verb, Datum.create(objectItem), session);
					}				
					else newTriple = store.createTriple(
							qoSpec.subject, qoSpec.verb, d, session);
					if (newTriple == null)
						throw (new AccessException("Cannot create triple  with subject " + 
													qoSpec.subject.getId()));
					newTriples.add(newTriple);
				}
			}
		}
		
		@Override
		void revert(StoreSession session) throws Exception {
			
			for (Triple t : newTriples)
				store.deleteTriple(t, session);
			for (RemovedTriple t : removedTriples)
				store.createTriple(t.subject, t.verb, t.object, session);
		}
		
		public String toString() {
			
			return "Change - Establish Triple Set " + qoSpec;
		}
	}
	
	/**
	 * The subject, verb and object of a deleted triple, saved so that
	 * a new triple with that subject, verb and object can be created
	 * to reverse the deletion.
	 */
	
	private class RemovedTriple {
		
		/**
		 * The subject
		 */
		
		Item subject;
		
		/**
		 * The verb
		 */
		
		Item verb;
		
		/**
		 * The object
		 */
		
		Datum object;
		
		RemovedTriple(Triple t, StoreSession session) throws Exception {
		
			this.subject = t.getSubject();
			this.verb = t.getVerb();
			this.object = t.getObject(session);
		}
	}
	
	/**
	 * <p>A change to rank a new item.
	 * </p>
	 * <p>A set of items is ranked by making each of them the subject of a
	 * triple whose verb is a member rank relation and whose object is an
	 * integer that specifies its rank. The new item is to be ranked in the
	 * set of items that are the values of the rank unknown when the constraints
	 * are applied.
	 * </p>
	 */
	
	private class NewItemToRank extends Change {

		private static final long serialVersionUID = 481859355210330732L;
		
		/**
		 * The join constraints defining the set of items in which the 
		 * new item is to be ranked.
		 */
		
		JoinConstraint[] constraints = null;
		
		/**
		 * The unknown defining the rank in the join constraints
		 */
		
		String rankUnknown = null;
		
		/**
		 * The member rank relation
		 */
		
		Item memberRankRelation = null;
		
		/**
		 * The handle specified in the change to add the new item.
		 */
		
		String newItemHandle = null;
		
		/**
		 * The triple setting the rank of the item
		 */
		
		Triple rankTriple = null;
		
		@Override
		void lock(Set<Item> lockedItems, LinkedList<Item> itemsToUnlock, StoreSession session) {}

		@Override
		void perform(StoreSession session) throws Exception {
			
			// Find the rank that is one greater than the maximum current 
			// display rank for the new item context
			Join join = new Join(constraints);
			List<ConstrainedTriples> constrainedTriplesList = 
					join.make(store, session);
			int rank = 1;
			for (ConstrainedTriples ct : constrainedTriplesList) {
				int ctr = (int)ct.getAssignment(rankUnknown).getIntegralValue();
				if (ctr >= rank) rank = ctr + 1;
			}
			
			// Create a triple giving the new item the rank
			Item subject = newItems.get(newItemHandle); 
			rankTriple = store.createTriple(
					subject, memberRankRelation, Datum.create(rank), session);
			if (rankTriple == null)
				throw (new AccessException("Cannot create display rank triple for " + subject.getId()));				
		}
		
		@Override
		void revert(StoreSession session) throws Exception {

			// Undo the creation if something went wrong
			if (rankTriple != null) store.deleteTriple(rankTriple, session);
		}
		
		public String toString() {
			
			return "Change - Rank Item " + newItemHandle;
		}
	}
	
	/**
	 * A change to delete an item.
	 * Deletion of an item cannot be reversed, but if a locked item
	 * cannot be deleted then something is seriously wrong
	 */
	
	private class ItemToDelete extends Change {

		private static final long serialVersionUID = -6874033545320244567L;
		
		/**
		 * The item to be deleted
		 */
		
		Item item;
		
		/**
		 * The specification of any dependents to be deleted with the item
		 */
		
		DependentsSpec dependentsSpec;
		
		/**
		 * Construct a change to delete an item
		 * 
		 * @param item the item to be deleted
		 * 
		 * @param dependentsSpec the specification of any dependents to be 
		 * deleted with the item. May be null.
		 */
		
		ItemToDelete(Item item, DependentsSpec dependentsSpec) {
			
			this.item = item;
			this.dependentsSpec = dependentsSpec;
		}
		
		@Override
		void lock(Set<Item> lockedItems, LinkedList<Item> itemsToUnlock, StoreSession session) throws Exception {
			
			if (!lockedItems.contains(item)) {
				if (item.lock(session)) {
					lockedItems.add(item);
					itemsToUnlock.addFirst(item);
				}
				else throw (new AccessException(
						"Cannot lock item: " + item.getId()));
			}
		}

		@Override
		void perform(StoreSession session) throws Exception {
			
			// Delete the item (unless it has already been deleted)
			if (item.isCurrent()) {
				if (!store.deleteItem(item, session))
					throw(new AccessException("Cannot delete item " + item.getId()));
			}
		}

		@Override
		void revert(StoreSession session) throws Exception {}		
		
		private void getDependents(LinkedList<ItemToDelete> itemDeletes, StoreSession session) 
				throws Exception {
			
			if (dependentsSpec != null) {
				for (Item verb : dependentsSpec.getVerbs()) {
					Set<Item> dependents = dependentsSpec.getImmediatelyDependentItems(
							item, verb, store, session);
					for (Item dependent : dependents) {
						itemDeletes.add(new ItemToDelete(dependent, dependentsSpec.getSubDependents(verb)));
					}
				}
			}
		}
		
		public String toString() {
			
			return "Change - Delete Item " + item;
		}
	}
}
