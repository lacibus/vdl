/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

/**
 * An exception that is thrown when the requesting store session does
 * not have sufficient access rights to perform the requested operation.
 */

public class AccessException extends Exception {
	
	private static final long serialVersionUID = 5675657713418828825L;

	/**
	 * Create an access exception
	 * 
	 * @param message a description of the reason for throwing the exception
	 */
	
	public AccessException(String message) {
		
		super(message);
	}
}
