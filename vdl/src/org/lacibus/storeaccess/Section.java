/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import org.lacibus.triplestore.Triple;

/**
 * A section of a join, defined by a subset of the join
 * constraints. (This subset is given by the
 * constraintIndexes array of the EntryContainer
 * superclass.)
 */

class Section extends EntryContainer {
	
	/**
	 * The entries that satisfy the subset of the join
	 * constraints that defines the section.
	 */
	
	Entry entries[];
	
	/**
	 * Construct a new section, with no content
	 */
	
	Section(){}
	
	/**
	 * Construct a new section from a SectionPair. When
	 * the join is made, pairs of sections are joined to
	 * create SectionPairs. These are then converted to
	 * Sections (via this constructor) that can be pairwise
	 * joined with other sections.
	 * 
	 * @param pair a SectionPair
	 */
	
	Section(SectionPair pair) {
		
		join = pair.join;
		constraintIndexes = pair.constraintIndexes;
		varNames = pair.varNames;
		entries = new Entry[pair.entries.size()];
		int i = 0;
		for (SectionPair.Entry e : pair.entries) {
			Entry entry = new Entry();
			entry.triples = e.triples;
			entry.varValues = e.varValues;
			entries[i++] = entry;
		}
	}
	
	int sizeIndicator() {
		
		if ((entries == null) || (varNames == null)) return 0;
		// else return entries.length*varNames.length;
		// Try just equating size to number of entries
		else return entries.length;
	}

	/**
	 * Arrange each entry so that its triples are in the 
	 * same order as the constraints. As a result, for
	 * each n < constraintIndexes.length, we will have
	 * constraintIndexes[n] = n
	 * 
	 * @throws JoinException 
	 */
	
	void arrangeEntriesByConstraints() throws JoinException
	
	{
		for (Entry entry : entries) {
			if (entry.triples.length != constraintIndexes.length)
				throw (new JoinException("Malformed join entry"));
			Triple[] triples = new Triple[constraintIndexes.length];
			for (int i = 0; i < constraintIndexes.length; i++) {
				triples[constraintIndexes[i]] = entry.triples[i];
			}
			entry.triples = triples;
		}
		for (int i = 0; i < constraintIndexes.length; i++)
			constraintIndexes[i] = i;
	}
	
	/**
	 * @return a text representation of the entries in the section
	 */
	
	String entriesToString() {
		
		String s;
		if (entries == null) s = "NULL ENTRIES";
		else if (entries.length == 0) s = "NO ENTRIES";
		else {
			s = "";
			for (int i = 0; i < entries.length; i++) s = s + '\n' + entries[i];
		}
		return s;
	}
	
	public String toString() {
		
		return "SECTION{\n" + constraintsToString() + entriesToString() + "}\n";
	}	
}
