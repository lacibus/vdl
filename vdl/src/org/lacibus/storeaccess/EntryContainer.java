/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.storeaccess;

import org.lacibus.triplestore.Triple;

/**
 * A container for entries that consist of arrays of 
 * triples that satisfy an array of constraints, and 
 * therefore define assignments of values to constraint
 * variables. 
 * 
 * The concrete subclasses of EntryContainer are Section
 * and SectionPair. A SectionPair is created when two
 * Sections are merged when a Join is made.
 */

abstract class EntryContainer {
	
	/**
	 * The join that uses this entry container
	 */
	
	Join join;
	
	/**
	 * Indexes of the constraints that the triples satisfy
	 * in the array of constraints of the join. The entry 
	 * container is concerned with a subset of the join 
	 * constraints. This array defines that subset.
	 */
	
	int[] constraintIndexes;
	
	/**
	 * The names of the variables in the constraints
	 */
	
	String[] varNames;
	
	/**
	 * Print the constraints
	 * 
	 * @return a string representation of the constraints
	 */
	
	String constraintsToString() {
		
		String s;
		if (constraintIndexes == null) s = "\tNULL CONSTRAINTS\n";
		else if (constraintIndexes.length == 0) s = "\tNO CONSTRAINTS\n";
		else {
			s = "";
			for (int i = 0; i < constraintIndexes.length; i++) {
				s = s + '\t' + join.constraints[constraintIndexes[i]] + '\n';
			}
		}
		return s;
	}
	
	/**
	 * An entry consists of an array of triples that satisfy
	 * an array of constraints, and a set of assignments of
	 * values to constraint variables. The assignments are
	 * implied by the triples, but are also stored separately.
	 */
	
	class Entry {
		
		/**
		 * An array of triples that satisfy an array of
		 * constraints. Triple triples[n] satisfies constraint
		 * constraintIndexes[n].
		 */
		
		Triple triples[] = new Triple[constraintIndexes.length];
		
		/**
		 * The summary values of the values of the constrained variables.
		 * varValues[n] is the summary value of the value of the
		 * variable with name varNames[n]
		 * 
		 * Note that the join algorithm relies on the fact that the 
		 * summary value of a datum containing an item is the summary 
		 * value of the item that it contains.
		 */
		
		long varValues[] = new long[varNames.length];
		
		/**
		 * Get an array containing the summary values of an array
		 * of constrained variables. The array is used as the key
		 * to a hash map of lists of entries.
		 * 
		 * @param names an array of names of constrained variables
		 * 
		 * @return an array containing the values of the constrained
		 * variables with the given names, with the values in the same
		 * order as the names.
		 */
		
		long[] subValues(String[] names) {
			
			long[] result = new long[names.length];
			for (int i = 0; i < names.length; i++) {
				for (int j = 0; j < varNames.length; j++) {
					if (names[i].equals(varNames[j])) result[i] = varValues[j];
				}
			}
			return result;
		}
		
		public String toString() {
			
			String s = "ENTRY{";
			for (int i = 0; i < varNames.length; i++) {
				s = s + varNames[i] + "=" + varValues[i] + "; ";
			}
			s = s + "}";
			return s;
		}
		
		String toDeepString() {
			
			StringBuilder sb = new StringBuilder(toString());
			for (int i = 0; i < triples.length; i++) {
				sb.append("\n\t");
				if (triples[i] == null) sb.append("NULL");
				else sb.append(triples[i].toString());
			}
			sb.append('\n');
			return sb.toString();
		}
	}	
}
