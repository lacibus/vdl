/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import org.lacibus.triplestore.NotInOperationException;
import org.lacibus.triplestore.TripleStoreFatalException;
import org.lacibus.triplestore.TripleStoreNonFatalException;

/**
 * An interface used by source connectors to invoke methods in the
 * stores that use them to connect to sources.
 */

public interface SourceConnectorClient {
	
	/**
	 * @return the numeric identifier of the special item source
	 */
	
	public long getSpecialItemSourceNr();
	
	/**
	 * @return the numeric identifier of the item that is the verb 
	 * in access level definition triples
	 */
	
	public long getIsAccessLevelRelationNr();
	
	/**
	 * Return the numeric identifier of the lowest access level
	 */
	
	public long getLowestAccessLevelNr();

	/**
	 * Return the numeric identifier of the highest access level
	 */
	
	public long getHighestAccessLevelNr();

	/**
	 * Apply a delta
	 * 
	 * @param sourceNr the numeric identifier of the source originating 
	 * the delta
	 * 
	 * @param delta a delta
	 * 
	 * @param load true if the triple is loaded in a load operation, 
	 * false if in a sync operation
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * @throws NotInOperationException 
	 */
	
	public void applyDelta(long sourceNr, Delta delta, boolean load) 
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException, 
							NotInOperationException;
	
}
