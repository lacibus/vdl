/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * A record of transactions that have been completed for
 * a source.
 */

public class TransactionsStatus implements Serializable {

	private static final long serialVersionUID = -999166637437455083L;

   /**
 	* A list of completed transactions, maintained in ascending
 	* order of transaction sequence number. 
 	*/
	
	private LinkedList<Long> completedTransactionsList = 
			new LinkedList<Long>();
	
   /**
 	* A sequence number such that all transactions with lower
 	* sequence numbers have been completed. The transaction
 	* with this sequence number has not been completed, however.
 	* Note that the transaction with the lowest possible sequence
 	* number is a special one used at start-up time; it should
 	* always be regarded as having been completed.
 	*/
	
	private long allCompletedBefore = Long.MIN_VALUE+1;
		
	/**
	 * Construct a new transactions status given a list of completed 
	 * transactions
	 * 
	 * @param completedTransactionSequenceNumbers a list of
	 * sequence numbers of completed transactions such that
	 * all transactions before the first one listed have been
	 * completed.
	 */
		
	public TransactionsStatus(List<Long> completedTransactionSequenceNumbers) {
		
		if (	(completedTransactionSequenceNumbers == null) ||
				completedTransactionSequenceNumbers.isEmpty() )
			return;
			
		setAllCompletedBefore(completedTransactionSequenceNumbers.remove(0));
		for (Long seq : completedTransactionSequenceNumbers) 
			noteCompleted(seq);
	}
	
	/**
	 * Construct a new transactions status in which all transactions with
	 * sequence numbers up to and including a given sequence number are 
	 * completed.
	 * 
	 * @param highestCompleted a sequence number such that all
	 * transactions with sequence numbers up to and including
	 * it are completed.
	 */
		
	TransactionsStatus(long highestCompleted) {
		
		setAllCompletedBefore(highestCompleted + 1);
	}	
	
	/**
	 * @return an identical copy
	 */
	
	public synchronized TransactionsStatus getClone() {
		
		TransactionsStatus status = 
				new TransactionsStatus(allCompletedBefore - 1);
		for (long seq : completedTransactionsList)
			status.noteCompleted(seq);
		return status;
	}
	
	/**
	 * Set the sequence number such that all transactions with
	 * lower sequence numbers have been completed.
	 * 
	 * @param seq the new value to be set as the sequence number
	 * such that all transactions with lower sequence numbers have
	 * been completed.
	 */
		
	synchronized void setAllCompletedBefore(long seq) {

		allCompletedBefore = seq;
	}
		
	/**
	 * Determine whether the transaction with a given sequence number
	 * has been completed.
	 * 
	 * @param seq the sequence number of the transaction whose
	 * completion is to be checked
	 * 
	 * @return true if that transaction has been completed, false
	 * otherwise.
	 */
		
	public synchronized boolean isCompleted(long seq) {
			
		if (seq < allCompletedBefore) return true;
		if (completedTransactionsList.isEmpty()) return false;
		for (Long l : completedTransactionsList) {
			if (seq < l) return false;
			if (seq == l) return true;
		}
		return false;
	}
	
	/**
	 * @return the sequence number of the highest completed transaction,
	 * or the lowest Long number if none have been completed.
	 */
	
	public synchronized long getHighestCompleted() {
		
		if (completedTransactionsList.isEmpty())
			return allCompletedBefore -1;
		else return completedTransactionsList.getLast();
		
	}
		
		
	/**
	 * Get a list of the sequence numbers of completed transactions,
	 * starting with one before which all transactions have completed.
	 * 
	 * @return  a list of the sequence numbers of transactions
	 * starting with one before which all transactions have
	 * completed and then listing sequence numbers of
	 * completed transactions.
	 * 
	 */
		
	public synchronized List<Long> getTransactionSequenceNumbers() {
			
		List<Long> sequenceNumbers = new ArrayList<Long>();
		sequenceNumbers.add(allCompletedBefore);
		for (Long l : completedTransactionsList)
			sequenceNumbers.add(l);
		return sequenceNumbers;
	}
		
	/**
	 * Update the list of completed transactions and recalculate
	 * the sequence number such that all transactions with lower
	 * sequence numbers have been completed, given a sequence
	 * number such that it and all transactions with lower sequence
	 * numbers have been completed.
	 * 
	 * @param seq a sequence number such that it and all transactions
	 * with lower sequence numbers have been completed
	 */
	
	public synchronized void recalcAllCompletedBefore(long highestCompleted) {
			
		allCompletedBefore = highestCompleted + 1;
		while (!completedTransactionsList.isEmpty() &&
			   (completedTransactionsList.getFirst() <
						   allCompletedBefore))
			completedTransactionsList.removeFirst();
		while (!completedTransactionsList.isEmpty() &&
			   (completedTransactionsList.getFirst() == 
				   			allCompletedBefore)) {
			completedTransactionsList.removeFirst();
			allCompletedBefore++;
		}
	}
			
	/**
	 * Note a transaction is completed. If the transaction
	 * is already noted as complete, do nothing.
	 * 
	 * @param seq the sequence number of a completed
	 * transaction
	 */
	
	public synchronized void noteCompleted(long seq) {
		
		if (seq < allCompletedBefore) return;
		// Is this the first one not noted as complete?
		if (seq == allCompletedBefore) {
			// Yes
			allCompletedBefore++;
			
			// Remove all but the last of any transactions at the start
			// of the list that are in numerical sequence. 
			while (completedTransactionsList.size() > 1) {
				if (completedTransactionsList.getFirst() ==
						allCompletedBefore) {
					completedTransactionsList.removeFirst();
					allCompletedBefore++;
				}
				else break;
			}
		}
		else {
			// No - there are preceding transactions that are not
			// yet complete
			// Add the transaction to the list in the correct
			// place
			int count = 0;
			for (Long l : completedTransactionsList) {
				if (seq == l) return;
				if (seq < l) break;
				count++;
			}
			completedTransactionsList.add(count, seq);
		}		
	}	
	
	/**
	 * Note that a list of transactions are all
	 * completed.
	 * 
	 * @param seqs a list of completed transaction
	 * sequence numbers
	 */
	
	synchronized void noteCompleted(List<Long> seqs) {
		
		for (Long seq : seqs) {
			if (seq >= allCompletedBefore) 
				noteCompleted(seq);
		}
	}
	
	/**
	 * List the sequence numbers of transactions that are 
	 * completed in another transactions status but not
	 * completed in this one.
	 * 
	 * @param targetStatus another transactions status
	 * 
	 * @return the sequence numbers of transactions that are 
	 * completed in the target transactions status but not
	 * completed in this one.
	 */
	
	synchronized List<Long> outstandingTransactions(
			TransactionsStatus targetStatus) {
		
		synchronized(targetStatus) {
			List<Long> result = new ArrayList<Long>();
			
			long nextToCheck = allCompletedBefore;
			Iterator<Long> done = 
					completedTransactionsList.iterator();
			Long nextdone = null;
			
			// Check transactions before the target allCompletedBefore
			if (done.hasNext()) nextdone = done.next();
			while (nextToCheck < targetStatus.allCompletedBefore) {
				result.add(nextToCheck++);
				while (	(nextdone != null) &&
						(nextToCheck == nextdone)){
					nextToCheck++;
					if (done.hasNext())
						nextdone = done.next();
					else nextdone = null;
				}
			}
			
			// Check transactions in the target completed transactions
			// list.
			Iterator<Long> targets = 
					targetStatus.completedTransactionsList.iterator();
			while (targets.hasNext()) {
				nextToCheck = targets.next();
				while (	(nextdone != null) &&
						(nextToCheck > nextdone)){
					if (done.hasNext())
						nextdone = done.next();
					else nextdone = null;
				}
				if (	(nextdone == null) ||
						(nextdone > nextToCheck))
					result.add(nextToCheck);
			}
			
			// Return the result
			return result;
		}	
	}
}
