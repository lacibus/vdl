/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;

import org.lacibus.triplestore.Item;

/**
 * An item triple stored in a triple store
 */

public class StoredItemTriple extends StoredTriple implements Serializable {
	
	private static final long serialVersionUID = 2457386818469661893L;
	
	/**
	 * The item that is the object of the triple
	 */
	
	public Item object;

	/**
	 * Construct a stored item triple with a given numeric identifier, 
	 * subject, verb, and item object
	 * 
	 * @param tripleNr the triple's numeric identifier
	 * 
	 * @param subject the subject item
	 * 
	 * @param verb the verb item
	 * 
	 * @param object the object item
	 */
	
	public StoredItemTriple(
			long tripleNr,
			Item subject,
			Item verb, 
			Item object) {
		
		this.tripleNr = tripleNr;
		this.subject = subject;
		this.verb = verb;
		this.object = object;
	}	
}
