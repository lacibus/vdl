/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;

/**
 * An exception that is thrown when an IO error occurs in the 
 * operation of the connector. 
 */

public class SourceConnectorIOException
				extends SourceConnectorException implements Serializable {

	private static final long serialVersionUID = -5861243056708166798L;

	/**
	 * Create a source connector IO exception
	 * 
	 * @param source the numeric identifier of the source that the
	 * connector connects to
	 * 
	 * @param s a description of the reason for throwing the exception
	 */
	
	public SourceConnectorIOException(
			long source, String s) {
		
		super(source, s);
	}
}
