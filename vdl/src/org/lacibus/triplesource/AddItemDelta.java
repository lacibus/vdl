/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;

/**
 * A record of a change to add an item to a source or store
 * of items and triples
 */

public class AddItemDelta extends Delta implements Serializable {

	private static final long serialVersionUID = -2443663348503148924L;

	/**
	  * The numeric identifier of the item in its source
	  */
	 
	 public long itemNr;
	 
	 /**
	  * The numeric identifier of the source of the item representing
	  * the item's only-read access level
	  */

	 public long readLevelSourceNr;

	 /**
	  * The numeric identifier in its source of the item representing 
	  * the item's read access level
	  */

	 public long readLevelItemNr;

	 /**
	  * The numeric identifier of the source of the item representing
	  * the item's read-or-write access level
	  */

	 public long writeLevelSourceNr;

	 /**
	  * The numeric identifier in its source of the item representing 
	  * the item's read-or-write access level
	  */

	 public long writeLevelItemNr;
	 
	 /**
	  * Create a record of a change that adds an item.
	  *  
	  * @param itemNr the numeric identifier of the item in its source
	  * 
	  * @param readLevelSourceNr the numeric identifier of the source of 
	  * the item representing the item's only-read access level
	  * 
	  * @param readLevelItemNr the numeric identifier in its source of 
	  * the item representing the item's read access level
	  * 
	  * @param writeLevelSourceNr the numeric identifier of the source of 
	  * the item representing the item's read-or-write access level
	  * 
	  * @param writeLevelItemNr the numeric identifier in its source of 
	  * the item representing the item's read-or-write access level
	  */
	 
	 public AddItemDelta(
			 long itemNr,
			 long readLevelSourceNr,
			 long readLevelItemNr,
			 long writeLevelSourceNr,
			 long writeLevelItemNr) {
		 
		 this.itemNr = itemNr;
		 this.readLevelSourceNr = readLevelSourceNr;
		 this.readLevelItemNr = readLevelItemNr;
		 this.writeLevelSourceNr = writeLevelSourceNr;
		 this.writeLevelItemNr = writeLevelItemNr;
	 }
}
