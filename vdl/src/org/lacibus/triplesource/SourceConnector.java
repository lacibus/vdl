/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Id;
import org.lacibus.triplestore.Item;
import org.lacibus.triplestore.Logger;
import org.lacibus.triplestore.Transaction;
import org.lacibus.triplestore.Triple;
import org.lacibus.triplestore.TripleStoreNonFatalException;

/**
 * <p>A connector to a source of items and triples.
 * </p>
 * <p>Each source is identified by a unique long integer.
 * </p>
 * <p>Positive integers are used for private sources, negative integers 
 * are used for shared sources, and zero is used for the virtual source
 * containing special items. 
 * </p>
 * <p>There is a maximum and a minimum value set for source numbers.
 * All integers outside this range are reserved to be allocated uses 
 * at a future time.
 * </p>
 * <p>Within a source, each item is identified by a unique reference that
 * is a long integer, and each triple  is identified by a unique reference
 * that is a long integer. These are the numeric identifiers of the items
 * and triples within their sources. (The identifier spaces are  separate.
 * An item and a triple can have the same numeric identifier.)
 * </p>
 */

public abstract class SourceConnector implements Serializable {	
	
	private static final long serialVersionUID = 634014974769761856L;
	
	/*
	 ******************************************************************************
	 *
	 *          MAIN CLASS DEFINITIONS
	 *          
	 ******************************************************************************
	 */
	
	/**
	 * The maximum allowed domain source number
	 */
	
	private final static long maxDomainSourceNr = 100;

	/**
	 * The minimum allowed domain source number
	 */
	
	private final static long minDomainSourceNr = 2;

	/**
	 * The numeric identifier of the source item.
	 * Each source contains a source item, and they
	 * all have the same numeric identifier. The source
	 * item is  locked whenever changes are made to
	 * named items in the source.
	 */
	
	public final static long sourceItemNr = Long.MIN_VALUE ;
	
	/**
	 * The numeric identifier of the item that represents
	 * the administration access level of the source.
	 * Each source has an administration access level,
	 * and they are all represented by items with the
	 * same numeric identifier. This item is the subject
	 * and object of an access level triple. When the
	 * source has been created, a user who has access
	 * at its administration level can perform various
	 * administration functions.
	 */
	
	public final static long adminLevelItemNr = Long.MIN_VALUE +1;
	
	/**
	 * The numeric identifier of the triple that defines
	 * the administration access level of the source.
	 */
	
	public final static long adminLevelTripleNr = Long.MIN_VALUE;
	
	/**
	 * The triple store or other client to which this 
	 * connector connects the source
	 */
	
	protected SourceConnectorClient client = null;
	
	/**
	 * The numeric identifier of the source that the connector is 
	 * connected to
	 */
	
	protected long sourceNr;
	
	/**
	 * Whether the connector is connected to the source as owner
	 */
	
	protected boolean owner = false;
	
	/**
	 * The directory that holds or caches source data
	 */
	
	protected File localDir;
	
	/**
	 * A file specifying the source's configuration
	 */
	
	protected File specFile;
	
	/**
	 * The logger to which events are logged. Can be null.
	 */
	
	protected Logger logger;
	
	/**
	 * Whether the source connector is currently operational
	 */
	
	private boolean isOperational = false;
	
	/**
	 * Determine whether a source number is that of an 
	 * installation-specific source
	 * 
	 * @param nr a source number
	 * 
	 * @return whether the source number is that of an 
	 * installation-specific source
	 */
	
	public static boolean isInstallationSourceNr(long nr) {
		
		return (nr == 1) || (nr == -1);
	}
	
	/**
	 * Determine whether a source number is that of a 
	 * domain source
	 * 
	 * @param nr a source number
	 * 
	 * @return whether the source number is that of a 
	 * domain source
	 */
	
	public static boolean isDomainSourceNr(long nr) {
		
		return (nr >= minDomainSourceNr) && (nr <= maxDomainSourceNr);
	}
	
	/**
	 * Determine whether a source number is that of a 
	 * registered source
	 * 
	 * @param nr a source number
	 * 
	 * @return whether the source number is that of a 
	 * registered source
	 * 
	 * @throws IOException 
	 */
	
	@SuppressWarnings("unchecked")
	public static boolean isRegisteredSourceNr(long nr) {
		
		try {
			URL url = new URL("https://lacibus.net/home/.api?command=vdl_exec"); 
			HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json; utf-8");
			con.setRequestProperty("Accept", "application/json");
			con.setDoOutput(true);
			con.connect();
			OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
			try {
				JSONArray cmds = new JSONArray();
				JSONObject cmd = new JSONObject();
				cmd.put("command", "get unique subject with non-item object");
				cmd.put("verb", "3:Source Has Number");
				cmd.put("object", nr);
				cmd.put("handle", "id");
				cmds.add(cmd);
				writer.write(cmds.toJSONString());
			} finally {
				writer.close();
			}
			int respCode = con.getResponseCode();
			if (respCode != 200) return false;
			BufferedReader br = new BufferedReader(
				  new InputStreamReader(con.getInputStream(), "utf-8"));
			StringBuilder response = new StringBuilder();
			try {
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) 
					response.append(responseLine.trim());
			} finally {
				br.close();
			}
			JSONArray result = (JSONArray) new JSONParser().parse(response.toString());
			for (Object o : result) {
				JSONObject jo = (JSONObject)(o);
				if ("id".equals(jo.get("name"))) return true;
			}
		} catch (Exception e) {}
		return false;
	}
	
	/**
	 * Create a source connector
	 * 
	 * @param client the triple store or other client to which this 
	 * connector connects the source
	 * 
	 * @param sourceNr the numeric identifier of the source that the
	 * connector is connected to
	 * 
	 * @param owner whether the connector connects to the source as owner
	 * 
	 * @param localDir the directory that holds or caches source data
	 * 
	 * @param specFile a file specifying the source's configuration
	 * 
	 * @throws SourceConnectorException 
	 */
	
	protected SourceConnector(
			SourceConnectorClient client, long sourceNr, boolean owner,
			File localDir, File specFile, Logger logger)
					throws SourceConnectorException {
		
		this.client = client;
		this.sourceNr = sourceNr;
		this.owner = owner;
		this.localDir = localDir;
		this.specFile = specFile;
		this.logger = logger;
	}
	
	/**
	 * @return the client of the source connector
	 */
	
	public SourceConnectorClient getClient() {
		
		return client;
	}
	
	/**
	 * @return the numeric identifier of the source that the
	 * connector is connected to
	 */
	
	public long getSourceNr() {
		
		return sourceNr;
	}
	
	/** 
	 * @return whether the source connector is connecting the source's owner
	 * to the source
	 */
	
	public boolean isOwner() {
		
		return owner;
	}
	
	public File getLocalDir() {
		
		return localDir;
	}
	
	public File getSpecFile() {
		
		return specFile;
	}
	
	public String getSpecFilePath() throws IOException {
		
		if (specFile == null) return null;
		else return specFile.getCanonicalPath();
	}
	
	/**
	 * @return whether the source connector is operational
	 */
	
	public boolean isOperational() {
		
		return isOperational;
	}
	
	/**
	 * Make the connector operational
	 */
	
	public void makeOperational() {
		
		isOperational = true;
	}
	
	/**
	 * Determine whether the source exists or must be created. 
	 * If this method returns true, then the caller should assume
	 * that the source has been created. If it returns false, then 
	 * the caller should create it.
	 * 
	 * @return true if the source exists, or creation of the source 
	 * has at least been started.
	 * 
	 * @throws SourceConnectorException
	 */
	
	public abstract boolean sourceExists() throws SourceConnectorException;
	
	/**
	 * <p>Create the source. 
	 * </p>
	 * <p>A new source is created. 
	 * </p>
	 * <p>A source must be created, or loaded, but not both, before
	 * it can be used.
	 * </p>
	 * <p>A source is created with a source item and a source
	 * administration access level. The only-read level of
	 * the source item is the lowest access level, and its
	 * read-or-write level is the highest access level.The
	 * only-read level of the source administration access
	 * level is the lowest access level, and it is its own 
	 * read-or-write level. 
	 * </p>
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public abstract void create()
			throws 	SourceConnectorException,
					TripleStoreNonFatalException;
	
	/**
	 * <p>Load items and triples into a triple store from the connector,
	 * but without adding the items to the triple store's index.
	 * </p>
	 * <p>Invoked by the loadFromSource() method to apply a set of deltas 
	 * that add items and triples in the source to the invoking triple store. 
	 * This part of the load process is specific to the source. 
	 * The subsequent part, in which the loaded items are indexed, is handled 
	 * by the loadFromSource() method.
	 * </p> 
	 * <p>Note that the deltas may contain deletions as well as 
	 * additions, and  must be applied in order. This implies
	 * that the connector must apply deltas adding the items 
	 * referenced by a triple before applying the delta that 
	 * adds the triple. However, triples setting access level
	 * superiority permissions can be loaded before the triples
	 * defining the access levels concerned.
	 * </p>
	 * <p>A source must be created, or loaded, but not both, before
	 * it can be used.
	 * </p>
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * 
	 */
	
	public abstract void load()
			throws SourceConnectorException, TripleStoreNonFatalException;
	
	/**
	 * <p>Start a new transaction if the source connector is operational.
	 * </p>
	 * <p>Every write operation is associated with a transaction. 
	 * It is not guaranteed that all writes for a transaction will be
	 * performed, or none. What is guaranteed is that, if there is a 
	 * system failure, only records resulting from completed transactions
	 * will be re-loaded when the system re-starts.
	 * </p>
	 * <p>A transaction is associated with an item, which must be from 
	 * the source that this connector connects to, and must have been 
	 * locked before the transaction is started. The only write operations
	 * associated with the transaction are write operations on the item, 
	 * or on triples of which it is the subject.
	 * <p>
	 * @return a newly-started transaction, or null if a transaction could 
	 * not be started/
	 * 
	 * @throws SourceConnectorException 
	 */
	
	public Transaction startSourceTransaction() 
					throws 	SourceConnectorException {
		
		if (isOperational) return startTransaction();
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * <p>Start a new transaction.
	 * </p>
	 * <p>Every write operation is associated with a transaction. 
	 * It is not guaranteed that all writes for a transaction will be
	 * performed, or none. What is guaranteed is that, if there is a 
	 * system failure, only records resulting from completed transactions
	 * will be re-loaded when the system re-starts.
	 * </p>
	 * <p>A transaction is associated with an item, which must be from 
	 * the source that this connector connects to, and must have been 
	 * locked before the transaction is started. The only write operations
	 * associated with the transaction are write operations on the item, 
	 * or on triples of which it is the subject.
	 * <p>
	 * @return a newly-started transaction, or null if a transaction could 
	 * not be started/
	 * 
	 * @throws SourceConnectorException 
	 */
	
	public abstract Transaction startTransaction() 
					throws 	SourceConnectorException;
	
	/**
	 * End a transaction if the source connector is operational.
	 * 
	 * @param trans a transaction to be ended
	 * 
	 * @throws SourceConnectorException
	 */
	
	public void endSourceTransaction(Transaction trans)
					throws 	SourceConnectorException {
		
		if (isOperational) endTransaction(trans);
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * End a transaction.
	 * 
	 * @param trans a transaction to be ended
	 * 
	 * @throws SourceConnectorException
	 */
	
	public abstract void endTransaction(Transaction trans)
					throws 	SourceConnectorException;
	
	/**
	 * Add an item if the source connector is operational.
	 * 
	 * @param onlyReadLevelId the identifier of the item representing
	 * the only-read access level that the item is to have
	 * 
	 * @param readOrWriteLevelId the identifier of the item representing 
	 * the read-or-write access level that the item is to have
	 * 
	 * @return a numeric item identifier and transaction. The identifier
	 * is a newly-assigned numeric identifier that identifies a new item
	 * that has been added to the source. The transaction is a newly 
	 * started transaction whose first operation is the addition of the item.
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public IdAndTrans addSourceItem(Id onlyReadLevelId, Id readOrWriteLevelId)
				throws 	SourceConnectorException {
		
		if (isOperational) return addItem(onlyReadLevelId, readOrWriteLevelId);
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * Add an item
	 * 
	 * @param onlyReadLevelId the identifier of the item representing
	 * the only-read access level that the item is to have
	 * 
	 * @param readOrWriteLevelId the identifier of the item representing 
	 * the read-or-write access level that the item is to have
	 * 
	 * @return a numeric item identifier and transaction. The identifier
	 * is a newly-assigned numeric identifier that identifies a new item
	 * that has been added to the source. The transaction is a newly 
	 * started transaction whose first operation is the addition of the item.
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public abstract IdAndTrans addItem(Id onlyReadLevelId, Id readOrWriteLevelId)
				throws 	SourceConnectorException;
	
	/**
	 * Numeric item identifier and transaction returned when an item is added
	 */
	
	public class IdAndTrans {
		
		/**
		 * The numeric identifier of an item in its source
		 */
		
		public long idNr;
		
		/**
		 * A newly started transaction whose first operation is the 
		 * addition of the item
		 */
		
		public Transaction trans;
		
		/**
		 * Create a numeric item identifier and transaction
		 * 
		 * @param idNr the numeric identifier of the item in its source
		 * 
		 * @param trans a newly started transaction whose first operation is 
		 * the addition of the item
		 */
		
		public IdAndTrans(long idNr, Transaction trans) {
			
			this.idNr = idNr;
			this.trans = trans;
		}
	}
	
	/**
	 * Update an item in the source if the source connector is operational.
	 * 
	 * @param itemNr the numeric identifier of the item in the source
	 * 
	 * @param onlyReadLevelId the identifier of the item representing
	 * the only-read access level that the item is to have
	 * 
	 * @param readOrWriteLevelId the identifier of the item representing 
	 * the read-or-write access level that the item is to have
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreNonFatalException 
	 */
	
	public void updateSourceItem(
			long itemNr,
			Id onlyReadLevelId,
   			Id readOrWriteLevelId,
	   		Transaction trans)
			   		throws 	SourceConnectorException, TripleStoreNonFatalException {
		
		if (isOperational) updateItem(
				itemNr, onlyReadLevelId, readOrWriteLevelId, trans);
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * Update an item in the source
	 * 
	 * @param itemNr the numeric identifier of the item in the source
	 * 
	 * @param onlyReadLevelId the identifier of the item representing
	 * the only-read access level that the item is to have
	 * 
	 * @param readOrWriteLevelId the identifier of the item representing 
	 * the read-or-write access level that the item is to have
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreNonFatalException 
	 */
	
	public abstract void updateItem(
			long itemNr,
			Id onlyReadLevelId,
   			Id readOrWriteLevelId,
	   		Transaction trans)
			   		throws 	SourceConnectorException, TripleStoreNonFatalException;
	
	/**
	 * Remove an item from the source if the source connector is operational.
	 * 
	 * @param itemNr the numeric identifier of the item in the source
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreNonFatalException 
	 */
	
	public void removeSourceItem(
			long itemNr, Transaction trans)
			   		throws 	SourceConnectorException, TripleStoreNonFatalException {
		
		
		if (isOperational) removeItem(itemNr, trans);
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * Remove an item from the source
	 * 
	 * @param itemNr the numeric identifier of the item in the source
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreNonFatalException 
	 */
	
	public abstract void removeItem(
			long itemNr, Transaction trans)
			   		throws 	SourceConnectorException, TripleStoreNonFatalException;
	
	/**
	 * Create a triple if the source connector is operational.
	 * 
	 * @param subject the triple's subject
	 * 
	 * @param verb the triple's verb
	 * 
	 * @param object the triple's object
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @return the newly-created triple
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public StoredTriple createSourceTriple( 
			Item subject,
			Item verb,
			Datum object,
			Transaction trans)
					throws 	SourceConnectorException, 
							TripleStoreNonFatalException {
		
		if (isOperational) return createTriple(subject, verb, object, trans);
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * Create a triple
	 * 
	 * @param subject the triple's subject
	 * 
	 * @param verb the triple's verb
	 * 
	 * @param object the triple's object
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @return the newly-created triple
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public abstract StoredTriple createTriple( 
			Item subject,
			Item verb,
			Datum object,
			Transaction trans)
					throws 	SourceConnectorException, 
							TripleStoreNonFatalException;
	
	/**
	 * Replace a triple in the source by a new triple with the
	 * same subject and verb, but with a new object and a new 
	 * numeric identifier, if the source connector is operational.
	 * 
	 * @param triple the triple to be replaced
	 * 
	 * @param newObject the triple's new object
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @return a new triple, with the previous subject and
	 * verb, the new object, and a new numeric identifier,
	 * or null if the triple can not be found
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public StoredTriple replaceSourceTriple(
			Triple triple, Datum newObject, Transaction trans)
					throws 	SourceConnectorException, 
							TripleStoreNonFatalException {
		
		if (isOperational) return replaceTriple(triple, newObject, trans);
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * Replace a triple in the source by a new triple with the
	 * same subject and verb, but with a new object and a new 
	 * numeric identifier
	 * 
	 * @param triple the triple to be replaced
	 * 
	 * @param newObject the triple's new object
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @return a new triple, with the previous subject and
	 * verb, the new object, and a new numeric identifier,
	 * or null if the triple can not be found
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public abstract StoredTriple replaceTriple(
			Triple triple, Datum newObject, Transaction trans)
					throws 	SourceConnectorException, 
							TripleStoreNonFatalException;
	
	/**
	 * Get a stored triple's text object if the source connector is operational.
	 * 
	 * @param tripleNr the numeric identifier in its source of a stored
	 * triple
	 * 
	 * @return the triple's object
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreNonFatalException 
	 */
	
	public Datum getSourceTextTripleObject(long tripleNr)
					throws 	SourceConnectorException, TripleStoreNonFatalException{
		
		if (isOperational) return getTextTripleObject(tripleNr);
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * Get a stored triple's text object
	 * 
	 * @param tripleNr the numeric identifier in its source of a stored
	 * triple
	 * 
	 * @return the triple's object
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreNonFatalException 
	 */
	
	public abstract Datum getTextTripleObject(long tripleNr)
					throws 	SourceConnectorException, TripleStoreNonFatalException;
	
	/**
	 * Get a stored triple's binary object if the source connector is operational.
	 * 
	 * @param tripleNr the numeric identifier in its source of a stored
	 * triple
	 * 
	 * @return the triple's object
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreNonFatalException 
	 */
	
	public Datum getSourceBinaryTripleObject(long tripleNr)
					throws 	SourceConnectorException, TripleStoreNonFatalException {
		
		if (isOperational) return getBinaryTripleObject(tripleNr);
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * Get a stored triple's binary object
	 * 
	 * @param tripleNr the numeric identifier in its source of a stored
	 * triple
	 * 
	 * @return the triple's object
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreNonFatalException 
	 */
	
	public abstract Datum getBinaryTripleObject(long tripleNr)
					throws 	SourceConnectorException, TripleStoreNonFatalException;
	
	/**
	 * Note that a triple is removed from the source if the source 
	 * connector is operational. The triple's underlying resources 
	 * (for example, a file containing its object) are not deleted 
	 * until the end of the transaction, so that the triple can be 
	 * reinstated if the transaction does not complete.
	 * 
	 * @param tripleNr the numeric identifier within the source of the
	 * triple to be removed
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @throws SourceConnectorException 
	 */
	
	public void removeSourceTriple(long tripleNr, Transaction trans) 
					throws 	SourceConnectorException {
		
		if (isOperational) removeTriple(tripleNr, trans);
		else throw (new SourceConnectorStateException(sourceNr, 
				"Source connector not operational"));
	}
	
	/**
	 * Note that a triple is removed from the source. The triple's
	 * underlying resources (for example, a file containing its object)
	 * are not deleted until the end of the transaction, so that the
	 * triple can be reinstated if the transaction does not complete.
	 * 
	 * @param tripleNr the numeric identifier within the source of the
	 * triple to be removed
	 * 
	 * @param trans the transaction of which this operation is a part
	 * 
	 * @throws SourceConnectorException 
	 */
	
	public abstract void removeTriple(long tripleNr, Transaction trans) 
					throws 	SourceConnectorException;
	
	
	/**
	 * Stop operation of the source connector
	 * 
	 * @throws SourceConnectorException
	 */
	
	public void stopOperation() 	throws 	SourceConnectorException {
		
		synchronized(this) {
			if (isOperational) isOperational = false;
			else throw (new SourceConnectorStateException(sourceNr, 
					"Source connector not operational"));
		}
	}
	
	/**
	 * Close the source connector
	 */
	
	public abstract void close();
	
	/**
	 * <p>Return an exception that caused connector operation to abort.
	 * </p>
	 * <p>If an exception is thrown in the course of background
	 * processing then operation of the connector is aborted
	 * and the exception is stored.
	 * </p>
	 * @return the Exception that aborted operation
	 */
	
	public abstract Exception getTerminator();
	
	/**
	 * Simulate an I/O error for testing purposes
	 */
	
	public abstract void simulateIoError();
	
	/**
	 * Create a log entry
	 * 
	 * @param message a message to be logged
	 */
	
	public void log(String message) {
		
		logger.log(message);
	}
}
