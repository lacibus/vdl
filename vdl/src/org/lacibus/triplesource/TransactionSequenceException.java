/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;

/**
 * An exception that is thrown when an operation
 * is attempted on a transaction that is out of
 * sequence.
 */

public class TransactionSequenceException
				extends Exception implements Serializable {

	private static final long serialVersionUID = -8121431074873355313L;

	/**
	 * Create a transaction sequence exception
	 * 
	 * @param s a description of the reason for throwing the exception
	 */
	
	public TransactionSequenceException(String s) {
		
		super(s);
	}
}
