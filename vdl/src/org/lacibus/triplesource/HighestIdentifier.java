/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;

/**
 * An instance of this class contains a long value and has synchronized
 * methods to update and retrieve it.
 */

public class HighestIdentifier implements Serializable {
	
	private static final long serialVersionUID = -1875932906650052863L;
	
	/**
	 * The value - initially set to the smallest long integer
	 */
	
	private long value = Long.MIN_VALUE;
	
	/**
	 * Set the value to the given value if this is higher than the
	 * current value.
	 * 
	 * Over-riding methods can throw exceptions
	 * 
	 * @param newValue the value to be set if it is higher than the
	 * current value
	 * 
	 * @throws SourceConnectorIOException 
	 * @throws SourceConnectorStateException 
	 */
	
	public synchronized void setIfHigher(long newValue) 
			throws SourceConnectorStateException, SourceConnectorIOException {
		
		if (newValue  > value) value = newValue;
	}
	
	/**
	 * @return the current value
	 */
	
	public synchronized long get() {
		return value;
	}
	
	/**
	 * Increment the value
	 * 
	 * Over-riding methods can throw exceptions
	 * 
	 * @return the incremented value
	 * 
	 * @throws SourceConnectorIOException 
	 * @throws SourceConnectorStateException 
	 */
	
	public synchronized long next() 
			throws SourceConnectorStateException, SourceConnectorIOException {
		
		value++;
		return value;
	}
}

