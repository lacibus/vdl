/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * An update to be applied to a triple store, consisting
 * of a list of deltas to be applied in order and an
 * indication of what completed transactions will have
 * been effected when the deltas have been applied. This
 * bulk update is a base for further incremental updates.
 */

public class Base implements Serializable {

	private static final long serialVersionUID = -4225617729933325207L;
	 
	 /**
	  * A record of the completed transactions that will have
	  * been effected when the update has been applied
	  */
	 
	TransactionsStatus status;
	
	/**
	 * The changes that comprise the update
	 */
	
	List<Delta> deltas;
	
	/**
	 * Create a base update with no changes
	 * 
	 * @param highestCompleted the sequence number of the highest completed
	 * transaction before the update is applied. (This will also be the 
	 * sequence number of the highest completed transaction after the update
	 * is applied, as the update contains no changes.)
	 */
	
	public Base(long highestCompleted) {
		
		deltas = new ArrayList<Delta>();
		status = new TransactionsStatus(highestCompleted);
	}
	
	/**
	 * Add a delta
	 * 
	 * @param delta the delta to be added
	 */
	
	public void add(Delta delta) {
		
		deltas.add(delta);
	}
	
	/**
	 * Note that a transaction is completed
	 * 
	 * @param l the sequence number of a completed transaction
	 * 
	 * @throws TransactionSequenceException
	 */
	
	public void noteCompleted(long l) 
			throws TransactionSequenceException {
		
		status.noteCompleted(l);
	}
	
	/**
	 * @return an indication of what completed transactions
	 * will have been effected when the deltas have been
	 * applied
	 */
	
	public TransactionsStatus getStatus() {
		
		return status;
	}
	
	/**
	 * @return a list of deltas to be applied in order
	 */
	
	public List<Delta> getDeltas() {
		
		return deltas;
	}
}
