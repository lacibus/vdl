/**
 * <p>This package defines the interface between a triple store and source connectors.
 * </p>
 * <p>It includes an abstract source connector class, classes describing changes
 * to the data, and the source connector client interface.
 * </p>
 **/

package org.lacibus.triplesource;
