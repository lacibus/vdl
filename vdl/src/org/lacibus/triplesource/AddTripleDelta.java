/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;

/**
 * A record of a change to add a triple to a source or store 
 * of items and triples. The object of the triple is not included
 * in this class, but is included in concrete subclasses.
 */

public abstract class AddTripleDelta extends Delta implements Serializable {

	private static final long serialVersionUID = 733321835497348709L;

	/**
	  * The numeric identifier of the triple in its source
	  */

	 public long tripleNr;

	 /**
	  * The numeric identifier of the subject in its source
	  */

	 public long subjectItemNr;

	/**
	  * The numeric identifier of the source of the verb
	  */
	 
	 public long verbSourceNr;
	 
	 /**
	  * The numeric identifier of the verb in its source
	  */

	 public long verbItemNr;

		/**
		 * Create a record of a change that adds a triple to a source
		 * 
		 * @param tripleNr the numeric identifier in the source of the
		 * triple to be added.
		 * 
		 * @param subjectItemNr the numeric identifier in the source of the
		 * subject of the triple to be added.
		 * 
		 * @param verbSourceNr the numeric identifier of the source of the
		 * verb of the triple to be added.
		 * 
		 * @param verbItemNr the numeric identifier in its source of the
		 * verb of the triple to be added.
		 */
		
	 public AddTripleDelta(
			 long tripleNr,
			 long subjectItemNr, 
			 long verbSourceNr, 
			 long verbItemNr) {
		 
		 this.tripleNr= tripleNr;
		 this.subjectItemNr = subjectItemNr;
		 this.verbSourceNr = verbSourceNr;
		 this.verbItemNr = verbItemNr;
	 }
}
