/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;

import org.lacibus.triplestore.Datum;
import org.lacibus.triplestore.Item;

/**
 * An non-item triple stored in a triple store
 */

public class StoredNonItemTriple extends StoredTriple implements Serializable {

	private static final long serialVersionUID = 6864832722222044908L;
	
	/**
	 * The type of the triple's object
	 */
	
	public Datum.Type objectType;
	
	/**
	 * The summary value of the triple's object
	 */
	
	public long objectSummaryValue;
	
	/**
	 * Construct a stored non-item triple
	 * 
	 * @param tripleNr the triple's numeric identifier
	 * 
	 * @param subject the subject item
	 * 
	 * @param verb the verb item
	 * 
	 * @param objectType the object type
	 * 
	 * @param objectSummaryValue the object summary value
	 */
	
	public StoredNonItemTriple(
			long tripleNr,
			Item subject,
			Item verb, 
			Datum.Type objectType,
			long objectSummaryValue) {
		
		this.tripleNr = tripleNr;
		this.subject = subject;
		this.verb = verb;
		this.objectType = objectType;
		this.objectSummaryValue = objectSummaryValue;
	}
}
