/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;

import org.lacibus.triplestore.Item;

/**
 * A triple stored in a triple store. This class only has 
 * the subject and verb. Concrete subclasses are defined 
 * to contain the object.
 */

public abstract class StoredTriple implements Serializable {
	
	private static final long serialVersionUID = 8621012235720648251L;
	
	/**
	 * The numeric identifier of the triple in its source
	 */
	
	public long tripleNr;
	
	/**
	 * The subject of the triple
	 */
	
	public Item subject;
	
	/**
	 * The verb of the triple
	 */
	
	public Item verb;
}
