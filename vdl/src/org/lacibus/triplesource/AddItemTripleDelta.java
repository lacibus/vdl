/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplesource;

import java.io.Serializable;

import org.lacibus.triplestore.Id;

/**
 * A record of a change to add an item triple to a source or store 
 * of items and triples
 */

public class AddItemTripleDelta extends AddTripleDelta implements Serializable {

	private static final long serialVersionUID = -3995969555139802895L;
	
	/**
	 * The identifier of the item that is the object of the item triple
	 */
	
	private Id objectId;

	/**
	 * Create a record of a change that adds an item triple
	 * 
	 * @param tripleNr the numeric identifier in the source of the
	 * triple to be added.
	 * 
	 * @param subjectItemNr the numeric identifier in the source of the
	 * subject of the triple to be added.
	 * 
	 * @param verbSourceNr the numeric identifier of the source of the
	 * verb of the triple to be added.
	 * 
	 * @param verbItemNr the numeric identifier in its source of the
	 * verb of the triple to be added.
	 * 
	 * @param objectId the identifier of the item that is the object of 
	 * the item triple
	 */
	
	public AddItemTripleDelta(
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr, 
			long verbItemNr,
			Id objectId) {
		
		super(tripleNr, subjectItemNr, verbSourceNr, verbItemNr);
		this.objectId = objectId;
	}

	/**
	 * Get the identifier of the item that is the object of 
	 * the item triple
	 * 
	 * @return the identifier of the item that is the object of 
	 * the item triple
	 */
	
	public Id getObjectId() {
		
		return objectId;
	}
}
