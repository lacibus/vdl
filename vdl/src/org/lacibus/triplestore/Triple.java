/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import org.lacibus.triplesource.SourceConnectorException;

/**
 * <p>A triple consists of a subject that is an item,
 * a verb that is also an item, and an object that
 * is a Datum. A Datum may be an item or a data value
 * that is a boolean, an integer, a real number, a
 * piece of text, or a sequence of bytes.
 * </p>
 * <p>A triple is held in the same source as its subject,
 * and has its own numeric identifier in that source.
 * </p>
 * <p>Access to a triple is determined by the access levels
 * of its subject, its verb, and its object if that is
 * an item.
 * </p>
 * <p>Objects that are pieces of text or sequences of bytes
 * can be of arbitrary size. Non-item triples contain
 * summaries of their objects, rather than the objects 
 * themselves. Searches in the triple store first find
 * triples with objects whose summaries match the criteria.
 * These objects are then retrieved from the sources to
 * determine whether they match. Objects whose summaries
 * do not meet the criteria are not retrieved.
 * </p>
 * <p>For triples with objects that are booleans, integers, or 
 * real numbers, the value of the object can be determined
 * from its summary. For triples with objects that are items,
 * the object is stored in the triple. It is only text and
 * binary objects that need to be retrieved from the sources.
 * </p>
 */

public abstract class Triple implements Comparable<Triple> {
	
	/**
	 * The numeric identifier of the triple in its source
	 */
	
	protected long tripleNr;
	
	/**
	 * The subject of the triple
	 */
	
	protected Item subject;
	
	/**
	 * The verb of the triple
	 */
	
	protected Item verb;
	
	/**
	 * The type of the object of the triple
	 */
	
	protected Datum.Type objectType;
	
	/**
	 * The triple store in which the triple is being
	 * processed
	 */
	
	protected TripleStore store = null;
	

	/**
	 * Construct a triple with a given numeric identifier,
	 * subject, verb, and object type
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param subject the subject item
	 * 
	 * @param verb the verb item
	 * 
	 * @param objectType the object type
	 * 
	 * @param store the triple store that will contain the triple
	 */
	
	protected Triple(
			long tripleNr,
			Item subject,
			Item verb, 
			Datum.Type objectType, 
			TripleStore store) {
		
		this.tripleNr = tripleNr;
		this.subject = subject;
		this.verb = verb;
		this.objectType = objectType;
		this.store = store;
	}
	
	/**
	 * Construct a triple taking the numeric identifier of the triple,
	 * subject, verb, object type and store from another triple
	 * 
	 * @param t the triple to be copied
	 */
	
	protected Triple(Triple t) {
		
		this.tripleNr = t.tripleNr;
		this.subject = t.subject;
		this.verb = t.verb;
		this.objectType = t.objectType;
		this.store = t.store;
	}
	
	/**
	 * Get the numeric identifier of triple's source
	 * 
	 * @return the numeric identifier of the triple's source
	 */
	
	public abstract long getSourceNr() ;
	
	/**
	 * Get the numeric identifier of the triple in its source
	 * 
	 * @return the numeric identifier of the triple in its source
	 */
	
	public long getTripleNr() {
		return tripleNr;
	}
	
	/**
	 * Get the triple's identifier
	 * 
	 * @return the triple's identifier
	 */
	
	public Id getId() {
		return new Id(getSourceNr(), tripleNr);
	}
	
	/**
	 * Get the triple's subject identifier
	 * 
	 * @return the triple's subject identifier
	 */
	
	public Id getSubjectId() {
		return subject.getId();
	}
	
	/**
	 * Get the triple's subject
	 * 
	 * @return the triple's subject
	 */
	
	public Item getSubject() {
		return subject;
	}
	
	/**
	 * Get the triple's verb identifier
	 * 
	 * @return the triple's verb identifier
	 */
	
	public Id getVerbId() {
		return verb.getId();
	}
	
	/**
	 * Get the triple's verb
	 * 
	 * @return the triple's verb
	 */
	
	public Item getVerb() {
		return verb;
	}
	
	/**
	 * Get the triple's object type
	 * 
	 * @return the triple's object type
	 */
	
	public Datum.Type getObjectType() {
		return objectType;
	}
	
	/**
	 * Get the object's summary value. 
	 * 
	 * @return the triple object's summary value
	 */
	
	public abstract long getObjectSummaryValue();
	
	/**
	 * Get the triple's object
	 * 
	 * @param session the session requesting the operation.
	 * It must have read access to the subject and verb,
	 * and to the object if this is an item.
	 * 
	 * @return the triple's object, or null if it cannot
	 * be returned
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * @throws NotInOperationException 
	 */
	
	public Datum getObject(StoreSession session) 
			throws 	TripleStoreNonFatalException,
					TripleStoreFatalException, 
					SourceConnectorException,
					NotInOperationException {
		
		return store.getTripleObject(this, session);
	}
	
	/**
	 * Determine the triple's state
	 * 
	 * @return EXISTS if the subject, verb and, if applicable
	 * object have state EXISTS, DELETED if any of them have
	 * state DELETED, MAYEXIST otherwise.
	 */
	
	abstract Item.State getState();
	
	public boolean equals(Object o) {
		
		Triple t = (Triple) o;
		return (	(getSourceNr() == t.getSourceNr()) &&
					(tripleNr == t.tripleNr) );
	}
	
	public int compareTo(Triple t) {
		
		long sn = getSourceNr();
		long tsn = t.getSourceNr();
		if (sn < tsn) return -1;
		if (sn > tsn) return 1;
		if (tripleNr < t.tripleNr) return -1;
		if (tripleNr > t.tripleNr) return 1;
		return 0;
	}
}
