/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;

import org.lacibus.triplesource.StoredNonItemTriple;
import org.lacibus.triplestore.Item.State;

/**
 * A triple whose object is not an item.
 */

class NonItemTriple extends Triple implements Serializable {

	private static final long serialVersionUID = 7730961543994868313L;
	
	/**
	 * The summary of the value of the triple's object
	 */
	
	protected long objectSummaryValue;
	
	/**
	 * Construct a non-item triple
	 * 
	 * @param tripleNr the triple's numeric identifier within its source
	 * 
	 * @param subject the subject item
	 * 
	 * @param verb the verb item
	 * 
	 * @param objectType the object type
	 * 
	 * @param objectSummaryValue the object summary value
	 * 
	 * @param store the triple store containing the triple
	 */
	
	NonItemTriple(
			long tripleNr,
			Item subject,
			Item verb, 
			Datum.Type objectType,
			long objectSummaryValue,
			TripleStore store) {
		
		super(tripleNr, subject, verb, objectType, store);
		this.objectSummaryValue = objectSummaryValue;
	}
	
	/**
	 * Construct a non-item triple from a stored non-item triple
	 * 
	 * @param storedNonItemTriple a stored non-item triple
	 * 
	 * @param store the triple store that will hold the triple
	 */
	
	NonItemTriple(
			StoredNonItemTriple storedNonItemTriple, 
			TripleStore store) {
		
		super(	storedNonItemTriple.tripleNr, 
				storedNonItemTriple.subject, 
				storedNonItemTriple.verb, 
				storedNonItemTriple.objectType,
				store);
		this.objectSummaryValue = storedNonItemTriple.objectSummaryValue;
	}
	
	/**
	 * Construct a non-item triple taking the numeric identifier,
	 * subject, verb, object type, and triple store from another 
	 * non-item triple
	 * 
	 * @param t the triple to be copied
	 */
	
	public NonItemTriple(NonItemTriple t) {
		super(t);
		this.objectSummaryValue = t.objectSummaryValue;
	}
	
	@Override
	public long getSourceNr() {

		return subject.sourceNr;
	}

	@Override
	public long getObjectSummaryValue() {
		return objectSummaryValue;
	}

	@Override
	State getState() {

		if (	(subject.getState() == Item.State.EXISTS) &&
				(verb.getState() == Item.State.EXISTS))
			return Item.State.EXISTS;
		if (	(subject.getState() == Item.State.DELETED) ||
				(verb.getState() == Item.State.DELETED))
			return Item.State.DELETED;
		return Item.State.MAYEXIST;
	}

	public String toString() {
		return ("Triple ID" + getSourceNr() + ':' + tripleNr + " " +
				getSubjectId() + "; " + getVerbId() + "; " +
				objectType + " - " +
				getObjectSummaryValue());
	}	
}
