/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;

/**
 * <p>An access level determines which users can access
 * what data.
 * </p>
 * <p>Each item has two access levels, which determine which 
 * users can read, update, or delete it. They are the item's 
 * only-read level and its read-or-write level.
 * </p>
 * <p>Each store session has an access level. It can read
 * an item if its access level is the same as or superior
 * to the item's only-read level or the item's read-or-write
 * level. It can update or delete the item if its access
 * level is the same as or superior to the item's read-or-write
 * level. If its level has been replaced then an exception is
 * thrown when it attempts to create, read, update or delete
 * an item.
 * </p>
 * <p>The rationale for having an only-read level and a
 * read-or-write level, as opposed to simply having a read
 * level and a write level, is that:</p>
 * <ul>
 *  <li>Unpredictable results could arise if a session
 *      could update or delete an item that it could not
 *      read</li>
 *  <li>A session must therefore always be able to read
 *      an item that it can update or delete</li>
 *  <li>Relationships between levels can change over time.
 *      New relationships can be introduced and existing
 *      relationships can be removed. Also, it is possible
 *      to retire an access level and replace it by
 *      another level.</li>
 *  <li>In these circumstances, it would not be possible to
 *      guarantee that an item's write level remains
 *      superior to its read level.</li>
 * </ul>
 * <p>Access levels also determine the circumstances in which
 * users can read, update, or delete triples. A session can
 * read a triple if it can read the triple's subject and
 * verb and also its object if that is an item. A session
 * can update or delete a triple if it can write the triple's
 * subject, can read the the triple's verb and also can read 
 * the triple's object if that is an item.
 * </p>
 * <p>Each access level is represented by an item. The access
 * levels of that item control access to the access level
 * that it represents.
 * </p>
 * <p>There is a superiority relation between access levels
 * such that, if one access level is superior to another, 
 * anything that can be done at the inferior level can
 * also be done at the superior level. 
 * </p>
 * <p>This is a powerful feature that system managers can
 * use to control access to information. For example, an
 * access level could be defined to control access to a
 * company's pricing discount data. Another access level
 * could be defined for people with a sales role. Making this
 * level superior to the first one gives the sales team
 * access to the pricing discount data.
 * </p>
 * <p>The superiority relation is, in the mathematical sense, 
 * reflexive and transitive, but not symmetric. "Reflexive" 
 * means that an access level is superior to itself. 
 * "Transitive" means that if access level A is superior to B,
 * and B is superior to C, then A is superior to C. So, if
 * an access level is defined for people with a sales manager
 * role, and is made superior to the level for the sales role,
 * then sales managers will also have access to the pricing  
 * discount data. "Not symmetric" means that, if A is superior 
 * to B, then B is not necessarily superior to A. The sales 
 * managers can be given access to data that the sales team
 * cannot see.
 * </p>
 * <p>An organisation defines a number of access levels to control
 * access to its information, and states that some are superior
 * to others. Because the superiority relation is transitive,
 * a level can be superior to another without this being stated
 * explicitly (as, in the example, is the case for the sales 
 * manager level and the pricing discount access level).
 * </p>
 * <p>An access level defined by one organisation can be superior
 * to an access level defined by another. The company in the
 * example might appoint another company as a distributor of
 * its products, and make the distributor's sales role access
 * level superior to its pricing discount access level. (In such
 * a situation, the access levels would probably be in different
 * sources, with the distributor's sales role access level in a
 * source owned by the distributor, and the pricing discount
 * access level in a source owned by the producer.)
 * </p>
 * <p>With access levels in different organizations, the question 
 * arises of which organization should be able to make a level
 * superior to another. In the example above, we would expect
 * the producer to have control over whether the distributor's
 * sales role level should be superior to its pricing discount
 * level. In other cases, however, an organization might want
 * to control whether another organization's level is inferior
 * to one of its levels. For example, it might wish to prevent
 * its people from accessing information that it would then have
 * to pay for, or that would put it in breach of a commercial or
 * ethical policy. The principle is therefore adopted that both
 * organizations must give permission for a level to be superior
 * to another. Once both have done so, a stated relationship
 * between the two levels is created automatically. Either can
 * revoke its permission at any time, and the stated relationship
 * is then automatically removed.
 * </p>
 * <p>There are two built-in access levels:</p>
 * <ul>
 *   <li>The highest level, which is superior to every other level</li>
 *   <li>The lowest level, which is inferior to every other level</li>
 * </ul>
 */

public class AccessLevel implements Serializable, Comparable<AccessLevel> {
	
	private static final long serialVersionUID = 2873694202066114764L;

	/**
	 * The access level manager that manages the access level
	 */
	
	private AccessLevelManager manager;
	
	/**
	 * <p>The item representing the access level in store.
	 * </p>
	 * <p>If this is null then the access level is invalid.
	 * This will be the case after it has been replaced 
	 * by another access level and deleted.
	 * </p>
	 */
	
	Item levelItem = null;
	
	/**
	 * The replacement for this level if has been replaced
	 */
	
	AccessLevel replacement = null;
	
	/**
	 * Create a new access level
	 * 
	 * @param id the access level identifier
	 * 
	 * @param levelItem the item representing the access
	 * level.
	 * 
	 * @param manager the access level manager for the
	 * access level. The manager is used to determine which
	 * other levels are superior or inferior to this one.
	 */
	
	AccessLevel(Item levelItem, AccessLevelManager manager) {
		
		this.levelItem = levelItem;
		this.manager = manager;
	}
	
	/**
	 * Get the item that represents the access level.
	 * 
	 * @return  the item that represents the access level.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public Item getItem()
			throws TripleStoreNonFatalException {
		
		// Not synchronized - work with copy of levelItem
		Item it = levelItem;
		if (it == null)
			throw (new TripleStoreNonFatalException(
					"Invalid access level"));
		return it;
	}
	
	/**
	 * Get the identifier of the item that represents
	 * the access level
	 * 
	 * @return the identifier of the item that represents
	 * the access level
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public Id getItemId()
			throws TripleStoreNonFatalException {
		
		Item it = levelItem;
		if (it == null)
			throw (new TripleStoreNonFatalException(
					"Invalid access level"));
		return it.getId();
	}
	
	/**
	 * Get the identifier of the item that represents
	 * the access level or its ultimate replacement
	 * 
	 * @return the identifier of the item that represents
	 * the access level or its ultimate replacement
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public Id getEffectiveItemId()
			throws TripleStoreNonFatalException {
		
		AccessLevel level = this;
		do {
			AccessLevel repl = level.replacement;
			if (repl == null) break;
			level = repl;
		} while (true);
		return level.getItemId();
	}
	
	/**
	 * Set the replacement access level
	 * 
	 * @param replacement the replacement access level
	 * to be set
	 */
	
	synchronized void setReplacement(
			AccessLevel replacement) {
		
		this.replacement = replacement;
		levelItem = null;
	}
	
	/**
	 * Determine whether the access level is current, meaning that
	 * it has not been replaced and its item is current.
	 * 
	 * @return whether the access level is current
	 */
	
	public synchronized boolean isCurrent() {
		
		return (levelItem != null) && levelItem.isCurrent();
	}
	
	/**
	 * Determine whether a given access level  is
	 * superior to this one
	 * 
	 * @return true if the given access level  is
	 * superior, false otherwise
	 * 
	 * @throws TripleStoreNonFatalException if this or
	 * the given level is invalid
	 */
	
	public boolean hasSuperior(AccessLevel level)
					throws TripleStoreNonFatalException {
		
		return manager.isSuperiorTo(level, this);
	}
	
	/**
	 * Determine whether the access level can be read by
	 * a store session.
	 * 
	 * @param session a store session
	 * 
	 * @return whether the access level can be read by
	 * the store session.
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public boolean canBeReadBy(StoreSession session) 
			throws TripleStoreNonFatalException {
		
		if (levelItem == null)
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		return levelItem.canBeReadBy(session);
	}
	
	/**
	 * Determine whether the access level can be written
	 * by a store session.
	 * 
	 * @param session a store session
	 * 
	 * @return whether the access level can be written by
	 * the store session.
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public boolean canBeWrittenBy(StoreSession session) 
			throws TripleStoreNonFatalException {
		
		if (levelItem == null)
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		return levelItem.canBeWrittenBy(session);
	}
	
	/**
	 * Determine whether this access level was created by
	 * a particular triple store.
	 * 
	 * @param store a triple store
	 * 
	 * @return true if this access level was created by
	 * the given triple store, false otherwise.
	 * 
	 * @throws TripleStoreNonFatalExceptionException if the access level is
	 * invalid or if its level item has been deleted.
	 */
	
	boolean isOfTripleStore(TripleStore store) 
			throws TripleStoreNonFatalException {
		if ((levelItem == null) || 
			(levelItem.store == null))
			throw new TripleStoreNonFatalException(
					"Invalid access level");
		return (levelItem.store == store);
	}
	
	public boolean equals(Object o) {
		if (o == null) return false;
		AccessLevel l;
		try {
			l = (AccessLevel)o;
		}
		catch (Exception e) {
			return false;
		}
		return (compareTo(l) == 0);
	}
	
	public String toString() {
		if (this == manager.highest) return"Access level: highest";
		if (this == manager.lowest) return"Access level: lowest";
		if(levelItem == null) {
			AccessLevel repl = replacement;
			if (repl == null) return "Access level: invalid";
			else return "Access level: replaced by " + repl;
		}
		return "Access level " + levelItem.getId();
	}  

	@Override
	public int compareTo(AccessLevel other) {
		if (levelItem == null) {
			if (other.levelItem == null) return 0; 
			else return -1;
		}
		if (other.levelItem == null) return 1;
		return levelItem.compareTo(other.levelItem);
	}
	
	public int hashCode() {
		if (levelItem == null) return -1;
		return levelItem.hashCode();
	}
}
