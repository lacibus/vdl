/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;
import org.lacibus.triplesource.SourceConnector;
import org.lacibus.triplesource.SourceConnectorException;

/**
 * <p>The TripleStoreMaintainer class extends Thread. Each triple store
 * has one instance of it, which cycles through the items and triples 
 * in the store to performs regular maintenance actions.
 * </p>
 * <p>These are to:</p>
 * <ul>
 *    <li>Replace AccessLevelSpecs by AccessLevels if the specs
 *    reference items from connected sources.
 *    </li>
 *    <li>Conversely, replace AccessLevels whose level items
 *    are not from connected sources by AccessLevelSpecs 
 *    </li>
 *    <li>Replace the items' read or write access levels if the
 *    existing levels are marked for replacement.
 *    </li>
 *    <li>Remove items whose sources were disconnected and then
 *    re-connected, but were not loaded on re-connection. 
 *    Remove any access level relations of which they were
 *    the objects.
 *    </li>
 *    <li>Remove all locks that were set a long time ago on items,
 *    to ensure that items cannot be left permanently locked.
 *    </li>
 *    <li>Remove items that are not from connected sources and are
 *    not verbs or objects of any triples. Note that this will
 *    not remove items that are objects of access level
 *    relations.
 *    </li>
 *    <li>Remove triples whose subjects are not from connected
 *    sources.
 *    </li>
 *    <li>Remove any triple whose subject, verb, or object is an
 *    item that has been deleted or is not in the index, unless
 *    the triple specifies an access level replacement and its
 *    subject is from a connected source. If the source
 *    connector for the subject is the owner of the source,
 *    delete the triple.
 *    </li>
 * </ul>
 */

class TripleStoreMaintainer extends Thread implements Serializable {

	private static final long serialVersionUID = 8241161632399508585L;
	
	/**
	 * The triple store that this maintainer maintains
	 */
	
	private TripleStore store;
	
	/**
	 * The store session that performs maintenance actions
	 */
	
	private StoreSession maintenanceSession = null;

	/**
	 * The interval between maintenance actions. This is
	 * adjusted to maintain a balance between maintenance
	 * and normal operation.
	 */
	
	private int interval = 1;

	/**
	 * The maximum interval between maintenance actions. 
	 */
			
	private final int maxInterval = 5;
	
	/**
	 * The time in milliseconds after which a lock is considered
	 * old and will be removed
	 */

	final long oldLocksRemovalTime = 300000;	// 5 minutes

	/**
	 * The number of items in the store at the start of the
	 * current maintenance cycle.
	 */
	
	private int startNrOfItems;
	
	/**
	 * The number of triples in the store at the start of the
	 * current maintenance cycle.
	 */
	
	private int startNrOfTriples;
		
	/**
	 * The time in milliseconds that a step can take to execute
	 * before it is considered to be stuck. Should be longer 
	 * than the maximum time allowed to unlock an item.
	 */
	
	final long maxCycleStepTime = 600000;		// 10 minutes
	
	/**
	 * The time (milliseconds since the epoch) when the current
	 * maintenance cycle started
	 */
	
	private long startTime;
	
	/**
	 * The time (milliseconds since the epoch) such that locks set 
	 * before it should be removed
	 */
	
	private long oldLockTime;
	
	/**
	 * Create a triple store maintainer
	 * 
	 * @param store the triple store that the maintainer is to maintain
	 */
	
	TripleStoreMaintainer(TripleStore store) {
		
		this.store = store;
	}
	
	public void run() {
		
		try {
			do {
    			cycle();
			} while (true);
		} catch (Exception e) {
			// maintenance aborted	
			// Close the triple store
			store.log(e);
			store.log("Maintenance abort - store closing");
			store.abortTripleStoreException(
					"Triple store maintenance abort");
		}
	}
	
	private void cycle() 
			throws NotInOperationException, TripleStoreFatalException  {
		
		startTime = System.currentTimeMillis();
		int step = 0;
		CycleChecker checker = new CycleChecker(this);
		checker.start();
		try {
			// Get a maintenance session
			try {
				maintenanceSession = new StoreSession(
						store.accessLevelManager.highest);
			} catch (TripleStoreNonFatalException e1) {
				throw (new TripleStoreFatalException(
						"Cannot get maintenance session"));
			}
			
			// Maintain the items
			store.itemsIndex.clearSetConsideredForRemoval();
			oldLockTime = 
					System.currentTimeMillis() - oldLocksRemovalTime;			
			startNrOfItems = store.itemsIndex.nrOfItems();
			Item item = store.itemsIndex.getOldest();
			while (item != null) {	
				maintainItem(item, maintenanceSession);
				checker.stepCompleted(step++);
				sleepAsNeeded(step);
				item = store.itemsIndex.getNextYounger();
			}

			// Maintain the triples
			startNrOfTriples = store.triplesIndex.nrOfTriples();
			Triple triple = store.triplesIndex.getOldestTriple();
			while (triple != null) {	
				try {
					maintainTriple(triple, maintenanceSession);
				} catch (TripleStoreNonFatalException | SourceConnectorException e) {
					store.logger.log(e);
				}
				checker.stepCompleted(step++);
				sleepAsNeeded(step);
				triple = store.triplesIndex.getNextYoungerTriple();
			}
			// Remove items that are not from connected sources and
			// that are not verbs or objects
			store.itemsIndex.removeConsideredItems();
		} catch (InterruptedException e) {
			store.log(e);
		} finally {
			checker.interrupt();;
		}
	}

	/**
	 * Pause between maintenance steps. Each step is the maintenance
	 * of a single item or triple. The length of time paused depends
	 * on the rate at which steps are being performed and the number 
	 * of items and triples in the store.
	 * 
	 * @param steps the number of steps performed so far in the current
	 * maintenance cycle.
	 * 
	 * @throws InterruptedException
	 */

	private void sleepAsNeeded(int steps) throws InterruptedException {
		
	    if (interval > 0) sleep(interval);
	    if (interrupted()) throw(new InterruptedException());
	    int additions = 	store.itemsIndex.nrOfItems() - startNrOfItems +
	    					store.triplesIndex.nrOfTriples() - startNrOfTriples;
		long time = (System.currentTimeMillis() - startTime)/1000;
		if (time == 0) time = 1;
		float additionRate = (float)additions/(float)time;
		float processingRate = (float)steps/(float)time;
		if (processingRate < 2*additionRate) {
			// Not processing fast enough
			if (interval > 1) interval = interval/2;
			else interval = 0;
		}
		else if (processingRate > 4*additionRate) {
			// Processing too fast
			if (interval == 0) interval = 1;
			else if (interval < maxInterval) interval = interval*2;
		}		        		
	}
		
	private class CycleChecker extends Thread implements Serializable {

		private static final long serialVersionUID = 7193409718954148931L;
		
		TripleStoreMaintainer maintainer;
		
		int lastCompletedStep = -1;
		
		CycleChecker(TripleStoreMaintainer maintainer) {
			
			this.maintainer = maintainer;
		}
		
		void stepCompleted(int step) {
			
			lastCompletedStep = step;
		}
		
		public void run() {
			
			int step;
			try {
				do {
					step = lastCompletedStep;
					sleep(maxCycleStepTime);
					if (lastCompletedStep == step) {
	    				// The cycle is stuck
	    				StackTraceElement[] trace = maintainer.getStackTrace();
	    				String message = "Maintenance cycle stuck\n";
	    				for (StackTraceElement ste : trace) message = 
	    						message + "    " + ste.toString() + '\n';
	    				store.log(message);
	    				store.close();
	    				break;
					}
				} while (true);
			} catch (InterruptedException e) {}
		}
	}

	/**
	 * Maintain an item
	 * 
	 * @param item the item to be maintained
	 * 
	 * @param session the store session to perform maintenance actions
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreFatalException
	 */
	
	private void maintainItem(Item item, StoreSession session) 
			throws TripleStoreFatalException, NotInOperationException {

		// Do not maintain special items - no need
		if (item.sourceNr == TripleStore.specialItemSource) return;
		
		// Is the item from a known source?
		SourceConnector connector = 
				store.connectorsManager.getConnector(item.sourceNr);
		if (connector == null) {
			// The item is not from a known source.
			// Note that it may not exist
			item.noteMayNotExist();
			// Mark it for removal unless it is found to be 
			// a triple verb  or object or an item's access level
			store.itemsIndex.considerForRemoval(item);
		}
		else if (item.getState() == Item.State.EXISTS) {
			// The item is from a known source, and exists
			
			// Replace AccessLevelSpecs by AccessLevels, and
			// vice versa, and check whether an access level
			// has been replaced
			item.maintainLevels();
			
			// Note that its access level items are in use, even if
			// they are not from connected sources
			item.noteLevelsInUse();
				
			// Unlock the item if its source is owned by this triple
			// store and it has been locked for a long time
			if (connector.isOwner()) try {
					item.unlockIfOld(oldLockTime, session);
			} catch (TripleStoreNonFatalException e) {
			} catch (SourceConnectorException e) {
				// Log the error
				store.log("Error unlocking old item in source " + connector.getSourceNr() + 
				": " + e.getMessage());
				// Disconnect the source
				store.connectorsManager.removeConnector(e.getSource());
			}
		}
	}

	/**
	 * Maintain a triple
	 * 
	 * @param t the triple to be maintained
	 * 
	 * @param session the store session to perform maintenance actions
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws NotInOperationException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 */

	private void maintainTriple(Triple t, StoreSession session) 
			throws 	TripleStoreNonFatalException, 
					NotInOperationException, 
					TripleStoreFatalException,
					SourceConnectorException  {

		// If the subject, verb or object has been deleted,
		// and the connector is operational, remove the triple, 
		// unless it is an access level replacement triple
		Item subject = t.subject;
		SourceConnector connector = store.connectorsManager.getConnector(subject.sourceNr);
		if (	(t.getState() == Item.State.DELETED) &&				
				(store.connectorsManager.isOperational(t.subject.sourceNr))){
			if (	!t.verb.equals(store.isAccessLevelRelation) ||
					!t.objectType.equals(Datum.Type.ITEM) ||
					(((ItemTriple) t).object.equals(t.subject))) {
				if (connector.isOwner()) {
					// If the verb or object has been deleted, but the
					// subject is still current, lock the subject to
					// delete the triple. If the subject cannot be locked,
					// leave the triple for the next maintenance cycle
					if (subject.isCurrent()) {
						if (subject.lock(session)) {
							try {
								if (store.triplesIndex.deIndexTriple(t) != null) {
									Transaction trans = subject.getTransaction();
									store.deleteTripleUnchecked(t, trans);
								}
							} finally {
								if (subject.isCurrent()) subject.unlock(session);
							}
						}
					}
					// If the subject has been deleted, use a transaction
					// not associated with the subject to delete the triple.
					else {
						if (store.triplesIndex.deIndexTriple(t) != null) {
							Transaction trans = connector.startSourceTransaction();
							connector.removeSourceTriple(t.tripleNr, trans);
							connector.endSourceTransaction(trans);
						}
					}
				}
			}
		}
		
		// If the subject is not from a connected source
		// (or, at least, a known source), remove the triple
		if (connector == null) store.triplesIndex.deIndexTriple(t);
		
		// If the verb or object is not from a connected
		// source, do not consider that item for removal
		if (store.connectorsManager.getConnector(t.verb.sourceNr) == null) 
			store.itemsIndex.noteInUse(t.verb);
		if (t instanceof ItemTriple) {
			ItemTriple it = (ItemTriple)t;
			if (store.connectorsManager.getConnector(
					it.object.sourceNr) == null) store.itemsIndex.noteInUse(it.object);
		}
	}
}
