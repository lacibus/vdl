/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.BufferedInputStream;
import java.io.Serializable;

import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplesource.SourceConnectorStateException;

/**
 * <p>An instance of this class is a BINARY datum whose value is
 * the object of a triple in a connected source.
 * </p>
 * <p>Binary source connector datums are used to hold triple objects
 * that have not been retrieved from their sources.
 * </p>
 */

class BinarySourceConnectorDatum extends Datum implements Serializable {
	
	private static final long serialVersionUID = -4610735982118265119L;
	
	/**
	 * The numeric identifier of the source containing the triple
	 * whose object is the value of the datum.
	 */
	
	long sourceNr;
	
	/**
	 * The numeric identifier within its source of the triple
	 * whose object is the value of the datum.
	 */
	
	long tripleNr;

	/**
	 * Create a new Binary Source Connector Datum
	 * 
	 * @param sourceNr the numeric identifier of the source containing
	 * the triple whose object is the value of the datum
	 * 
	 * @param tripleNr the numeric identifier within its source of the triple
	 * whose object is the value of the datum.
	 * 
	 * @param summaryValue the summary of the value of the datum.
	 */
	
	BinarySourceConnectorDatum(
			long sourceNr,
			long tripleNr,
			long summaryValue) {
		
		this.type = Type.BINARY;
		this.sourceNr = sourceNr;
		this.tripleNr = tripleNr;
		this.summaryValue = summaryValue;
	}
	
	/**
	 * Get the numeric identifier of the source containing
	 * the triple whose object is the value of the datum.
	 * 
	 * @return the numeric identifier of the source containing
	 * the triple whose object is the value of the datum.
	 */
	
	public long getSourceNr() {
		
		return sourceNr;
	}
	
	/**
	 * Get the numeric identifier within its source of the triple
	 * whose object is the value of the datum.
	 * 
	 * @return the numeric identifier within its source of the triple
	 * whose object is the value of the datum.
	 */
	
	public long getTripleNr() {
		
		return tripleNr;
	}

	@Override
	public boolean heldInSource() {
		
		return true;
	}

	@Override
	public byte[] getBinaryValueAsArray() 
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException {
		
		throw(new SourceConnectorStateException(
				sourceNr, 
				"Binary value not available"));
	}

	@Override
	public BufferedInputStream getBinaryValueStream() 
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException {
		
		throw(new SourceConnectorStateException(
				sourceNr, 
				"Binary value not available"));
	}

	@Override
	public boolean valueShouldBeRead() {
		return true;
	}

	@Override
	public String print() {
		return "Stream from which bytes are to be read";
	}

	@Override
	public String simpleTextRepresentation() {
		return "Stream from which bytes are to be read";
	}

	@Override
	public int getLength() {
		return Datum.lengthComponent(summaryValue);
	}

	@Override
	Object toObjectForJson() 
			throws TripleStoreNonFatalException, SourceConnectorStateException {
		
		throw(new SourceConnectorStateException(
				sourceNr, 
				"Binary value not available"));
	}
}
