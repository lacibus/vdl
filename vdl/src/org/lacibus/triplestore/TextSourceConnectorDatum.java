/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.BufferedReader;
import java.io.Serializable;

import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplesource.SourceConnectorStateException;

/**
 * <p>An instance of this class is a TEXT datum whose value is
 * the object of a triple in a connected source.
 * </p>
 * <p>Text source connector datums are used to hold triple objects
 * that have not been retrieved from their sources.
 * </p>
 */

class TextSourceConnectorDatum extends Datum implements Serializable {
	
	private static final long serialVersionUID = -7307271131828146922L;
	
	/**
	 * The numeric identifier of the source containing the triple
	 * whose object is the value of the datum.
	 */
	
	long sourceNr;
	
	/**
	 * The numeric identifier within its source of the triple
	 * whose object is the value of the datum.
	 */
	
	long tripleNr;


	/**
	 * Create a new Text Source Connector Datum
	 * 
	 * @param sourceNr the numeric identifier of the source containing the triple
	 * whose object is the value of the datum
	 * 
	 * @param tripleNr numeric identifier within its source of the triple
	 * whose object is the value of the datum.
	 * 
	 * @param summaryValue the summary of the value of the datum.
	 */
	
	TextSourceConnectorDatum(
			long sourceNr,
			long tripleNr,
			long summaryValue) {
		
		this.type = Type.TEXT;
		this.sourceNr = sourceNr;
		this.tripleNr = tripleNr;
		this.summaryValue = summaryValue;
	}

	/**
	 * Get the numeric identifier of the source containing
	 * the triple whose object is the value of the datum.
	 * 
	 * @return the numeric identifier of the source containing
	 * the triple whose object is the value of the datum.
	 */
	
	public long getSourceNr() {
		
		return sourceNr;
	}
	
	/**
	 * Get the numeric identifier within its source of the triple
	 * whose object is the value of the datum.
	 * 
	 * @return the numeric identifier within its source of the triple
	 * whose object is the value of the datum.
	 */
	
	public long getTripleNr() {
		
		return tripleNr;
	}
	
	@Override
	public boolean heldInSource() {
		
		return true;
	}

	@Override
	public Item getItemValue()
			throws TripleStoreNonFatalException {
		
		throw new TripleStoreNonFatalException(
				"Not an item datum");
	}

	@Override
	public String getTextValueAsString() 
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException {
		
		throw(new SourceConnectorStateException(
				sourceNr, 
				"Text not available"));
	}

	@Override
	public BufferedReader getTextValueReader() 
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException {
		
		throw(new SourceConnectorStateException(
				sourceNr, 
				"Text not available"));
	}

	@Override
	public boolean valueShouldBeRead() {
		return true;
	}

	@Override
	public String print() {
		return "Text to be read";
	}

	@Override
	public String simpleTextRepresentation() {
		return "Text to be read";
	}

	@Override
	public int getLength() {
		return Datum.lengthComponent(summaryValue);
	}

	@Override
	Object toObjectForJson() throws SourceConnectorStateException {
		
		throw(new SourceConnectorStateException(
				sourceNr, 
				"Text not available"));
	}
}
