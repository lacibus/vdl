/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Base64;

/**
 * An instance of this class is a BINARY datum whose value is
 * contained in a byte array.
 */

class ByteArrayDatum extends Datum implements Serializable {
	
	private static final long serialVersionUID = -772800538198875005L;
	
	/**
	 * The byte array containing the value of the datum
	 */
	
	byte[] bytes;

	/**
	 * Create a new Byte Array Datum
	 * 
	 * @param bytes the byte array that contains the datum's value
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	ByteArrayDatum(byte[] bytes) throws TripleStoreNonFatalException{
		
		type = Type.BINARY;
		this.bytes = bytes;
		summaryValue = updateSummary(0L, bytes);
	}

	@Override
	public byte[] getBinaryValueAsArray()  {
		
		return bytes;
	}

	@Override
	public boolean valueShouldBeRead() {
		return false;
	}

	@Override
	public BufferedInputStream getBinaryValueStream() {
		
		return new BufferedInputStream(
						new ByteArrayInputStream(bytes));
	}

	@Override
	public String print() {
		
		return Datum.getByteString(bytes);
	}

	@Override
	public String simpleTextRepresentation() {
		
		final int len = 32;
		if (bytes.length <= len)
			return "Byte Array: " + Arrays.toString(bytes);
		byte shortBytes[] = new byte[len];
		System.arraycopy(bytes, 0, shortBytes, 0, len);
		return "Byte Array starting: " +
					Arrays.toString(shortBytes);
	}

	@Override
	public int getLength() {
		
		return bytes.length;
	}

	@Override
	Object toObjectForJson() {

		return Base64.getEncoder().encodeToString(bytes);
	}
}
