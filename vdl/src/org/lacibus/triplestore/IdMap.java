/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;
import java.util.HashMap;

/**
 * <p>A hash map keyed by Ids and whose members form a doubly-
 * linked list.
 * </p>
 * <p>The methods are synchronized so that access is
 * thread-safe.
 * </p>
 * <p>A maximum size can be specified. In this case, the list
 * works as a FIFO cache, with the oldest member being
 * discarded when the cache is full and a new member is
 * added.
 * </p>
 * <p>The list can be traversed. The getOldest() method
 * starts the traversal and returns the oldest member.
 * Subsequent successive calls to getNextYoungest()
 * traverse the list and return the members in the order
 * that they were added. Members can be removed while the
 * traversal is in progress without ill effects. But
 * only one thread should traverse the list at any time.
 * If multiple threads traverse at the same time then
 * each thread will only get some of the members as the
 * next member will always be returned to whichever
 * thread happens to invoke getNextYoungest().
 * </p>
 * @param <T> the class of member in the list.
 * 
 */

class IdMap<T> implements Serializable {

	private static final long serialVersionUID = -4819589217879829492L;

	/**
	 * The hash map of records containing members
	 */
	
	private HashMap<Id, MemberRecord<T>> members =
		new HashMap<Id, MemberRecord<T>>();
	
	/**
	 * The maximum size of the list. If it is zero or
	 * negative then the list has no maximum size.
	 */
	
	private int maxSize;
	
	/**
	 * The record containing the first member in the list. 
	 * This is the newest member.
	 */
	
	private MemberRecord<T> firstInList = null;
	
	/**
	 * The record containing the last member in the list. 
	 * This is the oldest member.
	 */
	
	private MemberRecord<T> lastInList = null;
	
	/**
	 * The record containing the next member to be
	 * returned when the list is traversed.
	 */
	
	private MemberRecord<T> nextInList = null;
	
	/**
	 * Create a new linked hash map with no maximum size
	 */
	
	IdMap() {
		maxSize = -1;
	}
	
	/**
	 * Create a new linked hash map with a maximum size
	 * 
	 * @param size the maximum size. This should be greater
	 * than zero.
	 */
	
	IdMap(int size) {
		maxSize = size;
	}
	
	/**
	 * Get the number of members
	 * 
	 * @return the number of members
	 */
	
	int size() {
		return members.size();
	}
	
	/**
	 * Get the member indexed by the given Id
	 * 
	 * @param id the Id that indexes the member
	 * 
	 * @return the member indexed by the Id
	 */
	
	synchronized T get(Id id) {
		
		MemberRecord<T> m = members.get(id);
		if (m == null) return null;
		return m.content;
	}
	
	/**
	 * Put a member into the list, indexed by the given id.
	 * The member is added at the start of the list. If there
	 * is already a member with the given id then it is
	 * removed. If the list has a maximum size and that size 
	 * would be exceeded then the oldest member, which is at
	 * the end of the list, is removed.
	 * 
	 * @param id the given Id
	 * 
	 * @param o the member to be added
	 */
	
	synchronized void put(Id id, T o) {

		MemberRecord<T> m = members.get(id);
		if (m == null) {
			m = new MemberRecord<T>(id, o);
			if ((maxSize > 0) &&
			    (members.size() >= maxSize)){
				MemberRecord<T> oldest = lastInList;
				members.remove(oldest.id);
				unlink(oldest);
			}					
		}
		else {
			m.content = o;
			unlink(m);
		}
		link(m);
		members.put(id, m);
	}
	
	/**
	 * Remove a member from the list
	 * 
	 * @param id the member's id
	 * 
	 * @return the member that was removed, or null if
	 * the the list did not contain a member with the
	 * given id
	 */
	
	synchronized T remove(Id id) {
		MemberRecord<T> m = members.remove(id);
		if (m == null) return null;
		unlink(m);
		return m.content;
	}
	
	/**
	 * Start traversal of the list
	 * 
	 * @return the oldest member, which is at the end
	 * of the list, or null if the list is empty
	 */
	
	synchronized T getOldest() {
		if (lastInList == null) return null;
		nextInList = lastInList.previous;
		return lastInList.content;
	}
	
	/**
	 * Continue the traversal of the list
	 * 
	 * @return the next younger member, which is the one
	 * that was added next after the previous one to be returned,
	 * or null if there are no more members to return
	 */
	
	
	synchronized T getNextYounger() {
		if (nextInList == null) return null;
		MemberRecord<T> result = nextInList;
		nextInList = nextInList.previous;
		return result.content;
	}
	
	/**
	 * Link a member record into the list
	 * 
	 * @param m the member record to be linked in
	 */
	
	private void link(MemberRecord<T> m) {
		if (firstInList != null) firstInList.previous = m;
		m.previous = null;
		m.next = firstInList;
		firstInList = m;
		if (lastInList == null)
			lastInList = m;
	}
	
	/**
	 * Unlink a member record from the list
	 * 
	 * @param m the member record to be unlinked
	 */
	
	private void unlink(MemberRecord<T> m) {
		if (m == nextInList)
			nextInList = m.previous;
		if (m.previous == null) {
			firstInList = m.next;
			if (firstInList == null)
				lastInList = null;
			else firstInList.previous = null;
		}
		else m.previous.next = m.next;
		if (m.next == null) {
			lastInList = m.previous;
			if (lastInList == null) 
				firstInList = null;
			else lastInList.next = null;
		}
		else m.next.previous = m.previous;
	}
	
	/**
	 * A record containing a member with links to the
	 * previous and next records
	 *
	 * @param <U> The class of member
	 */
	
	private class MemberRecord<U> implements Serializable {

		private static final long serialVersionUID = -730094174358449642L;
		
		/**
		 * The identifier of the member
		 */
		
		Id id;
		
		/**
		 * The member
		 */
		
		U content;
		
		/**
		 * The previous record in the list
		 */
		
		MemberRecord<U> previous = null;
		
		/**
		 * The next record in the list
		 */
		
		MemberRecord<U> next = null;
		
		/**
		 * Create a member record
		 * 
		 * @param id the identifier of the member
		 * 
		 * @param o the member
		 */
		
		MemberRecord(Id id, U o){
			this.id = id;
			content = o;
		}
	}		
}
