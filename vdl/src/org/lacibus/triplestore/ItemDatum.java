/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;

/**
 * An instance of this class is a datum whose value is an item.
 */

class ItemDatum extends Datum implements Serializable  {
	
	private static final long serialVersionUID = -7366460235055421295L;
	
	/**
	 * The item that is the value of the datum
	 */
	
	Item item;

	/**
	 * Create an item datum
	 * 
	 * @param item the item that is the value of the datum
	 */
	
	ItemDatum (Item item) {
		type = Type.ITEM;
		this.item = item;
		summaryValue = getSummary(item);
	}
	
	/**
	 * Create an item datum
	 * 
	 * @param sourceNr the numeric identifier of the source of the item 
	 * that is the value of the datum
	 * 
	 * @param itemNr the numeric identifier within its source of the item 
	 * that is the value of the datum
	 */
	
	ItemDatum (long sourceNr, long itemNr) {
		type = Type.ITEM;
		this.item = new Item(sourceNr, itemNr);
		summaryValue = getSummary(item);
	}
	
	@Override
	public Item getItemValue() {
		return item;
	}
	
	@Override
	public String print() {
		return "" + item.getId();
	}
	
	/**
	 * Get an item given a textual specification
	 * 
	 * @param s a textual specification of the form Item: <id> where
	 * id is a textual representation of an item identifier.
	 * 
	 * @param store the triple store containing the item
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the specified item if it is in the store and visible
	 * to the requesting store session, null otherwise
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 */
	
	static Item getItem(String s, TripleStore store, StoreSession session) 
			throws NotInOperationException, TripleStoreNonFatalException {
		
		if (s.startsWith("Item: ")) {
			Id id = Id.parseId(s.substring(7));
			if (id != null) return store.getItem(id, session);
		}
		return null;
	}
	
	@Override
	public String simpleTextRepresentation() {
		return "Item: " + item.getId();
	}

	@Override
	public String toString() {
		return "Item: " + item.getId();
	}

	@Override
	public boolean valueShouldBeRead() {
		return false;
	}

	@Override
	public int getLength() {
		return 1;
	}

	@Override
	Object toObjectForJson() {

		return item.getId().toString();
	}
}
