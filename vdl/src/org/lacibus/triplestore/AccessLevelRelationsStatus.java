/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;
import java.util.Set;

import org.lacibus.settheoryrels.OrderedPair;

/**
 * An instance of this class gives the explicit and implicit
 * relationships between the access levels of a triple store
 * and the permissions that have been granted for levels to
 * be superior or inferior to other levels.
 */

public class AccessLevelRelationsStatus implements Serializable {
	
	private static final long serialVersionUID = 1517702208267089835L;

	/**
	 * The explicit (stated) relationships between access levels
	 */
	
	public Set<OrderedPair<AccessLevel>> explicitRelationships;
	
	/**
	 * The implicit (implied) relationships between access levels
	 */
	
	public Set<OrderedPair<AccessLevel>> implicitRelationships;
	
	/**
	 * The permissions for access levels to be superior to other
	 * levels
	 */
	
	public Set<OrderedPair<AccessLevel>> superiorPermissions;
	
	/**
	 * The permissions for access levels to be inferior to other
	 * levels
	 */
	
	public Set<OrderedPair<AccessLevel>> inferiorPermissions;
}
