/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;

import org.lacibus.triplesource.StoredItemTriple;
import org.lacibus.triplestore.Item.State;

/**
 * A triple whose object is an item
 */

class ItemTriple extends Triple implements Serializable {

	private static final long serialVersionUID = -6230581276554903641L;
	
	/**
	 * The item that is the triple's object
	 */
	
	protected Item object;
	
	/**
	 * Construct an item triple 
	 * 
	 * @param tripleId the triple's numeric identifier within its source
	 * 
	 * @param subject the subject item
	 * 
	 * @param verb the verb item
	 * 
	 * @param object the object item
	 * 
	 * @param store the triple store containing the triple
	 */
	
	ItemTriple(
			long tripleNr,
			Item subject,
			Item verb, 
			Item object,
			TripleStore store) {
		
		super(tripleNr, subject, verb, Datum.Type.ITEM, store);
		this.object = object;
	}
	
	/**
	 * Construct an item triple from a stored item triple
	 * 
	 * @param storedTriple a stored item triple
	 * 
	 * @param store the triple store that will hold the triple
	 */
	
	ItemTriple(
			StoredItemTriple storedTriple,
			TripleStore store) {
		
		super(	storedTriple.tripleNr, 
				storedTriple.subject, 
				storedTriple.verb, 
				Datum.Type.ITEM, 
				store);
		this.object = storedTriple.object;
	}
	
	/**
	 * Construct an item triple taking the numeric identifier,
	 * subject, verb, object, and triple store from another 
	 * item triple
	 * 
	 * @param t the triple to be copied
	 */
	
	public ItemTriple(ItemTriple t) {
		super(t);
		this.object = t.object;
	}
	
	@Override
	public long getSourceNr() {

		return subject.sourceNr;
	}
	
	public long getObjectSummaryValue() {
		
		return object.summary();
	}
	
	/**
	 * Get the item that is the object of the triple
	 * 
	 * @return the item that is the object of the triple
	 */
	
	public Item getObjectItem() {
		
		return object;
	}

	@Override
	State getState() {

		if (	(subject.getState() == Item.State.EXISTS) &&
				(verb.getState() == Item.State.EXISTS) &&
				(object.getState()  == Item.State.EXISTS))
			return Item.State.EXISTS;
		if (	(subject.getState() == Item.State.DELETED) ||
				(verb.getState() == Item.State.DELETED) ||
				(object.getState()  == Item.State.DELETED))
			return Item.State.DELETED;
		return Item.State.MAYEXIST;
	}

	public String toString() {
		return ("Triple ID" + getSourceNr() + ':' + tripleNr + " " +
				getSubjectId() + "; " + getVerbId() + "; ITEM " +
				object.getId());
	}	
}
