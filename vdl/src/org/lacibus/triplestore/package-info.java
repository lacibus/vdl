/**
 * <p>This package contains the classes for the fundamental entities of
 * a virtual data lake: its triple store, items, triples, datums, access 
 * levels, and store sessions.
 * </p>
 **/

package org.lacibus.triplestore;
