/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.BufferedReader;
import java.io.Serializable;
import java.io.StringReader;

/**
 * An instance of this class is a TEXT datum whose value is
 * contained in a string.
 */

class StringDatum extends Datum implements Serializable {
	
	private static final long serialVersionUID = -1376711244952628079L;
	
	/**
	 * The string containing the value of the datum
	 */
	
	String string;

	/**
	 * Create a new String Datum
	 * 
	 * @param string the string that contains the datum's value
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	StringDatum(String string) throws TripleStoreNonFatalException{
		
		type = Type.TEXT;
		this.string = string;
		summaryValue = updateSummary(0L, string);
	}

	@Override
	public String getTextValueAsString() {
		return string;
	}

	@Override
	public BufferedReader getTextValueReader() {
		return new BufferedReader(
					new StringReader(string));
	}

	@Override
	public boolean valueShouldBeRead() {
		return false;
	}

	@Override
	public String print() {
		return string;
	}

	@Override
	public String simpleTextRepresentation() {
		final int len = 32;
		if (string.length() <= len)
			return "String: " + string;
		else return "String starting: " +
						string.substring(0, len);
	}

	@Override
	public int getLength() {
		return string.length();
	}

	@Override
	Object toObjectForJson() {

		return string;
	}
}
