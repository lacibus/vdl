/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;

/**
 * <p>A session of interactions with a triple store.
 * </p>
 * <p>Each store session has an access level that determines
 * what data it can read and write.
 * </p>
 */

public class StoreSession implements Serializable {

	private static final long serialVersionUID = 7385052519907375511L;

	private AccessLevel accessLevel;
	
	/**
	 * Construct a new store session.
	 * 
	 * @param level the store session's access level.
	 * This must not be null, deleted, or being replaced.
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	StoreSession(AccessLevel level) 
			throws TripleStoreNonFatalException {
		
		if (level == null)
			throw (new TripleStoreNonFatalException(
					"Null access level"));
		if (level.levelItem == null)
			throw (new TripleStoreNonFatalException(
				"Invalid access level"));
		accessLevel = level;
	}

	/**
	 * <p>Get the store session's access level
	 * </p>
	 * <p>The access level must not be null, deleted,
	 * or being replaced.
	 * </p>
	 * <p>Note that a session can always read its own
	 * access level, because the superior-to relation 
	 * on access levels is reflexive.
	 * </p>
	 * @return the store session's access level.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public AccessLevel getAccessLevel() 
			throws TripleStoreNonFatalException {
		
		if ((accessLevel == null) || (accessLevel.levelItem == null)) 
			throw new TripleStoreNonFatalException(
					"Invalid session: null or invalid access level");
		return accessLevel;
	}
	
	/**
	 * Determine whether this session was created by
	 * a particular triple store.
	 * 
	 * @param store a triple store
	 * 
	 * @return true if this session was created by
	 * the given triple store, false otherwise.
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public boolean isOfTripleStore(TripleStore store) 
			throws TripleStoreNonFatalException {
		
		if (accessLevel == null) 
			throw new TripleStoreNonFatalException(
					"Invalid session: null access level");
		return (accessLevel.isOfTripleStore(store));
	}
}
