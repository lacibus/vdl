/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

/*
 ***********************************************************************
 *
 *  HOW THIS FILE IS ORGANIZED
 *  
 ***********************************************************************
 * 
 * This file contains:
 *    o Imports and the class definition
 *    o Definitions of the major class variables
 *    o Store creation, initialization, and closedown
 *    o Source connectors
 *    o Access levels
 *    o Items
 *    o Triples
 *    o Search and uniqueness operations
 * 
 */

/*
 ***********************************************************************
 *
 *  IMPORTS AND CLASS DEFINITION
 *  
 ***********************************************************************
 */

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.locks.ReentrantLock;

import org.lacibus.settheoryrels.OrderedPair;
import org.lacibus.triplesource.AddItemDelta;
import org.lacibus.triplesource.AddItemTripleDelta;
import org.lacibus.triplesource.AddNonItemTripleDelta;
import org.lacibus.triplesource.Delta;
import org.lacibus.triplesource.RemoveItemDelta;
import org.lacibus.triplesource.RemoveTripleDelta;
import org.lacibus.triplesource.SourceConnector;
import org.lacibus.triplesource.SourceConnectorClient;
import org.lacibus.triplesource.SourceConnectorContentException;
import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplesource.StoredItemTriple;
import org.lacibus.triplesource.StoredNonItemTriple;
import org.lacibus.triplesource.StoredTriple;
import org.lacibus.triplesource.UpdateItemDelta;
import org.lacibus.triplesource.SourceConnector.IdAndTrans;

/**
 * <p>A triple store holds items and triples from the sources
 * to which it is connected. For triples with text and binary
 * objects, an object is  only retrieved from the source
 * when its summary meets a search criterion.
 * </p>
 * <p>The triple store has methods to connect and disconnect 
 * sources, to read items and triples in all connected sources 
 * and to search for triples in all connected sources. It has
 * methods to create, delete, and update items and triples
 * in connected sources that it owns. 
 * </p>
 */

public class TripleStore 
		implements SourceConnectorClient, Serializable {
	
	private static final long serialVersionUID = 4331889582262192373L;

/*
 ***********************************************************************
 *
 *  MAJOR CLASS VARIABLES
 *  
 ***********************************************************************
 */

	/**
	 * Possible states of the triple store
	 */
	enum State {INITIAL, INITIALIZING, OPERATIONAL, CLOSED}
	
	/**
	 * The current state of the triple store
	 */
	
	State state = State.INITIAL;

	/**
	 * A cache of text objects held as strings. Short text
	 * objects are held in this cache when retrieved, and are
	 * then obtained from the cache rather than being retrieved
	 * from source again. When the cache is full, entries that
	 * have not been recently accessed are removed to make room
	 * for new ones.
	 */
	
	IdMap<String> stringCache;
	
	/**
	 * The length of the smallest text value that is regarded
	 * as 'long' for the purpose of caching
	 */
	
	private final int longTextLength = 2048;
	
	/**
	 * A cache of binary objects held as byte arrays. Short binary
	 * objects are held in this cache when retrieved, and are
	 * then obtained from the cache rather than being retrieved
	 * from source again. When the cache is full, entries that
	 * have not been recently accessed are removed to make room
	 * for new ones.
	 */
	
	IdMap<byte[]> binaryCache;
	
	/**
	 * The length of the smallest binary value that is regarded
	 * as 'long' for the purpose of caching
	 */
	
	private final int longBinaryLength = 2048;
	
	/**
	 * The index (actually a set of indexes) of triples in the
	 * triple store.
	 */
	
	TriplesIndex triplesIndex = new TriplesIndex();
	
	/**
	 * The access level manager for access levels of items in
	 * the triple store.
	 */
	
	AccessLevelManager accessLevelManager = null;
	
	/**
	 * The manager of the source connectors in use
	 */
	
	ConnectorsManager connectorsManager =
			new ConnectorsManager();
	
	/**
	 * The thread that performs regular maintenance operations
	 */
	
	TripleStoreMaintainer maintainer;
	
	/**
	 * The logger for status and error reports
	 */
	
	Logger logger = null;
	
	/**
	 * An indication of the size of the triple store. This indication 
	 * does not relate to the numbers of items and triples that the store
	 * can hold, but may reflect the scale of use that is intended for it.
	 */
	
	int sizeIndicator = 0;
	
	/*
	 ***********************************************************************
	 *
	 *  CREATION, INITIALIZATION, AND CLOSEDOWN
	 *  
	 ***********************************************************************
	 */

	/**
	 * Construct a new triple store
	 * 
	 * @param stringCacheSize the number of strings in
	 * the string cache
	 * 
	 * @param binaryCacheSize the number of byte arrays in
	 * the binary cache
	 */
	
	public TripleStore(int stringCacheSize, int binaryCacheSize) {

		stringCache = new IdMap<String>(stringCacheSize);
		binaryCache = new IdMap<byte[]>(binaryCacheSize);
		sizeIndicator = stringCacheSize + binaryCacheSize;
	}
	
	/**
	 * Get an indication of the size of the triple store. This indication 
	 * does not relate to the numbers of items and triples that the store
	 * can hold, but may reflect the scale of use that is intended for it.
	 * 
	 * @return the size of the string cache plus the size of the binary cache.
	 * 
	 */
	
	public int sizeIndicator() {
		
		return sizeIndicator;
	}
	
	/**
	 * Get the logger
	 * 
	 * @param logger the logger to be set
	 */
	
	public Logger getLogger() {
		
		return logger;
	}
	
	/**
	 * Set the logger
	 * 
	 * @param logger the logger to be set
	 */
	
	public void setLogger(Logger logger) {
		
		this.logger = logger;
	}
	
	/**
	 * Report status or an error
	 * 
	 * @param message the status or error to be reported
	 */
	
	public void log(String message) {
		
		if (logger != null) logger.log(message);
	}
			
	/**
	 * Report an exception
	 * 
	 * @param message the status or error to be reported
	 */
	
	public void log(Exception e) {
		
		if (logger != null) logger.log(e);
	}
			
	/**
	 * Lock used to ensure initialization takes place only once
	 */
	
	ReentrantLock initLock = new ReentrantLock();
	
	/**
	 * <p>Return a session issuer when the store has been initialised.
	 * </p>
	 * <p>If the store is in its initial state, initialise it and
	 * return a session issuer.If it is already being initialized 
	 * by another thread, wait until that process has completed, 
	 * then return null.If the store has already been initialized, 
	 * return null. 
	 * </p>
	 * <p>The session issuer is returned so that the software that
	 * set up the triple store is able to obtain store sessions and
	 * use it. This software is responsible for controlling the use
	 * of the issuer and the sessions that it issues to maintain
	 * secure access to the data in the store.
	 * </p>
	 * @return a session issuer or null
	 * 
	 * @throws TripleStoreFatalException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public SessionIssuer initialize() 
			throws 	TripleStoreFatalException,
					TripleStoreNonFatalException {


		try {
			initLock.lockInterruptibly();
		} catch (InterruptedException e) {
			throw (new TripleStoreNonFatalException(
					"Interrupted while waiting to initialize"));
		}
		
		try {
			if (state != State.INITIAL) return null;
			state = State.INITIALIZING;
			doInit();
			state = State.OPERATIONAL;
			return new SessionIssuer(this);
		} finally {
			initLock.unlock();
		}
	}
	
	/**
	 * Initialize the triple store
	 * 
	 * @throws TripleStoreFatalException
	 * @throws TripleStoreNonFatalException
	 */
	
	private void doInit()
			throws 	TripleStoreFatalException,
					TripleStoreNonFatalException {

		//	Set up the special items.
		//  Set them up initially with access level specs
		//  as access levels won't be available until the
		//  access level manager has started
		
		// Set up the special items representing the lowest access level
		lowest = new Item(
				specialItemSource, lowestLevelNr, null, null, this, true);
		itemsIndex.alwaysIndex(lowest);
				
		// Set up the special items representing the highest access level
		highest = new Item(
				specialItemSource, highestLevelNr, null, null, this, true);		
		itemsIndex.alwaysIndex(highest);

		// Set up the access level manager
		accessLevelManager = new AccessLevelManager(highest, lowest);
		
		// Now that the access level manager is started, set up the 
		// access levels of the items representing the lowest and 
		// highest access levels
		lowest.setAccessLevels(
				accessLevelManager.lowest, accessLevelManager.highest);
		highest.setAccessLevels(
				accessLevelManager.highest, accessLevelManager.highest);
		
		// Set up the special item that is the "is access level relation"
		// verb
		isAccessLevelRelation = new Item(
				specialItemSource, isAccessLevelRelationNr,
				accessLevelManager.lowest, accessLevelManager.highest, 
				this, true);
		itemsIndex.alwaysIndex(isAccessLevelRelation);
		
		// Set up the special item that is the "superior permission
		// relation" verb
		superiorPermissionRelation = new Item(
				specialItemSource, superiorPermissionRelationNr,
				accessLevelManager.lowest, accessLevelManager.highest, 
				this, true);
		itemsIndex.alwaysIndex(superiorPermissionRelation);
		
		// Set up the special item that is the "inferior permission
		// relation" verb
		inferiorPermissionRelation = new Item(
				specialItemSource, inferiorPermissionRelationNr,
				accessLevelManager.lowest, accessLevelManager.highest, 
				this, true);
		itemsIndex.alwaysIndex(inferiorPermissionRelation);
		
		// Set up the special item that is the "has credential relation"
		// verb
		hasCredentialRelation = new Item(
				specialItemSource, hasCredentialRelationNr,
				accessLevelManager.lowest, accessLevelManager.highest, 
				this, true);
		itemsIndex.alwaysIndex(hasCredentialRelation);
		
		// Set up the special item that is the "has secret digest relation"
		// verb
		hasSecretDigestRelation = new Item(
				specialItemSource, hasSecretDigestRelationNr,
				accessLevelManager.highest, accessLevelManager.highest, 
				this, true);
		itemsIndex.alwaysIndex(hasSecretDigestRelation);

		// Start the maintenance thread
		maintainer = new TripleStoreMaintainer(this);
		maintainer.setDaemon(true);
		maintainer.start();
	}

	/**
	 * Boolean used only to synchronize close operations
	 */
	
	private Boolean closeLock = false;

	/**
	 * Close the store
	 */
	
	public void close() {
		
		synchronized(closeLock) {
			if (state == State.CLOSED) return;
			state = State.CLOSED;
		}
		closedown();
	}
	
	/**
	 * Stop the maintenance thread and disconnect all connected sources
	 */
	
	private void closedown() {
		
		maintainer.interrupt();
		Collection<SourceConnector> connectors = 
				connectorsManager.close().values();
		for (SourceConnector connector : connectors) try {
			disconnect(connector);
		} catch (Exception e) {}
	}
		
	/**
	 * Abort triple store operation on detection of a fatal condition
	 * 
	 * @param message a string describing the fatal condition
	 * 
	 * @return a TripleStoreFatalException to be thrown
	 */
	
	TripleStoreFatalException abortTripleStoreException(String message) {
		
		TripleStoreFatalException e = new TripleStoreFatalException(message);
		synchronized(closeLock) {
			if (state == State.CLOSED) 
				return e;
			state = State.CLOSED;
		}
		closedown();
		return e;
	}
			
	/**
	 * Abort triple store operation on receipt of an exception indicating 
	 * a fatal condition
	 * 
	 * @param message a string describing the fatal condition
	 * 
	 * @return a TripleStoreFatalException to be thrown
	 */
	
	TripleStoreFatalException abortTripleStoreException(
			String message, Exception ex) {
		
		TripleStoreFatalException e = new TripleStoreFatalException(message);
		e.initCause(ex);
		synchronized(closeLock) {
			if (state == State.CLOSED) 
				return e;
			state = State.CLOSED;
		}
		closedown();
		return e;
	}
			
	/*
	 ***********************************************************************
	 *
	 *  SOURCE CONNECTORS
	 *  
	 ***********************************************************************
	 */
	
	/**
	 * Create a source
	 * 
	 * @param connector a source connector for the source
	 * 
	 * @param session the requesting store session
	 * 
	 * @throws Exception 
	 */
	
	public void create(
			SourceConnector connector, StoreSession session) 
					throws 	Exception {
		
		createOrConnect(true, connector, session);
	}
	
	/**
	 * Connect to a source
	 * 
	 * @param connector a source connector for the source
	 * 
	 * @param session the requesting store session
	 * 
	 * @throws Exception 
	 */
		
	public void connect(
			SourceConnector connector, StoreSession session) 
					throws 	Exception {
		
		createOrConnect(false, connector, session);
	}
	
	/**
	 * Create a source, or connect to an existing source
	 * 
	 * @param create if true, a new source is created, otherwise an
	 * existing source is connected
	 * 
	 * @param connector a source connector for the source
	 * 
	 * @param session the requesting store session
	 * 
	 * @throws Exception 
	 */
	
	private void createOrConnect(
			boolean create, SourceConnector connector, StoreSession session) 
					throws 	Exception {

		if (connector == null) 
			throw new TripleStoreNonFatalException(
					"Null Connector");
		if (connector.getClient() != this) 
			throw new TripleStoreNonFatalException(
					"Invalid Connector");

		long sourceNr = connector.getSourceNr();
		if (!SourceConnector.isDomainSourceNr(sourceNr) && 
			!SourceConnector.isInstallationSourceNr(sourceNr) &&
			!SourceConnector.isRegisteredSourceNr(sourceNr))
			throw new TripleStoreNonFatalException(
					"Source number out of range");
		
		connectorsManager.addConnector(connector);
		try {
			if (create) connector.create();
			else connector.load();
			completeConnection(connector);
		} catch (SourceConnectorException e) {
			log("Error connecting to source " + sourceNr + ": " + e.getMessage());
			connectorsManager.removeConnector(connector);
			throw(e);
		}
	}
	
	/**
	 * <p>Complete a connection to a source
	 * </p>
	 * <p>Once the items and triples have been loaded from the source,
	 * update the triple store to integrate that data with the
	 * previous data.
	 * </p>
	 * @param connector the source connector being connected
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	private void completeConnection(
			SourceConnector connector) 
					throws	TripleStoreNonFatalException, 
							TripleStoreFatalException, 
							SourceConnectorException, 
							NotInOperationException {

		log("Store completing connection");
		
		// Synchronize access level relationships with
		// the triple store, to take account of loaded
		// superiority permission triples
		accessLevelManager.syncLevelRelation(this, null);
		log("Level relations synced");
		
		// Make the source connector operational
		connector.makeOperational();
		log("Store connection complete");
	}
	
	/**
	 * Disconnect a source
	 * 
	 * @param sourceNr the numeric identifier of the source to
	 * be disconnected
	 * 
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	public void disconnect(long sourceNr)
			throws 	TripleStoreNonFatalException, 
					TripleStoreFatalException, 
					SourceConnectorException, 
					NotInOperationException {
		
		// Get the source connector
		log("Store disconnect source: " + sourceNr);
		SourceConnector connector = connectorsManager.getConnector(sourceNr);
		if (connector == null) throw (new TripleStoreNonFatalException(
				"Connector not found for source: " + sourceNr));
		
		// Disconnect the source
		disconnect(connector);
	}
		
	/**
	 * Disconnect a source
	 * 
	 * @param connector the source connector of the source to
	 * be disconnected
	 * 
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	void disconnect(SourceConnector connector) 
			throws 	TripleStoreFatalException, 
					SourceConnectorException, 
					NotInOperationException {
		
		// Remove the connector from the manager
		connectorsManager.removeConnector(connector);
		
		// Stop operation of the connector
		connector.stopOperation();
		
		// Synchronize access level relationships with
		// the triple store, to take account of the
		// removal of superiority permission triples
		// from the source
		accessLevelManager.syncLevelRelation(this, connector.getSourceNr());
	}
	
	/**
	 * Determine whether a source is known. A source is
	 * known if the triple store has started to connect to
	 * it and it has not been disconnected.
	 * 
	 * @param sourceNr the numeric identifier of a source
	 * 
	 * @return true if the source with the given source
	 * number is known, false otherwise
	 */
	
	public boolean isKnown(long sourceNr) {
		
		return connectorsManager.isKnown(sourceNr);
	}
	
	/**
	 * Get the connector for a given source
	 * 
	 * @param sourceNr the numeric identifier of a source
	 * 
	 * @return the connector for the given source
	 */
	
	public SourceConnector getSourceConnector(long sourceNr) {
		
		return connectorsManager.getConnector(sourceNr);
	}
	
	/**
	 * Determine whether a source is operational. A source is
	 * operational if the triple store has loaded all its items
	 * and triples and it has not been disconnected.
	 * 
	 * @param sourceNr the numeric identifier of a source
	 * 
	 * @return true if the source with the given source
	 * number is operational, false otherwise
	 */
	
	public boolean isOperational(long sourceNr) {
		
		return connectorsManager.isOperational(sourceNr);
	}
	
	/**
	 * <p>Get the administration access level of a source.
	 * </p>
	 * <p>This method is typically invoked by an application
	 * after it has connected a source, to perform
	 * initialization and management operations.
	 * </p>
	 * @param sourceNr a numeric source identifier
	 * 
	 * @return the administration access level of the 
	 * identified source
	 * 
	 * @throws NotInOperationException
	 */
	
	public AccessLevel getAdminAccessLevel(
			long sourceNr) 
					throws NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		SourceConnector connector = connectorsManager.getConnector(sourceNr);
		if (connector == null) return null;
		Id id = new Id(
				connector.getSourceNr(), 
				SourceConnector.adminLevelItemNr);		
		Item it = itemsIndex.get(id);
		if (it == null) return null;
		return accessLevelManager.get(it);
	}
	
	/**
	 * <p>Apply a delta
	 * </p>
	 * <p>A delta is an instruction to add, remove or change
	 * an item or triple.
	 * </p>
	 * <p>When a source is loaded while being connected, or when the 
	 * triple store is synchronizing with a source, a set of deltas
	 * is loaded from it.
	 * </p>
	 * @param sourceNr the numeric identifier of the source
	 * originating the delta
	 * 
	 * @param delta a delta
	 * 
	 * @param load true if the delta is loaded in a load
	 * operation, false if in a sync operation
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * @throws NotInOperationException 
	 */
	
	public void applyDelta(
			long sourceNr, Delta delta, boolean load) 
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException, 
					TripleStoreFatalException,
					NotInOperationException {

		if (delta instanceof AddItemDelta) {
			AddItemDelta addItemDelta =
					(AddItemDelta) delta;
			syncAddItem(sourceNr, addItemDelta.itemNr,
					addItemDelta.readLevelSourceNr, addItemDelta.readLevelItemNr, 
					addItemDelta.writeLevelSourceNr, addItemDelta.writeLevelItemNr);
		}
		else if (delta instanceof RemoveItemDelta) {
			syncRemoveItem(
					sourceNr, ((RemoveItemDelta) delta).itemNr);
		}
		else if (delta instanceof UpdateItemDelta) {
			UpdateItemDelta updateItemDelta =
					(UpdateItemDelta) delta;
			syncUpdateItem(
					sourceNr, 
					updateItemDelta.itemNr, 
					updateItemDelta.readLevelSourceNr, 
					updateItemDelta.readLevelItemNr, 
					updateItemDelta.writeLevelSourceNr, 
					updateItemDelta.writeLevelItemNr);
		}
		else if (delta instanceof AddNonItemTripleDelta) {
			AddNonItemTripleDelta addNonItemTripleDelta =
					(AddNonItemTripleDelta) delta;
			syncAddNonItemTriple(
					sourceNr,
					addNonItemTripleDelta.tripleNr,
					addNonItemTripleDelta.subjectItemNr,
					addNonItemTripleDelta.verbSourceNr,
					addNonItemTripleDelta.verbItemNr,
					addNonItemTripleDelta.getObject());
		}
		else if (delta instanceof AddItemTripleDelta) {
			AddItemTripleDelta addItemTripleDelta =
					(AddItemTripleDelta) delta;
			syncAddItemTriple(
					sourceNr,
					addItemTripleDelta.tripleNr,
					addItemTripleDelta.subjectItemNr,
					addItemTripleDelta.verbSourceNr,
					addItemTripleDelta.verbItemNr,
					addItemTripleDelta.getObjectId(),
					load);
		}
		else if (delta instanceof RemoveTripleDelta) {
			syncRemoveTriple(
					sourceNr, 
					((RemoveTripleDelta) delta).tripleNr,
					load);
		}
	}
	
	/**
	 * Add an item to synchronize with its source
	 * 
	 * @param sourceNr the numeric identifier of the source of the item
	 * 
	 * @param itemNr the numeric identifier of the item in its source
	 * 
	 * @param readLevelSourceNr the numeric identifier of the source of
	 * the new read level of the item
	 * 
	 * @param readLevelItemNr the numeric identifier in its source of 
	 * the new read level of the item
	 * 
	 * @param writeLevelSourceNr the numeric identifier of the source of 
	 * the new write level of the item
	 * 
	 * @param writeLevelItemNr the numeric identifier in its source of 
	 * the new write level of the item
	 */
	
	private void syncAddItem(
			long sourceNr, long itemNr,
			long readLevelSourceNr, long readLevelItemNr,
			long writeLevelSourceNr, long writeLevelItemNr) {
		
		// Get or add to the index the item's access level items
		Item orlItem = itemsIndex.getOrAddTripleElement(
				new Id(readLevelSourceNr, readLevelItemNr));
		Item rowlItem = itemsIndex.getOrAddTripleElement(
				new Id(writeLevelSourceNr, writeLevelItemNr));
		
		// Get the item's access levels		
		AccessLevel orl = accessLevelManager.getOrCreate(orlItem);
		AccessLevel rowl = accessLevelManager.getOrCreate(rowlItem);
				
		// Get or add the item to the index
		itemsIndex.getOrAddItem(
				new Id(sourceNr, itemNr), orl, rowl);
	}
	
	/**
	 * Remove an item to synchronize with its source
	 * 
	 * @param sourceNr the numeric identifier of the source of the
	 * item
	 * 
	 * @param itemNr the numeric identifier of the item in its source
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private void syncRemoveItem(long sourceNr, long itemNr)
			throws TripleStoreNonFatalException {

		Item item = itemsIndex.remove(new Id(sourceNr, itemNr));
		if (item == null) return;
		try {
			if (item.isLocked()) 
				throw (new TripleStoreNonFatalException(
						"Locked item " + itemNr +
						" from source " + sourceNr + " de-installed"));
			// If the item represents an access level, it can only
			// be deleted if that level was first replaced. The
			// triple specifying the replacement will already have
			// been received, and everything needed to note the
			// replacement will have been done.
		} finally {
			item.markDeleted();
		}
	}
	
	/**
	 * Update an installed item to synchronize with its source
	 * 
	 * @param sourceNr the numeric identifier of the source of
	 * the item
	 * 
	 * @param itemNr the numeric identifier of the item in 
	 * its source
	 * 
	 * @param readLevelSource the numeric identifier of the 
	 * source of the new read level of the item
	 * 
	 * @param readLevelRef the the numeric identifier in its
	 * source of the new read level of the item
	 * 
	 * @param writeLevelSource the numeric identifier of the 
	 * source of the new write level of the item
	 * 
	 * @param writeLevelRef the numeric identifier in its source
	 * of the new write level of the item
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private void syncUpdateItem(
			long sourceNr, long itemNr,
			long readLevelSource, long readLevelRef,
			long writeLevelSource, long writeLevelRef) 
					throws TripleStoreNonFatalException {
		
		// Get the item
		Item item = itemsIndex.get(
				new Id(sourceNr, itemNr));
		if (item == null) {
			throw (new TripleStoreNonFatalException(
					"Attempt to update non-existent item " + itemNr +
					" from source " + sourceNr));
		}
		// Try to set the item's access levels
		setItemLevels(item, readLevelSource, readLevelRef, 
				writeLevelSource, writeLevelRef);
	}
	
	/**
	 * Set an item's access levels
	 * 
	 * @param item an item
	 * 
	 * @param readLevelSource the numeric identifier of the 
	 * source of the new read level of the item
	 * 
	 * @param readLevelRef the the numeric identifier in its
	 * source of the new read level of the item
	 * 
	 * @param writeLevelSource the numeric identifier of the 
	 * source of the new write level of the item
	 * 
	 * @param writeLevelRef the numeric identifier in its source
	 * of the new write level of the item
	 */
	
	private void setItemLevels(Item item,
			long readLevelSource, long readLevelRef,
			long writeLevelSource, long writeLevelRef) {
		
		// Get the read level
		Item readLevelItem = itemsIndex.getOrAddTripleElement(
				new Id(readLevelSource, readLevelRef));
		AccessLevel onlyReadLevel = 
				accessLevelManager.getOrCreate(readLevelItem);
			
		// Get the write level
		Item writeLevelItem = itemsIndex.getOrAddTripleElement(
				new Id(writeLevelSource, writeLevelRef));
		AccessLevel readOrWriteLevel = 
				accessLevelManager.get(writeLevelItem);
		
		// Update the item
		item.setAccessLevels(onlyReadLevel, readOrWriteLevel);
	}
	
	/**
	 * Add a triple to synchronize with its source
	 * 
	 * @param sourceNr the numeric identifier of the source of the
	 * triple and its subject
	 * 
	 * @param tripleNr the numeric identifier of the triple in its
	 * source
	 * 
	 * @param subjectItemNr the numeric identifier of the subject 
	 * in its source
	 * 
	 * @param verbSourceNr the the numeric identifier of the source
	 * of the triple's verb
	 * 
	 * @param verbItemNr the numeric identifier of the verb in its
	 * source
	 * 
	 * @param object the triple's object
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private void syncAddNonItemTriple(
			long sourceNr,
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr, 
			long verbItemNr, 
			Datum object) 
					throws 	TripleStoreNonFatalException {
		
		// Get the triple's subject if it is in the index, 
		// or add it if it is not there already
		Item subject = itemsIndex.getOrAddTripleElement(
				new Id(sourceNr, subjectItemNr));
		
		// Get the verb if it is in the index, or add it
		// if it is not there already
		Item verb = itemsIndex.getOrAddTripleElement(
				new Id(verbSourceNr, verbItemNr));
		
		// Create the triple
		Triple t = new NonItemTriple(
					tripleNr, subject, verb, 
					object.type, object.getSummaryValue(), this);
		
		// Index the triple
		triplesIndex.indexTriple(t);
	}
	
	/**
	 * Add an item triple to synchronize with its source
	 * 
	 * @param sourceNr the numeric identifier of the source of the
	 * triple and its subject
	 * 
	 * @param tripleNr the numeric identifier of the triple in its
	 * source
	 * 
	 * @param subjectItemNr the numeric identifier of the subject in 
	 * its source
	 * 
	 * @param verbSourceNr the numeric identifier of the source of the
	 * triple's verb
	 * 
	 * @param verbItemNr the numeric identifier of the verb in its source
	 * 
	 * @param objectId the id of the triple's object
	 * 
	 * @param load true if the triple is loaded in a load operation, 
	 * false if in a sync operation
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * @throws NotInOperationException 
	 */
	
	private void syncAddItemTriple(
			long sourceNr,
			long tripleNr, 
			long subjectItemNr,
			long verbSourceNr, 
			long verbItemNr, 
			Id objectId,
			boolean load) 
					throws 	TripleStoreNonFatalException,
							SourceConnectorException,
							TripleStoreFatalException,
							NotInOperationException {

		// Get the triple's subject if it is in the index, 
		// or add it if it is not there already
		Id subjectId = new Id(sourceNr, subjectItemNr);
		Item subject = 
				itemsIndex.getOrAddTripleElement(subjectId);
		
		// Get the verb if it is in the index, or add it
		// if it is not there already
		Item verb = itemsIndex.getOrAddTripleElement(
				new Id(verbSourceNr, verbItemNr));
		
		// Get the object if it is in the index, or add it
		// if it is not there already
		Item objectItem = itemsIndex.getOrAddTripleElement(
					objectId);
		
		// Create the triple and index it
		ItemTriple t = new ItemTriple(
					tripleNr, subject, verb, objectItem, this);
		triplesIndex.indexTriple(t);
		
		// Check whether it is an access level triple or a
		// superior or inferior permission triple
		if (verbSourceNr == specialItemSource) {
			if (verbItemNr == isAccessLevelRelationNr) {
				// It is an access level triple
				// If this is the admin access level,
				// check its validity
				if (subjectItemNr == SourceConnector.adminLevelItemNr) {
					// It is the admin access level
					if (objectId.sourceNr != sourceNr) 
						// The source is being loaded with
						// the wrong source number
						throw (new TripleStoreNonFatalException(
								"Source loaded with incorrect source number"));
					else if (objectId.idNr != subjectItemNr)
						// The admin level has been replaced
						// This should not happen
						throw (new TripleStoreNonFatalException(
								"Source admin level has been replaced"));
				}
				if (objectId.equals(subjectId)) {
					// The triple specifies an access level. Invoke the
					// access level manager to process and index it
					accessLevelManager.loadAccessLevelTriple(t);
				}
				else {
					// It is a replacement triple
					accessLevelManager.setReplacementId(
							subjectId, objectId);
				}
			}

			else if (verbItemNr == superiorPermissionRelationNr) 
				// The triple specifies an access level superiority
				// permission
				accessLevelManager.addPermissionTriple(
						t, true, load);
			else if (verbItemNr == inferiorPermissionRelationNr) 
				// The triple specifies an access level inferiority
				// permission
				accessLevelManager.addPermissionTriple(
						t, false, load);
		}
	}
	
	/**
	 * Remove a triple to synchronize with its source
	 * 
	 * @param sourceNr the numeric identifier of the triple's
	 * source
	 * 
	 * @param tripleNr the numeric identifier of the triple
	 * in its source
	 * 
	 * @param load true if the triple is loaded in a load operation, 
	 * false if in a sync operation
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	private void syncRemoveTriple(
			long sourceNr, long tripleNr, boolean load)
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException {
		
		Triple t = triplesIndex.getTriple(new Id(sourceNr, tripleNr));
		if (t != null) {
			// If this triple specified an access level permission,
			// invoke the access level manager to handle the implications
			// of its removal
			if (t.objectType == Datum.Type.ITEM) {			
				if (t.getVerb().equals(superiorPermissionRelation)) 
					accessLevelManager.removePermissionTriple(
							(ItemTriple)t, true, load);
				else if (t.getVerb().equals(inferiorPermissionRelation))
					accessLevelManager.removePermissionTriple(
							(ItemTriple)t, false, load);
			}
			
			// De-index the triple
			triplesIndex.deIndexTriple(t);
		}
	}
	
	/*
	 ***********************************************************************
	 *
	 *  ACCESS LEVELS
	 *  
	 ***********************************************************************
	 */
	
	/*
	 * These methods manipulate access levels, which are
	 * maintained by the access level manager, and are
	 * described by items and relations held in the source
	 * so that they can be re-created following a re-start.
	 * They also enable creation, regeneration and deletion
	 * of access level credentials.
	 * 
	 * The methods may be invoked in the course of sessions,
	 * which themselves have access levels. The methods ensure
	 * in this case that:
     *   - the session level is superior to the access
     *     levels that are directly involved in operations
     *     performed within the session, 
     *   - any operation that is performed within a
     *     session can be reversed within that session or,
     *     in the case of retirement or deletion of an access
     *     level, a level with equivalent relationships
     *     can be created.
     *     
     * These rules facilitate the consistent operation of the
     * triple store with multiple users at different access
     * levels able to create and define relations between
     * further access levels.
	 */
	
	/**
	 * The item that represents the lowest access level
	 */
	
	private Item lowest;

	/**
	 * The item that represents the highest access level
	 */
	
	private Item highest;

	/**
	 * The item that is the verb such that the subject of a 
	 * triple with this verb represents an access level 
	 * if and only if the object of the triple is an item. 
	 * If the object is the same as the subject, then the 
	 * access level represented by the subject has not been 
	 * replaced. Otherwise, the access level represented
	 * by the subject has been replaced by the access
	 * level represented by the object.
	 */
	
	Item isAccessLevelRelation = null;
	
	/**
	 * The item that is the verb such that the subject of
	 * a triple with this verb represents an access level 
	 * that is permitted to be superior to that represented
	 * by the item that is the object of the triple.
	 */
	
	Item superiorPermissionRelation = null;
	
	/**
	 * The item that is the verb such that the subject of 
	 * a triple with this verb represents an access level 
	 * that is permitted to be inferior to that represented 
	 * by the item that is the object of the triple.
	 */
	
	Item inferiorPermissionRelation = null;

	/**
	 * The item that is the verb such that the subject of 
	 * a triple with this verb represents an access level 
	 * and the object is an item representing a credential
	 * for claiming the access level.
	 */
	
	Item hasCredentialRelation = null;

	/**
	 * The item that is the verb such that the subject of 
	 * a triple with this verb represents a credential for
	 * claiming the access level and the object is a digest 
	 * of the credential's secret.
	 */
	
	Item hasSecretDigestRelation = null;
	
	/**
	 * Random number generator used in generation of secrets
	 */
	
	private SecureRandom random = new SecureRandom();
	
	/**
	 * Get the item that is the verb such that the subject
	 * of a triple with this verb represents an access level. 
	 * If the object of the triple is the same as the subject,
	 * then the access level has not been replaced, otherwise 
	 * the access level represented by the subject has been
	 * replaced by the access level represented by the object.
	 * 
	 * @return the access level relation item
	 * 
	 */
	
	public Item getAccessLevelRelationItem() {
		
		return isAccessLevelRelation;
	}
	
	@Override
	public long getLowestAccessLevelNr() {

		return lowestLevelNr;
	}

	@Override
	public long getHighestAccessLevelNr() {

		return highestLevelNr;
	}

	/**
	 * Determine whether an access level is the highest 
	 * access level
	 * 
	 * @param level an access level
	 * 
	 * @return whether the given access level is the highest 
	 * access level
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public boolean isHighestAccessLevel(AccessLevel level)
			throws TripleStoreNonFatalException {
		
		return accessLevelManager.highest.hasSuperior(level);
	}
		
	/**
	 * Get the lowest access level
	 * 
	 * @return the lowest access level
	 * 
	 * @throws NotInOperationException if the triple store
	 * is not in operation
	 */
	
	public AccessLevel lowestAccessLevel()
			throws NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		return accessLevelManager.lowest;
	}
	
	/**
	 * <p>Create a new access level with given access levels
	 * </p>
	 * <p>The new access level will be stated to be inferior to
	 * the session level, to ensure that the operation can
	 * be reversed within the same session.
	 * </p>
	 * <p>The session must therefore have write access to its
	 * level item.
	 * </p>
	 * <p>The session may not be at the lowest access level,
	 * since this cannot be superior to another level.
	 * </p>
	 * @param sourceNr the numeric identifier of the source
	 * in which the access level is to be created.
	 * 
	 * @param onlyReadLevel the only-read access level that
	 * the new access level is to have
	 * 
	 * @param readOrWriteLevel the read-or-write access level
	 * that the new access level is to have
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the new access level, with its item unlocked.
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 */
	
	public AccessLevel createAccessLevel(
			long sourceNr, 
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			StoreSession session) 
					throws 	NotInOperationException, 
							SourceConnectorException,
							TripleStoreNonFatalException, 
							TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		AccessLevel level = null;
		level = accessLevelManager.makeAccessLevel(
				this,
				connectorsManager.getConnector(sourceNr), 
				onlyReadLevel,
				readOrWriteLevel,
				session, 
				false);
		return level;
	}
	
	/**
	 * <p>Create a new access level with given access levels, 
	 * and with its item locked.
	 * </p>
	 * <p>The new access level will be stated to be inferior to
	 * the session level, to ensure that the operation can
	 * be reversed within the same session.
	 * </p>
	 * <p>The session must therefore have write access to its
	 * level item.
	 * </p>
	 * <p>The session may not be at the lowest access level,
	 * since this cannot be superior to another level.
	 * </p>
	 * @param sourceNr the numeric identifier of the source
	 * in which the access level is to be created.
	 * 
	 * @param onlyReadLevel the only-read access level that
	 * the new access level is to have
	 * 
	 * @param readOrWriteLevel the read-or-write access level
	 * that the new access level is to have
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the new access level, with its item locked.
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 */
	
	public AccessLevel createLockedAccessLevel(
			long sourceNr, 
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			StoreSession session)
					throws 	NotInOperationException, 
							SourceConnectorException,
							TripleStoreNonFatalException, 
							TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		AccessLevel level = null;
		level = accessLevelManager.makeAccessLevel(
				this,
				connectorsManager.getConnector(sourceNr),
				onlyReadLevel,
				readOrWriteLevel,
				session, true);
		return level;
	}
	
	/**
	 * <p>Get or create a named access level from a source, with
	 * write access level set to the access level of the
	 * requesting session, and read access level set to
	 * the lowest level.
	 * </p>
	 * <p>The access level's item is related to a given name
	 * by the source item of the source. Only one such item
	 * can exist an any particular source. (The access level's 
	 * item is a named item like those created by the 
	 * namedItem method.)
	 * </p>
	 * @param sourceNr the numeric identifier of the source of
	 * the item
	 * 
	 * @param name the name for the access level. This forms
	 * the key by which the named access level or its item can 
	 * be retrieved.
	 * 
	 * @param session the store session requesting the operation.
	 * 
	 * @return the access level whose item is related to the
	 * given name by the source item of the source. If the access 
	 * level did not exist but a named item with the given name 
	 * did exist, or if it did exist but was not readable by the 
	 * requesting session, or if the first item could not be
	 * locked, or the triple defining the named item could not be
	 * created, then null is returned.
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 */
	
	public AccessLevel getOrCreateNamedAccessLevel(
			long sourceNr,
			String name,
			StoreSession session) 
					throws	NotInOperationException, 
							SourceConnectorException, 
							TripleStoreNonFatalException, 
							TripleStoreFatalException  {
		
		return getOrCreateNamedAccessLevel(
				sourceNr, 
				name,
				lowestAccessLevel(),
				session.getAccessLevel(),
				true,
				true,
				session);
	}
	
	/**
	 * <p>Create a named access level in a given source and with
	 * given only-read and read-or-write access levels.
	 * </p>
	 * <p>The access level's item is related to a given name
	 * by the source item of the source. Only one such item
	 * can exist an any particular source. (The access level's 
	 * item is a named item like those created by the 
	 * namedItem method.)
	 * </p>
	 * @param sourceNr the numeric identifier of the source in
	 * which the access level is to be created.
	 * 
	 * @param name the name for the access level. This forms
	 * the key by which the named access level or its item can 
	 * be retrieved.
	 * 
	 * @param onlyReadLevel the only-read level of the access
	 * level to be created
	 * 
	 * @param readOrWriteLevel the read-or-write level of the access
	 * level to be created
	 * 
	 * @param session the store session requesting the operation.
	 * 
	 * @return the access level whose item is related to the
	 * given name by the source item of the source if it was created,
	 * null otherwise. If an item (or access level) with the given name 
	 * existed, or if the first item could not be locked, or the triple
	 * defining the named item could not be created, then the access
	 * level is not created and null is returned.
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 */
	
	public AccessLevel createNamedAccessLevel(
			long sourceNr,
			String name,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			StoreSession session) 
					throws	NotInOperationException, 
							SourceConnectorException, 
							TripleStoreNonFatalException, 
							TripleStoreFatalException  {
		
		return getOrCreateNamedAccessLevel(
				sourceNr,
				name,
				onlyReadLevel,
				readOrWriteLevel,
				false, 
				true,
				session);
	}
	
	/**
	 * <p>Get or create a named access level from a source
	 * with given only-read and read-or-write access levels.
	 * </p>
	 * <p>The access level's item is related to a given name
	 * by the source item of the source. Only one such item
	 * can exist an any particular source. (The access level's 
	 * item is a named item like those created by the 
	 * namedItem method.)
	 * </p>
	 * <p>The created level will be made inferior to the access
	 * level of the requesting session. To enable this, the
	 * access level item of the requesting session MUST be locked
	 * when this method is invoked.
	 * </p>
	 * @param sourceNr the numeric identifier of the source of
	 * the access level
	 * 
	 * @param name the name for the access level. This forms
	 * the key by which the named access level or its item can 
	 * be retrieved.
	 * 
	 * @param onlyReadLevel the only-read level of the access
	 * level to be created
	 * 
	 * @param readOrWriteLevel the read-or-write level of the access
	 * level to be created
	 * 
	 * @param getIfExists whether the item with the given name
	 * is to be retrieved if one exists already. If this is set
	 * to false and an item with the given name already exists,
	 * the method will return null and not create a new item.
	 * 
	 * @param createIfNotExists whether the access level is
	 * to be created if it does not exist already
	 * 
	 * @param session the store session requesting the operation.
	 * 
	 * @return the access level whose item is related to the
	 * given name by the source item of the source. If the access 
	 * level did not exist and createIfNotExists was false, or 
	 * if it did not exist but a named item with the given name 
	 * did exist, or if it did exist but was not readable by the 
	 * requesting session, or if the first item could not be
	 * locked, or the triple defining the named item could not be
	 * created, then null is returned.
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * 
	 */
	
	private AccessLevel getOrCreateNamedAccessLevel(
			long sourceNr,
			String name,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			boolean getIfExists, 
			boolean createIfNotExists,
			StoreSession session) 
					throws	NotInOperationException, 
							SourceConnectorException, 
							TripleStoreNonFatalException, 
							TripleStoreFatalException  {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		SourceConnector connector = 
				connectorsManager.getConnector(sourceNr);
		if (connector == null)
			throw(new TripleStoreNonFatalException(
					"Source " + sourceNr + " is not connected."));
		
		// Get the source item
		Item sourceItem = itemsIndex.get(
				new Id(sourceNr, SourceConnector.sourceItemNr));
		if (sourceItem == null) {
			disconnect(connector);
			throw (new TripleStoreNonFatalException(
					"Cannot find source item for source " + sourceNr + 
					" - source closed"));
		}
		
		// Create a session at the highest access level
		StoreSession masterSession = 
				new StoreSession(accessLevelManager.highest);
		
		// Lock the first item of the source
		if (!sourceItem.lock(masterSession)) {
			return null;
		}
		try {
			
			// Find the named item with the given name, if it exists. 
			// Need to check items with all levels of visibility.
			List<Triple> triples = rawGetTriples(
					null, 
					sourceItem, 
					new StringDatum(name), 
					masterSession);
			Triple definingTriple = null;
			for (Triple t : triples) {
				if (t.getSourceNr() == sourceNr) {
					if (definingTriple == null)
						definingTriple = t;
					else {
						// Duplicate triples exist for this source
						// and name
						throw new TripleStoreNonFatalException(
								"Named item triple not unique");
					}
				}
			}
			
			if (definingTriple == null) {
				// The named item does not exist
				
				if (createIfNotExists) {
					// Create and name a new access level
					
					// Create the access level, with its item locked
					AccessLevel newLevel = 
							createLockedAccessLevel(
									sourceNr, 
									onlyReadLevel,
									readOrWriteLevel,
									session);
					if (newLevel == null) {
						return null;
					}
					try {
						// Relate the access level item to its name
						createTripleUnchecked(
								newLevel.levelItem, 
								sourceItem,
								new StringDatum(name),
								connector,
								newLevel.levelItem.getTransaction());
						
						// Return the new level
						return newLevel;
					} finally {
						// Unlock the item of the new level
						newLevel.levelItem.unlock(session);
					}
				}
				else return null;
			}
			else {
				// The named item exists
				
				if (getIfExists) 
					// Return the access level of which it is the item if there
					// is one, if it is visible to the requesting session, and
					// if the requesting session has superior access level
						return getAccessLevel(definingTriple.subject, session);
				else return null;
			}
		}
		finally {
			// Unlock the first item of the source
			sourceItem.unlock(masterSession);
		}
	}
	
	/**
	 * Get the access level represented by an item
	 * 
	 * @param item the identifying item
	 * 
	 * @param session the requesting store session, which
	 * must have read access to the item.
	 * 
	 * @return the access level represented by the given
	 * item, or null if there is no access level represented 
	 * by the item, or if the requesting session does not 
	 * have read access to it.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public AccessLevel getAccessLevel(Item item, StoreSession session)
			throws 	NotInOperationException, 
					TripleStoreNonFatalException {

		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!item.isCurrent())
			throw(new TripleStoreNonFatalException(
					"Item not current"));
		if (item.store != this)
			throw(new TripleStoreNonFatalException(
					"Item does not belong to store"));
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		
		if (!item.canBeReadBy(session))
			return null;
		AccessLevel level = accessLevelManager.get(item);
		return level;
	}
	
	/**
	 * <p>Get the access level represented by an item
	 * </p>
	 * <p>This method is used by other classes in the package
	 * </p>
	 * @param item the representing item
	 * 
	 * @return the access level represented by the given
	 * item, or null if there is no access level
	 * represented by the item.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	AccessLevel getAccessLevel(Item item)
			throws 	TripleStoreNonFatalException {

		if (item.getState() != Item.State.EXISTS)
			throw(new TripleStoreNonFatalException(
					"Item not current"));
		if (item.store != this)
			throw(new TripleStoreNonFatalException(
					"Item does not belong to store"));
		return accessLevelManager.get(item);
	}
	
	/**
	 * Get the access levels visible to a store session
	 * 
	 * @param session a store session
	 * 
	 * @return a list of the access levels that can be
	 * read by the session. The list is not in any
	 * particular order.
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public List<AccessLevel> accessLevels(StoreSession session) 
			throws TripleStoreNonFatalException {
		
		List<AccessLevel> result = new ArrayList<AccessLevel>();
		for (AccessLevel l : accessLevelManager.getLevels()) {
			if (l.canBeReadBy(session)) result.add(l);
		}
		return result;
	}
	
	/**
	 * <p>Permit one access level to be superior to another.
	 * If the other is already permitted to be the inferior 
	 * of the pair, make a stated superiority relationship 
	 * between them.
	 * </p>
	 * <p>The requesting store session must have locked the level 
	 * that is permitted to be the superior one, and have read 
	 * access to the other. Neither level may be being replaced.
	 * </p>
	 * @param superior the level that is permitted to be the
	 * superior one
	 * 
	 * @param inferior the level that the superior is permitted
	 * to be superior to
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the permission was accepted, false
	 * otherwise. The permission will not be accepted if
	 * one of the relations is the predefined lowest or
	 * predefined highest level, or if the requesting store
	 * session does not have write access to the superior
	 * level, or does not have read access to the inferior
	 * level. If the superior level was already permitted 
	 * to be superior to the other one, then true is returned
	 * and no change is made.
	 * 
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException
	 */
	
	public boolean setSuperiorPermission(
			AccessLevel superior, 
			AccessLevel inferior,
			StoreSession session) 
					throws 	NotInOperationException, 
							TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException {

		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((superior == null) || (inferior == null))
			return false;
		if (!superior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		if (!inferior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));

		return accessLevelManager.setSuperiorPermission(
				superior, inferior, session);
	}
	
	/**
	 * <p>Permit one access level to be inferior to another.
	 * If the other is already permitted to be the superior 
	 * of the pair, make a stated superiority relationship
	 * between them.
	 * </p>
	 * <p>The requesting store session must have locked the level that
	 * is permitted to be the inferior one, and have read 
	 * access to the other. Neither level may be being replaced.
	 * </p>
	 * @param inferior the level that is permitted to be the
	 * inferior one
	 * 
	 * @param superior the level that the inferior is permitted
	 * to be inferior to
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the permission was accepted, false
	 * otherwise. The permission will not be accepted if
	 * one of the relations is the predefined lowest or
	 * predefined highest level, or if the requesting store
	 * session does not have write access to the inferior 
	 * level, or does not have read access to the superior
	 * level. If the inferior level was already permitted 
	 * to be inferior to the other one, then true is returned
	 * and no change is made.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException
	 * 
	 */
	
	public boolean setInferiorPermission(
			AccessLevel inferior, 
			AccessLevel superior,
			StoreSession session) 
					throws 	NotInOperationException, 
							TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException {

		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((inferior == null) || (superior == null))
			return false;
		if (!inferior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		if (!superior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		return accessLevelManager.setInferiorPermission(
				inferior, superior, session);
	}
	
	/**
	 * <p>Remove a permission that an access level can be superior
	 * to another. Any stated relationship that the level is
	 * superior to the other is removed also.
	 * </p>
	 * <p>The requesting store session must have locked the superior 
	 * level and have read access to the other level, and must
	 * continue to have write access to the superior level and 
	 * read access to the other level once the permission has been
	 * removed. Neither level may be being replaced. 
	 * </p>
	 * @param superior the level that is permitted to be
	 * superior to the other one
	 * 
	 * @param inferior the level that the superior level is
	 * permitted to be superior to
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the removal was accepted, false
	 * otherwise. The removal will not be accepted if the 
	 * superior relation is not permitted to be superior 
	 * to the other one, or if one of the relations is the 
	 * predefined lowest or predefined highest level, or if 
	 * the requesting store session does not have write 
	 * access to to the superior level, or does not have 
	 * read access to the other level, or if the requesting 
	 * store session would not continue to have write access 
	 * to the superior and read access to the other level 
	 * after the operation.
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException  
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * 
	 */
	
	public boolean unsetSuperiorPermission(
			AccessLevel superior, 
			AccessLevel inferior,
			StoreSession session) 
					throws 	NotInOperationException, 
					TripleStoreNonFatalException, 
					TripleStoreFatalException,
					SourceConnectorException  {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((superior == null) || (inferior == null))
			return false;
		if (!superior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		if (!inferior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));		
			
		// Remove the relationship
		return accessLevelManager.unsetSuperiorPermission(
				superior, inferior, session);
	}
	
	/**
	 * <p>Remove a permission that an access level can be inferior
	 * to another. Any stated relationship that the other level 
	 * is superior to the level is removed also.
	 * </p>
	 * <p>The requesting store session must have locked the inferior 
	 * level and have read access to the other level, and must 
	 * continue to have write access to the inferior level and 
	 * read access to the other level once the permission has been 
	 * removed. Neither level may be being replaced. 
	 * </p>
	 * @param inferior the level that is permitted to be
	 * inferior to the other one
	 * 
	 * @param superior the level that the inferior level is
	 * permitted to be inferior to
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the removal was accepted, false otherwise. 
	 * The removal will not be accepted if the inferior relation 
	 * is not permitted to be inferior to the other one, or if 
	 * one of the relations is the predefined lowest or predefined
	 * highest level, or if the requesting store session does not 
	 * have write access to to the inferior level, or does not have 
	 * read access to the other level, or if the requesting store
	 * session would not continue to have write access to the 
	 * inferior and read access to the other level after the 
	 * operation.
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException  
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * 
	 */
	
	public boolean unsetInferiorPermission(
			AccessLevel inferior, 
			AccessLevel superior,
			StoreSession session) 
					throws 	NotInOperationException, 
					TripleStoreNonFatalException, 
					TripleStoreFatalException,
					SourceConnectorException  {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((inferior == null) || (superior == null))
			return false;
		if (!inferior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		if (!superior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
			
			// Remove the relationship
			return accessLevelManager.unsetInferiorPermission(
					inferior, superior, session);
	}
	
	/**
	 * Determine whether one access level is stated to be
	 * superior to another
	 * 
	 * @param superior the possibly superior level
	 * 
	 * @param inferior the possibly inferior level
	 * 
	 * @return true if the possibly superior level is really 
	 * stated to be superior to the possibly inferior one,
	 * false otherwise
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 * 
	 */
	
	public boolean isStatedAccessRelationship(
			AccessLevel superior, 
			AccessLevel inferior) 
					throws 	NotInOperationException, 
					TripleStoreNonFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((superior == null) || (inferior == null))
			return false;
		if (!superior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		if (!inferior.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));

		return accessLevelManager.isStatedSuperiorTo(
				superior, inferior);
	}
	
	/**
	 * List the implied access level relationships that are
	 * readable at the session access level
	 * 
	 * @param session the store session requesting the
	 * operation
	 * 
	 * @return an unsorted list of the implied access level
	 * relationships for which both elements are readable
	 * by the session.
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 * 
	 */
	
	public List<OrderedPair<AccessLevel>>
			impliedAccessLevelRelationships(StoreSession session)
					throws 	NotInOperationException, 
							TripleStoreNonFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		List<OrderedPair<AccessLevel>> l = 
				new ArrayList<OrderedPair<AccessLevel>>();
		for (OrderedPair<AccessLevel> p : 
				accessLevelManager.impliedRelationships()) {
			if (p.firstElement.canBeReadBy(session) &&
				p.secondElement.canBeReadBy(session)) l.add(p);
		}
		return l;
	}

	/**
	 * Get the superiority permissions and relations that are visible 
	 * to a store session
	 * 
	 * @param session a store session
	 * 
	 * @return the superiority permissions and relations
	 * that are visible to the given store session
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	public AccessLevelRelationsStatus
			getAccessLevelRelationsStatus(
					StoreSession session)
					throws 	TripleStoreNonFatalException, 
							TripleStoreFatalException, 
							SourceConnectorException,
							NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");

		AccessLevelRelationsStatus rawStatus =
				accessLevelManager.getAccessLevelRelationsStatus(
						this);
		AccessLevelRelationsStatus result =
				new AccessLevelRelationsStatus();
		
		// Filter the explicit relationships for session visibility
		Set<OrderedPair<AccessLevel>> explicitRelationships =
				new HashSet<OrderedPair<AccessLevel>>();
		for (OrderedPair<AccessLevel> p :
			rawStatus.explicitRelationships) {
			if (p.firstElement.canBeReadBy(session) &&
				p.secondElement.canBeReadBy(session))
				explicitRelationships.add(p);
		}
		result.explicitRelationships = explicitRelationships;
		
		// Filter the implicit relationships for session visibility
		Set<OrderedPair<AccessLevel>> implicitRelationships =
				new HashSet<OrderedPair<AccessLevel>>();
		for (OrderedPair<AccessLevel> p :
			rawStatus.implicitRelationships) {
			if (p.firstElement.canBeReadBy(session) &&
				p.secondElement.canBeReadBy(session))
				implicitRelationships.add(p);
		}
		result.implicitRelationships = implicitRelationships;
		
		// Filter the superior permissions for session visibility
		Set<OrderedPair<AccessLevel>> superiorPermissions =
				new HashSet<OrderedPair<AccessLevel>>();
		for (OrderedPair<AccessLevel> p :
			rawStatus.superiorPermissions) {
			if (p.firstElement.canBeReadBy(session) &&
				p.secondElement.canBeReadBy(session))
				superiorPermissions.add(p);
		}
		result.superiorPermissions = superiorPermissions;
		
		// Filter the inferior permissions for session visibility
		Set<OrderedPair<AccessLevel>> inferiorPermissions =
				new HashSet<OrderedPair<AccessLevel>>();
		for (OrderedPair<AccessLevel> p :
			rawStatus.inferiorPermissions) {
			if (p.firstElement.canBeReadBy(session) &&
				p.secondElement.canBeReadBy(session))
				inferiorPermissions.add(p);
		}
		result.inferiorPermissions = inferiorPermissions;
		
		// Return the result
		return result;
	}

	/**
	 * Get the set of access levels that are superior to a given level 
	 * and that are visible to the requesting store session
	 * 
	 * @param level an access level
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the set of access levels that are superior to the
	 * given access level and visible to the store session
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws NotInOperationException 
	 */
	
	public Set<AccessLevel> getVisibleSuperiors(
			AccessLevel level, StoreSession session) 
					throws 	TripleStoreNonFatalException, 
							NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		Set<AccessLevel> result = new HashSet<AccessLevel>();
		for (AccessLevel l : accessLevelManager.getSuperiors(level)) {
			try {
				if (l.canBeReadBy(session)) result.add(l);
			}
			catch (Exception e) {}
		}
		return result;	
	}
	
	/**
	 * Get the set of access levels that are inferior to a given level 
	 * and that are visible to the requesting store session
	 * 
	 * @param level an access level
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the set of access levels that are superior to 
	 * the given access level and visible to the store session
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws NotInOperationException 
	 */
	
	public synchronized Set<AccessLevel> getVisibleInferiors(
			AccessLevel level, StoreSession session) 
					throws 	TripleStoreNonFatalException, 
							NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		Set<AccessLevel> result = new HashSet<AccessLevel>();
		for (AccessLevel l : accessLevelManager.getInferiors(level)) {
			try {
				if (l.canBeReadBy(session)) {
					result.add(l);
				}
			}
			catch (Exception e) {}
		}
		return result;	
	}
	
	/**
	 * <p>Retire an access level and replace it by by another.
	 * </p>
	 * <p>The requesting session must have locked the retired
	 * access level. The retired level must not be the 
	 * highest or lowest access level, and must not be the 
	 * same as the access level of the requesting session. 
	 * The replacement may not be the highest access level. 
	 * It may be the lowest access level. If it is not, the
 	 * requesting session must have locked it. It must not
 	 * be the same as the access level that it is replacing,
 	 * and must not itself have been replaced. 
	 * </p>
	 * <p>Retirement of an access level implies the removal of
	 * its stated relationships, the deletion of its representing
	 * item, and the consequent deletion of all triples of which
	 * that item is subject, verb, or object. Note that the same
	 * session may not be able to create another level with the
	 * same relationships as the retired level.
	 * </p>
	 * <p>Testing whether one access level is superior to another
	 * is a frequently-used operation. It is performed without
	 * using code that is synchronized on the access levels in
	 * question, to avoid deadlocks, and to improve
	 * performance. The retirement mechanism must work under
	 * these circumstances.
	 * </p>
	 * <p>When an access level is retired, its "replacement"
	 * field is set to reference its replacement. This is an
	 * atomic operation. The replacement is used in place of
	 * the level when the only-read or the read-or-write
	 * level of an item has been retired, and access to the
	 * item is checked.
	 * </p>
	 * <p>If the access level of a session is replaced, the
	 * replacement field is not used when checking whether the
	 * session can read or write an item. Instead, a
	 * TripleStoreNonFatalException is thrown.
	 * </p>
	 * <p>Once the "replacement" field has been changed, the 
	 * access level remains usable for a considerable time. 
	 * A thread that has tested the access level and 
	 * determined that it has not been retired should 
	 * encounter no problems if the level is retired while
	 * the thread is using it.
	 * </p>
	 * <p>Changes to item read and write levels, and access 
	 * level replacements, are all done by threads that have
	 * obtained the access levels lock. An item can therefore
	 * not have its read or write access level set to a level
	 * that has been retired.
	 * </p>
	 * <p>A background task goes through the items and replaces
	 * retired only-read and read-or-write levels with their
	 * replacements. By this means, all references in the
	 * triple store to the retired level are eventually removed.
	 * </p>
	 * @param level the access level to be replaced.
	 * This level will be deleted once it has been replaced.
	 * 
	 * @param replacement the replacement access level
	 * 
	 * @param session the requesting session
	 * 
	 * @return true if the operation was performed, false
	 * otherwise
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public boolean retireAccessLevel(
			AccessLevel level, 
			AccessLevel replacement, 
			StoreSession session) 
					throws 	NotInOperationException, 
							TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException {
	
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!level.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		if (!replacement.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		
		return accessLevelManager.replaceAccessLevel(
					level, replacement, session); 
	}
	
	/**
	 * Create a credential for an access level. The credential
	 * will be represented by a new item in the same source as
	 * the access level.
	 * 
	 * @param level an access level
	 * 
	 * @param session the requesting store session, whose access level
	 * must be superior to the admin access level of the source of the
	 * access level that will have the credential
	 * 
	 * @return the item representing the created credential, or null
	 * if a credential cannot be created.
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 * @throws TripleStoreFatalException
	 */
	
	public Item createCredential(
			AccessLevel level, 
			StoreSession session) 
					throws 	NotInOperationException, 
							TripleStoreNonFatalException,
							SourceConnectorException, 
							TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!level.isOfTripleStore(this))
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		
		// Get the source number and admin level of the 
		// access level requiring the credential
		long sourceNr = level.levelItem.sourceNr;		
		AccessLevel sourceAdminLevel = getAdminAccessLevel(sourceNr);
		
		// Check that the requesting session has source admin rights
		if (!sourceAdminLevel.hasSuperior(session.getAccessLevel()))
			throw(new TripleStoreNonFatalException(
					"Inadequate access level"));
		
		// Create a session at the highest access level
		StoreSession masterSession = 
				new StoreSession(accessLevelManager.highest);

		// Create an item to represent the credential in the
		// source of the access level, publicly readable,
		// and writable only by the highest level
		Item credential = createLockedItem(
				sourceNr, accessLevelManager.lowest, 
				accessLevelManager.highest, masterSession);

		try {
			// Create a triple associating the credential with
			// the access level			
			createTripleUnchecked(
					level.levelItem, 
					hasCredentialRelation, 
					Datum.create(credential), 
					connectorsManager.getConnector(sourceNr), 
					credential.getTransaction());
			return credential;
		} catch (Exception e) {
			deleteItem(credential, masterSession);
			return null;
		}
		finally {
			if (credential.isCurrent()) credential.unlock(masterSession);
		}
	}
	
	/**
	 * Generate a secret for a credential, and store its digest as the
	 * unique secret digest for the credential
	 * 
	 * @param credential a credential
	 * 
	 * @param session the requesting store session, whose access level
	 * must be superior to the admin access level of the source of the
	 * credential
	 * 
	 * @return the created secret, or null if its digest cannot be stored.
	 * The previous secret may no longer be valid if null is returned.
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 * @throws TripleStoreFatalException
	 */
	
	public byte[] generateSecret(Item credential, StoreSession session)
			throws 	NoSuchAlgorithmException, 
					NotInOperationException, 
					TripleStoreNonFatalException, 
					SourceConnectorException, 
					TripleStoreFatalException {
		
		// Get the source number and admin level of the credential
		long sourceNr = credential.sourceNr;		
		AccessLevel sourceAdminLevel = getAdminAccessLevel(sourceNr);
		
		// Check that the requesting session has source admin rights
		if (!sourceAdminLevel.hasSuperior(session.getAccessLevel()))
			throw(new TripleStoreNonFatalException(
					"Inadequate access level"));
		
		// Create a session at the highest access level
		StoreSession masterSession = 
				new StoreSession(accessLevelManager.highest);		

		// Lock the credential
		if (!credential.lock(masterSession)) 
			throw(new TripleStoreNonFatalException(
					"The credential is locked by another session"));

		try {		
			byte[] secret = new byte[16];
			random.nextBytes(secret);
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] digest = md.digest(secret);
			// Delete any existing secret triples.
			// NB. can't use setUniqueObject as this won't delete
			// triples whose verbs are special items.
			List<Triple> triples = getTriples(
					credential, hasSecretDigestRelation, null, masterSession);
			SourceConnector connector = 
					connectorsManager.getConnector(credential.sourceNr);
			if (connector == null) throw(new TripleStoreNonFatalException(
					"The source is not connected"));
			Transaction trans = credential.getTransaction();
			for (Triple t : triples)
					deleteTripleUnchecked(t, trans);
			Triple t = this.createTripleUnchecked(
					credential, hasSecretDigestRelation, 
					Datum.createBinary(digest), connector, trans);
			if (t == null) return null;
			return secret;
		}
		finally {
			if (credential.isCurrent()) credential.unlock(masterSession);
		}
	}
	
	/**
	 * Create a store session with the access level that has a credential
	 * 
	 * @param credential a credential
	 * 
	 * @param secret the credential's secret
	 * 
	 * @param requestingSession
	 * 
	 * @return a store session with the access level that has the credential,
	 * or null if the secret does not match the secret stored for the
	 * credential or the access level cannot be found
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 * @throws NoSuchAlgorithmException
	 */
	
	public StoreSession createStoreSession(
			Item credential, byte[] secret, StoreSession requestingSession) 
					throws 	TripleStoreNonFatalException, 
							TripleStoreFatalException, 
							SourceConnectorException, 
							NotInOperationException, 
							NoSuchAlgorithmException {
				
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		
		// Create a session at the highest access level
		StoreSession masterSession = 
				new StoreSession(accessLevelManager.highest);		

		// Get the digest of the supplied secret
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] digest = md.digest(secret);
		
		// Get the access level that has the credential
		List<Triple> triples = getTriples(
				null, hasCredentialRelation, Datum.create(credential), masterSession);
		if (triples.isEmpty()) return null;
		if (triples.size() > 1)	throw(new TripleStoreNonFatalException(
				"Multiple access levels have the credential"));
		Item levelItem = triples.get(0).subject;
		AccessLevel level = getAccessLevel(levelItem);
		if (level == null) return null;

		// Get the credential's secret digest
		triples = getTriples(
				credential, hasSecretDigestRelation, null, masterSession);
		if (triples.isEmpty()) return null;
		if (triples.size() > 1)	throw(new TripleStoreNonFatalException(
				"The credential has multiple secret digests"));
		byte[] secretDigest = 
				triples.get(0).getObject(masterSession).getBinaryValueAsArray();

		// Check that the digest matches
		int len = digest.length;
		if (secretDigest.length != len) return null;
		for (int i = 0; i < len; i++) {
			if (digest[i] != secretDigest[i]) return null;
		}
		
		// Return a session with the access level that has the credential
		return new StoreSession(level);
	}
	
	/**
	 * Delete a credential
	 * 
	 * @param credential the credential to be deleted
	 * 
	 * @param session the requesting store session, whose access level
	 * must be superior to the admin access level of the source of the
	 * credential
	 * 
	 * @return true if the credential is deleted, false otherwise
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 * @throws TripleStoreFatalException
	 */
	
	public boolean deleteCredential(
			Item credential, 
			StoreSession session) 
					throws 	NotInOperationException, 
							TripleStoreNonFatalException,
							SourceConnectorException, 
							TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		
		// Get the source number and admin level of the credential
		long sourceNr = credential.sourceNr;		
		AccessLevel sourceAdminLevel = getAdminAccessLevel(sourceNr);
		
		// Check that the requesting session has source admin rights
		if (!sourceAdminLevel.hasSuperior(session.getAccessLevel()))
			throw(new TripleStoreNonFatalException(
					"Inadequate access level"));
		
		// Create a session at the highest access level
		StoreSession masterSession = 
				new StoreSession(accessLevelManager.highest);		

		// Lock the credential
		if (!credential.lock(masterSession)) 
			throw(new TripleStoreNonFatalException(
					"The credential is locked by another session"));

		try {
			// Delete the credential
			return deleteItem(credential, masterSession);
		}
		finally {
			if (credential.isCurrent()) credential.unlock(masterSession);
		}
	}
	
	/*
	 ***********************************************************************
	 *
	 *  ITEMS
	 *  
	 ***********************************************************************
	 */
	
	/**
	 * The special item source number.
	 * The special items do not exist in any source.
	 * Their ids have zero as source number.
	 */
	
	public static final long specialItemSource = 0;
	
	/**
	 * The numeric identifier of the special item representing
	 * the lowest access level
	 */
	
	public static final long lowestLevelNr = 1;
	
	/**
	 * The numeric identifier of the special item representing
	 * the highest access level
	 */
	
	public static final long highestLevelNr = 2;
	
	/**
	 * The numeric identifier of the special item representing
	 * the "is access level relation" verb
	 */
	
	public static final long isAccessLevelRelationNr = 3;
	
	/**
	 * The numeric identifier of the special item representing
	 * the superior permission relation verb
	 */
	
	public static final long superiorPermissionRelationNr = 4;
	
	/**
	 * The numeric identifier of the special item representing
	 * the inferior permission relation verb
	 */
	
	public static final long inferiorPermissionRelationNr = 5;
	
	/**
	 * The numeric identifier of the special item representing
	 * the "has credential" verb
	 */
	
	public static final long hasCredentialRelationNr = 6;
	
	/**
	 * The numeric identifier of the special item representing
	 * the "has secret digest" verb
	 */
	
	public static final long hasSecretDigestRelationNr = 7;
	
	/**
	 * The identifier of the special item representing
	 * the lowest access level
	 */
	
	final Id lowestLevelId = new Id(specialItemSource, lowestLevelNr);
	
	/**
	 * The identifier of the special item representing
	 * the highest access level
	 */
	
	final Id highestLevelId = new Id(specialItemSource, highestLevelNr);
	
	/**
	 * The identifier of the special item representing
	 * the "is access level relation" verb
	 */
	
	final Id isAccessLevelRelationId = new Id(specialItemSource, isAccessLevelRelationNr);
	
	/**
	 * The identifier of the special item representing
	 * the superior permission relation verb
	 */
	
	final Id superiorPermissionRelationId = new Id(specialItemSource, superiorPermissionRelationNr);
	
	/**
	 * The identifier of the special item representing
	 * the inferior permission relation verb
	 */
	
	final Id inferiorPermissionRelationId = new Id(specialItemSource, inferiorPermissionRelationNr);
	
	/**
	 * The identifier of the special item representing
	 * the "has credential" verb
	 */
	
	final Id hasCredentialRelationId = new Id(specialItemSource, hasCredentialRelationNr);
	
	/**
	 * The identifier of the special item representing
	 * the "has secret digest" verb
	 */
	
	final Id hasSecretDigestRelationId = new Id(specialItemSource, hasSecretDigestRelationNr);
	
	@Override
	public long getSpecialItemSourceNr() {

		return specialItemSource;
	}

	@Override
	public long getIsAccessLevelRelationNr() {

		return isAccessLevelRelationNr;
	}

	/**
	 * The items index
	 */
	
	ItemsIndex itemsIndex = new ItemsIndex(this);

	/**
	 * Create a new item and return it unlocked
	 * 
	 * @param connector the connector to the source in which 
	 * the item is to be written
	 * 
	 * @param onlyReadLevel the only-read level that the item
	 * is to have
	 * 
	 * @param readOrWriteLevel the read-or-write level that the
	 * item is to have
	 * 
	 * @param session the store session requesting creation of
	 * the new item. 
	 * 
	 * @return the item that is created, unlocked, or null if it
	 * cannot be created
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * 
	 */
	
	public Item createUnlockedItem(
			SourceConnector connector,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			StoreSession session) 
					throws 	NotInOperationException, 
							SourceConnectorException,
							TripleStoreNonFatalException,
							TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		Item it = alwaysCreateLockedItem(
				connector, onlyReadLevel, readOrWriteLevel, session);
		if (it == null) return null;
		it.unlock(session);
		return it;
	}
	
	/**
	 * Create a new item and return it locked
	 * 
	 * @param sourceNr the numeric identifier of the source in
	 * which the item is to be written
	 * 
	 * @param onlyReadLevel the only-read level that the item
	 * is to have
	 * 
	 * @param readOrWriteLevel the read-or-write level that the
	 * item is to have
	 * 
	 * @param session the store session requesting creation of
	 * the new item. 
	 * 
	 * @return the item that is created, locked, or null if it
	 * cannot be created
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * 
	 */
	
	public Item createLockedItem(
			long sourceNr,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			StoreSession session) 
					throws 	NotInOperationException, 
							SourceConnectorException, 
							TripleStoreNonFatalException,
							TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		SourceConnector connector =
				connectorsManager.getConnector(sourceNr);
		if (connector == null)
			throw(new TripleStoreNonFatalException(
					"Source " + sourceNr + " is not connected"));
		return alwaysCreateLockedItem(
				connector, onlyReadLevel, readOrWriteLevel, session);
	}
	
	/**
	 * <p>Get a named item in a source, creating it if it does not exist.
	 * If a new named item is created, it has read-or-write access 
	 * level set  to the access level of the requesting session, and
	 * only-read access level set to the lowest level.
	 * </p>
	 * <p>The item is related to a given name by the source item of the 
	 * source. Only one such item can exist in any particular source. 
	 * </p>
	 * @param sourceNr the numeric identifier of the source of the item.
	 * 
	 * @param name the name for the item. This forms the key by which 
	 * the named item can be retrieved.
	 * 
	 * @param session the store session requesting the operation.
	 * 
	 * @return the item that is related to the given name by the source 
	 * item of the given source. If the item exists but is not visible 
	 * to the requesting session, or if the source item cannot be locked, 
	 * or the triple defining the named item could not be created, then 
	 * null is returned.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * 
	 */
	
	public Item getOrCreateNamedItem(
			long sourceNr,
			String name,
			StoreSession session) 
					throws 	NotInOperationException, 
							SourceConnectorException, 
							TripleStoreFatalException,
							TripleStoreNonFatalException {
		
		return getOrCreateNamedItem(
				sourceNr,
				name,
				lowestAccessLevel(), 
				session.getAccessLevel(), 
				true,
				true,
				session);
	}
	
	/**
	 * <p>Create a named item in a source, with access levels set to given 
	 * values.
	 * </p>
	 * <p>The item is related to a given name by the source item of the source.
	 * Only one such item can exist in any particular source. 
	 * </p>
	 * @param sourceNr the numeric identifier of the source of the item.
	 * 
	 * @param name the name for the item. This forms the key by which 
	 * the named item can be retrieved.
	 * 
	 * @param onlyReadLevel the only-read access level that the newly-created 
	 * item is to have
	 * 
	 * @param readOrWriteLevel the read-or-write access level that the newly-
	 * created item is to have.
	 * 
	 * @param session the store session requesting the operation.
	 * 
	 * @return a newly-created item that is related to a given name by the 
	 * source item of the triple store, if such an item does not already 
	 * exist. If such an item does exist, or if the source item cannot be 
	 * locked, or the triple defining the named item could not be created, 
	 * then null is returned.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * 
	 */
	
	public Item createNamedItem(
			long sourceNr,
			String name,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			StoreSession session) 
					throws 	NotInOperationException, 
							SourceConnectorException, 
							TripleStoreFatalException,
							TripleStoreNonFatalException {
		
		return getOrCreateNamedItem(
				sourceNr,
				name,
				onlyReadLevel,
				readOrWriteLevel,
				false,
				true,
				session);
	}
	
	/**
	 * <p>Get a named item in a source, creating it if it does not exist. 
	 * If a new named item is created, it has access levels set to 
	 * values supplied as arguments to the method.
	 * </p>
	 * <p>The item is related to a given name by the source item of the 
	 * source. Only one such item can exist in any particular source. 
	 * </p>
	 * @param sourceNr the numeric identifier of the source of the item.
	 * 
	 * @param name the name for the item. This forms the key by which 
	 * the named item can be retrieved.
	 * 
	 * @param onlyReadLevel the only-read access level that the item is 
	 * to have if it is created.
	 * 
	 * @param readOrWriteLevel the read-or-write access level  that the 
	 * item is to have if it is created.
	 * 
	 * @param getIfExists whether the item with the given name is to be 
	 * retrieved if one exists already. If this is set to false and an 
	 * item with the given name already exists, the method will return 
	 * null and not create a new item.
	 * 
	 * @param createIfNotExists whether the item is to be created if it 
	 * does not exist already
	 * 
	 * @param session the store session requesting the operation.
	 * 
	 * @return the item that is related to a given name by the source item 
	 * of the given source. If the item already exists but is not visible 
	 * to the requesting session, or if the source item cannot be locked, 
	 * or the triple defining the named item could not be created, then null 
	 * is returned.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * 
	 */
	
	private Item getOrCreateNamedItem(
			long sourceNr,
			String name,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			boolean getIfExists,
			boolean createIfNotExists,
			StoreSession session) 
					throws 	NotInOperationException, 
							SourceConnectorException, 
							TripleStoreFatalException,
							TripleStoreNonFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		
		// Get the source item
		Item sourceItem = itemsIndex.get(
				new Id(sourceNr, SourceConnector.sourceItemNr));
		if (sourceItem == null) return null;
		
		// Create a session at the highest access level
		StoreSession masterSession = 
				new StoreSession(accessLevelManager.highest);

		// Lock the source item of the source
		if (!sourceItem.lock(masterSession)) return null;
		try {			
			// Find the named item, if it exists. Need to check
			// items with all levels of visibility.
			List<Triple> triples = rawGetTriples(
					null, 
					sourceItem, 
					new StringDatum(name), 
					masterSession);
			if (triples.size() > 1)
				// Duplicate triples exist for this source
				// and name
				throw new TripleStoreNonFatalException(
						"Named item triple is not unique");
			
			if (triples.size() <= 0) {
				// The named item does not exist				
				// Create it
				Item it = null;
				try {
					if (createIfNotExists) it = createNamedItemLocked(
							sourceItem, name, 
							onlyReadLevel, readOrWriteLevel, session);
				}
				finally {
					if ((it != null) && it.isCurrent())
						it.unlock(session);
				}
				return it;
			}
			
			else {
				// The named item exists
				
				if (getIfExists) {
					// Check that it is visible to the requesting 
					// session
					Item it = triples.get(0).subject;
					if (it.canBeReadBy(session))
						// It is visible
						return it;
					else
						// It is not visible
						return null;
				} else return null;
			}
		}
		finally {
			// Unlock the first item in the source
			sourceItem.unlock(masterSession);
		}
	}
	
	/**
	 * <p>Give an item a unique name. If the item already has a name, 
	 * then rename it.
	 * </p>
	 * <p>The item is related to a given name by the source item of its 
	 * source. Only one such item can exist in any particular source. 
	 * </p>
	 * <p>If the item already has the given name, no change is made.
	 * </p>
	 * @param item the item to be named. It must be locked by the 
	 * requesting store session before this method is invoked.
	 * 
	 * @param name the name that the item is to have
	 * 
	 * @param session the requesting store session
	 * 
	 * @return whether the item has the given name as a result of 
	 * this method invocation. If the item is not locked then no 
	 * change is made and false is returned.
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 * @throws TripleStoreFatalException
	 */
	
	public boolean nameItem(Item item, String name, StoreSession session) 
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException, 
					NotInOperationException,
					TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (name == null) return false;
		if (!item.checkLocked(session)) return false;
		SourceConnector connector = 
				connectorsManager.getConnector(item.sourceNr);
		
		// Get the source item
		Item sourceItem = itemsIndex.get(
				new Id(item.sourceNr, SourceConnector.sourceItemNr));
		if (sourceItem == null) return false;
		
		// Create a session at the highest access level
		StoreSession masterSession = 
				new StoreSession(accessLevelManager.highest);
		
		// Lock the source item of the source
		if (!sourceItem.lock(masterSession)) return false;
		try {
			// Check whether any item already has the name. Need to check
			// items with all levels of visibility.
			Datum nameDatum = new StringDatum(name);
			List<Triple> triples = rawGetTriples(
					null, 
					sourceItem, 
					nameDatum, 
					masterSession);
			if (triples.size() > 1)
				// Duplicate items have the name
				throw new TripleStoreNonFatalException(
						"Named item triple is not unique");
			if (triples.size() == 1)
				// An item already has the name. If it is the
				// given item, no change is needed. If not,
				// no change can be made.
				return (triples.get(0).subject.equals(item));

			// The name can be used. Check whether the item already 
			// has another name.
			triples = rawGetTriples(
					item, 
					sourceItem, 
					null, 
					masterSession);
			if (triples.size() > 1)
				// Duplicate names exist for this item
				throw new TripleStoreNonFatalException(
						"Item name triple is not unique");
			if (triples.size() == 1)
				// The item already has a name. Remove it.
				deleteTripleUnchecked(triples.get(0), item.getTransaction());
			
			// Relate the item to its given name
			createTripleUnchecked(
					item, 
					sourceItem,
					nameDatum,
					connector,
					item.getTransaction());
			return true;
		} finally {
			sourceItem.unlock(masterSession);
		}
	}
	
	/**
	 * Get a named item in a source
	 * 
	 * @param sourceNr the numeric identifier of a source
	 * 
	 * @param name the name of a named item in the source
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the item in the given source with the given
	 * name, or null if it cannot be found.
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 * @throws TripleStoreFatalException
	 * @throws NotInOperationException 
	 */
	
	public Item getNamedItem(
			long sourceNr, 
			String name,
			StoreSession session) 
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException,
							NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();

		// Get the first item of the source
		Item sourceItem = itemsIndex.get(
				new Id(sourceNr, 
						SourceConnector.sourceItemNr));
		if (sourceItem == null) return null;
		
		List<Triple> triples = rawGetTriples(
				null, 
				sourceItem, 
				new StringDatum(name), 
				session);
		if (triples.size() > 1)
			// Duplicate triples exist for this source
			// and name
			throw new TripleStoreNonFatalException(
					"Named item triple is not unique");

		if (triples.size() <= 0) return null;
		return triples.get(0).subject;
	}

	/**
	 * <p>Create a new named item and return it locked.
	 * </p>
	 * <p>This method is invoked when the source item is locked and it 
	 * has been established that an item with this name does not 
	 * already exist.
	 * </p>
	 * @param firstItemInSource the first item in the
	 * source of the item.
	 * 
	 * @param name the name of the named item
	 * 
	 * @param onlyReadLevel the only-read level that the item
	 * is to have
	 * 
	 * @param readOrWriteLevel the read-or-write level that the
	 * item is to have
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the created named item.
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * 
	 */
	
	private Item createNamedItemLocked(
			Item firstItemInSource,
			String name,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			StoreSession session) 
					throws 	NotInOperationException, 
							SourceConnectorException,
							TripleStoreNonFatalException,
							TripleStoreFatalException  {
	
		SourceConnector connector =
				connectorsManager.getConnector(
						firstItemInSource.sourceNr);
		Item it = alwaysCreateLockedItem(
				connector, 
				onlyReadLevel, 
				readOrWriteLevel, 
				session);
		if (it == null) return null;
	
		// Relate the item to its name
		createTripleUnchecked(
				it, 
				firstItemInSource,
				new StringDatum(name),
				connector,
				it.getTransaction());
		
		// Return the new named item
		return it;
	}
	
	/**
	 * Create a new item and return it locked
	 * 
	 * @param connector the connector to the source in which the item 
	 * is to be written
	 * 
	 * @param onlyReadLevel the only-read level that the item is to 
	 * have
	 * 
	 * @param readOrWriteLevel the read-or-write level that the item 
	 * is to have. This must not be the lowest access level.
	 * 
	 * @param session the store session requesting creation of the new 
	 * item. Its access level must be superior to the read-or-write level 
	 * that the item is to have. It therefore must not be the lowest
	 * access level.
	 * 
	 * @return the item that is created, or null if it cannot be created
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws NotInOperationException 
	 */
	
	Item alwaysCreateLockedItem(
			SourceConnector connector,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			StoreSession session) 
			throws 	SourceConnectorException, 
					TripleStoreNonFatalException,
					TripleStoreFatalException,
					NotInOperationException {
		
		if (session == null) return null;
		if (readOrWriteLevel.hasSuperior(
				lowestAccessLevel())) return null;
		if (!readOrWriteLevel.hasSuperior(
				session.getAccessLevel())) return null;
		
		if (connector == null)
			throw (new TripleStoreNonFatalException(
					"Source is not connected"));
		while (true) {
			IdAndTrans idat = connector.addSourceItem(
					onlyReadLevel.getItemId(), 
					readOrWriteLevel.getItemId());
			Item it = new Item(
					connector.getSourceNr(),
					idat.idNr,
					onlyReadLevel,
					readOrWriteLevel,
					this,
					idat.trans,
					session);
			if (itemsIndex.index(it)) return it;
			else {
				// The item may already be in the index if it was
				// deleted, but no other items were added before the
				// source was re-started, and it is referenced by a 
				// triple that was not deleted before the source was
				// restarted. Rare situation but possible. Remove
				// the new item and try again.
				connector.removeSourceItem(idat.idNr, idat.trans);
				connector.endSourceTransaction(idat.trans);
			}
		}
	}
	
	/**
	 * <p>Get an item.
	 * </p>
	 * <p>The requesting store session must have read access to the item. 
	 * The item must be from a connected source
	 * </p>
	 * @param id the item's identifier
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the item with the given identifier, or null if there is 
	 * no such item 
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public Item getItem(Id id, StoreSession session)
			throws 	NotInOperationException, 
					TripleStoreNonFatalException {

		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		if (id == null) return null;
		if (	(id.sourceNr != specialItemSource) &&
				(connectorsManager.getConnector(
						id.sourceNr) == null))
			throw (new TripleStoreNonFatalException(
					"Source " + id.sourceNr + 
					" is not connected"));
		
		Item it = itemsIndex.get(id);
		if ((it == null) ||
		    (!it.canBeReadBy(session))) {
			return null;
		}
		return it;
	}
	
	/**
	 * <p>Get an item.
	 * </p>
	 * <p>This method is used by other classes in the package to retrieve 
	 * an item from the index.
	 * </p>
	 * @param id the item's identifier
	 * 
	 * @return the item with the given identifier, or null if there is 
	 * no such item 
	 */
	
	Item getItem(Id id) {
		
		return itemsIndex.get(id);
	}
	
	/**
	 * Get the name of an item if it is a named item.
	 * A named item is related to its name by its source's first item.
	 * 
	 * @param item an item
	 * 
	 * @param session the requesting session
	 * 
	 * @return the name of the item if it has one and is visible to 
	 * the requesting session, null otherwise.
	 * 
	 * @throws NotInOperationException
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException 
	 */
	
	public String getItemName(Item item, StoreSession session) 
			throws 	NotInOperationException,
					SourceConnectorException, 
					TripleStoreNonFatalException,
					TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		
		Item sourceItem =
				itemsIndex.get(new Id(
						item.sourceNr, 
						SourceConnector.sourceItemNr));
		if (sourceItem == null) return null;
		List<Triple> triples;
		triples = getTriples(
				item, sourceItem, null, session);
		if (triples.isEmpty()) return null;
		if (triples.size() == 1) {
			try {
				return triples.get(0).getObject(session).
										getTextValueAsString();
			}
			catch (Exception e) {}
		}
		throw new TripleStoreNonFatalException(
				"Named item triple is not unique");
	}
		
	/**
	 * <p>Delete an item
	 * </p>
	 * <p>The item is marked as deleted and is deleted from the store.
	 * </p>
	 * <p>The item must not be a special item or the item of an access level 
	 * or a source item.
	 * </p>
	 * @param it the item to be deleted
	 * 
	 * @param session the store session requesting deletion of the item. 
	 * The item must have been locked by this session for the operation to 
	 * succeed.
	 * 
	 * @return true if the item was deleted, false otherwise (including if 
	 * there is no such item or the item was already deleted or the item 
	 * was a special item or the item of an access level)
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public boolean deleteItem(Item it, StoreSession session) 
			throws 	NotInOperationException, 
					TripleStoreNonFatalException,
					SourceConnectorException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
			
		if (it == null) return false;
		if (!it.checkLocked(session)) return false;
		if (isSpecialItem(it)) return false;
		if (it.itemNr == SourceConnector.sourceItemNr) return false;
		if (accessLevelManager.get(it) !=  null) return false;
		SourceConnector connector = 
				connectorsManager.getConnector(it.sourceNr);
		if (connector == null) 
			throw (new TripleStoreNonFatalException(
					"Source " + it.sourceNr + 
					" is not connected"));
		Item i = itemsIndex.remove(it.getId());
		if (i == null) return false;
		Transaction trans = it.getTransaction();
		try {
			connector.removeSourceItem(it.itemNr, trans);
			connector.endSourceTransaction(it.getTransaction());
		} catch (Exception e) {
			log("Error deleting item in source " + connector.getSourceNr() + 
					": " + e.getMessage());
			connectorsManager.removeConnector(connector);
			throw(e);
		}
		it.markDeleted();
		return true;
	}
	
	/**
	 * <p>Delete an item without checking whether it is safe to do so. 
	 * </p>
	 * <p>This method should only be invoked by other methods that have 
	 * already made these checks. Either the item will be deleted, or 
	 * a fatal exception will be thrown
	 * </p>
	 * @param it an item to be deleted
	 * 
	 * @param session the requesting store session
	 * 
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 */
	
	void deleteItemUnchecked(
			Item it, StoreSession session)
					throws 	TripleStoreFatalException,
							SourceConnectorException {
		
		SourceConnector connector =
				connectorsManager.getConnector(it.sourceNr);
		try {
			Transaction trans = it.getTransaction();
			connector.removeSourceItem(it.itemNr, trans);
			itemsIndex.remove(it.getId());
			it.markDeleted();
			connector.endSourceTransaction(trans);
		} catch (SourceConnectorException e) {
			log("Error deleting item in source " + connector.getSourceNr() + 
					": " + e.getMessage());
			connectorsManager.removeConnector(connector);
			throw(e);
		} catch (Exception e) {
			throw(abortTripleStoreException(
					"Cannot delete item unchecked", e));
		}
	}
	
	/**
	 * Determine whether an item is a special item.
	 * 
	 * @param it an item
	 * 
	 * @return true if and only if the given item is a special one.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public boolean isSpecialItem(Item it) 
			throws 	TripleStoreNonFatalException,
					NotInOperationException {

		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (it == null) return false;
		if (!it.isCurrent())
			throw new TripleStoreNonFatalException(
					"Item not current");
		if (it.store != this)
			throw new TripleStoreNonFatalException(
					"Item does not belong to store");
		return (it.sourceNr == specialItemSource);
	}
	
	/**
	 * Get the name of a special item
	 * 
	 * @param it a special item
	 * 
	 * @return the name of the special item, or null if the item is not 
	 * a special item.
	 * 
	 * @throws NotInOperationException 
	 */
	
	public String specialItemName(Item it)
			throws NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		
		if (it.sourceNr == specialItemSource) {
			if (	(it.itemNr > (long)Integer.MAX_VALUE) ||
					(it.itemNr < (long)Integer.MIN_VALUE))
				return null;
			switch ((int)it.itemNr) {
			case 1 : return "lowestAccessLevel";
			case 2 : return "highestAccessLevel";
			case 3 : return "isAccessLevelRelation";
			case 4 : return "superiorPermissionRelation";
			case 5 : return "inferiorPermissionRelation";
			case 6 : return "hasCredentialRelation";
			case 7 : return "hasSecretDigestRelation";
			default : return null;
			}
		}
		else if (it.itemNr == SourceConnector.sourceItemNr) 
			return "sourceItem";
		else if (it.itemNr == SourceConnector.adminLevelItemNr) 
			return "adminLevelItem";
		else return null;
	}
	
	/**
	 * Get a special item, given its name
	 * 
	 * @param name a name
	 * 
	 * @return the special item with the given name, or
	 * null if it is not the name of a special item.
	 * 
	 * @throws NotInOperationException 
	 */
	
	public Item getSpecialItemByName(String name) 
			throws NotInOperationException {
	
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		
		if (name.startsWith("sourceItem")) {
			Long nr;
			try {
				nr = Long.parseLong(name.substring(9));
			} catch (Exception e) {
				return null;
			}
			return itemsIndex.get(
					new Id(nr, SourceConnector.sourceItemNr));
		}
		if (name.startsWith("adminLevelItem")) {
			Long nr;
			try {
				nr = Long.parseLong(name.substring(9));
			} catch (Exception e) {
				return null;
			}
			return itemsIndex.get(
					new Id(nr, SourceConnector.adminLevelItemNr));
		}
		if (name.equals("lowestAccessLevel")) 
			return accessLevelManager.lowest.levelItem;
		if (name.equals("highestAccessLevel")) 
			return accessLevelManager.highest.levelItem;
		if (name.equals("isAccessLevelRelation"))
			return isAccessLevelRelation;
		if (name.equals("superiorPermissionRelation")) 
			return superiorPermissionRelation;
		if (name.equals("inferiorPermissionRelation")) 
			return inferiorPermissionRelation;
		if (name.equals("hasCredentialRelation"))
			return hasCredentialRelation;
		if (name.equals("hasSecretDigestRelation"))
			return hasSecretDigestRelation;
		return null;
	}
	
	/**
	 * <p>Lock the item objects of triples whose subject is
	 * a given item and whose verbs are in a given set
	 * of verbs.
	 * </p>
	 * <p>This method is used by applications that need to
	 * process an item and other items related to it
	 * together - for example that need to delete an
	 * item together with the items related to it.
	 * </p>
	 *
	 * @param item the given item. It must have been 
	 * locked by the requesting session when the method
	 * is invoked.
	 * 
	 * @param verbs the verbs of triples whose subject
	 * is the given item and whose item objects are to be
	 * locked.
	 * 
	 * @param storeSession the requesting store session.
	 * 
	 * @return a list of the object items of triples 
	 * whose subject is the given item and whose verbs are 
	 * in the given set of verbs, or null. If a list is
	 * returned then all the items in it are locked, and the
	 * list is ordered by the time of locking, with the most
	 * recently locked item first. If any of the object items 
	 * cannot be locked - including object items that are not
	 * visible to the requesting session - then null is 
	 * returned.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public List<Item> lockObjectsOfItem(Item item, Item[] verbs, StoreSession storeSession) 
			throws TripleStoreNonFatalException {
		
		LinkedList<Item> lockedItems = new LinkedList<Item>();
		// Need to look for triples including those whose object items
		// are not visible to the requesting session
		StoreSession masterSession = new StoreSession(accessLevelManager.highest);
		try {
			for (Item verb : verbs) {
				List<Triple> triples = getTriples(
						item, verb, null, masterSession);
				for (Triple triple : triples) {
					Datum d = triple.getObject(masterSession);
					if (d.getType() == Datum.Type.ITEM) {
						Item object = d.getItemValue();
						if (object.lock(storeSession)) 
							lockedItems.addFirst(object);
						else throw (new TripleStoreNonFatalException(""));
					}
				}
			}
		} catch (Exception e) {
			for (Item it : lockedItems) 
				try {
					it.unlock(storeSession);
				} catch (Exception x){} 
			return null;
		}
		return lockedItems;
	}
	
	/**
	 * Unlock the objects in a list of objects.
	 * 
	 * @param objects a list of objects
	 * 
	 * @param storeSession the requesting store session
	 */
	
	public void unlockObjects(List<Item> objects, StoreSession storeSession) {
		
		for (Item item : objects) {
			try {
				item.unlock(storeSession);
			} catch (Exception e) {}
		}
	}

	/*
	 ***********************************************************************
	 *
	 *  TRIPLES
	 *  
	 ***********************************************************************
	 */

	/**
	 * <p>Get a triple. 
	 * </p>
	 * <p>The requesting session must have read access to the triple's subject 
	 * and verb, and to the object if this is an item. The triple must be 
	 * from a connected source.
	 * </p>
	 * @param id the identifier of the requested triple
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the triple with the given identifier, or null if there is no such 
	 * triple, or the session does not have read access, or the triple's subject
	 * verb or object has been deleted.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException
	 */
	
		public Triple getTriple(Id id, StoreSession session)
			throws 	NotInOperationException, 
					TripleStoreNonFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		if (id == null) return null;
		if (connectorsManager.getConnector(
				id.sourceNr) == null)
			throw (new TripleStoreNonFatalException(
					"Source " + id.sourceNr + 
					" is not connected"));

		Triple t = triplesIndex.getTriple(id);
		try {
			if ((t == null) || !canReadTriple(t, session)) return null;
		} catch (TripleStoreNonFatalException e) {
			return null;
		}
		return t;
	}

	/**
	 * <p>Get the subjects, verbs and objects of a list of triples
	 * that are specified by their identifiers. 
	 * </p>
	 * <p>The requesting session should have read access to the triples' subjects
	 * and verbs, and to their objects if these are items, and the triples must be 
	 * from connected sources. Null values are returned for triples that do not
	 * satisfy these conditions.
	 * </p>
	 * @param ids the identifiers of the requested triples
	 * 
	 * @param session the requesting store session
	 * 
	 * @return an array containing a member for each input triple identifier.
	 * This member gives the subject, verb and object of the identified triple,
	 * or is null if there is no such triple, or the session does not have read
	 * access, or the triple's subject verb or object has been deleted.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException 
	 * @throws TripleStoreFatalException 
	 */
		
	public TripleData[] getTriplesData(Id[] ids, StoreSession session)
			throws 	NotInOperationException, TripleStoreNonFatalException,
					TripleStoreFatalException, SourceConnectorException {
			
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if (!session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		Map<Long, SourceConnector> connectorsMap =
				new HashMap<Long, SourceConnector>();
		TripleData[] tds = new TripleData[ids.length];
		for (int i = 0; i < ids.length; i++) {
			Id id = ids[i];
			if (id == null) tds[i] = null;
			else {
				SourceConnector connector = connectorsMap.get(id.sourceNr);
				if (connector == null) {
					connector = connectorsManager.getConnector(
							id.sourceNr);
					if (connector != null) connectorsMap.put(id.sourceNr, connector);
				}
				if (connector == null) tds[i] = null;
				else {
					Triple t = triplesIndex.getTriple(id);
					try {
						if ((t == null) || !canReadTriple(t, session))  tds[i] = null;
						else tds[i] = new TripleData(t.subject, t.verb, getTripleObject(t));
					} catch (TripleStoreNonFatalException e) {
						 tds[i] = null;
					}
				}
			}
		}
		return tds;
	}
		
	/**
	 * Create a new triple
	 * 
	 * @param subject the item that is the triple's subject. 
	 * This item must have been locked by the requesting store
	 * session for the operation to succeed, and the requesting 
	 * store session must have write access to it.
	 * 
	 * @param verb the item that is the triple's verb. The 
	 * requesting store session must have read access to the verb. 
	 * The verb must not be a special item (triples with special 
	 * items as verbs are created by special-purpose methods).
	 * 
	 * @param object the Datum to be set as the triple's object. 
	 * The requesting store session must have read access to its 
	 * value if that value is an item.
	 * 
	 * @param session the store session requesting creation of the 
	 * new triple. 
	 * 
	 * @return the triple that is created, or null if it cannot be 
	 * created as specified
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * 
	 */
	
	public Triple createTriple(
			Item subject,
			Item verb, 
			Datum object,
			StoreSession session) 
					throws 	NotInOperationException, 
							SourceConnectorException,
							TripleStoreNonFatalException,
							TripleStoreFatalException {		

		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((session == null) || !session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		if (subject == null) return null;
		if (!subject.isCurrent()) 
			throw(new TripleStoreNonFatalException(
					"Subject not current"));
		if (subject.store != this)
			throw new TripleStoreNonFatalException(
					"Subject does not belong to store");
		if (verb == null) return null;
		if (!verb.isCurrent()) 
			throw(new TripleStoreNonFatalException(
					"Verb not current"));
		if (verb.store != this)
			throw new TripleStoreNonFatalException(
					"Verb does not belong to store");
		if (isSpecialItem(verb)) return null;
		if (verb.itemNr == SourceConnector.sourceItemNr) return null;
		if (object == null) return null;
		if (object.type == Datum.Type.ITEM) {
			Item objectItem = null;
			try {
				objectItem = object.getItemValue();
			}
			catch(TripleStoreNonFatalException e) {
				return null;
			}
			if (objectItem == null) return null;
			if (!objectItem.isCurrent()) 
				throw(new TripleStoreNonFatalException(
						"Object item not current"));
			if (objectItem.store != this)
				throw new TripleStoreNonFatalException(
						"Object item does not belong to store");			
		}
		if (!canWriteTriple(subject, verb, object, session)) return null;
		SourceConnector connector = 
				connectorsManager.getConnector(subject.sourceNr);
		if (connector == null)
			throw (new TripleStoreNonFatalException(
					"Source " + subject.sourceNr + " is not connected."));
		return createTripleUnchecked(subject, verb, object,
					connector, subject.getTransaction());
	}
	
	/**
	 * Create a new triple without checking the subject, verb, and 
	 * object.
	 * 
	 * @param subject the item that is the triple's subject. 
	 * 
	 * @param verb the item that is the triple's verb.
	 * 
	 * @param object the Datum to be set as the triple's object. 
	 * 
	 * @param sourceConnector the connector to the source where the 
	 * triple will be stored. This MUST be the source of the triple's 
	 * subject.
	 * 
	 * @param trans the transaction of which the write to source is 
	 * part
	 * 
	 * @return the triple that is created, or null if it cannot be 
	 * created as specified
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreFatalException 
	 * @throws NotInOperationException 
	 */
	
	Triple createTripleUnchecked(
		Item subject,
    	    Item verb, 
    	    Datum object, 
    	    SourceConnector connector,
    	    Transaction trans) 
    	    		throws 	TripleStoreNonFatalException,
    	    				TripleStoreFatalException,
    	    				SourceConnectorException, 
    	    				NotInOperationException {	
		
		if (	(connector.getSourceNr() != subject.sourceNr) &&
				!isSpecialItem(subject) )
			throw (new TripleStoreFatalException(
					"Attempt to create triple in different source " +
					"than that of its subject."));
		StoredTriple st = 
				connector.createSourceTriple(subject, verb, object, trans);
		Triple t;
		if (st instanceof StoredItemTriple)
			t = new ItemTriple((StoredItemTriple)st, this);
		else if (st instanceof StoredNonItemTriple)
			t = new NonItemTriple((StoredNonItemTriple)st, this);
		else throw (new SourceConnectorContentException(
				connector.getSourceNr(), 
				"Unrecognised stored triple"));
		cacheTripleObject(t.getId(), object);
		triplesIndex.indexTriple(t);
		return t;
	}
	
	/**
	 * <p>Replace a triple's object
	 * </p>
	 * <p>Note that the triple will not change, but a new triple with 
	 * updated information - including the object and summary value - 
	 * will be returned and should be used in place of the original 
	 * triple. The new triple will have a different numeric identifier
	 * to the original triple.
	 * </p>
	 * @param t the triple whose object is to be replaced. Its subject 
	 * must have been locked by the requesting store session. The 
	 * requesting store session must have read access to its verb. 
	 * The verb must not be a special item or a source item (triples 
	 * with special items or source items as verbs are updated by 
	 * special-purpose methods).
	 * 
	 * @param object the datum that is to be the triple's new object. 
	 * The requesting store session must have read access to  its value 
	 * if that value is an item.
	 * 
	 * @param session the store session requesting the triple be updated. 
	 * 
	 * @return an updated copy of the triple if it was put successfully, 
	 * null otherwise
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreFatalException 
	 * 
	 */
	
	public Triple replaceTripleObject(
			Triple t, 
			Datum object,
			StoreSession session) 
					throws 	NotInOperationException, 
					TripleStoreNonFatalException,
					TripleStoreFatalException,
					SourceConnectorException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((session == null) || !session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		if (t.store != this)
			throw new TripleStoreNonFatalException(
					"Triple does not belong to store");
		if (isSpecialItem(t.verb)) return null;
		if (t.verb.itemNr == SourceConnector.sourceItemNr) return null;
		if (object.type == Datum.Type.ITEM) { 
			Item objectItem = null;
			try {
				objectItem = object.getItemValue();
			}
			catch(TripleStoreNonFatalException e) {
				return null;
			}
			if (objectItem == null) return null;
			if (!objectItem.isCurrent()) 
				throw(new TripleStoreNonFatalException(
						"Item not current"));
			if (objectItem.store != this)
				throw new TripleStoreNonFatalException(
						"Object item does not belong to store");			
		}
		
		if (!canWriteTriple(t.subject, t.verb, object, session))
			return null;
		
		if (triplesIndex.deIndexTriple(t) == null) return null;
		return replaceTripleObjectUnchecked(
				t, object);
	}
	
	/**
	 * Replace a triple object without checking access permissions
	 * 
	 * @param t a triple
	 * 
	 * @param object the replacement object
	 * 
	 * @return a new triple with the replaced object
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreFatalException 
	 */
	
	Triple replaceTripleObjectUnchecked(
			Triple t, 
			Datum object) 
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException {
		
		if (triplesIndex.deIndexTriple(t) == null) 
			throw (new TripleStoreNonFatalException(
				"Triple " + t.getId() + " is not in store"));
		StoredTriple st;
		SourceConnector connector =
				connectorsManager.getConnector(
						t.subject.sourceNr);
		if (connector == null) 
			throw (new TripleStoreNonFatalException(
					"Source " + t.subject.sourceNr + 
					" is not connected"));
		try {
			st = connector.replaceSourceTriple(
					t, object, t.subject.getTransaction());
		} catch (SourceConnectorException e) {
			log("Error replacing triple object in source " + connector.getSourceNr() + 
					": " + e.getMessage());
			connectorsManager.removeConnector(connector);
			throw(e);
		}
		if (st == null) return null;
		Triple tnew;
		if (st instanceof StoredItemTriple)
			tnew = new ItemTriple((StoredItemTriple)st, this);
		else if (st instanceof StoredNonItemTriple)
			tnew = new NonItemTriple((StoredNonItemTriple)st, this);
		else throw (new SourceConnectorContentException(
				connector.getSourceNr(), 
				"Unrecognised stored triple"));
		triplesIndex.indexTriple(tnew);
		return tnew;
	}
	
	/**
	 * Get the object of a triple
	 * 
	 * @param session the store session requesting the operation.
	 * It must have read access to the subject and verb, and to the 
	 * object if this is an item.
	 * 
	 * @param t the triple whose object is to be returned
	 * 
	 * @return the triple's object
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreFatalException 
	 */
	
	public Datum getTripleObject(Triple t, StoreSession session) 
			throws 	NotInOperationException, 
					SourceConnectorException,
					TripleStoreNonFatalException,
					TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((session == null) || !session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		if (t.store != this)
			throw new TripleStoreNonFatalException(
					"Triple does not belong to store");
		if (!canReadTriple(t, session)) 
			return null;
		return getTripleObject(t);
	}
	
	/**
	 * <p>Get the object of a triple without checking access rights.
	 * </p>
	 * <p>The requester is assumed to have access rights.
	 * </p>
	 * @param t the triple whose object is to be returned
	 * 
	 * @return the triple's object
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreFatalException 	 * 
	 */
	
	private Datum getTripleObject(Triple t) 
			throws 	TripleStoreNonFatalException,
					TripleStoreFatalException,
					SourceConnectorException  {
		
		Datum object = null;
		switch (t.objectType) {
		case ITEM : object = new ItemDatum(
				((ItemTriple)t).object);
					break;
		case BOOLEAN : object = new SimpleDatum(
				Datum.getBooleanValueFromSummary(
						((NonItemTriple)t).
							objectSummaryValue));
					break;
		case INTEGRAL : object = new SimpleDatum(
				Datum.getIntegralValueFromSummary(
						((NonItemTriple)t).
							objectSummaryValue));
					break;
		case REAL : object = new SimpleDatum(
				Datum.getRealValueFromSummary(
						((NonItemTriple)t).
							objectSummaryValue));
					break;
		case TEXT : 
			String s = stringCache.get(t.getId());
			if (s == null) {
				SourceConnector connector =
						connectorsManager.getConnector(
								t.subject.sourceNr);
				if (connector == null) 
					throw (new TripleStoreNonFatalException(
							"Source " + t.subject.sourceNr + 
							" is not connected"));
				try {
					object = connector.getSourceTextTripleObject(t.tripleNr);
				} catch (SourceConnectorException e) {
					log("Error getting triple object in source " + connector.getSourceNr() + 
							": " + e.getMessage());
					connectorsManager.removeConnector(connector);
					throw (e);
				}
			}
			else {
				object = new StringDatum(s);
				object.setSummaryValue(t.getObjectSummaryValue());
			}
			break;
		case BINARY : 
			byte b[] = binaryCache.get(t.getId());
			if (b == null) {
				SourceConnector connector =
						connectorsManager.getConnector(
								t.subject.sourceNr);
				if (connector == null) 
					throw (new TripleStoreNonFatalException(
							"Source " + t.subject.sourceNr + 
							" is not connected"));
				try {
					object = connector.getSourceBinaryTripleObject(t.tripleNr);
				} catch (SourceConnectorException e) {
					log("Error getting triple object in source " + connector.getSourceNr() + 
							": " + e.getMessage());
					connectorsManager.removeConnector(connector);
					throw (e);
				}
			}
			else {
				object = new ByteArrayDatum(b);
				object.setSummaryValue(t.getObjectSummaryValue());
			}
			break;
		}
		if (object != null) cacheTripleObject(t.getId(), object);
		return object;
	}
	
	/**
	 * Delete a triple.
	 * 
	 * @param t the triple to be deleted. Its subject must have been 
	 * locked by the requesting store session and the requesting store
	 * session must have write access to it. The requesting store 
	 * session must have read access to its verb, and to its object if
	 * this is an item. The verb must not be a special item or a source 
	 * item (triples with special items or source items as verbs are 
	 * deleted by special-purpose methods).
	 * 
	 * @param session the store session requesting the triple be deleted. 
	 * 
	 * @return true if the triple was deleted, false otherwise
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreFatalException 
	 */
	
	public boolean deleteTriple(Triple t, StoreSession session) 
			throws 	NotInOperationException, 
					TripleStoreNonFatalException, 
					SourceConnectorException,
					TripleStoreFatalException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((session == null) || !session.isOfTripleStore(this))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		if (t.store != this)
			throw new TripleStoreNonFatalException(
					"Triple does not belong to store");
		if (isSpecialItem(t.verb)) return false;
		if (t.verb.itemNr == SourceConnector.sourceItemNr) return false;
		if (!canDeleteTriple(t, session))
			return false;
		deleteTripleUnchecked(t, t.subject.getTransaction());
		return true;
	}
	
	/**
	 * Delete a triple without checking its subject, verb, and object.
	 * 
	 * @param t the triple to be deleted.
	 * 
	 * @param trans the transaction of which the deletion is to be part.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 */
	
	void deleteTripleUnchecked(Triple t, Transaction trans) 
					throws 	SourceConnectorException,
							TripleStoreNonFatalException {
		
		if (triplesIndex.deIndexTriple(t) == null) return;
		SourceConnector connector =
				connectorsManager.getConnector(
						t.subject.sourceNr);
		if (connector == null)
			throw (new TripleStoreNonFatalException(
					"Source " + t.subject.sourceNr + 
					" is not connected"));
		try {
			connector.removeSourceTriple(t.tripleNr, trans);
		} catch (SourceConnectorException e) {
			log("Error deleting triple in source " + connector.getSourceNr() + 
					": " + e.getMessage());
			connectorsManager.removeConnector(connector);
			throw (e);
		}
	}
	
	/**
	 * Cache a triple object
	 * 
	 * @param id the identity of the triple
	 * 
	 * @param object the object to be cached
	 * 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	private void cacheTripleObject(Id id, Datum object) 
			throws 	TripleStoreFatalException,
					SourceConnectorException {
		
		try {
			if ((object.type == Datum.Type.TEXT) &&
				(object.getLength() < longTextLength))
					stringCache.put(id, 
						object.getTextValueAsString());
			if ((object.type == Datum.Type.BINARY) &&
				(object.getLength() < longBinaryLength))
					binaryCache.put(id,
						object.getBinaryValueAsArray());
		} catch (TripleStoreNonFatalException e) {
			throw(abortTripleStoreException(
					"Cannot Cache Triple Object", e));
		}
	}
	
	/**
	 * <p>Determine whether a triple can be read.
	 * </p>
	 * <p>The subject and verb must not have been deleted, and the 
	 * requesting store session must have read access to them.
	 * </p>
	 * <p>The triple must be from a connected source
	 * </p>
	 * <p>If the object is an item, it must not have been deleted,
	 * and the requesting store session must have read access
	 * to it.
	 * </p>
	 * @param t a triple
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the triple can be read, false otherwise
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	private boolean canReadTriple(Triple t, StoreSession session) 
			throws TripleStoreNonFatalException {
		
		if (!t.subject.isCurrent()) 
			// Can happen after source disconnection
			return false;
		if (!t.subject.canBeReadBy(session)) 
			return false;
		if (!t.verb.isCurrent()) 
			// Can happen after source disconnection
			return false;
		if (!t.verb.canBeReadBy(session)) 
			return false;
		if (t.objectType == Datum.Type.ITEM) {
			if ((((ItemTriple)t).object == null) ||
				(!((ItemTriple)t).object.isCurrent()))	
				// Can happen after source disconnection
				return false;
			if (!((ItemTriple)t).object.canBeReadBy(session)) return false;
		}
		if (connectorsManager.getConnector(
						t.subject.sourceNr) == null) 
			// Can happen after source disconnection
			return false;
		return true;
	}
	
	/**
	 * <p>Determine whether a triple can be deleted
	 * </p>
	 * <p>The subject must exist, not have been deleted, and have been 
	 * locked by the requesting store session for the operation to 
	 * succeed.
	 * </p>
	 * <p>The verb must exist, must not have been deleted, and the 
	 * requesting store session must have read access to it.
	 * </p>
	 * <p>If the object is an item then the requesting store session 
	 * must have read access to it.
	 * </p>
	 * @param t a triple
	 * 
	 * @param session the session requesting deletion of the triple. 
	 * 
	 * @return true if the triple can be deleted, false otherwise
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException 
	 */
	
	private boolean canDeleteTriple(Triple t, StoreSession session)
			throws 	TripleStoreNonFatalException,
					TripleStoreFatalException {

		if (!t.subject.checkLocked(session) ||
			!t.subject.isCurrent())  return false;
		// The subject cannot be locked unless the session access level
		// is superior to its read-or-write level

		if (!t.verb.isCurrent() || 
			!t.verb.canBeReadBy(session)) return false;
		if (!(t.objectType == Datum.Type.ITEM)) return true;
		if (!(t instanceof ItemTriple))
			throw(new TripleStoreFatalException(
					"Triple " + t.getId() + " is an ITEM triple " +
							"but not an ItemTriple"));

		Item object = ((ItemTriple) t).object;
		// Allow a triple with non-existent object to
		// be written or deleted
		if (object == null) return true;
		try {
			return (object.isCurrent() && object.canBeReadBy(session));
		} catch (TripleStoreNonFatalException e) {
			// Allow a triple with invalid object to
			// be written or deleted
			return true;
		}
	}
	
	/**
	 * Determine whether a triple can be created or written.
	 * 
	 * @param subject the item that is the triple's subject. This item 
	 * must exist, must not have been deleted, and must have been locked
	 * by the requesting store session for the operation to succeed.
	 * 
	 * @param verb the item that is the triple's verb. This item must exist, 
	 * must not have been deleted, and the requesting session must have read 
	 * accessto it.
	 * 
	 * @param object the Datum to be set as the triple's object. If it is an 
	 * item then it must exist, must not have been deleted, and the requesting
	 * store session must haven read access to it.
	 * 
	 * @param session the store session requesting creation or writing of the 
	 * triple. 
	 * 
	 * @return true if the triple can be created or written, false otherwise
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	private boolean canWriteTriple(
			Item subject,Item verb, Datum object, StoreSession session)
					throws TripleStoreNonFatalException {
		
		if (!subject.checkLocked(session) ) return false;
		// The subject cannot be locked unless the session access level
		// is superior to its read-or-write level
		
		if (!verb.canBeReadBy(session)) return false;
		
		if (object == null) return false;
		if (object.type == Datum.Type.ITEM) {
			try {
				if (!object.getItemValue().canBeReadBy(session))
					return false;
			} catch (TripleStoreNonFatalException e) {
				return false;
			}					
		}
		return true;
	}
	
	/*
	 ***********************************************************************
	 *
	 *  SEARCH AND UNIQUENESS OPERATIONS
	 *  
	 ***********************************************************************
	 */

	/**
	 * The three indexes that are maintained:
	 * 		SVO - indexed by subject, verb, then object
	 * 		VOS - indexed by verb, object, then subject
	 * 		OSV - indexed by object, subject, then verb 
	 */
	
	enum Index {SVO, VOS, OSV}
	
	/**
	 * Comparator used to sort triples by subject, verb, 
	 * then object
	 */
	
	private SVOComparator svoComparator = new SVOComparator();
	
	/**
	 * Comparator used to sort triples by verb, object,
	 * then subject
	 */
	
	private VOSComparator vosComparator = new VOSComparator();
	
	/**
	 * Comparator used to sort triples by object, subject,
	 * then verb
	 */
	
	private OSVComparator osvComparator = new OSVComparator();

	/**
	 * Get the triples with given subject, verb, and object
	 * 
	 * @param subject the given subject, or null, in which case all 
	 * subjects match
	 * 
	 * @param verb the given verb, or null, in which case all verbs 
	 * match
	 * 
	 * @param object the given object, or null, in which case all 
	 * objects match
     *    
     * @param session the store session requesting the triples.
	 * 
	 * @return a list of the triples that match and such that the 
	 * requesting session has read access to the subject and verb, 
	 * and also to the object if this is an item
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public List<Triple> getTriples (
			Item subject,
			Item verb,
			Datum object,
			StoreSession session)
					throws 	TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException,
							NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		return rawGetTriples(subject, verb, object, session);
	}

	/**
	 * Get all the triples with given subject and verb, or
	 * return null if some are not visible to the requesting session.
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
     * @param session the store session requesting the triples.
	 * 
	 * @return a list of the triples that match and such that the 
	 * requesting session has read access to the subject and verb, 
	 * and also to the object if this is an item, or null if there
	 * is a matching triple that the requesting session cannot read,
	 * or if the supplied subject or verb is null
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public List<Triple> getAllTriples (
			Item subject,
			Item verb,
			StoreSession session)
					throws 	TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException,
							NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((subject == null) || (verb == null)) return null;
		StoreSession masterSession = new StoreSession(accessLevelManager.highest);
		List<Triple> triples = rawGetTriples(
				subject, verb, null, masterSession);
		for (Triple t : triples) {
			if (!canReadTriple(t, session)) return null;
		}
		return triples;
	}
	
	/**
	 * Determine whether all the triples with given subject and verb
	 * are visible to the requesting session. The subject must be
	 * locked by the requesting session when this methoid is invoked.
	 * 
	 * @param subject the given subject
	 * 
	 * @param verb the given verb
	 * 
     * @param session the requesting store session.
	 * 
	 * @return true if the subject is locked by the requesting session
	 * and all the triples with given subject and verb are visible to 
	 * the requesting session, false otherwise.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public boolean allTriplesVisible (
			Item subject,
			Item verb,
			StoreSession session)
					throws 	TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException,
							NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		if ((subject == null) || (verb == null)) return false;
		if (!subject.checkLocked(session)) return false;
		StoreSession masterSession = new StoreSession(accessLevelManager.highest);
		List<Triple> triples = rawGetTriples(
				subject, verb, null, masterSession);
		for (Triple t : triples) {
			if (!canReadTriple(t, session)) return false;
		}
		return true;
	}
	
	/**
	 * Get the triples with given subject, verb, and object without checking 
	 * whether the store is in operation. Used by the public getTriples
	 * method and by "housekeeping" routines. 
	 * 
	 * @param subject the given subject, or null, in which case all 
	 * subjects match
	 * 
	 * @param verb the given verb, or null, in which case all verbs 
	 * match
	 * 
	 * @param object the given object, or null, in which case all 
	 * objects match
     *    
     * @param session the store session requesting the triples.
	 * 
	 * @return a list of the triples that match and such that the 
	 * requesting session has read access to the subject and verb, 
	 * and also to the object if this is an item
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */

	List<Triple> rawGetTriples (
			Item subject,
	 		Item verb,
	 		Datum object,
	 		StoreSession session) 
	 				throws 	SourceConnectorException,
	 						TripleStoreFatalException,
	 						TripleStoreNonFatalException {

		// Find the appropriate index, and the lowest and highest
		// possible matching triples in that index, and then get
		// all the triples in that range
		TreeSet<Triple> index = 
				triplesIndex.getIndex(subject, verb, object);
		Triple low = lowestMatchingTriple(subject, verb, object);
		Triple high = highestMatchingTriple(subject, verb, object);	
		List<Triple> results = 
				triplesIndex.getTriplesInRange(index, low, high);
		
		// Now remove the triples that should not be returned.
		// These are (a) triples to which the invoking session
		// does not have read access, and (b) text or binary triples
		// whose summaries are the same as summaries of matching
		// triples, but that do not themselves match.
		// Note that this is not done within the synchronized
		// triplesIndex method as this would create a 
		// performance bottleneck - a source access may
		// be needed to check a text or binary triple object.
		Iterator<Triple> iterator = results.iterator();
		while (iterator.hasNext()) {
			Triple t = iterator.next();
			try {
				if (!canReadTriple(t, session)) iterator.remove();
				else if (object != null) {
					if ((t.objectType == Datum.Type.TEXT) || 
						(t.objectType == Datum.Type.BINARY)) {
						Datum d = getTripleObject(t);
						if ((d != null) && !d.equals(object))
							iterator.remove();
					}
				}
			} catch (TripleStoreNonFatalException e) {
				// Triple could have been deleted or 
				// triple subject, verb, or object could
				// have been deleted since triple was
				// returned from the index
				iterator.remove();
			}
		}
		return results;
	}
	
	/**
	 * <p>Get the triples that have either of two verbs.
	 * </p>
	 * <p>This method is used to find triples defining access permissions.
	 * Having a single method to do this means that the two lists can
	 * be created in a single synchronized piece of code and possible
	 * inconsistencies introduced by concurrent access are avoided.
	 * </p>
	 * @param verb1 a verb
	 * 
	 * @param verb2 a verb
	 * 
	 * @return an ordered pair of lists of triples. The first list is the 
	 * list of triples that have the first verb. The second list is the 
	 * list of triples that have the second verb. The triples are not 
	 * filtered to remove invalid ones or for access at a particular access 
	 * level.
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	OrderedPair<List<Triple>> rawGetTriplesWithVerbs (Item verb1, Item verb2)
			throws TripleStoreNonFatalException {
		
		TreeSet<Triple> index = triplesIndex.getVerbIndex();
		Triple low1 = lowestMatchingTriple(null, verb1, null);
		Triple high1 = highestMatchingTriple(null, verb1, null);	
		Triple low2 = lowestMatchingTriple(null, verb2, null);
		Triple high2 = highestMatchingTriple(null, verb2, null);
		return triplesIndex.getTriplesInRanges(index, low1, high1, index, low2, high2);
	}

	/**
	 * Sort a list of triples by subject, then verb, then object
	 *  
	 * @param triples the list to be sorted
	 * 
	 * @throws NotInOperationException if the triple store is not in operation
	 */
	
	public void orderBySubjectVerbObject(List<Triple> triples) 
			throws NotInOperationException {
		
		if (state != State.OPERATIONAL)
			throw new NotInOperationException();
		Collections.sort(triples, svoComparator);
	}
	
	/**
	 * Sort a list of triples by verb, then object, then subject
	 *  
	 * @param triples the list to be sorted
	 * 
	 * @throws NotInOperationException if the triple store is not in operation
	 */
	
	public void orderByVerbObjectSubject(List<Triple> triples)
			throws NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		Collections.sort(triples, vosComparator);
	}
	
	/**
	 * Sort a list of triples by object, then subject, then verb
	 *  
	 * @param triples the list to be sorted
	 * 
	 * @throws NotInOperationException if the triple store is not in operation
	 */
	
	public void orderByObjectSubjectVerb(List<Triple> triples) 
			throws NotInOperationException {
		
		if (state != State.OPERATIONAL) 
			throw new NotInOperationException();
		Collections.sort(triples, osvComparator);
	}
	
	/**
	 * Return the lowest triple that matches the given specification. 
	 * If the specified subject, verb, or  object is null then the 
	 * returned triple will have the lowest possible subject, verb, or 
	 * object. Also, the triple identifier will have the least possible 
	 * value. The returned triple will be less than or equal to any 
	 * triple that matches the specification in any of the three orders 
	 * used to index triples.
	 * 
	 * @param subject the specified subject, or null if no subject is 
	 * specified
	 * 
	 * @param verb the specified verb, or null if no verb is specified
	 * 
	 * @param object the specified object, or null if no object is 
	 * specified
	 * 
	 * @return the lowest triple that matches the given specification
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private Triple lowestMatchingTriple(Item subject, Item verb, Datum object) 
			throws TripleStoreNonFatalException {
		
		long subjectSourceNr;
		if (subject == null) subjectSourceNr = Long.MIN_VALUE;
		else subjectSourceNr = subject.sourceNr;
		long subjectItemNr;
		if (subject == null) subjectItemNr = Long.MIN_VALUE;
		else subjectItemNr = subject.itemNr;
		long verbSourceNr;
		if (verb == null) verbSourceNr = Long.MIN_VALUE;
		else verbSourceNr = verb.sourceNr;
		long verbItemNr;
		if (verb == null) verbItemNr = Long.MIN_VALUE;
		else verbItemNr = verb.itemNr;
		if (object == null) return new ItemTriple(
				Long.MIN_VALUE,
				new Item(subjectSourceNr, subjectItemNr, null, null, this, false),
				new Item(verbSourceNr, verbItemNr, null, null, this, false),
				new Item(Long.MIN_VALUE, Long.MIN_VALUE, null, null, this, false),
				this);
		else {
			if (object.type == Datum.Type.ITEM)
				return new ItemTriple(
						Long.MIN_VALUE,
						new Item(subjectSourceNr, subjectItemNr, null, null, this, false),
						new Item(verbSourceNr, verbItemNr, null, null, this, false),
						object.getItemValue(),
						this); 
			else return new NonItemTriple(
						Long.MIN_VALUE,
						new Item(subjectSourceNr, subjectItemNr, null, null, this, false),
						new Item(verbSourceNr, verbItemNr, null, null, this, false),
						object.type,
						object.summaryValue,
						this);	
		}
	}
	
	/**
	 * Return the highest triple that matches the given specification. 
	 * If the specified subject, verb, or object is null then the 
	 * returned triple will have the highest possible subject, verb, or 
	 * object. Also, the triple identifier will have the greatest possible 
	 * value. The returned triple will be greater than or equal to any 
	 * triple that matches the specification in any of the three orders 
	 * used to index triples.
	 * 
	 * @param subject the specified subject, or null if no subject is 
	 * specified
	 * 
	 * @param verb the specified verb, or null if no verb is specified
	 * 
	 * @param object the specified object, or null if no object is 
	 * specified
	 * 
	 * @return the highest triple that matches the given specification
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	private Triple highestMatchingTriple(Item subject, Item verb, Datum object) 
			throws TripleStoreNonFatalException {
		
		long subjectSourceNr;
		if (subject == null) subjectSourceNr = Long.MAX_VALUE;
		else subjectSourceNr = subject.sourceNr;
		long subjectItemNr;
		if (subject == null) subjectItemNr = Long.MAX_VALUE;
		else subjectItemNr = subject.itemNr;
		long verbSourceNr;
		if (verb == null) verbSourceNr = Long.MAX_VALUE;
		else verbSourceNr = verb.sourceNr;
		long verbItemNr;
		if (verb == null) verbItemNr = Long.MAX_VALUE;
		else verbItemNr = verb.itemNr;
		if (object == null) return new NonItemTriple(
				Long.MAX_VALUE,
				new Item(subjectSourceNr, subjectItemNr, null, null, this, false),
				new Item(verbSourceNr, verbItemNr, null, null, this, false),
				Datum.Type.BINARY,
				Long.MAX_VALUE,
				this);
		else {
			if (object.type == Datum.Type.ITEM)
				return new ItemTriple(
						Long.MAX_VALUE,
						new Item(subjectSourceNr, subjectItemNr, null, null, this, false),
						new Item(verbSourceNr, verbItemNr, null, null, this, false),
						object.getItemValue(),
						this); 
			else return new NonItemTriple(
						Long.MAX_VALUE,
						new Item(subjectSourceNr, subjectItemNr, null, null, this, false),
						new Item(verbSourceNr, verbItemNr, null, null, this, false),
						object.type,
						object.summaryValue,
						this);		
		}
	}

	/**
	 * Get the unique triple with the given subject, verb, and object, 
	 * if it is readable by the requesting store session.
	 * 
	 * @param subject the given subject, or null, in which case all 
	 * subjects match
	 * 
	 * @param verb the given verb, or null, in which case all verbs 
	 * match
	 * 
	 * @param object the given object, or null, in which case all 
	 * objects match
     *    
     * @param session the store session requesting the triples.
	 * 
	 * @return the triple if there is exactly one that matches and it 
	 * is readable by the session, or null.
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public Triple uniqueTriple(
			Item subject, 
			Item verb, 
			Datum object, 
			StoreSession session) 
					throws 	NotInOperationException, 
							TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException  {

		List<Triple> triples = getTriples(subject, verb, object, 
				new StoreSession(accessLevelManager.highest));
		if (triples.size() != 1) return null;
		Triple t = triples.get(0);
		if (canReadTriple(t, session)) return t;
		return null;
	}
	
	/**
	 * Ensure there is a unique triple with a given subject, verb and object. 
	 * Delete other triples with this subject verb and object if they exist
	 * and can be deleted by the requesting store session. If one exists that 
	 * cannot be deleted, return null.
	 * 
	 * @param subject the given subject. It must be locked by the requesting 
	 * store session before this method is invoked
	 * 
	 * @param verb the given verb. The requesting store session must have read
	 * access to it
	 *  
	 * @param object the given object. The requesting store session must have 
	 * read access to it if it is an item.
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the unique triple with the given subject, verb and object if it 
	 * already exists or can be created, null otherwise.
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 */
	
	public Triple setUniqueTriple(
			Item subject, 
			Item verb, 
			Datum object, 
			StoreSession session) 
					throws 	NotInOperationException, 
							TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException  {
		
		if (!subject.checkLocked(session)) return null;
		List<Triple> triples = getTriples(subject, verb, object, new StoreSession(accessLevelManager.highest));
		// Ensure all matching triples can be read
		for (Triple t : triples) {
			if (!canReadTriple(t, session)) return null;
		}
		// If there are no matching triples, return a new one
		if (triples.isEmpty()) return createTriple(subject, verb, object, session);
		// If there already is a unique matching triple, return it
		if (triples.size() == 1) return triples.get(0);
		// Ensure that all non-unique triples can be deleted.
		for (Triple t : triples) {
			if (!canDeleteTriple(t, session)) return null;
		}
		// Find the first matching triple and delete the others
		Triple first = null;
		for (Triple t : triples) {
			if (first == null) first = t;
			else if (!deleteTriple(t, session)) throw (new TripleStoreNonFatalException(
					"Cannot delete non-unique triple: " + t));
		}
		// Return the first matching triple
		return first;
	}
	
	/**
	 * Ensure there is a unique triple with a given subject and verb, and that 
	 * it has a given object. Delete other triples with this subject and verb 
	 * if they exist and can be deleted by the requesting store session. If one 
	 * exists that cannot be deleted, return null.
	 * 
	 * @param subject the given subject. It must be locked by the requesting store
	 * session before this method is invoked
	 * 
	 * @param verb the given verb. The requesting store session must have read 
	 * access to it
	 *  
	 * @param object the given object. The requesting store session must have read 
	 * access to it if it is an item.
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the unique triple with the given subject and verb if it already 
	 * exists with the given object or can be created with the given object.
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 */
	
	public Triple setUniqueObject(
			Item subject, 
			Item verb, 
			Datum object, 
			StoreSession session) 
					throws 	NotInOperationException, 
							TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException  {
		
		if (!subject.checkLocked(session)) return null;
		List<Triple> triples = getTriples(subject, verb, null, 
				new StoreSession(accessLevelManager.highest));
		// If there already is a unique matching triple, and it can be read 
		// by the session, return it
		if (triples.size() == 1) {
			Triple t = triples.get(0);
			if (canReadTriple(t, session) && t.getObject(session).equals(object))
				return t;
		}
		// Ensure that any triple with matching subject and verb can be
		// deleted, and find one that matches completely if possible
		// (If a triple can be deleted, it can also be read.)
		Triple foundTriple = null;
		for (Triple t : triples) {
			if (!canDeleteTriple(t, session)) return null;
			if ((foundTriple == null) && t.getObject(session).equals(object)) 
				foundTriple = t;			
		}
		// Delete all triples with matching subject and verb except for
		// the found one (if any) with matching object
		for (Triple t : triples) {
			if (t != foundTriple) {
				if (!deleteTriple(t, session)) throw (new TripleStoreNonFatalException(
						"Cannot delete non-unique object triple: " + t));
			}
		}
		// If no matching triple was found, return a new one
		if (foundTriple == null) {
			return createTriple(subject, verb, object, session);
		}
		// Otherwise return the one that was found
		else return foundTriple;
	}	
	
	/**
	 * Ensure that there are no triples with a given subject
	 * and verb
	 * 
	 * @param subject the subject
	 * 
	 * @param verb the verb
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if there were no matching triples or all
	 * matching triples have been deleted, false if there were
	 * matching triples that could not be deleted. If false is
	 * returned then no triples will have been deleted.
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 */
	
	public boolean ensureNoMatchingTriples(
			Item subject, 
			Item verb, 
			StoreSession session) 
					throws 	NotInOperationException, 
							TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException  {
		
		if (!subject.checkLocked(session)) return false;
		List<Triple> triples = getTriples(subject, verb, null, 
				new StoreSession(accessLevelManager.highest));
		// Ensure that any triple with matching subject and verb can be
		// deleted. (If a triple can be deleted, it can also be read.)
		for (Triple t : triples) {
			if (!canDeleteTriple(t, session)) return false;
		}
		// Delete all triples with matching subject and verb
		for (Triple t : triples) {
			if (!deleteTriple(t, session)) throw (new TripleStoreNonFatalException(
					"Cannot delete object triple: " + t));
		}
		return true;
	}	
}
