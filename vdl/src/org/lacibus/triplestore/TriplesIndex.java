/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

import org.lacibus.settheoryrels.OrderedPair;

	/**
	 * <p>An instance of this class contains the set of indexes 
	 * that are maintained for the triples in a triple store.
	 * These are:</p>
	 * <ul>
	 * <li>The triple records (which contain the triples and
	 *     their source connector references), indexed by the
	 *     triple identifiers</li>
	 * <li>The triples, indexed by subject, then verb, 
	 *     then object</li>
	 * <li>The triples, indexed by verb, then object,
	 *     then subject</li>
	 * <li>The triples, indexed by object, then subject,
	 *     then verb</li>
	 * </ul>
	 * <p>The methods of this class are synchronized so that
	 * access is thread-safe.
	 * </p>
	 */
	
class TriplesIndex implements Serializable {
		
	private static final long serialVersionUID = -1758109463691578846L;

	/**
	 * The triples, indexed by the triple identifiers
	 */

	private IdMap<Triple> triples = new IdMap<Triple>();

	/**
	 * The triples, indexed by subject, then verb, then object
	 */
	
	private TreeSet<Triple> svoIndex =
		new TreeSet<Triple>(new SVOComparator());
	// Note that initializing using svoComparator does not
	// work. Possibly to do with the order in which
	// initialization occurs.
	
	/**
	 * The triples, indexed by verb, then object, then subject
	 */
	
	private TreeSet<Triple> vosIndex =
		new TreeSet<Triple>(new VOSComparator());
	
	/**
	 * The triples, indexed by object, then subject, then verb 
	 */
	
	private TreeSet<Triple> osvIndex =
		new TreeSet<Triple>(new OSVComparator());
	
	/**
	 * Return the number of indexed triples
	 */
	
	synchronized int nrOfTriples() {
		return triples.size();
	}
	
	/**
	 * Add a triple to the indexes, replacing any equivalent triple
	 * that is already there. (When a source is disconnected and then
	 * quickly reconnected, a triple with the same id, subject, verb,
	 * and object may still be in the index, but will reference items
	 * that are not current.)
	 * 
	 * @param t the triple to be added
	 */
	
	synchronized void indexTriple(Triple t) {
		
		triples.put(t.getId(), t);
		svoIndex.remove(t);
		svoIndex.add(t);
		vosIndex.remove(t);
		vosIndex.add(t);
		osvIndex.remove(t);
		osvIndex.add(t);
	}

	/**
	 * Find any triples with the same subject and verb, and with 
	 * object of the same type and with the same summary value, 
	 * as a given triple.
	 * 
	 * @param t a triple
	 * 
	 * @return a list of potential matches
	 */
	
	synchronized List<Triple> findDuplicates(Triple t) {
		
		Triple low;
		Triple high;
		if (t.objectType == Datum.Type.ITEM) {
			low = new ItemTriple(Long.MIN_VALUE, t.subject, t.verb, ((ItemTriple)t).object, t.store);
			high = new ItemTriple(Long.MAX_VALUE, t.subject, t.verb, ((ItemTriple)t).object, t.store);
		}
		else {
			low = new NonItemTriple(
					Long.MIN_VALUE, t.subject, t.verb, t.objectType, t.getObjectSummaryValue(), t.store);
			high = new NonItemTriple(
					Long.MAX_VALUE, t.subject, t.verb, t.objectType, t.getObjectSummaryValue(), t.store);
		}
		return getTriplesInRange(svoIndex, low, high);
	}
	
	/**
	 * Remove a triple from the indexes
	 * 
	 * @param id the Id of the triple to be removed
	 * 
	 * @return the triple that was removed, or null if
	 * the index did not contain a triple with the
	 * given id.
	 */
	
	synchronized Triple deIndexTriple(Id id) {
		
		Triple t = triples.remove(id);
		if (t == null) return null;
		svoIndex.remove(t);
		vosIndex.remove(t);
		osvIndex.remove(t);
		return t;
	}
	
	/**
	 * Remove a triple from the indexes
	 * 
	 * @param t the triple to be removed
	 * 
	 * @return the triple that was removed, or null if
	 * the index did not contain a triple with the id
	 * of the given triple.
	 */
	
	synchronized Triple deIndexTriple(Triple t) {
		if (triples.remove(t.getId()) == null) return null;
		svoIndex.remove(t);
		vosIndex.remove(t);
		osvIndex.remove(t);
		return t;
	}
	
	/**
	 * Get a triple
	 * 
	 * @param id the triple's identifier
	 * 
	 * @return the triple with the given identifier,
	 * or null if there is no such triple
	 */
	
	synchronized Triple getTriple(Id id) {

		return triples.get(id);
	}
	
	/**
	 * Get the oldest triple record
	 * 
	 * @return the oldest triple record in the index
	 */
	
	synchronized Triple getOldestTriple() {
		return triples.getOldest();
	}
	
	/**
	 * Get the next younger triple record.
	 * 
	 * Successive calls to this method return all the
	 * triple records in order.
	 * 
	 * @return the next younger record in the index
	 */
	
	synchronized Triple getNextYoungerTriple() {
		
		return triples.getNextYounger();
	}
	
	/**
	 * Get the triples that lie within a given range in a
	 * given index
	 * 
	 * @param index the index 
	 * 
	 * @param low the least triple under the comparator of
	 * the index that is in range
	 * 
	 * @param high the greatest triple under the comparator of
	 * the index that is in range 
	 * 
	 * @return a list of the triples in the range
	 */
	
	synchronized List<Triple> getTriplesInRange(
			TreeSet<Triple> index, Triple low, Triple high) {
		
		List<Triple> results = new LinkedList<Triple>();
		Triple from = index.ceiling(low);
		if (from == null) return results;
		Triple to = index.floor(high);
		if (to == null) return results;
		if (index.comparator().compare(from, to) > 0) return results;
		NavigableSet<Triple> matches = index.subSet(from, true, to, true);
		for (Triple t : matches) results.add(t);
		return results;
	}
	
	/**
	 * <p>Get the triples that lie within given ranges in two
	 * given indexes.
	 * </p>
	 * <p>Because this method is synchronized, the two lists of
	 * triples reflect the state of the index at a single point
	 * in time.
	 * </p>
	 * @param index1 the first index
	 * 
	 * @param low1 the least triple under the comparator of
	 * the first index that is in range
	 * 
	 * @param high1 the least triple under the comparator of
	 * the first index that is in range
	 * 
	 * @param index2 the second index
	 * 
	 * @param low2 the least triple under the comparator of
	 * the second index that is in range
	 * 
	 * @param high2 the greatest triple under the comparator of
	 * the second index that is in range
	 * 
	 * @return an ordered pair of lists of triples in the 
	 * respective ranges
	 */
	
	synchronized OrderedPair<List<Triple>> getTriplesInRanges(
			TreeSet<Triple> index1, Triple low1, Triple high1,
			TreeSet<Triple> index2, Triple low2, Triple high2) {
		
		List<Triple> results1 = getTriplesInRange(index1, low1, high1);
		List<Triple> results2 = getTriplesInRange(index2, low2, high2);
		return new OrderedPair<List<Triple>>(results1, results2);
	}

	/**
	 * Determine the appropriate index to search for a
	 * given specification
	 * 
	 * @param subject the specified subject, or null if
	 * no subject is specified
	 * 
	 * @param verb the specified verb, or null if
	 * no verb is specified
	 * 
	 * @param object the specified object, or null if
	 * no object is specified
	 * 
	 * @return the appropriate index to search
	 */
	
	TreeSet<Triple> getIndex(
			Item subject, Item verb, Datum object) {			
		
		if (object == null) {
			if (verb == null) {
				if (subject == null) {
					// No subject, verb or
					// object is supplied
					return svoIndex;
				}
				else {
					// A subject is supplied, 
					// but no verb or object
					return svoIndex;
				}
			}
			else {
				if (subject == null) {
					// A verb is supplied, 
					// but no subject or object
					return vosIndex;
				}
				else {
					// A subject and verb are 
					// supplied, but no object
					return svoIndex;
				}
			}			
		}
		else {
			if (verb == null) {
				if (subject == null) {
					// An object is supplied, 
					// but no subject or verb
					return osvIndex;
				}
				else {
					// An object and subject are 
					// supplied, but no verb
					return osvIndex;					
				}
			}
			else {
				if (subject == null) {
					// An object and verb are 
					// supplied, but no subject
					return vosIndex;					
				}
				else {
					// A subject, verb and object 
					// are supplied
					return vosIndex;					
				}
			}	
		}
	}	
	
	/**
	 * @return the best index for finding triples given
	 * their verbs
	 */
	
	TreeSet<Triple> getVerbIndex() {
		
		return vosIndex;
	}	
}
	