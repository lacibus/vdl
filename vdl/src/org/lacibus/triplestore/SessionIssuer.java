package org.lacibus.triplestore;

import java.io.Serializable;

/**
 * An instance of this class issues store sessions. Because the
 * StoreSession constructor is not public, the only way that a
 * method in a class outside the triplestore package can obtain
 * a store session is by using an instance of this class. 
 */

public class SessionIssuer implements Serializable {

	private static final long serialVersionUID = 4103992519502789369L;
	
	private TripleStore store;
	
	/**
	 * Construct a session issuer for a triple store
	 * 
	 * @param store a triple store
	 */
	
	SessionIssuer(TripleStore store) {
		
		this.store = store;
	}
	
	/**
	 * @return the store for which the session issuer issues sessions
	 */
	
	public TripleStore getStore() {
		
		return store;
	}
	
	/**
	 * Get a store session
	 * 
	 * @param level the store session's access level.
	 * This must not be null, deleted, or being replaced.
	 * 
	 * @return a store session with the given access level,
	 * provided that it is an access level of the issuer's
	 * store
	 * 
	 * @throws TripleStoreNonFatalException if the access
	 * level is not an access level of the issuer's store,
	 * or if an error occurs when creating the session.
	 */
	
	public StoreSession getStoreSession(AccessLevel level) 
			throws TripleStoreNonFatalException {
		
		if (level == null) throw(new TripleStoreNonFatalException(
				"Session requested with null access level"));
		if (level.isOfTripleStore(store)) return new StoreSession(level);
		throw(new TripleStoreNonFatalException(
				"Session requested for another store"));
	}
}
