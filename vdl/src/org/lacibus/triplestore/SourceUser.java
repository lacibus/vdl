package org.lacibus.triplestore;

import org.lacibus.triplesource.SourceConnector;

/**
 * An implementation of this interface connects the triple store to
 * sources in order to use their data.
 */

public interface SourceUser {
	
	/**
	 * Get a source connector for a source
	 * 
	 * @param sourceNr the numeric identifier for the source
	 * 
	 * @param session the requesting store session
	 * 
	 * @return a source connector for the source with the given
	 * numeric identifier, or null. 
	 * 
	 * @throws Exception
	 */
	
	public SourceConnector getSourceConnector(
			long sourceNr, StoreSession session) throws Exception;

}
