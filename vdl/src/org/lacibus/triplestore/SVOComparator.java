/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparator for sorting triples in subject-verb-object order
 */

class SVOComparator implements Comparator<Triple>, Serializable {

	private static final long serialVersionUID = 6217584530973303656L;

	@Override
	public int compare(Triple t0, Triple t1) {
		try {
			int c = t0.subject.compareTo(t1.subject);
			if (c != 0) return c;
			c = t0.verb.compareTo(t1.verb);
			if (c != 0) return c;
			c = t0.objectType.compareTo(t1.objectType);
			if (c != 0) return c;
			if (t0.objectType == Datum.Type.ITEM) {
				c = ((ItemTriple)t0).object.compareTo(
						((ItemTriple)t1).object);
				if (c != 0) return c;
			}
			else {
				long objsum0 = t0.getObjectSummaryValue();
				long objsum1 = t1.getObjectSummaryValue();
				if (objsum0 < objsum1) return -1;		
				if (objsum0 > objsum1) return 1;
			}
		} catch (Exception e) {}
		return t0.compareTo(t1);
	}
}
