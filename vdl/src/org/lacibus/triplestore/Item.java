/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;
import java.util.Date;
import org.lacibus.triplesource.SourceConnector;
import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplesource.SourceConnectorStateException;

/**
 * <p>An item generally represents something, and can be the
 * subject, verb, or object of a triple.
 * </p>
 * <p>For example, one item might represent the person John Smith,
 * and another might represent the concept of a person having a 
 * family name. A triple whose subject is the first item, whose 
 * verb is the second item, and whose object is the string "Smith" 
 * then conveys the information that John Smith's family name is
 * "Smith".
 * </p>
 * <p>Items do not convey meaning in themselves. There is nothing
 * about the item representing John Smith to indicate that it
 * represents anything or anyone in particular. The meaning can be
 * conveyed in separate textual descriptions, such as, "The item
 * represents the person John Smith" and by the triples in which the
 * item appears. 
 * </p>
 * <p>Each item, apart from a few special items, is stored in a source.
 * </p>
 * <p>Each item is uniquely identified by a combination of two numbers,
 * one identifying the source that contains the item, and the other
 * uniquely identifying the item within that source. For special items,
 * the source number is zero; there is no real source with that number.
 * </p>
 * <p>Each item has two access levels: an only-read level, which 
 * controls who can read it, and a read-or-write level, which 
 * controls who can write it (and enables anyone who can write it
 * to read it also). Access to a triple is controlled by the access
 * levels of the items in the triple. The access levels of the items
 * thus provide fine-grained access control for all the data in a 
 * triple store.
 * </p>
 * <p>An item can be locked to prevent simultaneous writes by different
 * sessions. This also prevents simultaneous writes to triples,
 * because writing to a triple requires its subject to be locked.
 * The series of write actions performed while an item is locked
 * is a transaction. When the item is unlocked, the transaction is
 * completed, and the write actions are carried out in the item's source.
 * </p>
 */

public class Item implements Serializable, Comparable<Object> {
	
	private static final long serialVersionUID = -2914458731049964840L;

	/**
	 * The possible states of an item
	 */
	
	enum State {MAYEXIST, EXISTS, DELETED}
	
	/**
	 * The numeric identifier of the source containing the item.
	 */
	
    protected long sourceNr;
	
	/**
	 * The numeric identifier of the item within its source.
	 */
	
    protected long itemNr;
    
    /**
     * The only-read access level of the item
     */
    
    private AccessLevel onlyReadLevel = null;
    
    /**
     * The read-or-write access level of the item
     */
    
    private AccessLevel readOrWriteLevel = null;
    
    /**
     * The state of the item
     */
    
    private State state;
    
    /**
     * Get the state of the item
     * 
     * @return the state of the item
     */
    
    public State getState() {
    	
    	return state;
    }
    
    /**
     * The triple store holding the item. 
     */

    TripleStore store = null;

    /**
     * The lock that is used to lock the item to prevent
     * simultaneous writes.
     */
    
	volatile ItemLock itemLock = null;
	
	/**
	 * The maximum time allowed to unlock an item and complete
	 * the writes instigated while it was locked.
	 */
	
	private final int maxCompletionTime = 120000;		// 2 minutes
    
	/**
	 * The maximum time that a thread will wait to lock an item.
	 * Cannot be too long as user may be waiting.
	 */
	
	private final int maxLockWaitTime = 30000;		// 30 seconds
    
	/**
	 * The interval between checks made by a thread in case the
	 * lock has been released without it being interrupted
	 */
	
	private final int lockCheckTime = 3000;		// 3 seconds
    
	/**
	 * <p>Construct a new Item with a given source and a
	 * given numeric identifier within that source.
	 * </p>
	 * <p>The access levels and triple store are set to null.  
	 * The state is set to MAYEXIST. The item is not locked. 
	 * </p>
	 * <p>This constructor is used when constructing an item 
	 * datum with given numeric identifier.
	 * </p>
     * @param sourceNr the numeric identifier of the source of the item
     * 
     * @param itemNr the numeric identifier of the item in the source
	 */
	
	Item(long sourceNr, long itemNr) {
		
		this.sourceNr = sourceNr;
		this.itemNr = itemNr;
		this.onlyReadLevel = null;
		this.readOrWriteLevel = null;
		store = null;
		state = State.MAYEXIST;
		itemLock = null;
	}
	
	/**
	 * <p>Construct a new Item with given source, numeric identifier and 
	 * access levels. 
	 * </p>
	 * <p>The state of the item is set to EXISTS or MAYEXIST depending on
	 * the value of the knownToExist argument. The item is not locked. 
	 * </p>
	 * <p>This constructor is used when an item is loaded from its source. 
	 * </p>
     * @param sourceNr the numeric identifier of the source of the item
     * 
     * @param itemNr the numeric identifier of the item in the source
     * 
     * @param onlyReadLevel the only-read level
     * 
     * @param readOrWriteLevel the read-or-write level
     * 
     * @param store the triple store into which the item is being
     * loaded
     * 
     * @param knownToExist whether the item is known to exist
	 */
	
	Item(	long sourceNr, 
			long itemNr,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			TripleStore store,
			boolean knownToExist) {
		
		this.sourceNr = sourceNr;
		this.itemNr = itemNr;
		this.onlyReadLevel = onlyReadLevel;
		this.readOrWriteLevel = readOrWriteLevel;
		this.store = store;
		if (knownToExist) state = State.EXISTS;
		else state = State.MAYEXIST;
		itemLock = null;
	}
	
	/**
	 * <p>Construct a new Item with given source, numeric identifier
	 * and access levels, locked, and with a started transaction. 
	 * </p>
	 * <p>The state of the item is set to EXISTS.
	 * </p>
	 * <p>This constructor is used when an item is created. 
	 * </p>
     * @param sourceNr the identifier of the source of the item
     * 
     * @param itemNr the identifier of the item in the source
     * 
     * @param onlyReadLevel the only-read level of the item
     * 
     * @param readOrWriteLevel the read-or-write level of the item
     * 
     * @param tripleStore the triple store in which the item is
     * created
     * 
     * @param trans a newly-started transaction for the item.
     * 
     * @param session the store session requesting creation of
     * the item. This must not be null, deleted, or being
     * replaced.
     * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	Item(	
		long sourceNr, 
		long itemNr,
		AccessLevel onlyReadLevel,
		AccessLevel readOrWriteLevel,
		TripleStore store,
		Transaction trans,
		StoreSession session)
				throws 	SourceConnectorException, 
						TripleStoreNonFatalException {
		
		itemLock = new ItemLock(session);
		session.getAccessLevel();	// Checks that the session is valid
		this.sourceNr = sourceNr;
		this.itemNr = itemNr;
		this.onlyReadLevel = onlyReadLevel;
		this.readOrWriteLevel = readOrWriteLevel;
		this.store = store;
		itemLock.setTransaction(trans);
		state = State.EXISTS;
	}
	
	/**
	 * <p>Note that the item exists.
	 * </p>
	 * <p>If the state is MAYEXIST then it is set to EXISTS.
	 * </p>
	 */
	
	synchronized void noteExists() {
		
		if (state != State.MAYEXIST) return;
		state = State.EXISTS;
	}
	
	/**
	 * <p>Note that the item may not exist
	 * </p>
	 * <p>If the state is EXISTS then it is set to MAYEXIST and the
	 * items access levels are cleared.
	 * </p>
	 */
	
	synchronized void noteMayNotExist() {
		
		if (state != State.EXISTS) return;
		state = State.MAYEXIST;
		onlyReadLevel = null;
		readOrWriteLevel = null;
	}
	
	/**
	 * <p>Mark the item as deleted.
	 * </p>
	 * <p>The state is set to DELETED
	 * </p>
	 */
	
	synchronized void markDeleted() {

		state = State.DELETED;
	}
	
	/**
	 * Determine whether the item is current
	 * 
	 * @return true if the state of the item is EXISTS and its
	 * source is operational (or it is a special item that has no
	 * source), and if its only-read and read-or-write access levels
	 * exist and are in connected sources, false otherwise
	 */
	
	public boolean isCurrent() {
		
		if (state != State.EXISTS) return false;
		if (sourceNr == TripleStore.specialItemSource) return true;
		if (!store.connectorsManager.isOperational(sourceNr)) return false;
		try {
			AccessLevel orl = getLatestOnlyReadLevel();
			if (orl == null) return false;
			Item orlItem = orl.getItem();
			if (orlItem.state != State.EXISTS) return false;
			long orlSourceNr = orlItem.sourceNr;
			if (	(orlSourceNr != TripleStore.specialItemSource) &&
					(orlSourceNr != sourceNr) && 
					!store.connectorsManager.isOperational(orlSourceNr)) 
				return false;
			AccessLevel rowl = getLatestReadOrWriteLevel();
			Item rowlItem = rowl.getItem();
			if (rowlItem.state != State.EXISTS) return false;
			long rowlSourceNr = rowlItem.sourceNr;
			if (	(rowlSourceNr != TripleStore.specialItemSource) &&
					(rowlSourceNr != orlSourceNr) &&
					!store.connectorsManager.isOperational(rowlSourceNr)) 
				return false;
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Get the item's source number
	 * 
	 * @return the numeric identifier of the item's source
	 */
	
	public long getSourceNr() {
		return sourceNr;
	}
	
	/**
	 * Get the identification number of the item within its source
	 * 
	 * @return the numeric identifier of the item within its source
	 */
	
	public long getItemNr() {
		return itemNr;
	}
	
	/**
	 * Get the item's identifier
	 * 
	 * @return the item's identifier (which is a combination of the
	 * numeric identifier of the item's source and the numeric 
	 * identifier of the item within its source).
	 */
	
	public Id getId() {
		return new Id(sourceNr, itemNr);
	}
	
	/**
	 * <p>Lock the item.
	 * </p>
	 * <p>A transaction is started to include source connector
	 * operations involving the item while it is locked
	 * </p>
	 * @param session the store session for which the item
	 * is to be locked. This session must have write
	 * access to the item. This implies that the session
	 * access level is not null, deleted, or being replaced.
	 * 
	 * @return true if the item is locked successfully,
	 * false otherwise. If an exception is thrown then
	 * the item is not locked.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public boolean lock(
			StoreSession session) 
				throws 	TripleStoreNonFatalException,
						SourceConnectorException {
		
		// Check that the user can lock the item
		AccessLevel rowl = getEffectiveReadOrWriteLevel();
		if ((rowl == null) || !rowl.hasSuperior(session.getAccessLevel()))
			return false;
		SourceConnector connector = null;
		connector = store.connectorsManager.getConnector(sourceNr);
		if (connector == null) 
			// Source must have been disconnected
			return false;
		if (!connector.isOwner()) 
			throw (new TripleStoreNonFatalException(
					"Cannot lock item in non-owned source: " + sourceNr));

		// Update the lock in a synchronized section
		synchronized(this) {
			if (state != State.EXISTS)
				throw new TripleStoreNonFatalException("Item not current");
			// Wait if the item is locked by another session
			long endTime = System.currentTimeMillis() + maxLockWaitTime;
			while (	(itemLock != null) && !session.equals(itemLock.locker) && 
					(System.currentTimeMillis() < endTime)) {
				try {
					wait(lockCheckTime);
				} catch (InterruptedException e) {}
			}
			// If the item is no longer locked, lock it.
			// Otherwise, return true if the item is already locked by this session,
			// false if it is still locked by another session
			if (itemLock == null) {
				// Create the item lock before installing it in the item,
				// because it can be read by unsynchronized code
				ItemLock il = new ItemLock(session);
				il.nestedApplications = 1;
				itemLock = il;
			}
			else if (session.equals(itemLock.locker)) {
				itemLock.nestedApplications++;
				return true;
			}
			else if (state == State.EXISTS) return false;
			else throw new TripleStoreNonFatalException("Item not current");
		}
		
		// Start a transaction. Not synchronized as may require
		// disc access and take time
		Transaction trans = connector.startSourceTransaction();
		itemLock.setTransaction(trans);
		return true;
	}
	
	/**
	 * <p>Unlock the item
	 * </p>
	 * <p>The transaction that was started when the item
	 * was locked is ended.
	 * </p>
	 * <p>If the session is valid, source containing the item 
	 * is connected and owned by the triple store, the item is 
	 * current and was locked by the session, and this method 
	 * returns, then the item will no longer be locked. If it 
	 * cannot be unlocked, then its source is disconnected and 
	 * a source connector exception is thrown or, if this is
	 * not possible, operation of the triple store is aborted
	 * and a Fatal exception is thrown.
	 * </p>
	 * @param session the store session requesting the item
	 * be unlocked. 
	 * 
	 * @throws SourceConnectorException 
	 * @throws NotInOperationException 
	 * @throws TripleStoreFatalException 
	 */

	public void unlock(
			StoreSession session) 
					throws TripleStoreNonFatalException, SourceConnectorException,
					TripleStoreFatalException, NotInOperationException {
		
		SourceConnector connector = 
				store.connectorsManager.getConnector(sourceNr);
		if (connector == null)
			throw (new TripleStoreNonFatalException(
					"Source " + sourceNr + " is not connected"));
		if (!connector.isOwner()) 
			throw (new TripleStoreNonFatalException(
					"Cannot unlock item in non-owned source: " + sourceNr));
		completeUnlock(connector, null, session);
	}
	
	/**
	 * Unlock the item if it was locked before a given
	 * date. Used by the triple store maintainer to clean up
	 * old locks.
	 * 
	 * @param beforeDate the given date
	 * 
	 * @param session the store session requesting the item
	 * be unlocked. This will typically be a triple store
	 * maintenance session.
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException 
	 * @throws NotInOperationException 
	 * @throws TripleStoreFatalException 
	 */
	
	void unlockIfOld(long beforeDate, StoreSession session) 
			throws TripleStoreNonFatalException, SourceConnectorException, 
			TripleStoreFatalException, NotInOperationException {
		
		// Make initial check whether the item is locked while not synchronized		
		if (itemLock == null) return;
		
		// Find the source connector. If it is null then the source 
		// must have been disconnected; the item will be cleaned up 
		// later by the maintainer. 
		SourceConnector connector = 
				store.connectorsManager.getConnector(sourceNr);
		if (connector == null) return;
		
		// If the connector does not own the source, return
		if (!connector.isOwner()) return;
		
		// Complete the unlock and log the removal
		completeUnlock(connector, beforeDate, session);
	}
	
	/**
	 * Complete the unlocking of an item. (Performs processing that
	 * is common to unlocking requested by a user's session and
	 * removal of an old lock requested by the maintainer.) If the
	 * unlock operation cannot be completed, the item's source
	 * connector is closed and exception is thrown.
	 * 
	 * @param connector the source connector of the item's source
	 * 
	 * @param beforeDate if the unlocking is requested by the
	 * maintainer then this is the system time in milliseconds 
	 * such that the item should be unlocked if it was locked 
	 * before this time, but if the unlocking is requested by a 
	 * user's session then beforeDate is null.
	 * 
	 * @param session the requesting store session - either a
	 * user's or the maintainer's
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreNonFatalException 
	 * @throws NotInOperationException 
	 * @throws TripleStoreFatalException 
	 */
	
	private void completeUnlock(
			SourceConnector connector, Long beforeDate, StoreSession session) 
					throws SourceConnectorException, TripleStoreNonFatalException, 
					TripleStoreFatalException, NotInOperationException {
		
		// Test and update the lock in a synchronized section, 
		// because the method may be invoked simultaneously by 
		// normal processing and by maintenance.
		// Removal is then performed if appropriate by a separate 
		// TransactionCompleter thread, started by this method.		 
		TransactionCompleter completer = null;
		synchronized (this) {
			// Check that the item is current and the
			// session is valid
			if (state != State.EXISTS)
				throw new TripleStoreNonFatalException(
						"Item not current");			
			if (	(session == null)  || 
					!session.isOfTripleStore(store)) {
				throw new TripleStoreNonFatalException("Invalid session");	
			}
			
			// If the item is not locked, return
			if (itemLock == null) return;
			
			// If the session locked the item, decrement the number
			// of nested applications. 
			if (session == itemLock.locker) {
				itemLock.nestedApplications--;
				
				// If this is still greater than zero, return
				if (itemLock.nestedApplications > 0) return;
				
				// Otherwise, if an unlock is in progress, wait for the
				// unlock to complete. Initially, it must be an unlock for
				// maintenance. There is however a chance that a maintenance 
				// unlock could complete and the item could then be re-locked
				// by another session. If this happens, the data in the source
				// may not have been updated as intended.
				while (	(itemLock != null) && 
						itemLock.unlockInProgress && 
						(session == itemLock.locker)) {
					try {
						wait(1000);
					} catch (InterruptedException e) {}
				}
				
				// If the item lock is now null, it has been unlocked by a
				// maintenance thread
				if (itemLock == null) return;
				
				// If the item is now locked by another session, a maintenance 
				// unlock has completed and the item was then re-locked
				// by another session. 
				if (session != itemLock.locker)
					throw new TripleStoreNonFatalException(
							"Could not complete unlock. " +
							"Some data may not have been updated as intended");
				
				// Start another thread to complete the unlock
				itemLock.unlockInProgress = true;
				completer = new TransactionCompleter(itemLock.transaction, connector);
				completer.start();				
			}
			
			// If the session did not lock the item, and this is
			// not a maintenance operation, throw an exception
			else if (beforeDate == null)
				throw new TripleStoreNonFatalException("Session does not hold lock");
			
			// Otherwise, this is for maintenance; only unlock if the lock is old
			// and is not already being unlocked
			else if (itemLock.unlockInProgress || (itemLock.lockTime >= beforeDate)) return;
			
			// Otherwise, start another thread to complete the unlock for maintenance
			else {
				itemLock.unlockInProgress = true;
				completer = new TransactionCompleter(itemLock.transaction, connector);
				completer.start();
			}
		}
		
		// Wait for the transaction to be completed
		SourceConnectorException x = null;
		try {
			completer.join(maxCompletionTime);
		} catch (InterruptedException e) {}
		if (completer.isAlive()) x = new SourceConnectorStateException(
				connector.getSourceNr(), "Cannot complete transaction");
		else x = completer.exception;
		
		// If the transaction was not completed, close the source connector,
		// but don't throw an exception yet, or other threads will hang	
		if (x != null) store.disconnect(connector);
	
		synchronized(this) {
			// Remove the lock and notify any waiting threads.
			itemLock = null;
			notifyAll();
			
			// If the transaction was not completed, throw an exception
			if (x != null) throw(x);
		}
		
		// Log the removal if it was for maintenance
		if (beforeDate != null) store.logger.log(
				"Removed old lock from item: " + this);
	}
	
	/*
	 * FOR DEBUGGING THE LOCKING MECHANISM
	
	class LockRecord implements Comparable<LockRecord> {
		
		boolean forUnlock;
		boolean forMaintenance;
		long time;
		StoreSession session;
		String trace;
		
		LockRecord(boolean forUnlock, boolean forMaintenance, StoreSession session) {
			
			this.forUnlock = forUnlock;
			this.forMaintenance = forMaintenance;
			this.session = session;
			time = System.currentTimeMillis();
			try {
				throw (new IOException());
			} catch (Exception e) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				trace = sw.toString();
			}
		}
		
		@Override
		public int compareTo(LockRecord lr) {

			if (time < lr.time) return -1;
			if (time > lr.time) return 1;
			return 0;
		}
		
		public String toString() {
			
			String s;
			if (forUnlock) s = "UNLOCK AT "; else s = "LOCK AT ";
			if (forMaintenance) s += "FOR MAINTENANCE ";
			return s + " BY " + session + '\n' + trace + '\n';
		}
	}
	
	LinkedList<LockRecord> lockRecords = new LinkedList<LockRecord>();
	
	int lri = 0;
	
	void saveLockRecord(boolean forUnlock, boolean forMaintenance, StoreSession session) {
		
		synchronized(lockRecords) {
			lockRecords.add(new LockRecord(forUnlock, forMaintenance, session));
			if (lockRecords.size() > 10) lockRecords.remove();
		}
	}
	
	void printLockRecords() {
		
		synchronized(lockRecords) {
			System.out.println("LOCK RECORDS FOR " + toString());
			for (LockRecord lr : lockRecords) System.out.println(lr);
			lockRecords.clear();
		}
	}
	
	*/
	
	/**
	 * Check that the item is locked by a store session
	 * 
	 * @param session the store session for which the check
	 * is made. If this is the session that locked
	 * the item then true is returned and no exception
	 * is thrown, even if the session is invalid. If
	 * it is not the session that locked the item, and
	 * is invalid, then an exception is thrown.
	 * 
	 * @return true if the item is locked by the given
	 * session, false otherwise
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public synchronized boolean checkLocked(
					StoreSession session)
							throws TripleStoreNonFatalException {
		
		if (session == null)
				throw new TripleStoreNonFatalException(
						"Invalid session");
		if (state != State.EXISTS)
			throw new TripleStoreNonFatalException(
					"Item not current");
		if ((itemLock == null) || (session != itemLock.locker)) {
			if (!session.isOfTripleStore(store))
					throw new TripleStoreNonFatalException(
							"Invalid session");
			return false;
		}
		itemLock.lockTime = System.currentTimeMillis();
		return true;
	}
	
	/** 
	 * Determine whether the item is locked
	 * 
	 * @return true if the item is locked, false otherwise
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public synchronized boolean isLocked() 
			throws TripleStoreNonFatalException {
		
		if (state != State.EXISTS)
			throw new TripleStoreNonFatalException(
					"Item not current");
		return (itemLock != null);
	}
	
	/**
	 * Get the session that has locked the item.
	 * 
	 * @return the session that has locked the item
	 * if there is one, null otherwise
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	synchronized StoreSession getLocker()
			throws TripleStoreNonFatalException {
		
		if (state != State.EXISTS)
			throw new TripleStoreNonFatalException(
					"Item not current");
		if (itemLock == null) return null;
		else return itemLock.locker;
	}
	
	/**
	 * Get the transaction associated with the item.
	 * 
	 * @return the transaction associated with the item 
	 * if it is locked, null otherwise
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public synchronized Transaction getTransaction()
			throws TripleStoreNonFatalException {
		
		if (state != State.EXISTS)
			throw new TripleStoreNonFatalException(
					"Item not current");
		if (itemLock == null) return null;
		else return itemLock.transaction;
	}
	
	/**
	 * Determine whether a store session has read access
	 * 
	 * @param session a store session
	 * 
	 * @return whether the store session has read access
	 * to the item
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public boolean canBeReadBy(StoreSession session) 
			throws TripleStoreNonFatalException {
				
		// This method is not synchronized. Work with copy
		// of session level in case it changes.
		AccessLevel sessionLevel = session.getAccessLevel();

		AccessLevel orl = getEffectiveOnlyReadLevel();
		if (	(orl != null) &&
				(orl.hasSuperior(sessionLevel)))
			return true;

		AccessLevel rowl = (AccessLevel) getEffectiveReadOrWriteLevel();
		return (rowl != null) && rowl.hasSuperior(sessionLevel);
	}
	
	/**
	 * Get the effective only-read level
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the last access level in the chain of replacements,
	 * starting with the only-read level, if the chain contains
	 * only access levels, and if the last level can be read by
	 * the requesting session, null otherwise.
	 * 
	 * @throws TripleStoreNonFatalException
	 * 
	 */
	
	public AccessLevel getEffectiveOnlyReadLevel(StoreSession session)
			throws TripleStoreNonFatalException {
		
		AccessLevel orl = getEffectiveOnlyReadLevel();
		if (orl == null) return null;
		if (orl.canBeReadBy(session)) return orl;
		return null;
	}
	
	/**
	 * Get the effective only-read level
	 * 
	 * @return the last access level in the chain of replacements,
	 * starting with the only-read level, if the chain contains
	 * only access levels and the last level is current, null otherwise.
	 */
	
	AccessLevel getEffectiveOnlyReadLevel() {
		
		AccessLevel orl = getLatestOnlyReadLevel();
		if (orl == null) return null;
		else if (orl.isCurrent()) return orl;
		else return null;
	}
	
	/**
	 * Get the latest only-read level
	 * 
	 * @return the last access level in the chain of replacements,
	 * starting with the only-read level.
	 */
	
	AccessLevel getLatestOnlyReadLevel() {
		
		AccessLevel orl = onlyReadLevel;
		if (orl == null) return null;
		AccessLevel repl = orl.replacement;
		while (repl != null) {
			orl = repl;
			repl = orl.replacement;
		}
		return orl;
	}
	
	/**
	 * Get the effective read-or-write level
	 * 
	 * @return the last access level in the chain of replacements,
	 * starting with the read-or-write level, if the chain contains
	 * only access levels and the last level is current, null otherwise.
	 * 
	 */
	
	AccessLevel getEffectiveReadOrWriteLevel() {
		
		AccessLevel rowl = getLatestReadOrWriteLevel();
		if (rowl == null) return null;
		else if (rowl.isCurrent()) return rowl;
		else return null;
	}
	
	/**
	 * Get the latest read-or-write level
	 * 
	 * @return the last access level in the chain of replacements,
	 * starting with the read-or-write level.
	 * 
	 */
	
	AccessLevel getLatestReadOrWriteLevel() {
		
		AccessLevel rowl = readOrWriteLevel;
		if (rowl == null) return null;
		AccessLevel repl = rowl.replacement;
		while (repl != null) {
			rowl = repl;
			repl = rowl.replacement;
		}
		return rowl;
	}
	
	/**
	 * Determine whether a store session has write access
	 * 
	 * @param session a session
	 * 
	 * @return whether the session has write access
	 * to the item
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public boolean canBeWrittenBy(StoreSession session) 
			throws TripleStoreNonFatalException {
		
		// This method is not synchronized. Work with a copy
		// of the session level in case it changes.
		AccessLevel sessionLevel = session.getAccessLevel();

		// Check that the item is from a connected source - in 
		// particular, that it is not a special item
		SourceConnector connector = store.getSourceConnector(sourceNr);
		if (connector == null) return false;
		
		// Check that the item is from an owned source
		// (if not, it cannot be written whatever the session
		// access level)
		if (!connector.isOwner()) return false;
		
		// Check that the session access level is sufficient
		AccessLevel rowl = getEffectiveReadOrWriteLevel();
		return (rowl != null) && rowl.hasSuperior(sessionLevel);
	}
	
	/**
	 * Get the effective read-or-write level
	 * 
	 * @param session the requesting store session
	 * 
	 * @return the last access level in the chain of replacements,
	 * starting with the read-or-write level, if the chain contains
	 * only access levels, and if the last level can be read by
	 * the requesting session, null otherwise.
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public AccessLevel getEffectiveReadOrWriteLevel(StoreSession session)
			throws TripleStoreNonFatalException {
		
		AccessLevel rowl = getEffectiveReadOrWriteLevel();
		if (rowl == null) return null;
		if (rowl.canBeReadBy(session)) return rowl;
		return null;
	}
	
	/**
	 * <p>Maintain the item's access levels. 
	 * </p>
	 * <p>If the only-read level or the read-or-write level is an
	 * access level spec (which may be the case when the item has 
	 * been loaded from its source) and the access level specified
	 * by that access level spec is in the triple store then set it
	 * to the actual access level instead of to the spec.
	 * </p>
	 * <p>If the only-read level or the read-or-write level is an
	 * access level that has been replaced then it is set to the
	 * replacement. 
	 * </p>
	 * <p>This method is invoked by the maintenance thread of the
	 * item's triple store.
	 * </p>
	 */
	
	synchronized void maintainLevels() {
		
		if (onlyReadLevel != null) {
			AccessLevel level = onlyReadLevel;
			while (level.replacement != null)
				level = level.replacement;
			onlyReadLevel = level;
		}

		if (readOrWriteLevel != null) {
			AccessLevel level = readOrWriteLevel;
			while (level.replacement != null)
				level = level.replacement;
			readOrWriteLevel = level;
		}
	}
	
	void noteLevelsInUse() {
		
		if (onlyReadLevel instanceof AccessLevel) {
			Item orlItem = ((AccessLevel) onlyReadLevel).levelItem;
			if (!store.connectorsManager.isKnown(orlItem.sourceNr))
				store.itemsIndex.noteInUse(orlItem);
		}
		if (readOrWriteLevel instanceof AccessLevel) {
			Item rowlItem = ((AccessLevel) readOrWriteLevel).levelItem;
			if (!store.connectorsManager.isKnown(rowlItem.sourceNr))
				store.itemsIndex.noteInUse(rowlItem);
		}
	}
	
	/**
	 * <p>Set the item's only-read access level.
	 * </p>
	 * <p>The item must not be a special item of the triple
	 * store, or have been deleted.
	 * </p>
	 * <p>The item is written to its source by this operation.
	 * </p>
	 * @param level the only-read access level to be set. This
	 * level must be valid, and must not be being replaced.
	 * 
	 * @param session the store session requesting the change
	 * of level. The item must be locked by this session for 
	 * the operation to succeed, and it must have read access to
	 * the new only-read access level.
	 * 
	 * @return true if the level was set successfully,
	 * false otherwise
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public synchronized boolean setOnlyReadLevel(
			AccessLevel level, StoreSession session)
					throws	TripleStoreNonFatalException, 
							SourceConnectorException,
							NotInOperationException {
		
		// Check that the session and triple store are valid
		if (	(session == null) || 
				(!session.isOfTripleStore(store)))
				throw new TripleStoreNonFatalException(
						"Invalid session");
		if (!isCurrent())
			throw new TripleStoreNonFatalException(
					"Item not current");
		if (level == null)
			throw new TripleStoreNonFatalException(
					"Invalid access level");
		if (store.isSpecialItem(this))
			return false;
		// The item must be locked by the session
		if (!checkLocked(session)) return false;
		// The new level must not be being replaced
		if (level.replacement != null) return false;
		// The requesting session must have read access to 
		// the access level
		if (!level.canBeReadBy(session))
			return false;
		// Set the level
		onlyReadLevel = level;
		// Write the item to its source
		putItem();
		return true;
	}
	
	/**
	 * <p>Set the item's read-or-write access level.
	 * </p>
	 * <p>The item must not be a special item of the triple
	 * store, or have been deleted
	 * </p>
	 * <p>The item is written to the sourcee by this operation. 
	 * </p>
	 * @param level the read-or-write access level to be set.
	 * This level must be valid, and must not be being replaced.
	 * 
	 * @param session the session requesting the change of level. 
	 * The item must be locked by this session for the operation 
	 * to succeed and the session access level must be superior 
	 * to the new read-or-write level so that the session will 
	 * continue to have write access. The session must have read
	 * access to the new level.
	 * 
	 * @return true if the level was set successfully, false 
	 * otherwise
	 * 
	 * @throws NotInOperationException 
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public synchronized boolean setReadOrWriteLevel(
			AccessLevel level, 
			StoreSession session) 
					throws 	NotInOperationException,
							TripleStoreNonFatalException,
							TripleStoreFatalException,
							SourceConnectorException {
		
		if ((session == null) || 
			(!session.isOfTripleStore(store)))
			throw new TripleStoreNonFatalException(
					"Invalid session");
		if (!isCurrent())
			throw new TripleStoreNonFatalException(
					"Item not current");
		if (level == null) 
			throw new TripleStoreNonFatalException(
					"Invalid access level");
		if (store.isSpecialItem(this))
			return false;
		// The item must be locked by the session
		if (!checkLocked(session)) return false;
		// The new level must not be being replaced
		if (level.replacement != null) return false;
		// The requesting session must have read access to 
		// the access level
		if (!level.canBeReadBy(session))
			return false;
		// The requesting session must continue to have 
		// write access to the item after the change
		if (!level.hasSuperior(session.getAccessLevel())) 
			return false;
		// Set the new access level
		readOrWriteLevel = level;
		// Write the item to its source
		putItem();
		return true;
	}
	
    /**
     * Set the access levels. Not for general use, but may be
     * invoked by other classes in the package when creating
     * or loading an item. BEWARE that if this method is used
     * in other situations then the new levels will not persist
     * across a store re-start because they will not be written
     * to source.
     * 
     * @param onlyReadLevel the only-read level to be set.
     * 
     * @param readOrWriteLevel the read-or-write level to be set.
     */
    
    void setAccessLevels(
    			AccessLevel onlyReadLevel, 
    			AccessLevel readOrWriteLevel) {
    		this.onlyReadLevel = onlyReadLevel;
    		this.readOrWriteLevel = readOrWriteLevel;
    }
    
    /**
     * Write the item to its source if the triple store is the 
     * owner of the source
     * 
     * @throws SourceConnectorException
     * @throws TripleStoreNonFatalException
     */
    
	private void putItem() 
			throws 	SourceConnectorException, 
					TripleStoreNonFatalException {
		
		SourceConnector connector = 
				store.connectorsManager.getConnector(sourceNr);
		if ((connector != null) && connector.isOwner())
			connector.updateSourceItem(
					itemNr,
					onlyReadLevel.getEffectiveItemId(), 
					readOrWriteLevel.getEffectiveItemId(), 
					getTransaction());
	}
	
	/**
	 * Determine whether the item is equal to another item
	 * 
	 * @param it another item. If this is null, false is returned.
	 * 
	 * @return whether the item is equal to another item
	 */
	
	public boolean equals(Item it) {
		
		return (	(it != null) &&
					(sourceNr == it.sourceNr) &&
					(itemNr == it.itemNr) );
	}
	
	@Override
	public int compareTo(Object o) {

		Item it = (Item) o;
		if (sourceNr < it.sourceNr) return -1;
		if (sourceNr > it.sourceNr) return 1;
		if (itemNr < it.itemNr) return -1;
		if (itemNr > it.itemNr) return 1;
		return 0;
	}
	
	/**
	 * @return a long integer that summarises the item
	 */
	
	public long summary() {
		
		return sourceNr ^ itemNr;
	}
	
	@Override
	public int hashCode() {
		
		return ((int)sourceNr) ^ ((int)itemNr);
	}

	@Override
	public synchronized String toString() {
		// Needs to be synchronized to avoid null pointer errors
		String s = "Item " + "ID" + sourceNr + ':' + itemNr +
				" state = " + state +
				" only-read level = " + onlyReadLevel +
				" read-or-writelevel = " + readOrWriteLevel;
		if (itemLock != null) s = s + ", locked by " +
			itemLock.locker + " on " +
			new Date(itemLock.lockTime).toString();
		return s;
	}
	
	/**
	 * A lock that can be used to prevent concurrent writes to an item.
	 * It is a re-entrant lock. If a thread requests the lock while
	 * another thread holds it, the thread will wait until the other
	 * thread releases it. If several threads are waiting, the thread
	 * that obtains the lock need not be the one that has waited longest.
	 */
	
	private class ItemLock implements Serializable {

		private static final long serialVersionUID = -3598230455427242302L;
		
		/**
		 * The store session that created the lock
		 */
		
		private StoreSession locker = null;
		
		/**
		 * The time (milliseconds since the epoch) when the lock
		 * was created. Initially maximum long value to prevent
		 * lock being classed as "old". 
		 */
		
		private long lockTime = Long.MAX_VALUE;
		
		/**
		 * The number of times the lock has been applied by the
		 * requesting session. This is incremented when the session
		 * requests to apply the lock, and decremented when the
		 * session requests to remove it. The lock remains applied
		 * while it is non-zero.
		 */
		
		private int nestedApplications = 0;
		
		/**
		 * The transaction of actions performed while the lock
		 * is locked  
		 */
		
	    private Transaction transaction;
	    
	    /**
	     *  Whether the item is currently being unlocked
	     */
	    
	    private boolean unlockInProgress = false;
	    
	    /**
	     * Construct an item lock
	     * 
	     * @param session the requesting store session
	     */
	    
	    ItemLock(StoreSession session) {
	    	
			locker = session;
			lockTime = System.currentTimeMillis();   
	    }
	    	    
	    /**
	     * Set the transaction.
	     * 
	     * @param trans the transaction of actions performed while the lock
		 * is locked  
		 * 
	     * @throws TripleStoreNonFatalException
	     */
	    
	    void setTransaction(Transaction trans) throws TripleStoreNonFatalException {
	    	
	    	// Set the item lock information
			transaction = trans;
	    }
	}
	
	/**
	 * An instance of this class executes as a separate thread to
	 * write a transaction
	 */
	
	private class TransactionCompleter extends Thread implements Serializable {
		
		private static final long serialVersionUID = -2495329059404432954L;
		
		/**
		 * The transaction that is written
		 */
		
		Transaction trans;
		
		/**
		 * The source connecter of the source to which the transaction
		 * is written
		 */
		
		SourceConnector connector;
		
		/**
		 * An exception thrown during the write operation
		 */
		
		SourceConnectorException exception = null;
		
		/**
		 * Create a transaction completer
		 * 
		 * @param trans the transaction to be written
		 * 
		 * @param connector The source connecter of the source to which 
		 * the transaction is to be written
		 */
		
		private TransactionCompleter(Transaction trans, SourceConnector connector) {
			
			this.trans = trans;
			this.connector = connector;
		}
		
		@Override
		public void run() {
			
			try {
				connector.endSourceTransaction(trans);
			} catch (SourceConnectorException e) {
				exception = e;
			}
		}
	}
}
