package org.lacibus.triplestore;

import java.io.Serializable;
import java.util.Comparator;

/**
 * A Datum comparator that compares text datums lexicographically.
 * The default comparator does not do this, for good reasons
 * concerned with the way that datums are stored and retrieved.
 * Numeric datums are compared in the expected way.
 */

public class LexicalDatumComparator implements Comparator<Datum>, Serializable {

	private static final long serialVersionUID = 8245019193334132231L;

	@Override
	public int compare(Datum d1, Datum d2) {

		if (d1 == null) {
			if (d2 == null) return 0;
			else return -1;
		}
		else if (d2 == null) return 1;
		else
			try {
				return d1.lexicalCompareTo(d2);
			} catch (Exception e) {
				return d1.hashCode() - d2.hashCode();
			}
	}

}
