/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;

/**
 * An instance of this class is an identifier that can identify
 * an item or a triple. It has two components: the numeric
 * identifier of a source, and the numeric identifier of an item
 * or triple in that source.
 */

public class Id implements Comparable<Id>, Serializable {
	
	private static final long serialVersionUID = 8895813069969674049L;
	
	/**
	 * The numeric identifier of a source
	 */
	
	long sourceNr;
	
	/**
	 * The numeric identifier of an item or triple in the source.
	 */
	
	long idNr;
	
	/**
	 * Create an identifier.
	 * 
	 * @param sourceNr the numeric identifier of a source
	 * 
	 * @param idNr the numeric identifier of an item or triple in the source
	 */
	
	public Id (long sourceNr, long idNr) {
		
		this.sourceNr = sourceNr;
		this.idNr = idNr;
	}
	
	/**
	 * Get the numeric identifier of the source
	 * 
	 * @return the numeric identifier of the source
	 */
	
	public long getSourceNr() {
		
		return sourceNr;
	}
	
	/**
	 * Get the numeric identifier of the item or triple in the source
	 * 
	 * @return the numeric identifier of the item or triple in the source
	 */
	
	public long getIdNr() {
		
		return idNr;
	}
	
	@Override
	public int compareTo(Id id) {

		if (sourceNr < id.sourceNr) return -1;
		if (sourceNr > id.sourceNr) return 1;
		if (idNr < id.idNr) return -1;
		if (idNr > id.idNr) return 1;
		return 0;
	}
	
	public boolean equals(Object o) {
		
		if (o == null) return false;
		Id id = (Id) o;
		return (	(sourceNr == id.sourceNr) &&
					(idNr == id.idNr) );
	}
	
	public int hashCode() {
		
		return ((int)sourceNr) ^ ((int)idNr);
	}

	public String toString() {
		
		return "" + sourceNr + '_' + idNr;
	}
	
	/**
	 * Parse a string representation of an identifier
	 * 
	 * @param s a string representation of an identifier
	 * 
	 * @return the identifier that the string represents
	 */
	
	public static Id parseId(String s) {
		
		int i = s.indexOf('_');
		try {
			long source = Long.parseLong(s.substring(0, i));
			long ref = Long.parseLong(s.substring(i+1));
			return new Id(source, ref);
		} catch (Exception e) {
			return null;
		}
	}
}
