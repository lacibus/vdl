/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;

import org.lacibus.triplesource.SourceConnectorStateException;

/**
 * An instance of this class is a datum whose value is a boolean, an
 * integer, or a real number.
 */

class SimpleDatum extends Datum implements Serializable {

	private static final long serialVersionUID = 2774549322315497268L;

	/**
	 * Create a simple datum whose value is a boolean
	 * 
	 * @param b the boolean value of the datum
	 */
	
	SimpleDatum (boolean b) {
		
		type = Type.BOOLEAN;
		summaryValue = getSummary(b);
	}
	
	/**
	 * Create a simple datum whose value is an integer supplied as a
	 * 16-bit signed two's complement integer
	 * 
	 * @param s the integer value of the datum
	 */
	
	SimpleDatum (short s) {
		
		type = Type.INTEGRAL;
		summaryValue  = getSummary(s);
	}
	
	/**
	 * Create a simple datum whose value is an integer supplied as a 
	 * 32-bit signed two's complement integer
	 * 
	 * @param i the integer value of the datum
	 */
	
	SimpleDatum (int i) {
		
		type = Type.INTEGRAL;
		summaryValue  = getSummary(i);
	}
	
	/**
	 * Create a simple datum whose value is an integer supplied as a 
	 * 64-bit two's complement integer
	 * 
	 * @param l the integer value of the datum
	 */
	
	SimpleDatum (long l) {
		
		type = Type.INTEGRAL;
		summaryValue = getSummary(l);
	}
	
	/**
	 * Create a simple datum whose value is a real number supplied as a
	 * single-precision 32-bit IEEE 754 floating point number
	 * 
	 * @param f the real number value of the datum
	 */
	
	SimpleDatum (float f) {
		
		type = Type.REAL;
		summaryValue = getSummary(f);
	}
	
	/**
	 * Create a simple datum whose value is a real number supplied as a 
	 * double-precision 64-bit IEEE 754 floating point number
	 * 
	 * @param d the real number value of the datum
	 */
	
	SimpleDatum (Double d) {
		
		type = Type.REAL;
		summaryValue = getSummary(d);
	}
	
	@Override
	public boolean getBooleanValue() 
			throws TripleStoreNonFatalException {
		if (type == Type.BOOLEAN) 
			return getBooleanValueFromSummary(summaryValue);
		throw new TripleStoreNonFatalException(
				"Not a boolean datum");
	}
	
	@Override
	public long getIntegralValue() 
			throws TripleStoreNonFatalException {
		
		if (type == Type.INTEGRAL) 
			return (long) getIntegralValueFromSummary(summaryValue);
		throw new TripleStoreNonFatalException(
				"Not an integral datum");
	}
	
	@Override
	public double getRealValue() 
			throws TripleStoreNonFatalException {
		
		if (type == Type.REAL) 
			return getRealValueFromSummary(summaryValue);
		throw new TripleStoreNonFatalException(
				"Not a real datum");
	}

	@Override
	public String print() {
		
		return simpleTextRepresentation();
	}
	
	@Override
	public String simpleTextRepresentation() {
		try {
			switch (type) {
			case BOOLEAN: 
				return Boolean.toString(getBooleanValue());
			case INTEGRAL: 
				return Long.toString(summaryValue);
			case REAL: 
				return Double.toString(
					Double.longBitsToDouble(summaryValue));
			default:
				break;
			}
		}
		catch (Exception e) {}
		return null;
	}

	@Override
	public boolean valueShouldBeRead() {
		return false;
	}

	@Override
	public int getLength() {
		return 1;
	}

	@Override
	Object toObjectForJson() 
			throws TripleStoreNonFatalException, SourceConnectorStateException {

			switch (type) {
			case BOOLEAN: 
				return (Boolean)getBooleanValue();
			case INTEGRAL: 
				return (Long)getIntegralValue();
			case REAL: 
				return (Double)getRealValue();
			default:
				return null;
			}
	}
}
