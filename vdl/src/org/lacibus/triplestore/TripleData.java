package org.lacibus.triplestore;

import java.io.Serializable;

public class TripleData implements Serializable {
	
	private static final long serialVersionUID = 839917463680039886L;

	public Item subject;
	
	public Item verb;
	
	public Datum object;
	
	public TripleData(Item subject, Item verb, Datum object)
			throws TripleStoreNonFatalException {
		
		if ((subject == null) || (verb == null) || (object == null)) 
			throw (new TripleStoreNonFatalException(
					"TripleData cannot have null subject, verb or object"));
		this.subject = subject;
		this.verb = verb;
		this.object = object;
	}
}
