/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Base64;

/**
 * <p>An instance of this class is a BINARY datum whose value
 * is contained in a file. 
 * </p>
 * <p>Binary file datums are used to hold triple objects that
 * have been input but not yet stored in sources, and triple
 * objects that have been retrieved from the sources in which
 * they are stored. 
 * </p>
 */

class BinaryFileDatum extends Datum implements Serializable {
	
	private static final long serialVersionUID = 3099777014042818610L;
	
	/**
	 * A file that contains the value of the datum.
	 */
	
	File file;
	
	/**
	 * <p>Create a new Binary File Datum
	 * </p>
	 * <p>The summary value is not set, and will not be set until the
	 * datum is stored as a triple object.  
	 * </p>
	 * @param file a file containing the information represented by
	 * the datum.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	BinaryFileDatum(File file) throws TripleStoreNonFatalException{
		
		if (file == null) throw new TripleStoreNonFatalException(
				"Null file");
		if (Integer.MAX_VALUE <= file.length()) 
			throw (new TripleStoreNonFatalException("Too many bytes"));
		this.type = Type.BINARY;
		this.file = file;
	}

	@Override
	public byte[] getBinaryValueAsArray() 
			throws TripleStoreNonFatalException {
		
		// Get an input stream for the file, and get the bytes from it. 
		// If the file has been deleted, an exception will be thrown. 
		// If the input stream is obtained, then the file will not be 
		// actually deleted until the input stream is closed, even if 
		// another thread issues a delete call.
		try {
			return getBytesFromStream(
					new BufferedInputStream(
							new FileInputStream(file)));
		} catch (FileNotFoundException e) {
			TripleStoreNonFatalException ex =
					new TripleStoreNonFatalException(
							"File not found");
			ex.initCause(e);
			throw(ex);
		}
	}

	@Override
	public BufferedInputStream getBinaryValueStream() 
			throws TripleStoreNonFatalException {
		
		// Get an input stream for the file. If the file has been deleted,
		// an exception will be thrown. If the input stream is obtained, then
		// the file will not be actually deleted until the input stream is
		// closed, even if another thread issues a delete call.
		try {
			return new BufferedInputStream(
					new FileInputStream(file));
		} catch (FileNotFoundException e) {
			TripleStoreNonFatalException ex = 
					new TripleStoreNonFatalException(
							"File not found: " + file.getAbsolutePath());
			ex.initCause(e);
			throw(ex);
		}
	}

	@Override
	public boolean valueShouldBeRead() {
		return true;
	}

	@Override
	public String print() {
		try {
			return Datum.getByteString(getBinaryValueAsArray());
		} catch (TripleStoreNonFatalException e) {
			return "Binary file could not be read: " + e.getMessage();
		}
	}

	@Override
	public String simpleTextRepresentation() {
		return "Stream from which bytes are to be read";
	}

	@Override
	public int getLength() throws TripleStoreNonFatalException {
		long l = file.length();
		if (Integer.MAX_VALUE <= l) 
			throw (new TripleStoreNonFatalException("Too many bytes"));
		else return (int)l;
	}

	/**
	 * Get the binary file containing the value of the datum.
	 * 
	 * @return the binary file containing the value of the datum.
	 */
	
	public File getFile() {
		
		return file;
	}

	@Override
	Object toObjectForJson() throws TripleStoreNonFatalException {

		return Base64.getEncoder().encodeToString(getBinaryValueAsArray());
	}
}
