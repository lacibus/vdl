/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import org.json.simple.JSONObject;
import org.lacibus.triplesource.SourceConnectorException;
import org.lacibus.triplesource.SourceConnectorStateException;

/**
 * <p>An instance of this class represents a data value that can be
 * the object of a triple. It can be an item, or it can be a boolean,
 * an integer, a real number, a piece of text, or a sequence
 * of binary digits.
 * </p>
 * <p>A datum contains the type of the value and a summary of the value.
 * For some binary and text datums that are objects of triples, the 
 * value itself cannot be obtained from the datum and must be retrieved 
 * from the source holding the triple. For other binary and text datums, 
 * and for item, boolean, integer, and real number triples, the value can 
 * be obtained from the datum. 
 * </p>
 */

public abstract class Datum 
		implements Serializable, Comparable<Datum> {
	
	private static final long serialVersionUID = -5357642094411685623L;

	/**
	 * The possible types that a datum can have
	 */
	
	public enum Type {
		ITEM, BOOLEAN, INTEGRAL, REAL, TEXT, BINARY
	}
	// It is important that they are declared with 
	// ITEM least, BINARY greatest.
	
	/**
	 * The type of the datum
	 */
	
	protected Type type;
	
	/**
	 * The summary value of the datum. This is used in searches,
	 * so that large datums need only be retrieved from their
	 * sources if their summaries meet the search criteria.
	 */
	
	long summaryValue = 0;
	
	/**
	 * Get the type of the datum
	 * 
	 * @return the type of the datum
	 */
	
	public Type getType() {
		return type;
	}
	
	/**
	 * Get the summary value of the datum
	 * 
	 * @return the summary value of the datum
	 */
	
	public long getSummaryValue(){
		return summaryValue;
	}
	
	/**
	 * Set the summary value of the datum
	 * 
	 * @param value the summary value to be set
	 */
	
	public void setSummaryValue(Long value) {
		summaryValue = value;
	}
	
	public boolean equals(Object obj) {
		
		try {
			Datum d = (Datum) obj;
			return (compareTo(d) == 0);
		} catch (Exception e) {
			return false;
		}
	}
	
	public int hashCode() {
		return (int)summaryValue;		
	}
	
	/**
	 * Create an ITEM datum, given an item
	 * 
	 * @param item the value of the datum
	 * 
	 * @return an ITEM datum with the given item as value
	 */
	
	public static Datum create(Item item) {
		
		return new ItemDatum(item);
	}
	
	/**
	 * Create an ITEM datum, given the identification of an item
	 * 
	 * @param sourceNr the numeric identifier of the source of the 
	 * value of the datum
	 * 
	 * @param itemNr the numeric identifier of the value of the datum
	 * in its source
	 * 
	 * @return an ITEM datum with the identified item as value
	 */
	
	public static Datum create(long sourceNr, long itemNr) {
		
		return new ItemDatum(sourceNr, itemNr);
	}
	
	/**
	 * Create a BOOLEAN datum
	 * 
	 * @param b the boolean value of the datum
	 */
	
	public static Datum create(boolean b) {

		return new SimpleDatum(b);
	}
	
	/**
	 * Create an INTEGRAL datum whose value is supplied as a
	 * 16-bit signed two's complement integer
	 * 
	 * @param s the integer value of the datum
	 */
	
	public static Datum create(short s) {

		return new SimpleDatum(s);
	}
	
	/**
	 * Create an INTEGRAL datum whose value is supplied as a 
	 * 32-bit signed two's complement integer
	 * 
	 * @param i the integer value of the datum
	 */
	
	public static Datum create(int i) {

		return new SimpleDatum(i);
	}
	
	/**
	 * Create an INTEGRAL datum whose value is supplied as a 
	 * 64-bit two's complement integer
	 * 
	 * @param l the integer value of the datum
	 */
	
	public static Datum create(long l) {

		return new SimpleDatum(l);
	}
	
	/**
	 * Create a REAL datum whose value is supplied as a
	 * single-precision 32-bit IEEE 754 floating point number
	 * 
	 * @param f the real number value of the datum
	 */
	
	public static Datum create(float f) {

		return new SimpleDatum(f);
	}
	
	/**
	 * Create a REAL datum whose value is supplied as a 
	 * double-precision 64-bit IEEE 754 floating point number
	 * 
	 * @param d the real number value of the datum
	 */
	
	public static Datum create(Double d) {

		return new SimpleDatum(d);
	}
	
	/**
	 * Create a TEXT datum, given a string
	 * 
	 * @param string the value of the datum
	 * 
	 * @return a TEXT datum with the given value
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static Datum createText(String string) 
			throws TripleStoreNonFatalException {
		
		return new StringDatum(string);
	}
	
	/**
	 * Create a TEXT datum, given a text file
	 * 
	 * @param file a file containing the value of the datum
	 * 
	 * @return a TEXT datum with the given value
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static Datum createText(File file)
			throws TripleStoreNonFatalException {
		
		return new TextFileDatum(file);
	}
	
	/**
	 * Create a TEXT datum, given the identification of a triple
	 * with a text object.
	 * 
	 * @param sourceNr the numeric identifier of the source of the 
	 * triple whose object is the value of the datum
	 * 
	 * @param tripleNr the numeric identifier in its source of the 
	 * triple whose object is the value of the datum
	 * 
	 * @param summaryValue the summary of the triple object's value
	 * 
	 * @return a TEXT datum with the given value
	 */
	
	public static Datum createText(long sourceNr, long tripleNr, long summaryValue) {
		
		return new TextSourceConnectorDatum(sourceNr, tripleNr, summaryValue);
	}
	
	/**
	 * Create a BINARY datum, given a byte array
	 * 
	 * @param bytes the value of the datum
	 * 
	 * @return a BINARY datum with the given value
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static Datum createBinary(byte[] bytes) 
			throws TripleStoreNonFatalException {
		
		return new ByteArrayDatum(bytes);
	}
	
	/**
	 * Create a BINARY datum, given a binary file
	 * 
	 * @param file a file containing the value of the datum
	 * 
	 * @return a BINARY datum with the given value
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static Datum createBinary(File file) 
			throws TripleStoreNonFatalException {
		
		return new BinaryFileDatum(file);
	}
	
	/**
	 * Create a BINARY datum, given the identification of a triple
	 * with a binary object.
	 * 
	 * @param sourceNr the numeric identifier of the source of the 
	 * triple whose object is the value of the datum
	 * 
	 * @param tripleNr the numeric identifier in its source of the 
	 * triple whose object is the value of the datum
	 * 
	 * @param summaryValue the summary of the triple object's value
	 * 
	 * @return a BINARY datum with the given value
	 */
	
	public static Datum createBinary(long sourceNr, long tripleNr, long summaryValue) {
		
		return new BinarySourceConnectorDatum(sourceNr, tripleNr, summaryValue);
	}
	
	/**
	 * Determine whether the value needs to be retrieved from the
	 * source of a triple
	 * 
	 * @return whether the value needs to be retrieved from the
	 * source of a triple
	 */
	
	public boolean heldInSource() {
		
		// Over-ridden by datum subclasses for which the value
		// is held in the source.
		return false;
	}
	
	/**
	 * Get the value of the datum if this is an ITEM,
	 * or throw an exception if it is not.
	 * 
	 * @return the datum's value if it is an ITEM
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public Item getItemValue()
			throws TripleStoreNonFatalException {
		
		// Over-ridden by the item datum subclass. For other
		// kinds of datum, throw an exception.
		throw new TripleStoreNonFatalException(
				"Not an item datum");
	}
	
	/**
	 * Get the value of a datum if this is a BOOLEAN,
	 * or throw an exception if it is not.
	 * 
	 * @return the datum's value if it is a BOOLEAN
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public boolean getBooleanValue() 
			throws TripleStoreNonFatalException {
		
		// Over-ridden by the boolean datum subclass. For other
		// kinds of datum, throw an exception.
		throw new TripleStoreNonFatalException(
				"Not a boolean datum");
	}
	
	/**
	 * Get the value of a datum if this is an INTEGRAL,
	 * or throw an exception if it is not.
	 * 
	 * @return the datum's value if it is an INTEGRAL
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public long getIntegralValue()
			throws TripleStoreNonFatalException {
		
		// Over-ridden by the integral datum subclass. For other
		// kinds of datum, throw an exception.
		throw new TripleStoreNonFatalException(
				"Not an integral datum");
	}

	/**
	 * Get the value of a datum if this is a REAL,
	 * or throw an exception if it is not.
	 * 
	 * @return the datum's value if it is a REAL
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	public double getRealValue() 
			throws TripleStoreNonFatalException {
		
		// Over-ridden by the SimpleDatum subclass. For other
		// kinds of datum, throw an exception.
		throw new TripleStoreNonFatalException(
				"Not a real datum");
	}

	/**
	 * <p>Get the value of the  datum as a String if
	 * it is a TEXT object, or throw an exception if it is not.
	 * </p>
	 * <p>NOTE that the value can be very large. This method
	 * should only be used where it is known that the
	 * value is not too large.
	 * </p>
	 * @return the datum's value if it is a TEXT
	 * object
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public String getTextValueAsString()
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException {
		
		// Over-ridden by text datum subclasses. For other
		// kinds of datum, throw an exception.
		throw new TripleStoreNonFatalException(
				"Not a text datum");
	}

	/**
	 * Get the value of the datum as a string for display
	 * if it is a TEXT object, or throw an exception if it is not.
	 * 
	 * @return a string representation of the datum's value
	 * if it is a short TEXT object, or a string saying that
	 * it is long if it is a long TEXT object. 
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public String getTextValueForDisplay()
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException {
		
		if (type != Type.TEXT) throw new TripleStoreNonFatalException(
					"Not a text datum");
		if (getLength() < 256) {
			return getTextValueAsString();
		}
		return "(Long text)";
	}
	
	/**
	 * Get a Reader to read the value of the datum if
	 * it is a TEXT object, or throw an exception if it is not.
	 * 
	 * @return a Reader to read the datum's value if it is a TEXT
	 * object
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public BufferedReader getTextValueReader()
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException {
		
		// Over-ridden by text datum subclasses. For other
		// kinds of datum, throw an exception.
		throw new TripleStoreNonFatalException(
				"Not a text datum");
	}

	/**
	 * <p>Get the value of the datum as a byte array
	 * if it is a BINARY object
	 * </p>
	 * <p>NOTE that the value can be very large. This method
	 * should only be used where it is known that the
	 * value is not too large.
	 * </p>
	 * @return the datum's value if it is a BINARY
	 * object, or throw an exception if it is not.
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public byte[] getBinaryValueAsArray()
			throws 	TripleStoreNonFatalException,
					SourceConnectorException {
		
		// Over-ridden by binary datum subclasses. For other
		// kinds of datum, throw an exception.
		throw new TripleStoreNonFatalException(
				"Not binary datum");
	}
	
	/**
	 * Get the value of the datum as a string for display
	 * if its is a BINARY object
	 * 
	 * @return a string representation of the datum's value
	 * if it is a short BINARY object, or a string saying that
	 * it is long if it is a long BINARY object. An exception
	 * is thrown if it is not a BINARY object.
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException
	 */
	
	public String getBinaryValueForDisplay()
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException {
		if (type != Type.BINARY)
			throw new TripleStoreNonFatalException(
					"Not a binary datum");
		if (getLength() < 256) {
			byte[] bytes = getBinaryValueAsArray();
			return Arrays.toString(bytes);
		}
		return "(Long binary)";
	}
	
	/**
	 * Get an InputStream to read the value of the datum if
	 * it is a BINARY object
	 * 
	 * @return an InputStream to read the datum's value if it is a
	 * BINARY object, or throw an exception if it is not.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorException 
	 */
	
	public BufferedInputStream getBinaryValueStream() 
			throws TripleStoreNonFatalException, SourceConnectorException {
		
		// Over-ridden by binary datum subclasses. For other
		// kinds of datum, throw an exception.
		throw new TripleStoreNonFatalException(
				"Not binary datum");
	}

	/**
	 * <p>Compare to another datum. Note that the summaries of TEXT
	 * and BINARY values are compared first, and the actual values
	 * are only compared if the summaries are equal. This means that
	 * a result of zero is returned if, and only if, the values are
	 * equal but, if the summaries are not equal, then the value 
	 * returned (-1 or 1) does not correspond to the natural comparison
	 * between the values. For example, if the datum is a TEXT datum
	 * and its value is lexicographically less than the value of a
	 * TEXT datum with which it is compared, but its summary is greater
	 * than the summary of the other datum, a result of +1 will be
	 * returned, not -1.
	 * </p>
	 * <p>This enables comparisons to be made without retrieving large
	 * values unless their summaries are equal.
	 * </p>
	 */
	
	@Override
	public int compareTo(Datum d) {
		if (d == null) return 1;
		int c = type.compareTo(d.type);
		if (c != 0) return c;
		switch (type) {
		case ITEM :
			try {
				return getItemValue().compareTo(d.getItemValue());
			} catch (TripleStoreNonFatalException e1) {
				// Cannot occur as both datums have ITEM type
				return -1;
			}
		case BOOLEAN :
		case INTEGRAL : 
			if (summaryValue < d.summaryValue) return -1;
			if (summaryValue > d.summaryValue) return 1;
			return 0;
		case REAL :
			double r1 = Double.longBitsToDouble(summaryValue);
			double r2 = Double.longBitsToDouble(d.summaryValue);
			if (r1 < r2) return -1;
			if (r1 > r2) return 1;
			return 0;
		case TEXT :
			try {
				int l1 = lengthComponent(summaryValue);
				int l2 = lengthComponent(d.summaryValue);
				if (l1 < l2) return -1;
				if (l1 > l2) return 1;
				return textCompare(d);
			}
			catch (Exception e) {
				return -2;
			}
		case BINARY :
			try {
				int l1 = lengthComponent(summaryValue);
				int l2 = lengthComponent(d.summaryValue);
				if (l1 < l2) return -1;
				if (l1 > l2) return 1;
				if ((valueShouldBeRead()) || 
					(d.valueShouldBeRead())) {
					InputStream s1 = getBinaryValueStream();
					InputStream s2 = d.getBinaryValueStream();
					try {
						int b1;
						int b2;
						do {
							b1 = s1.read();
							b2 = s2.read();
							if (b1 < b2) return -1;
							if (b1 > b2) return 1;
						} while ((b1 >= 0) && (b2 >= 0));
						return 0;
					} catch(Exception e) {
						return -2;
					}
					finally {
						try {
							s1.close();
							s2.close();
						}
						catch(Exception e) {
							return -2;
						}						
					}
				}
				else {
					byte[] b1 = getBinaryValueAsArray();
					byte[] b2 = d.getBinaryValueAsArray();
					for (int i = 0;
							(i < b1.length) && 
							(i < b2.length); i++){
						if (b1[i] < b2[i]) return -1;
						if (b1[i] > b2[i]) return 1;
					}
					if (b1.length < b2.length) return -1;
					if (b1.length > b2.length) return 1;
					return 0;
				}
			}
			catch (Exception e) {
				return -2;
			}
		}
		return 0;
	}
	
	/**
	 * Compare the value of the datum, if it is a TEXT datum,
	 * with that of another TEXT datum. Throw an exception if 
	 * either this or the other datum is not a TEXT datum.
	 * 
	 * @param datum a TEXT datum.
	 * 
	 * @return -1, 0, or 1 if the value is lexicographically
	 * less than, equal to, or greater than the value of the
	 * given TEXT datum. 
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 */
	
	private int textCompare(Datum datum) 
			throws 	TripleStoreNonFatalException, 
					SourceConnectorException {
		
		if (	(valueShouldBeRead()) ||
				(datum.valueShouldBeRead())) {
			Reader r1 = getTextValueReader();
			Reader r2 = datum.getTextValueReader();
			try {
				int c1;
				int c2;
				do {
					c1 = r1.read();
					c2 = r2.read();
					if (c1 < c2) return -1;
					if (c1 > c2) return 1;
				} while ((c1 >= 0) && (c2 >= 0));
				return 0;
			}catch(Exception e) {
				return -2;
			}
			finally {
				try {
					r1.close();
					r2.close();
				}
				catch(Exception e) {
						return -2;
				}
			}
		}
		else {
			return getTextValueAsString().compareTo(
				datum.getTextValueAsString());
		}
	}
	
	/**
	 * Compare the datum with another datum. If they are both
	 * TEXT datums, the values are compared lexicographically. 
	 * (There is a good reason why this is not done in the 
	 * straightforward compareTo() method.)
	 * 
	 * @param d another datum
	 * 
	 * @return -1, 0, or 1, depending on whether this datum
	 * is less than, equal to, or greater than the other, or
	 * -2 if an error occurs.
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public int lexicalCompareTo(Datum d) 
			throws 	TripleStoreNonFatalException, 
				SourceConnectorException {

		if (d == null) return 1;
		if ((type == Type.TEXT) && (d.type == Type.TEXT)) return textCompare(d);
		return compareTo(d);
	}
	
	/**
	 * Determine whether it is better to read the value
	 * of this datum rather than obtaining it as a
	 * String or byte array.
	 * 
	 * @return true if it is better to read the value,
	 * false otherwise
	 */
	
	public abstract boolean valueShouldBeRead();
	
	/**
	 * Get the length of the value, expressed in the
	 * units of the value's type (bytes for BINARY,
	 * characters for TEXT).
	 * 
	 * @return the length of the value
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public abstract int getLength() throws TripleStoreNonFatalException;
	
	/**
	 * Get a text representation of the value of the 
	 * datum suitable for display
	 * 
	 * @return a textual representation of the datum's value.
	 */
	
	public abstract String print();

	/**
	 * Get a simple text representation of the datum suitable
	 * for display. If the value is large then the representation
	 * will just indicate what kind of value it is, rather than 
	 * including the whole value.
	 * 
	 * @return a simple textual representation of the
	 * datum.
	 */
	
	public abstract String simpleTextRepresentation();

	public String toString(){
		return type + ": " + simpleTextRepresentation();
	}

	/**
	 * <p>Get the length of a text or binary object from
	 * its summary value.
	 * </p>
	 * <p>The length of a text or binary object is contained
	 * in the top 32 bits of the summary value. 
	 * </p>
	 * @param summary the summary value of a text or
	 * binary object
	 * 
	 * @return the length of the object, in characters
	 * or bytes.
	 */
	
	public static int lengthComponent(long summary) {
		return (int)(summary >>> 32);		
	}
	
	/**
	 * Get the summary of an item value.
	 * 
	 * @param item an item
	 * 
	 * @return the summary value of the item
	 */
	
	public static long getSummary(Item item) {
		return item.summary();
	}
	
	/**
	 * Get the summary of a boolean value.
	 * 
	 * @param b a boolean value
	 * 
	 * @return the summary value of the boolean
	 */
	
	public static long getSummary(boolean b) {
		if (b) return 1;
		else return 0;		
	}

	/**
	 * Get the summary of an integral value.
	 * 
	 * @param integral an integer
	 * 
	 * @return the summary value of the integer
	 */
	
	public static long getSummary(long integral) {
		return integral;
	}
	
	/**
	 * Get the summary of a real value.
	 * 
	 * @param real a real number
	 * 
	 * @return the summary value of the real number
	 */
	
	public static long getSummary(double real) {
		return Double.doubleToRawLongBits(real);
	}
	
	/**
	 * Update the summary of a sequence of bytes when more bytes
	 * are added to it from a ByteBuffer.
	 * 
	 * @param summary the summary of the sequence of bytes to which
	 * more are to be added.
	 * 
	 * @param buf a buffer containing the bytes to be added
	 * 
	 * @return the summary of the sequence with the bytes in the 
	 * buffer added.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static long updateSummary(long summary, ByteBuffer buf) 
			throws TripleStoreNonFatalException {
		
		buf.position(0);
		int hash = (int)(summary & 0xFFFFFFFFL);
		int len = (int)(summary >>> 32);
		if ((Integer.MAX_VALUE - buf.remaining()) <= len) 
			throw (new TripleStoreNonFatalException("Too many bytes"));
		try {
			do {
				byte b = buf.get();
				hash = (hash << 13) ^ (hash >>> 19) ^ b;
				len++;
			} while (true);
		}
		catch (Exception e) {}
		return (((long)len) << 32) | 
				(((long)hash) & 0xFFFFFFFFL);
	}
	
	/**
	 * Update the summary of a sequence of bytes when more bytes
	 * are added to it from a byte array.
	 * 
	 * @param summary the summary of the sequence of bytes to which
	 * more are to be added.
	 * 
	 * @param buf a byte array containing the bytes to be added,
	 * which occupy the whole of the array.
	 * 
	 * @return the summary of the sequence with the bytes in the 
	 * array added.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static long updateSummary(long summary, byte[] buf) 
			throws TripleStoreNonFatalException {
		
		int hash = (int)(summary & 0xFFFFFFFFL);
		int len = (int)(summary >>> 32);
		if ((Integer.MAX_VALUE - buf.length) <= len) 
			throw (new TripleStoreNonFatalException("Too many bytes"));
		for (int i = 0; i < buf.length; i++)
			hash = (hash << 13) ^ (hash >>> 19) ^ buf[i];
		len = len + buf.length;
		return (((long)len) << 32) | 
				(((long)hash) & 0xFFFFFFFFL);
	}
	
	/**
	 * Update the summary of a sequence of bytes when more bytes
	 * are added to it from part of a byte array.
	 * 
	 * @param summary the summary of the sequence of bytes to which
	 * more are to be added.
	 * 
	 * @param buf a byte array containing the bytes to be added,
	 * which occupy the first part of the array.
	 * 
	 * @param length the number of bytes to be added. If this is
	 * greater than the length of the array then the whole of the
	 * array is added.
	 * 
	 * @return the summary of the sequence with the bytes in the 
	 * array added.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static long updateSummary(long summary, byte[] buf, int length) 
			throws TripleStoreNonFatalException {
		
		int hash = (int)(summary & 0xFFFFFFFFL);
		int len = (int)(summary >>> 32);
		if ((Integer.MAX_VALUE - length) <= len) 
			throw (new TripleStoreNonFatalException("Too many bytes"));
		if (length > buf.length) length = buf.length;
		for (int i = 0; i < length; i++)
			hash = (hash << 13) ^ (hash >>> 19) ^ buf[i];
		len = len + length;
		return (((long)len) << 32) | 
				(((long)hash) & 0xFFFFFFFFL);
	}
	
	/**
	 * Update the summary of a piece of text when a text string is
	 * added to it.
	 * 
	 * @param summary the summary of the piece of text to which the
	 * text string is to be added.
	 * 
	 * @param string the string to be added.
	 * 
	 * @return the summary of the text with the string added.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static long updateSummary(long summary, String string) 
			throws TripleStoreNonFatalException {
		
		int hash = (int)(summary & 0xFFFFFFFFL);
		int len = (int)(summary >>> 32);
		if ((Integer.MAX_VALUE - string.length()) <= len) 
			throw (new TripleStoreNonFatalException("Too many bytes"));
		char[] buf = string.toCharArray();
		for (int i = 0; i < buf.length; i++)
			hash = (hash << 13) ^ (hash >>> 19) ^ buf[i];
		len = len + string.length();
		return (((long)len) << 32) | 
				(((long)hash) & 0xFFFFFFFFL);
	}
	
	/**
	 * Update the summary of a piece of text when a character is
	 * added to it.
	 * 
	 * @param summary the summary of the piece of text to which the
	 * character is to be added.
	 * 
	 * @param c the character to be added.
	 * 
	 * @return the summary of the text with the character added.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static long updateSummary(long summary, char c) 
			throws TripleStoreNonFatalException {
		
		int hash = (int)(summary & 0xFFFFFFFFL);
		int len = (int)(summary >>> 32);
		if ((Integer.MAX_VALUE - 1) <= len) 
			throw (new TripleStoreNonFatalException("Too many bytes"));
		hash = (hash << 13) ^ (hash >>> 19) ^ c;
		len++;
		return (((long)len) << 32) | 
				(((long)hash) & 0xFFFFFFFFL);
	}
	
	/**
	 * Update the summary of a piece of text when more characters
	 * are added to it from a character array.
	 * 
	 * @param summary the summary of the piece of text to which
	 * more characters are to be added.
	 * 
	 * @param buf an array containing the characters to be added,
	 * which occupy the whole of the array.
	 * 
	 * @return the summary of the piece of text with the characters 
	 * in the array added.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static long updateSummary(long summary, char[] buf) 
			throws TripleStoreNonFatalException {
		
		int hash = (int)(summary & 0xFFFFFFFFL);
		int len = (int)(summary >>> 32);
		if ((Integer.MAX_VALUE - buf.length) <= len) 
			throw (new TripleStoreNonFatalException("Too many bytes"));
		for (int i = 0; i < buf.length; i++)
			hash = (hash << 13) ^ (hash >>> 19) ^ buf[i];
		len = len + buf.length;
		return (((long)len) << 32) | 
				(((long)hash) & 0xFFFFFFFFL);
	}
	
	/**
	 * Update the summary of a piece of text when more characters
	 * are added to it from part of an array.
	 * 
	 * @param summary the summary of the piece of text to which
	 * more characters are to be added.
	 * 
	 * @param buf an array containing the characters to be added,
	 * which occupy the first part of the array.
	 * 
	 * @param length the number of characters to be added. If this is
	 * greater than the length of the array then the whole of the
	 * array is added.
	 * 
	 * @return the summary of the piece of  with the characters in the 
	 * array added.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	public static long updateSummary(long summary, char[] buf, int length) 
			throws TripleStoreNonFatalException {
		
		int hash = (int)(summary & 0xFFFFFFFFL);
		int len = (int)(summary >>> 32);
		if ((Integer.MAX_VALUE - length) <= len) 
			throw (new TripleStoreNonFatalException("Too many bytes"));
		if (length > buf.length) length = buf.length;
		for (int i = 0; ((i < length) && (i < buf.length)); i++)
			hash = (hash << 13) ^ (hash >>> 19) ^ buf[i];
		len = len + length;
		return (((long)len) << 32) | 
				(((long)hash) & 0xFFFFFFFFL);
	}
	
	/**
	 * Get a boolean value from its summary
	 * 
	 * @param summary a summary of a boolean value
	 * 
	 * @return the boolean value that has the given summary
	 */
	
	public static boolean getBooleanValueFromSummary(long summary) {
		if (summary == 0)
			return (Boolean) false;
		else return (Boolean) true;
	}
	
	/**
	 * Get an integral value from its summary
	 * 
	 * @param summary a summary of an integral value
	 * 
	 * @return the integral value that has the given summary
	 */
	
	public static long getIntegralValueFromSummary(long summary) {
		return summary;
	}
	
	/**
	 * Get a real number value from its summary
	 * 
	 * @param summary a summary of a real number value
	 * 
	 * @return the real number value that has the given summary
	 */
	
	public static double getRealValueFromSummary(long summary) {
		return (Double) Double.longBitsToDouble(summary);
	}
	
	/**
	 * Read a stream into a byte array
	 * 
	 * @param stream a BufferedInputStream from which the
	 * bytes are to be read
	 * 
	 * @return a byte array containing the bytes read from
	 * the stream
	 * 
	 * @throws TripleStoreNonFatalException if an IO error occurs
	 */
	
	public static byte[] getBytesFromStream(BufferedInputStream stream)
			throws TripleStoreNonFatalException {
		
		List<Buffer> buffers = new ArrayList<Buffer>();
		int length = 0;
		do {
			Buffer buffer = new Buffer();
			try {
				buffer.length = stream.read(buffer.bytes);
			} catch (IOException e) {
				TripleStoreNonFatalException ex =
						new TripleStoreNonFatalException(
								"Could not read stream");
				ex.initCause(e);
				throw(ex);
			}
			if (buffer.length > 0) {
				buffers.add(buffer);
				length = length + buffer.length;
			}
			else if (buffer.length < 0) break;
		} while(true);
		byte[] result = new byte[length];
		int n = 0;
		for (Buffer b : buffers) {
			System.arraycopy(b.bytes, 0, result, n, b.length);
			n = n + b.length;
		}
		return result;
	}
	
	/**
	 * Get a text representation of a byte array
	 * 
	 * @param bytes a byte array
	 * 
	 * @return a text representation of the byte array
	 */
	
	public static String getByteString(byte[] bytes) {
		
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			if (i > 0) sb.append(' ');
			sb.append(Byte.toString(bytes[i]));
		}
		return sb.toString();
	}
	
	/**
	 * Parse a text representation of a sequence of bytes
	 * 
	 * @param s a string that is a text representation of a sequence 
	 * of bytes
	 * 
	 * @return the sequence of bytes represented by the string.
	 */
	
	public static byte[] parseByteString(String s) {
		
		String[] b = s.trim().split("\\s");
		byte[] result = new byte[b.length];
		for (int i = 0; i < b.length; i++)
			result[i] = Byte.decode(b[i]);
		return result;
	}
	
	/**
	 * Get a JSON object that represents the datum
	 * 
	 * @return a JSON object that represents the datum
	 * 
	 * @throws SourceConnectorStateException
	 * @throws TripleStoreNonFatalException
	 */
	
	@SuppressWarnings("unchecked")
	public JSONObject toJsonObject() 
			throws SourceConnectorStateException, TripleStoreNonFatalException {
		
		JSONObject jo = new JSONObject();
		jo.put("type", type.toString());
		jo.put("value",  toObjectForJson());
		return jo;
	}
	
	/**
	 * Get a datum from a JSON object that represents it.
	 * NOT exposed publicly as needs further testing.
	 * 
	 * @param jo a JSON object that represents a datum.
	 * 
	 * @param store the triple store containing the item
	 * that is the value of the datum if it is an item datum
	 * 
	 * @param session the requesting session
	 * 
	 * @return the datum represented by the JSON object, or
	 * null if the JSON object does not represent a datum
	 * 
	 * @throws NotInOperationException
	 * @throws TripleStoreNonFatalException
	 */
	
	static Datum getDatum(
			JSONObject jo, TripleStore store, StoreSession session) 
					throws NotInOperationException, TripleStoreNonFatalException {
		
		try {
			switch ((String)jo.get("type")) {
			case "ITEM":
				Id id;
				try {
					id = Id.parseId((String)jo.get("value"));
				} catch (Exception x) {
					return null;
				}
				Item item = store.getItem(id, session);
				if (item == null) return null;
				return new ItemDatum(item);
			case "BOOLEAN": return new SimpleDatum((Boolean)jo.get("value"));
			case "INTEGRAL": return new SimpleDatum((Long)jo.get("value"));
			case "REAL": return new SimpleDatum((Double)jo.get("value"));
			case "TEXT": return new StringDatum((String)jo.get("value"));
			case "BINARY": return new ByteArrayDatum(
					Base64.getDecoder().decode((String)jo.get("value")));
			}
		} catch (Exception e) {}
		return null;
	}
	
	/**
	 * Get an object that represents the datum. Depending on the type,
	 * this may be a String, a Java primitive type object or a 
	 * byte array that is base64-encoded as a string.
	 * 
	 * @return an object that represents the datum
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws SourceConnectorStateException 
	 */
	
	abstract Object toObjectForJson() 
			throws TripleStoreNonFatalException, SourceConnectorStateException;
}
