/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.lacibus.triplesource.SourceConnector;

/**
 * An instance of this class maintains records of the source connectors
 * that a triple store uses, and of their states.
 */

class ConnectorsManager implements Serializable {

	private static final long serialVersionUID = -6090643032529771711L;
	
	/**
	 * An index of the source connectors in use, indexed by the numeric
	 * identifiers of the sources to which they are connected. Each source
	 * connector in the index has a connector record that contains the
	 * connector and its current state.
	 */
	
	private Map<Long, SourceConnector> sourceConnectors
		= new HashMap<Long, SourceConnector>();
	
	/**
	 * Close the connectors manager
	 * 
	 * @return the index of the source connectors in use
	 */
	
	public synchronized Map<Long, SourceConnector> close() {
		
		Map<Long, SourceConnector> scs = sourceConnectors;
		sourceConnectors = null;
		return scs;
	}
	
	/**
	 * <p>Add a source connector.
	 * </p>
	 * <p>Invoked when a connection is started and its source connector has been created.
	 * </p>
	 * @param connector the source connector to be added.
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	synchronized void addConnector(
			SourceConnector connector) 
					throws TripleStoreNonFatalException {
		
		Long sourceNr = connector.getSourceNr();
		if (sourceConnectors.containsKey(sourceNr))
			throw (new TripleStoreNonFatalException(
					"Source " + sourceNr +
					" already has a connector."));
		sourceConnectors.put(sourceNr, connector);
	}
	
	/**
	 * Determine whether a source has a connector that is being managed
	 * by the connectors manager.
	 * 
	 * @param sourceNr the numeric identifier of a source
	 * 
	 * @return true if there is a connector being managed by the
	 * connectors manager that connects the identified source, false
	 * otherwise.
	 */
	
	synchronized boolean isKnown(long sourceNr) {
		
		return sourceConnectors.containsKey(sourceNr);
	}
	
	/**
	 * Determine whether the source is connected and operational.
	 * 
	 * @param sourceNr the numeric identifier of a source
	 * 
	 * @return true if there is a connector being managed by the
	 * connectors manager that connects the identified source and 
	 * is operational, false otherwise.
	 */
	
	synchronized boolean isOperational(long sourceNr) {
		
		// If store operation has been aborted, return false
		if (sourceConnectors == null) return false;
		
		// Return whether there is a connector being managed by the
		// connectors manager that connects the identified source and 
		// is operational
		SourceConnector connector = sourceConnectors.get(sourceNr);
		if (connector == null) return false;
		return connector.isOperational();
	}
	
	/**
	 * Get the source connector for a given source
	 * 
	 * @param sourceNr the numeric identifier of a source
	 * 
	 * @return the source connector for the given source if it has a
	 * connector being managed by the connectors manager, null otherwise.
	 */
	
	synchronized SourceConnector getConnector(long sourceNr) {

		return sourceConnectors.get(sourceNr);
	}
	
	/**
	 * <p>Remove a source connector.
	 * </p>
	 * <p>If the given source connector is being managed by the connectors 
	 * manager, remove it from the index of managed connectors.
	 * </p>
	 * <p>This method is invoked when a source is disconnected.
	 * </p>
	 * @param connector the source connector to be removed.
	 */
	
	synchronized void removeConnector(SourceConnector connector) {

		removeConnector(connector.getSourceNr());
	}
	
	/**
	 * <p>Remove the source connector for a given source.
	 * </p>
	 * <p>If the given source has a connector being managed by the connectors 
	 * manager, remove it from the index of managed connectors.
	 * </p>
	 * <p>This method is invoked when a source is disconnected.
	 * </p>
	 * @param sourceNr the numeric identifier of a source
	 */
	
	synchronized void removeConnector(long sourceNr) {

		sourceConnectors.remove(sourceNr);
	}
}
