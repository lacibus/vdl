/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;

/**
 * A set of operations that change the data in a source, and 
 * that should be performed together, with none being performed
 * unless all are performed.
 */

public abstract class Transaction implements Serializable {

	private static final long serialVersionUID = -3225780140658202548L;
	
	/**
	 * <p>The sequence number of the transaction.
	 * </p>
	 * <p>Each transaction has a sequence number that is unique for
	 * transactions for its source, and that increases with each
	 * new transaction for the source.
	 * </p>
	 */
	
	protected long seq;
	
	/**
	 * Create a transaction
	 * 
	 * @param seq the sequence number of the transaction
	 */
	
	public Transaction(long seq) {
		this.seq = seq;
	}
	
	/**
	 * Get the sequence number of the transaction
	 * 
	 * @return the sequence number of the transaction
	 */
	
	public long seq() {
		
		return seq;
	}
}
