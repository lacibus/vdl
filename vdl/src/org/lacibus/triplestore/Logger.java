package org.lacibus.triplestore;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.Date;

public class Logger implements Serializable {

	private static final long serialVersionUID = -7143515141208678283L;

	/**
	 * The path to the log file for status and error messages
	 */
	
	private String logfilePath;
	
	/**
	 * A print stream that writes to the log file
	 */
	
	private PrintStream logStream;
	
	public Logger(String logfilePath) {
		
		this.logfilePath = logfilePath;
	}
	
	public void initialize() throws IOException {
		
		// Start the log
		if (logfilePath == null) logStream = System.out;
		else {
			int n = 0;
			File f = new File(logfilePath + suffix(n));
			while (f.exists()) {
				n++;
				f = new File(logfilePath + suffix(n));
			}
			logStream = new PrintStream(new BufferedOutputStream(
				new FileOutputStream(f)), true);
		}
		logMemory();
	}
	
	private String suffix(int n) {
		
		if (n >= 100) return Integer.toString(n);
		else if (n >= 10) return '0' + Integer.toString(n);
		else return "00" + Integer.toString(n);
	}
	
	public void log(String message) {
		
		logStream.println("MESSAGE at " + new Date());
		logStream.println(message);
	}
	
	public void log(Exception e) {
		
		logStream.println("EXCEPTION at " + new Date());
		e.printStackTrace(logStream);
	}
	
	public void logMemory() {
		
		Runtime rt = Runtime.getRuntime();
		rt.gc();
		long freeMB = rt.freeMemory()/(1024*1024);
	    long totalMB = rt.totalMemory()/(1024*1024);
	    long maxMB = rt.maxMemory()/(1024*1024);
		logStream.println("Free memory (Mbytes): " + freeMB + " of " + totalMB + " from " + maxMB);
	}
}
