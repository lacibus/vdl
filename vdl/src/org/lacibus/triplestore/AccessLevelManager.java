/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.lacibus.settheoryrels.OrderedPair;
import org.lacibus.settheoryrels.ReflexiveTransitiveHashSetRelation;
import org.lacibus.triplesource.SourceConnector;
import org.lacibus.triplesource.SourceConnectorContentException;
import org.lacibus.triplesource.SourceConnectorException;

/**
 * <p>An access level manager enables a triple store to:</p>
 * <ul>
 *   <li>create access levels</li>
 *   <li>permit an access level to be superior or
 *       inferior to another</li>
 *   <li>remove a permission for an access level to
 *       be superior or inferior to another (this need
 *       not stop the level being superior or inferior
 *       to the other; there may be a chain of stated 
 *       relationships that implies that it is still 
 *       superior or inferior)</li>
 *   <li>Determine whether an access level is superior
 *       to another</li>
 *   <li>retire an access level, replacing it by another</li>
 *   <li>delete an access level that has been replaced</li>
 * </ul>
 * <p>An access level is stated to be superior to another
 * if permission has been given for it to be superior
 * and permission has also been given for the other
 * level to be its inferior. This means that superiority
 * relationships between independently-governed access
 * levels are established by mutual consent.
 * </p>
 */

class AccessLevelManager implements Serializable {
	
	private static final long serialVersionUID = -5735339731067224297L;

	/**
	 * The predefined highest access level
	 */
	
	AccessLevel highest = null;

	/**
	 * The predefined lowest access level
	 */
	
	AccessLevel lowest = null;
	
	/**
	 * The reflexive, transitive relation between access levels.
	 * An access level is superior to another if it is followed
	 * by the other in the access level relation.
	 */
	
	private ReflexiveTransitiveHashSetRelation<AccessLevel> levelRelation =
				new ReflexiveTransitiveHashSetRelation<AccessLevel>();
	
	/**
	 * Index of access levels that have been created
	 */
	
	private Map<Id, AccessLevel> index = new HashMap<Id, AccessLevel>();
	
	/**
	 * Index of replacements. The keys are the ids of items
	 * in connected sources representing replaced access levels,
	 * and the values are the ids of the items representing
	 * their replacements.
	 */
	
	private Map<Id, Id> replacements = new HashMap<Id, Id>();
	
	/**
	 * Create a new AccessLevelManager
	 * 
	 * @param highestLevelItem the item representing the
	 * predefined highest access level.
	 * 
	 * @param lowestLevelItem the item representing the
	 * predefined lowest access level.
	 */

	AccessLevelManager (
				Item highestLevelItem,
				Item lowestLevelItem) {
		
		highest = new AccessLevel(
				highestLevelItem, this);
		index.put(highestLevelItem.getId(), highest);
		lowest = new AccessLevel(
				lowestLevelItem, this);
		index.put(lowestLevelItem.getId(), lowest);
	}
	
	/**
	 * Make a new access level.
	 * 
	 * The new access level will be stated to be inferior to
	 * the session level, to ensure that the operation can
	 * be reversed within the same session. The session level
	 * must therefore not be the lowest level, and the session
	 * must have locked its level item when this method
	 * is invoked.
	 * 
	 * @param store the triple store in which the new access
	 * level is to be created
	 * 
	 * @param connector the connector to the source in which the
	 * new access level is to be stored
	 * 
	 * @param onlyReadLevel the only-read access level that
	 * the new access level is to have
	 * 
	 * @param readOrWriteLevel the read-or-write access level
	 * that the new access level is to have
	 * 
	 * @param session the requesting store session
	 * 
	 * @param locked whether the item of the new access level
	 * should be left locked after creation
	 * 
	 * @return a new access level, or null if one could not
	 * be created. The item of the new access level will be
	 * locked on return if, and only if, locked is true.
	 * 
	 * @throws NotInOperationException 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 */
	
	synchronized AccessLevel makeAccessLevel(
			TripleStore store,
			SourceConnector connector,
			AccessLevel onlyReadLevel,
			AccessLevel readOrWriteLevel,
			StoreSession session, 
			boolean locked) 
					throws 	NotInOperationException, 
							SourceConnectorException, 
							TripleStoreNonFatalException,
							TripleStoreFatalException {
		
		AccessLevel sessionLevel = session.getAccessLevel();
		if (sessionLevel.equals(lowest)) return null;
		if (!sessionLevel.equals(highest) &&
		    !sessionLevel.levelItem.checkLocked(session))
			return null;
		AccessLevel level = null;
		Item item = store.alwaysCreateLockedItem(
				connector, 
				onlyReadLevel,
				readOrWriteLevel,
				session);
		if (item == null) return null;
		try {
			// Create the triple defining the access level
			store.createTripleUnchecked(
					item, 
					item.store.isAccessLevelRelation, 
					new ItemDatum(item),
					connector, 
					item.getTransaction());
			// Create the access level, but don't index it yet
			level = new AccessLevel(item, this);
			if (!sessionLevel.equals(highest)) {
				// Make the new level inferior to the session level
				if (	!setSuperiorPermission(
							sessionLevel, level, session) || 
						!setInferiorPermission(
								level, sessionLevel, session))
					level = null;
			}
			// Now index the level if everything OK
			if (level != null) {
				levelRelation.addElement(level);
				index.put(item.getId(), level);	
			}
		}
		finally {
			if (level == null) store.deleteItem(item, session);
			else if (!locked) item.unlock(session);
		}
		return level;
	}

	/**
	 * Get the access level identified by an
	 * item.
	 * 
	 * @param item the identifying item
	 * 
	 * @return the access level identified by that
	 * item
	 */
	
	synchronized AccessLevel get(Item item) {
		if (item == null) return null;
		if (item.equals(highest.levelItem)) return highest;
		if (item.equals(lowest.levelItem)) return lowest;
		return index.get(item.getId());
	}

	/**
	 * Get the access level identified by an
	 * item, creating it if it does not exist.
	 * 
	 * @param item the identifying item
	 * 
	 * @return the access level identified by that
	 * item, which may be newly created
	 */
	
	synchronized AccessLevel getOrCreate(Item item) {
		if (item == null) return null;
		AccessLevel level = get(item);
		if (level == null) {
			level = new AccessLevel(item, this);
			index.put(item.getId(), level);	
		}
		return level;
	}
	
	/**
	 * <p>Permit one access level to be superior to another.
	 * If the other is already permitted to be the
	 * inferior of the pair, make a stated superiority
	 * relationship between them.
	 * </p>
	 * <p>The requesting store session must have locked the level
	 * that is permitted to be the superior one, and have read 
	 * access to the other. Neither level may be being replaced.
	 * </p>
	 * <p>A triple recording the permission is written to the
	 * source of the superior level
	 * </p>
	 * @param superior the level that is permitted to be the
	 * superior one
	 * 
	 * @param inferior the level that the permitted level
	 * is permitted to be superior to
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the permission was accepted, false
	 * otherwise. The permission will not be accepted if one
	 * of the levels is the predefined lowest, or predefined
	 * highest level. If the permission has been set already, 
	 * true is returned.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * @throws NotInOperationException 
	 */
	
	synchronized boolean setSuperiorPermission(
			AccessLevel superior, 
			AccessLevel inferior,
			StoreSession session) 
					throws 	SourceConnectorException, 
							TripleStoreFatalException, 
							TripleStoreNonFatalException, 
							NotInOperationException {
		
		if ((inferior == null) || 
			(superior == null)) return false;
		
		if (inferior.equals(lowest) ||
			inferior.equals(highest) ||
		    superior.equals(lowest) ||
		    superior.equals(highest)) return false;
		
		TripleStore store = superior.levelItem.store;
		
		// Check that the session has locked the superior level 
		if (!superior.levelItem.checkLocked(session)) return false;
		if (!inferior.canBeReadBy(session)) return false;

		// Check that neither level is being replaced
		if (superior.replacement != null) return false;
		if (inferior.replacement != null) return false;

		// Get the source connector for the level item
		// of the superior level
		SourceConnector connector = 
				store.connectorsManager.getConnector(
						superior.levelItem.sourceNr);
		if (connector == null) 
			throw (new TripleStoreNonFatalException(
					"Source " + superior.levelItem.sourceNr + 
					" is not connected."));
		
		// Determine whether the permission already exists
		List<Triple> superiorPermissions = store.rawGetTriples(
				superior.levelItem, 
				store.superiorPermissionRelation, 
				new ItemDatum(inferior.levelItem), 
				session);
		if (!superiorPermissions.isEmpty())
			// The permission already exists
			return true;
		
		// Create the triple recording the permission
		if (store.createTripleUnchecked(
				superior.levelItem, 
				store.superiorPermissionRelation, 
				new ItemDatum(inferior.levelItem),
				connector, 
				superior.levelItem.getTransaction()) == null)
			return false;
		// Determine whether the superior relation now exists
		List<Triple> inferiorPermissions = store.rawGetTriples(
				inferior.levelItem, 
				store.inferiorPermissionRelation, 
				new ItemDatum(superior.levelItem), 
				new StoreSession(highest));
		if (!inferiorPermissions.isEmpty()) {
			// The superior relation now exists
			levelRelation.stateRelationship(superior, inferior);
		}
		return true;
	}
	
	/**
	 * <p>Permit one access level to be inferior to another.
	 * If the other is already permitted to be the
	 * superior of the pair, make a stated superiority
	 * relationship between them.
	 * </p>
	 * <p>The requesting store session must have locked the level 
	 * that is permitted to be the inferior one, and have read 
	 * access to the other. Neither level may be being replaced.
	 * </p>
	 * <p>A triple recording the permission is written to the
	 * source of the inferior level
	 * </p>
	 * @param inferior the level that is permitted to be the
	 * inferior one
	 * 
	 * @param superior the level that the permitted level
	 * is permitted to be inferior to
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the permission was accepted, false
	 * otherwise. The permission will not be accepted if one
	 * of the relations is the predefined lowest, predefined
	 * highest level. If the permission has been set already, 
	 * true is returned.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * @throws NotInOperationException 
	 * 
	 */
	
	synchronized boolean setInferiorPermission(
			AccessLevel inferior, 
			AccessLevel superior,
			StoreSession session)
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException,
							NotInOperationException {
		
		if ((inferior == null) || 
			(superior == null)) return false;
		
		if (inferior.equals(lowest) ||
		    inferior.equals(highest) ||
		    superior.equals(lowest) ||
		    superior.equals(highest)) return false;
		
		TripleStore store = inferior.levelItem.store;
		
		// Check that the session has locked the inferior level 
		if (!inferior.levelItem.checkLocked(session)) return false;
		// Check that the session has read access to the other level
		if (!superior.canBeReadBy(session)) return false;
		
		// Check that neither level is being replaced
		if (inferior.replacement != null) return false;
		if (superior.replacement != null) return false;

		// Get the source connector for the level item
		// of the inferior level
		SourceConnector connector = 
				store.connectorsManager.getConnector(
						inferior.levelItem.sourceNr);
		if (connector == null) 
			throw (new TripleStoreNonFatalException(
					"Source " + inferior.levelItem.sourceNr + 
					" is not connected."));

		// Determine whether the permission already exists
		List<Triple> inferiorPermissions = store.rawGetTriples(
				inferior.levelItem, 
				store.inferiorPermissionRelation, 
				new ItemDatum(superior.levelItem), 
				session);
		if (!inferiorPermissions.isEmpty())
			// The permission already exists
			return true;
		
		// Create the triple recording the permission
		if (store.createTripleUnchecked(inferior.levelItem, 
				store.inferiorPermissionRelation, 
				new ItemDatum(superior.levelItem),
				connector, 
				inferior.levelItem.getTransaction()) == null)
			return false;
	
		// Determine whether the inferior relation now exists
		List<Triple> superiorPermissions = store.rawGetTriples(
				superior.levelItem, 
				store.superiorPermissionRelation, 
				new ItemDatum(inferior.levelItem), 
				new StoreSession(highest));
		if (!superiorPermissions.isEmpty()) {
			// The inferior relation now exists
			levelRelation.stateRelationship(superior, inferior);
		}
		return true;
	}
	
	/**
	 * <p>Remove a permission that an access level can be superior
	 * to another. Any stated relationship that the level is
	 * superior to the other is removed also.
	 * </p>
	 * <p>The requesting store session must have locked the superior 
	 * level and have read access to the other level, and  must 
	 * continue to have write access to the superior  level and read
	 * access to the other level once the permission has been removed.
	 * Neither level may be being replaced. 
	 * </p>
	 * @param superior the level that is no longer permitted to
	 * be superior to the other
	 * 
	 * @param inferior the level that the superior level is no
	 * longer permitted to be superior to
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the removal was accepted, false
	 * otherwise. The removal will not be accepted if
	 * the superior relation is not already permitted to be
	 * superior to the inferior one, or if one of the
	 * relations is the predefined lowest or predefined
	 * highest level, or if access permissions are not
	 * sufficient.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	synchronized boolean unsetSuperiorPermission(
			AccessLevel superior, 
			AccessLevel inferior,
			StoreSession session) 
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException,
							TripleStoreFatalException {
		
		if ((inferior == null) || 
			(superior == null)) return false;
		
		TripleStore store = superior.levelItem.store;
		
		// Check that the session has locked the superior level 
		if (!superior.levelItem.checkLocked(session)) return false;
		// Check that the session has read access to the other level
		if (!inferior.canBeReadBy(session)) return false;
		
		// Check that neither level is being replaced
		if (superior.replacement != null) return false;
		if (inferior.replacement != null) return false;

		// Determine whether the permission already exists
		List<Triple> superiorPermissions = store.rawGetTriples(
				superior.levelItem, 
				store.superiorPermissionRelation, 
				new ItemDatum(inferior.levelItem), 
				session);
		if (superiorPermissions.isEmpty()) {
			// The permission does not already exist
			return false;
		}
	
		// Get the triple recording the permission
		if (superiorPermissions.size() != 1)
			throw (new SourceConnectorContentException(
					superior.levelItem.sourceNr, 
					"Superior permissions triple for " +
					superior + " and " + inferior +
					" not unique."));
		Triple permissionTriple = superiorPermissions.get(0);
		// Remove the stated relationship if it exists
		if (levelRelation.removeStatedRelationship(
				superior, inferior)) {
			// If the session no longer has sufficient permission,
			// reverse the removal
			if (!superior.canBeWrittenBy(session) ||
				!inferior.canBeReadBy(session)) {
				levelRelation.isStatedToBeFollowedBy(
						superior, inferior);
				return false;
			}
		}

		// Delete the triple recording the permission
			store.deleteTripleUnchecked(
					permissionTriple, 
					superior.levelItem.getTransaction());
			
		return true;
	}
	
	/**
	 * <p>Remove a permission that an access level can be inferior
	 * to another. Any stated relationship that the other level
	 * is superior to it is removed also.
	 * </p>
	 * <p>The requesting store session must have locked the inferior 
	 * level and have read access to the other level, and must 
	 * continue to have write access to the inferior level and read 
	 * access to the other level once the permission has been removed. 
	 * Neither level may be being replaced. 
	 * </p>
	 * @param inferior the level that is no longer permitted to
	 * be inferior to the other
	 * 
	 * @param superior the level that the inferior level is no
	 * longer permitted to be inferior to
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the removal was accepted, false
	 * otherwise. The removal will not be accepted if
	 * the inferior relation is not already permitted to be
	 * inferior to the superior one, or if one of the
	 * relations is the predefined lowest or predefined
	 * highest level, or if access permissions are not
	 * sufficient.
	 * 
	 * @throws SourceConnectorException 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * 
	 */
	
	synchronized boolean unsetInferiorPermission(
			AccessLevel inferior, 
			AccessLevel superior,
			StoreSession session) 
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException {
		
		if ((inferior == null) || 
			(superior == null)) return false;
		
		TripleStore store = inferior.levelItem.store;
		
		// Check that the session has locked the inferior level 
		if (!inferior.levelItem.checkLocked(session)) return false;
		// Check that the session has read access to the other level
		if (!superior.canBeReadBy(session)) return false;		
		// Check that neither level is being replaced
		if (inferior.replacement != null) return false;
		if (superior.replacement != null) return false;

		// Determine whether the permission already exists
		List<Triple> inferiorPermissions = store.rawGetTriples(
				inferior.levelItem, 
				store.inferiorPermissionRelation, 
				new ItemDatum(superior.levelItem), 
				session);
		if (inferiorPermissions.isEmpty())
			// The permission does not already exist
			return false;
		
		// Get the triple recording the permission
		if (inferiorPermissions.size() != 1)
			throw (new SourceConnectorContentException(
					superior.levelItem.sourceNr, 
					"Inferior permissions triple for " +
					inferior + " and " + superior +
					" not unique."));
		Triple permissionTriple = inferiorPermissions.get(0);
		
		// Remove the stated relationship if it exists
		if (levelRelation.removeStatedRelationship(
				superior, inferior)) {
			// If the session no longer has sufficient permission,
			// reverse the removal
			if (!inferior.canBeWrittenBy(session) ||
				!superior.canBeReadBy(session)) {
				levelRelation.isStatedToBeFollowedBy(
						superior, inferior);
				return false;
			}
		}

		// Delete the triple recording the permission
		store.deleteTripleUnchecked(
				permissionTriple, 
				inferior.levelItem.getTransaction());
	
		// Remove the stated relationship if it exists
		levelRelation.removeStatedRelationship(superior, inferior);
				
		return true;
	}
	
	/**
	 * Synchronize the access levels superiority relation
	 * with the permissions triples in a triple store.
	 * 
	 * @param store a triple store
	 * 
	 * @param removedSourceNr the numeric identifier of
	 * a source whose disconnection is the reason for
	 * synchronization. Null if the synchronization is
	 * not being performed as part of the disconnection
	 * of a source.
	 * 
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 * @throws NotInOperationException
	 */
	
	synchronized void syncLevelRelation(TripleStore store, Long removedSourceNr) 
			throws 	TripleStoreFatalException, 
					SourceConnectorException, 
					NotInOperationException {
		
		// If a source has been removed, update the levels index
		// and the replacements index
		if (removedSourceNr != null) {
			Map<Id, AccessLevel> newIndex = new HashMap<Id, AccessLevel>();
			for (Entry<Id, AccessLevel> e : index.entrySet()) {
				if (e.getKey().sourceNr != removedSourceNr)
					newIndex.put(e.getKey(), e.getValue());
			}
			index = newIndex;
			Map<Id, Id> newReplacements = new HashMap<Id, Id>();
			for (Entry<Id, Id> e : replacements.entrySet()) {
				if (		(e.getKey().sourceNr != removedSourceNr) &&
						(e.getValue().sourceNr != removedSourceNr) )
					newReplacements.put(e.getKey(), e.getValue());
			}
			replacements = newReplacements;
		}
		
		// Create a new access level relation
		ReflexiveTransitiveHashSetRelation<AccessLevel> newLevelRelation =
				new ReflexiveTransitiveHashSetRelation<AccessLevel>();

		// Add the access levels that are not in the removed source
		// to the new relation
		for (AccessLevel l : levelRelation.baseSet()) {
			if ((removedSourceNr == null) || (l.levelItem.sourceNr != removedSourceNr))
				newLevelRelation.addElement(l);
		}
		
		// Get the triples defining superior and inferior
		// permissions
		OrderedPair<List<Triple>> permissionLists;
		try {
			permissionLists = store.rawGetTriplesWithVerbs(
					store.superiorPermissionRelation, 
					store.inferiorPermissionRelation);
		} catch (TripleStoreNonFatalException e) {
			throw(store.abortTripleStoreException(
					"Cannot get triples to synchronize level relations", e));
		}
		
		// Get the superior permissions
		Set<OrderedPair<AccessLevel>> superiorPermissions =
				new HashSet<OrderedPair<AccessLevel>>();
		for (Triple t : permissionLists.firstElement) {
			AccessLevel superior = index.get(t.subject.getId());
			AccessLevel inferior = index.get(((ItemTriple)t).object.getId());
			if (	(superior != null) && (inferior != null))
				superiorPermissions.add(
						new OrderedPair<AccessLevel>(superior, inferior));
		}
		// Check the inferior permissions to find the explicit relationships, 
		// and add them to the new access level relation
		for (Triple t : permissionLists.secondElement) {
			AccessLevel inferior = index.get(t.subject.getId());
			AccessLevel superior = index.get(((ItemTriple)t).object.getId());
			if (	(superior != null) && (inferior != null) &&
					(superiorPermissions.contains(
						new OrderedPair<AccessLevel>(superior, inferior)))) {
				newLevelRelation.stateRelationship(
						superior, inferior);					
			}
		}
		// The relation newLevelRelation now contains the superiority
		// relationships between the access levels in the store. Replace
		// the existing level relation with it. Note that this is done
		// in a single assignment statement, allowing concurrent access
		// by unsynchronized methods.
		levelRelation = newLevelRelation;
	}
	
	/**
	 * Get the superior permissions of a triple store.
	 * 
	 * @param store a triple store
	 * 
	 * @return the set of pairs of access levels such that the
	 * first access level is permitted to be superior to the 
	 * second.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	private synchronized Set<OrderedPair<AccessLevel>>
			getSuperiorPermissions(TripleStore store) 
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException {
	
		
		Set<OrderedPair<AccessLevel>> result =
				new HashSet<OrderedPair<AccessLevel>>();
		StoreSession session = new StoreSession(highest);
		
		// Find the superior permissions
		List<Triple> superiorPermissions = store.rawGetTriples(
				null, store.superiorPermissionRelation, null, session);
		for (Triple t : superiorPermissions) {
			AccessLevel superior = index.get(t.subject.getId());
			if (superior != null) {
				AccessLevel inferior = null;
				try {
					inferior = index.get(((ItemTriple)t).object.getId());
				} catch (Exception e) {
					SourceConnectorContentException x = 
							new SourceConnectorContentException(
									t.subject.sourceNr,
									"Invalid superior permission triple " + t);
					x.initCause(e);
					throw(x);
				}
				if (inferior != null)
					result.add(new OrderedPair<AccessLevel>(
							superior, inferior));
			}
		}
		return result;
	}
				
	/**
	 * Get the inferior permissions of a triple store.
	 * 
	 * @param store a triple store
	 * 
	 * @return the set of pairs of access levels such that the
	 * first access level is permitted to be inferior to the 
	 * second.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	private synchronized Set<OrderedPair<AccessLevel>>
			getInferiorPermissions(TripleStore store) 
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException {
	
		
		Set<OrderedPair<AccessLevel>> result =
				new HashSet<OrderedPair<AccessLevel>>();
		StoreSession session = new StoreSession(highest);
		
		// Find the inferior permissions
		List<Triple> inferiorPermissions = store.rawGetTriples(
				null, store.inferiorPermissionRelation, null, session);
		for (Triple t : inferiorPermissions) {
			AccessLevel inferior = index.get(t.subject.getId());
			if (inferior != null) {
				AccessLevel superior = null;
				try {
					superior = index.get(((ItemTriple)t).object.getId());
				} catch (Exception e) {
					SourceConnectorContentException x = 
							new SourceConnectorContentException(
									t.subject.sourceNr,
									"Invalid inferior permission triple " + t);
					x.initCause(e);
					throw(x);
				}
				if (superior != null)
					result.add(new OrderedPair<AccessLevel>(
							inferior, superior));
			}
		}
		return result;
	}
				
	/**
	 * Determine whether one access level is permitted to be 
	 * superior to another
	 * 
	 * @param superior a first access level
	 * 
	 * @param inferior a second access level
	 * 
	 * @return true if the first access level is permitted to
	 * be superior to the second access level, false otherwise.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	synchronized boolean isPermittedSuperiorTo(
		AccessLevel superior, AccessLevel inferior) 
				throws 	SourceConnectorException, 
						TripleStoreFatalException, 
						TripleStoreNonFatalException {
			
		if ((superior == null) || 
			(inferior == null)) return false;
	
		TripleStore store = superior.levelItem.store;
		
		List<Triple> superiorPermissions = store.rawGetTriples(
				superior.levelItem, 
				store.superiorPermissionRelation, 
				new ItemDatum(inferior.levelItem), 
				new StoreSession(highest));
		return !superiorPermissions.isEmpty();
	}
	
	/**
	 * Determine whether one access level is permitted to be 
	 * inferior to another
	 * 
	 * @param inferior a first access level
	 * 
	 * @param superior a second access level
	 * 
	 * @return true if the first access level is permitted to
	 * be inferior to the second access level, false otherwise.
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 */
	
	synchronized boolean isPermittedInferiorTo(
		AccessLevel inferior, AccessLevel superior) 
				throws 	SourceConnectorException, 
						TripleStoreFatalException, 
						TripleStoreNonFatalException {
			
		if ((inferior == null) || 
			(superior == null)) return false;
	
		TripleStore store = inferior.levelItem.store;
		
		List<Triple> inferiorPermissions = store.rawGetTriples(
				inferior.levelItem, 
				store.inferiorPermissionRelation, 
				new ItemDatum(superior.levelItem), 
				new StoreSession(highest));
		return !inferiorPermissions.isEmpty();
	}
	
	/**
	 * Determine whether one access level is stated to be 
	 * superior to another
	 * 
	 * @param superior a first access level
	 * 
	 * @param inferior a second access level
	 * 
	 * @return true if the first access level is stated to
	 * be superior to the second access level, false otherwise.
	 */
	
	synchronized boolean isStatedSuperiorTo(
		AccessLevel superior, AccessLevel inferior) {
			
	if ((inferior == null) || 
		(superior == null)) return false;
	
		return levelRelation.isStatedToBeFollowedBy(
				superior, inferior);
	}
	
	/**
	 * <p>Retire an access level and replace it by by another.
	 *</p>
	 * <p>The requesting store session must have locked the retired
	 * access level. The retired level must not be the highest or  
	 * lowest access level, and must not be the same as the access
	 * level of the requesting session. The replacement may not 
	 * be the highest access level. It may be the lowest  access level.  
	 * If it is not, the requesting session must have locked it. 
	 * It must not be the same as the access level that it is replacing,
 	 * and must not itself have been replaced. 
	 * </p>
	 * <p>All relationships that the retired level has are
	 * removed, and its item is deleted.
	 * </p>
	 * @param level the access level to be replaced.
	 * 
	 * @param replacement the replacement access level
	 * 
	 * @param session the requesting store session
	 * 
	 * @return true if the operation was performed, false
	 * otherwise
	 * 
	 * @throws TripleStoreNonFatalException 
	 * @throws TripleStoreFatalException 
	 * @throws SourceConnectorException 
	 * @throws NotInOperationException 
	 * 
	 */
		
	synchronized boolean replaceAccessLevel(
			AccessLevel level, 
			AccessLevel replacement,
			StoreSession session)
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException, 
							NotInOperationException {
		
		if ((level == null) || (replacement == null))
			return false;
		if (level.equals(replacement)) return false;
		if (level.equals(highest)) return false;
		if (level.equals(lowest)) return false;
		if (replacement.equals(highest)) return false;
		
		AccessLevel sessionLevel = session.getAccessLevel();
		if (sessionLevel.equals(level)) return false;
		
		Item levelItem = level.levelItem;
		if (levelItem == null) 
			// The level is invalid or being replaced
			throw(new TripleStoreNonFatalException(
					"Replaced or invalid access level"));
		if (!levelItem.checkLocked(session)) return false;
		
		TripleStore store = levelItem.store;
		
		Item replacementItem = replacement.levelItem;
		if (replacementItem == null) 
			// The replacement is invalid or being replaced
			throw(new TripleStoreNonFatalException(
					"Replaced or invalid replacement access level"));
		
		// Check that the replacement is the lowest level,
		// or that it is locked 
		if (!replacement.equals(lowest) &&
			!replacementItem.checkLocked(session)) return false;
		
		// Get the access level triple of the level being replaced
		Triple levelTriple = store.uniqueTriple(
				levelItem, 
				store.isAccessLevelRelation, 
				null, 
				session);
		if (levelTriple == null) 
			// If the access level is not being replaced then
			// the triple should be readable by the session
			throw(new TripleStoreFatalException(
					"Cannot get access level triple"));
		
		// Replace the access level triple in the same session as 
		// deleting the level item and to the same source
		// so that both actions or neither survive a restart
		Triple t = store.replaceTripleObjectUnchecked(
				levelTriple,
				new ItemDatum(replacementItem));
		if (t == null)
			throw (new SourceConnectorContentException(
					levelTriple.getSourceNr(),
					"Cannot replace level triple"));
		store.deleteItemUnchecked(levelItem, session);
		
		// Make the replacement
		Id id = level.getItemId();
		levelRelation.removeElement(level);
		index.remove(id);
		level.setReplacement(replacement);
		replacements.put(id, replacement.getItemId());
		
		return true;
	}
	
	/**
	 * <p>Get the identifier of the item representing the
	 * replacement of an access level.
	 * </p>
	 * <p>The item representing the access level may have been 
	 * deleted. The item representing the replacement level 
	 * may have been deleted, if the replacement has itself
	 * been replaced.
	 * </p>
	 * @param id an identifier
	 * 
	 * @return the identifier of the item representing the
	 * replacement if the given identifier is that of an item
	 * that represented an access level that has been replaced,
	 * null otherwise.
	 */
	
	synchronized Id getReplacementLevelId(Id id) {
		
		return replacements.get(id);
	}
	
	/**
	 * Set the replacement of an access level identifier in
	 * the replacements map. This method is invoked when a
	 * replacement triple is loaded, to record a replacement
	 * made previously.
	 * 
	 * @param replacedId the Id of the item representing the
	 * replaced level
	 * 
	 * @param replacingId the Id of the item representing the
	 * replacing level
	 */
	
	synchronized void setReplacementId(Id replacedId, Id replacingId) {
		
		replacements.put(replacedId, replacingId);
	}
	
	/**
	 * Get the effective access level id for a given id. 
	 * 
	 * @param id an Id
	 * 
	 * @return the last access level id in the chain of
	 * replacements starting with a given id. This is
	 * the id itself if there is no chain.
	 */
	
	synchronized Id getEffectiveLevelId(Id id) {
		
		Id result = id;
		Id i = id;
		while (i != null) {
			result = i;
			i = replacements.get(i);
		}
		return result;
	}
	
	/**
	 * Get a list of the access levels
	 * 
	 * @return a list of the access levels
	 */
	
	synchronized List<AccessLevel> getLevels() {
		
		List<AccessLevel> levels = new ArrayList<AccessLevel>();
		for (AccessLevel l : index.values()) levels.add(l);
		return levels;
	}
	
	/**
	 * Get the set of access levels that are superior to
	 * a given level
	 * 
	 * @param level an access level
	 * 
	 * @return the set of access levels that are superior
	 * to the level
	 */
	
	synchronized Set<AccessLevel> getSuperiors(
			AccessLevel level) {
		
		return levelRelation.followedBy(level);	
	}
	
	/**
	 * Get the set of access levels that are inferior to
	 * a given level
	 * 
	 * @param level an access level
	 * 
	 * @return the set of access levels that are inferior
	 * to the level
	 */
	
	synchronized Set<AccessLevel> getInferiors(
			AccessLevel level) {
		
		return levelRelation.followersOf(level);	
	}
	
	/**
	 * List the stated access level relationships
	 * 
	 * @return a list of the stated access level
	 * relationships.
	 */
	
	synchronized List<OrderedPair<AccessLevel>>
				statedRelationships() {
		
		return levelRelation.statedRelatedPairs();
	}

	/**
	 * List the implied access level relationships 
	 * 
	 * @return a list of the implied access level
	 * relationships.  
	 */
	
	synchronized List<OrderedPair<AccessLevel>>
				impliedRelationships() {
		
		return levelRelation.relatedPairs();
	}
	
	/**
	 * Get the superiority permissions and relations
	 * of a triple store.
	 * 
	 * @param store a triple store
	 * 
	 * @return the superiority permissions and relations
	 * of the given triple store
	 *     
	 * @throws TripleStoreNonFatalException
	 * @throws TripleStoreFatalException
	 * @throws SourceConnectorException
	 */
	
	synchronized AccessLevelRelationsStatus
			getAccessLevelRelationsStatus(TripleStore store) 
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException {
		
		AccessLevelRelationsStatus result = 
				new AccessLevelRelationsStatus();
		result.explicitRelationships = 
				levelRelation.statedRelatedPairsSet();
		result.implicitRelationships = 
				levelRelation.relatedPairsSet();
		result.superiorPermissions = 
				getSuperiorPermissions(store);
		result.inferiorPermissions =
				getInferiorPermissions(store);
		return result;
	}
	
	/**
	 * <p>Determine whether one level is superior to another
	 * </p>
	 * <p>Note that this method is not synchronized so that
	 * it can be invoked while other methods are executing.
	 * This is desirable because it is used very frequently.
	 * </p>
	 * @param superior the possibly superior level
	 * 
	 * @param inferior the possibly inferior level
	 * 
	 * @return true if  the possibly superior level
	 * is indeed superior to the possibly inferior level,
	 * false otherwise
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	boolean isSuperiorTo(AccessLevel superior,
			AccessLevel inferior)
					throws TripleStoreNonFatalException {

		if ((inferior == null) ||
		    (inferior.levelItem == null) ||
			(superior == null) ||
			(superior.levelItem == null)) 
			throw(new TripleStoreNonFatalException(
					"Invalid access level"));
		if (inferior.equals(lowest) ||
			superior.equals(highest))
			return true;
		if (inferior.equals(highest) ||
			superior.equals(lowest))
			return false;
		return levelRelation.isFollowedBy(
				superior, inferior);
	}
		
	/**
	 * Handle a triple specifying an access level
	 * loaded from a source
	 * 
	 * @param t a triple specifying an access level 
	 * 
	 * @throws TripleStoreNonFatalException
	 */
	
	synchronized void loadAccessLevelTriple(ItemTriple t) 
					throws TripleStoreNonFatalException {
		
		// Ensure the access level exists or is created.
		Item item = t.subject;
		AccessLevel level = get(item);
		if (level == null) {
			// Create the access level
			level = new AccessLevel(item, this);
		}	
		Id id = item.getId();
		if (t.object.equals(item)) {
			// The access level has not been replaced
			levelRelation.addElement(level);
			index.put(id, level);	
		}
		else {
			// The access level has been replaced
			// Note the replacement. 
			AccessLevel replacement = getOrCreate(t.object);
			levelRelation.removeElement(level);
			index.remove(id);
			level.setReplacement(replacement);
			replacements.put(id, replacement.getItemId());
		}
	}
	
	/**
	 * Handle the addition to a source of a triple specifying 
	 * a superiority permission
	 * 
	 * @param t a triple specifying a superiority permission
	 * 
	 * @param superior true if the triple specifies a 
	 * superior permission, false if it specifies an 
	 * inferior permission
	 * 
	 * @param load true if the triple is added in a load
	 * operation, false if in a sync operation
	 * 
	 * @throws SourceConnectorException
	 * @throws TripleStoreFatalException
	 * @throws TripleStoreNonFatalException
	 * @throws NotInOperationException 
	 */
	
	synchronized void addPermissionTriple(
			ItemTriple t, 
			boolean superior, 
			boolean load) 
					throws 	SourceConnectorException, 
							TripleStoreFatalException, 
							TripleStoreNonFatalException, 
							NotInOperationException {

		// If this is a load operation, nothing more needs doing
		// The relation will be noted if appropriate when relations
		// are synced at the end of the load
		if (load) return;
		
		// It is a sync operation
		// State the superiority relation if appropriate
		AccessLevel l1 = get(t.subject);
		if (l1 != null) {
			AccessLevel l2 = get(t.object);
			if (l2 != null) {
				if (superior) {
					// The first level is permitted to be superior to
					// the second. If the second level is permitted to 
					// be inferior to the first, state the superiority
					// relationship
					if (!t.store.rawGetTriples(
								t.object, 
								t.store.inferiorPermissionRelation, 
								new ItemDatum(t.subject), 
								new StoreSession(highest)).isEmpty()) {
						levelRelation.stateRelationship(l1, l2);
					}
				}
				else {
					// The first level is permitted to be inferior to
					// the second. If the second level is permitted to
					// be superior to the first, state the superiority 
					// relationship
					if (!t.store.rawGetTriples(
								t.object, 
								t.store.superiorPermissionRelation, 
								new ItemDatum(t.subject), 
								new StoreSession(highest)).isEmpty()) {
						levelRelation.stateRelationship(l2, l1);
					}
				}
			}				
		}		
	}
	
	/**
	 * Handle the removal from a source of a triple specifying a
	 * superiority permission 
	 * 
	 * @param t a triple specifying a superiority permission
	 * 
	 * @param superior true if the triple specifies a 
	 * superior permission, false if it specifies an 
	 * inferior permission
	 * 
	 * @param load true if the triple is removed in a load
	 * operation, false if in a sync operation
	 * 
	 * @throws TripleStoreNonFatalException
	 * @throws SourceConnectorException
	 * @throws TripleStoreFatalException
	 */
	
	synchronized void removePermissionTriple(
			ItemTriple t, 
			boolean superior, 
			boolean load) 
					throws 	TripleStoreNonFatalException, 
							SourceConnectorException, 
							TripleStoreFatalException {
		
		// If the triple comes from a load operation, no more
		// needs to be done
		if (load) return;
		
		// It is a sync operation
		// If there is a stated relationship between the levels,
		// remove it
		AccessLevel l1 = get(t.subject);
		if (l1 != null) {
			AccessLevel l2 = get(t.object);
			if (l2 != null) {
				if (superior)
					levelRelation.removeStatedRelationship(l1, l2);
				else levelRelation.removeStatedRelationship(l2, l1);
			}				
		}		
	}
}


