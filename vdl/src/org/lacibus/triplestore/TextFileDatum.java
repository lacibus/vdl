/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/**
 * <p>An instance of this class is a TEXT datum whose value
 * is contained in a file. 
 * </p>
 * <p>Text file datums are used to hold triple objects that
 * have been input but not yet stored in sources, and triple
 * objects that have been retrieved from the sources in which
 * they are stored.
 * </p>
 */

class TextFileDatum extends Datum implements Serializable {
	
	private static final long serialVersionUID = 3816033204380928356L;

	final static Charset utf8 = Charset.forName("UTF-8");
	
	/**
	 * A file that contains the value of the datum.
	 */
	
	File file;

	/**
	 * <p>Create a new Text File Datum.
	 * </p>
	 * <p>The summary value is not set, and will not be set until the
	 * datum is stored as a triple object.  
	 * </p>
	 * @param file a text file - with utf-8 encoding - containing
	 * the value of the datum.
	 * 
	 * @throws TripleStoreNonFatalException 
	 */
	
	TextFileDatum(File file) throws TripleStoreNonFatalException{
		
		if (file == null) throw new TripleStoreNonFatalException(
				"Null file");
		if (file.length() >= Integer.MAX_VALUE) 
			throw new TripleStoreNonFatalException("Too many bytes");
		this.type = Type.TEXT;
		this.file = file;
	}

	@SuppressWarnings("resource")
	@Override
	public String getTextValueAsString() 
			throws TripleStoreNonFatalException {
		
		// Get a reader to read the file. If the file has been deleted,
		// an exception will be thrown. If the reader is obtained, then
		// the file will not be actually deleted until the reader is
		// closed, even if another thread issues a delete call.
		BufferedReader bufReader;
		try {
			bufReader = new BufferedReader(
				new InputStreamReader(
					new FileInputStream(file), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			throw(new TripleStoreNonFatalException(
					"Unsupported encoding"));
		} catch (FileNotFoundException e) {
			TripleStoreNonFatalException ex =
					new TripleStoreNonFatalException(
							"File not found: " + file.getAbsolutePath());
			ex.initCause(e);
			throw(ex);
		}
		
		// Copy the file to a String 
		StringWriter w = new StringWriter();
		int c = -1;
		try {
			while ((c = bufReader.read()) >= 0) w.append((char) c);
		} catch (IOException e) {
			TripleStoreNonFatalException ex =
					new TripleStoreNonFatalException(
							"File IO error");
			ex.initCause(e);
			throw(ex);
		} finally {
			try {
				bufReader.close();
			} catch (IOException e) {
				TripleStoreNonFatalException ex =
						new TripleStoreNonFatalException(
								"File IO error");
				ex.initCause(e);
				throw(ex);
			}
		}
		return w.toString();
	}

	@Override
	public BufferedReader getTextValueReader() {
		
		// Get a reader to read the file. If the file has been deleted,
		// an exception will be thrown. If the reader is obtained, then
		// the file will not be actually deleted until the reader is
		// closed, even if another thread issues a delete call.
		try {
			return new BufferedReader(
				new InputStreamReader(
					new FileInputStream(file), "utf-8"));
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public boolean valueShouldBeRead() {
		return true;
	}

	@Override
	public String print() {
		try {
			return getTextValueAsString();
		} catch (TripleStoreNonFatalException e) {
			return "Text could not be read: " + e.getMessage();
		}
	}

	@Override
	public String simpleTextRepresentation() {
		return "Text to be read";
	}

	@Override
	public int getLength() throws TripleStoreNonFatalException {
		long l = file.length();
		if (l >= Integer.MAX_VALUE) 
			throw new TripleStoreNonFatalException("Too many bytes");

		else return (int)l;
	}

	/**
	 * Get the text file containing the value of the datum.
	 * 
	 * @return the text file containing the value of the datum.
	 */
	
	public File getFile() {
		
		return file;
	}

	@Override
	Object toObjectForJson() throws TripleStoreNonFatalException {

		return getTextValueAsString();
	}
}
