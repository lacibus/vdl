/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;

/**
 * An instance of this class is a buffer used in reading a stream
 */

class Buffer implements Serializable {

	private static final long serialVersionUID = 3085475336852740158L;

	/**
	 * The size of the buffers used in reading a stream
	 */
	
	final static int buflen = 4096;
	
	/**
	 * A byte array that holds the data
	 */
	
	byte[] bytes = new byte[buflen];
	
	/**
	 * The amount of data in the byte array
	 */
	
	int length = 0;
}
