/*
    Copyright (C) 2018 Lacibus Ltd

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
 */

package org.lacibus.triplestore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * An instance of this class is the index of items in a triple store
 */

class ItemsIndex implements Serializable {

	private static final long serialVersionUID = 1955482205446019379L;

	/**
	 * The items, indexed by their Ids
	 */
	
	IdMap<Item> items = new IdMap<Item>();
	
	/**
	 * The triple store
	 */
	
	TripleStore store;
	
	/**
	 * The items to be considered by removal. These items are determined,
	 * and the appropriate removals are carried out, by the triple store
	 * maintenance thread. 
	 */
	
	Set<Item> consideredForRemoval = new HashSet<Item>();
	
	/**
	 * The items that are in use. These items are determined
	 * by the triple store maintenance thread. 
	 */
	
	Set<Item> inUse = new HashSet<Item>();
	
	/**
	 * Construct a new items index for a triple store
	 * 
	 * @param store a triple store
	 */
	
	ItemsIndex(TripleStore store) {
		
		this.store = store;
	}
	
	/** 
	 * Get the number of items in the index
	 * 
	 * @return the number of items in the index
	 */
	
	int nrOfItems() {
		
		return items.size();
	}
	
	/**
	 * Get the oldest item in the index
	 * 
	 * @return the oldest item in the index
	 */
	
	synchronized Item getOldest() {
		
		return items.getOldest();
	}
	
	/**
	 * Get the next younger item
	 * 
	 * @return the next younger item than that returned by
	 * the previous invocation of this method or of the
	 * getOldest() method if that was invoked more
	 * recently.
	 */
	
	synchronized Item getNextYounger() {
		
		return items.getNextYounger();
	}

	
	/**
	 * Index an item
	 * 
	 * @param item the item to be indexed
	 * 
	 * @return false if the item was already in the index,
	 * otherwise the item is added and true is returned 
	 */
	
	synchronized boolean index(Item item) {
		
		if (items.get(item.getId()) == null) {
			items.put(item.getId(), item);
			return true;
		}
		return false;
	}
	
	/**
	 * Index an item or throw an exception
	 * 
	 * @param item the item to be indexed
	 * 
	 * @return false if the item was already in the index,
	 * otherwise the item is added and true is returned 
	 * 
	 * @throws TripleStoreFatalException 
	 */
	
	synchronized void alwaysIndex(Item item) 
			throws TripleStoreFatalException {
		
		if (!index(item))
			throw (new TripleStoreFatalException(
					"Cannot index item " + item.getId()));
	}
	
	/**
	 * Get the item with a given id
	 * 
	 * @param id an Id
	 * 
	 * @return the item with the given id, or null if it
	 * is not in the index
	 */
	
	synchronized Item get(Id id) {
		
		return items.get(id);
	}
	
	/**
	 * <p>Get the item with a given id if it is in the index, 
	 * or add a new one with that id if there is not one
	 * there already. If the item is in the index then it
	 * is noted to be in use. If it is not in the index,
	 * then it is created, and its state is set to MAYEXIST.
	 * </p>
	 * <p>This method is used when a triple is added as part of
	 * the synchronization process
	 * </p>
	 * @param id an Id
	 * 
	 * @return an item that has the given id and is in the
	 * index
	 */
	
	synchronized Item getOrAddTripleElement(Id id) {
		
		Item item = items.get(id);
		if (item == null) {
			item = new Item(id.sourceNr, id.idNr, null, null, store, false);
			items.put(id, item);
		}
		else consideredForRemoval.remove(item);
		return item;
	}
	
	/**
	 * <p>Get the item with a given id if it is in the index, 
	 * or add a new one with that id if there is not one
	 * there already, set its access levels, and set its state 
	 * to EXISTS. If the item is in the index then it is noted 
	 * to be in use. If it is not in the index, then it is 
	 * created.
	 * </p>
	 * <p>This method is used when an item is added as part of
	 * the synchronization process
	 * </p>
	 * @param id an Id
	 * 
	 * @param onlyReadLevel the only-read level of the item
	 * 
	 * @param readOrWriteLevel the read-or-write level of the item
	 * 
	 * @return an item that has the given id and access levels,
	 * and is in the index
	 */
	
	synchronized Item getOrAddItem(
			Id id,
    		AccessLevel onlyReadLevel, 
    		AccessLevel readOrWriteLevel) {
		
		Item item = items.get(id);
		if (item == null) {
			item = new Item(id.sourceNr, id.idNr, 
					onlyReadLevel, readOrWriteLevel, store, true);
			items.put(id, item);
		}
		else {
			item.setAccessLevels(onlyReadLevel, readOrWriteLevel);
			item.noteExists();
			consideredForRemoval.remove(item);
		}
		return item;
	}
	
	/**
	 * Remove an item from the index. 
	 * 
	 * @param id the Id of the item to be removed
	 * 
	 * @return the item if it was removed, null otherwise.
	 */
	
	synchronized Item remove(Id id) {
		
		Item it = items.remove(id);
		return it;
	}
	
	/**
	 * <p>Note that an item is in use, and remove it from
	 * the set of items considered for removal.
	 * </p>
	 * <p>This method is invoked by the triple store maintenance thread
	 * for items that are found to be objects or verbs of triples, or
	 * level items of item access levels.
	 * </p>
	 * @param item the item to be noted as being in use
	 */
	
	synchronized void noteInUse(Item item) {
		
		inUse.add(item);
	}
	
	/**
	 * Note that an item is to be considered for removal
	 * 
	 * @param item the item to be considered for removal
	 */
	
	synchronized void considerForRemoval(Item item) {
		
		consideredForRemoval.add(item);
	}
	
	/**
	 * Remove the items that are considered for removal, are
	 * not in use, and are not from connected sources, and 
	 * clear the sets of items in use and considered for removal.
	 */
	
	synchronized void removeConsideredItems() {
		
		consideredForRemoval.removeAll(inUse);
		for (Item item : consideredForRemoval) {
			if (!store.connectorsManager.isKnown(item.sourceNr)) 
				items.remove(item.getId());
		}
		inUse.clear();
		consideredForRemoval.clear();
	}
	
	/**
	 * Clear the set of items considered for removal.
	 */
	
	synchronized void clearSetConsideredForRemoval() {
		
		consideredForRemoval.clear();
	}
}
